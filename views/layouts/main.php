<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

    <?php 
        if($this->context->getRoute() == 'site/index'){
            $styleWrap = ' style="" ';
            $styleCon = '';
        }else{
            $styleWrap = '';
            $styleCon = ' style="" ';
        }
    ?>
    <div class="wrap" <?= $styleCon; ?>>
    <?php
    if (\app\models\PointsMessage::find()->where(['user_id'=>Yii::$app->user->id, 'show'=>0])->count() >0) {
        $message_point = \app\models\PointsMessage::find()->where(['user_id'=>Yii::$app->user->id, 'show'=>0])->orderBy(['id'=>SORT_DESC])->all();
    } else {
       $message_point = \app\models\PointsMessage::find()->where(['user_id'=>Yii::$app->user->id])->limit(5)->orderBy(['id'=>SORT_DESC])->all();
    }
    $notification = [];
    if ($message_point) {

        foreach($message_point as $item)
            if ($item->url == 1) {
                $notification[] = ['label' => $item->message, 'url' => '/stormer/projectintroduction'];
            } else {
                $notification[] = ['label' => $item->message, 'url' => '#'];
            }
    };
    $notification[] = '<li class="divider"></li>';
    $notification[] = ['label' => 'All notification', 'url' => '/notification'];

    NavBar::begin([
        'brandLabel' => '<img src="'.Url::home().'image/logo2.png">',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => (Yii::$app->user->isGuest) ? 'top-bar guest navbar-inverse navbar-fixed-top' : 'top-bar navbar-inverse navbar-fixed-top',
        ],
    ]);
    if($this->context->getRoute() == 'site/index'){   
        if(Yii::$app->user->isGuest){
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [
                    ['label' => 'Leave The Box Behind', 'url' => ['/signup/stormer'],'options' => [
                                    'title' => "Join our high-performance team ",
                                    'class' => 'menuTooltip customer-register',
                                    'data-toggle' => "tooltip",
                                    'data-placement' => "bottom"
                                ]],
                    ['label' => 'Transformation Beyond Limits', 'url' => ['/signup/customer/step_1'],'options' => [
                                    'title' => "Explore our professional services ",
                                    'class' => 'menuTooltip',
                                    'data-toggle' => "tooltip",
                                    'data-placement' => "bottom" 
                                ]],
                    Yii::$app->user->isGuest ?
                            ['label' => 'Login', 
                                'options' => [
                                    'data-toggle' => "modal",
                                    'data-target' => "#myModalCompany",
                                    'class' => "btn-login"
                                ]
                            ] :
                            ['label' =>   'Exit '.'(' . Yii::$app->user->identity->username . ')',
                                'url' => ['/site/logout'],
                                'linkOptions' => ['data-method' => 'post']],
                    ],
            ]);
        }else{
            if(!Yii::$app->user->isGuest){
                echo Nav::widget([
                        'options' => ['class' => 'navbar-nav navbar-right'],
                        'items' => [
                            [
                                'label' => Html::img(Yii::$app->session["avatar"] ? '/image/users_images/'.Yii::$app->session["avatar"]: '/image/default-avatar.png',['style'=>'width: 20px', 'class'=>'top-profile-img']),
                                'encode' => false,
                                'url' => [Yii::$app->user->identity->users_type.'/profile/'.Yii::$app->user->id],
                            ],
                            [
                                'label' => \app\models\PointsMessage::find()->where(['user_id'=>Yii::$app->user->id, 'show'=>0])->count()? '<span class="badge" id="notification_count">>'.\app\models\PointsMessage::find()->where(['user_id'=>Yii::$app->user->id, 'show'=>0])->count().'</span><i class="fa fa-bell"></i>':'<i class="fa fa-bell"></i>',
                                'encode' => false,
//                                'items' => $notification,
                                'options' => [
                                    'id' => "btn-notification",
                                    'onclick' => 'notification()'
                                    ],
                            ],
                            ['label' => 'Dashboard', 'url' => [(Yii::$app->user->identity->users_type == 'admin')? '/admin' : Yii::$app->user->identity->users_type.'/'.Yii::$app->user->id]],
                            Yii::$app->user->isGuest ?
                                    ['label' => 'Login', 
                                        'options' => [
                                            'data-toggle' => "modal",
                                            'data-target' => "#myModalCompany"
                                        ]
                                    ] :
                                    ['label' =>   \Yii::t('app','Exit ').'(' . Yii::$app->user->identity->username . ')',
                                        'url' => ['/site/logout'],
                                        'linkOptions' => ['data-method' => 'post']],
                            ],
                    ]);
            }
        }
    }else{
       if(!Yii::$app->user->isGuest){
            echo Nav::widget([
                 'options' => ['class' => 'navbar-nav navbar-right'],
                 'items' => [

                     [
                         'label' => Html::img(Yii::$app->session["avatar"] ? '/image/users_images/'.Yii::$app->session["avatar"]: '/image/default-avatar.png',['style'=>'width: 20px', 'class'=>'top-profile-img']),
                         'encode' => false,
                         'url' => [Yii::$app->user->identity->users_type.'/profile/'.Yii::$app->user->id],
                     ],
                     [
                         'label' => \app\models\PointsMessage::find()->where(['user_id'=>Yii::$app->user->id, 'show'=>0])->count() ? '<span class="badge" id="notification_count">'.\app\models\PointsMessage::find()->where(['user_id'=>Yii::$app->user->id, 'show'=>0])->count().'</span><i class="fa fa-bell"></i>' :  '<i class="fa fa-bell"></i>',
                         'encode' => false,
//                         'items' => $notification,
                         'options' => [
                            'id' => "btn-notification",
                            'onclick' => 'notification()'],
                     ],
                     ['label' => 'Dashboard', 'url' => [(Yii::$app->user->identity->users_type == 'admin')? '/admin' : Yii::$app->user->identity->users_type.'/'.Yii::$app->user->id]],
                     Yii::$app->user->isGuest ?
                             ['label' => 'Login', 
                                 'options' => [
                                     'data-toggle' => "modal",
                                     'data-target' => "#myModalCompany"
                                 ]
                             ] :
                             ['label' =>   \Yii::t('app','Exit ').'(' . Yii::$app->user->identity->username . ')',
                                 'url' => ['/site/logout'],
                                 'linkOptions' => ['data-method' => 'post']],
                     ],
             ]);
       }
    }
    NavBar::end();
    ?>
    <div class="container-fluid" <?= $styleWrap; ?>>
    	<div class="row">
	        <?= Breadcrumbs::widget([
	            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	        ]) ?>
	        <?= $content ?>
    	</div>
    </div>

    <!-- NOTIFICATION PANEL -->
    <div id="notification-panel">
        <ul>
            <?php
            if (\app\models\PointsMessage::find()->where(['user_id'=>Yii::$app->user->id, 'show'=>0])->count() >0) {
                $message_point = \app\models\PointsMessage::find()->where(['user_id'=>Yii::$app->user->id, 'show'=>0])->orderBy(['id'=>SORT_DESC])->all();
            } else {
                $message_point = \app\models\PointsMessage::find()->where(['user_id'=>Yii::$app->user->id])->limit(15)->orderBy(['id'=>SORT_DESC])->all();
            }
            $notification = [];
            if ($message_point) {

                foreach($message_point as $item)
                    if ($item->url == 1) {
                        echo "<li id='notification_url'><a href='/stormer/projectintroduction' id='notification_url'></a> ".$item->message."</li>";
                    } else {
                        echo "<li><a href='#'></a> ".$item->message."</li>";
                    }
            };
            echo '<li class="divider"></li>';
            echo "<li><a class='all-ntf' href='/notification'>All notification</a></li>";?>

        </ul>
    </div>

</div>
<footer id="footer">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-9">
				<ul class="footer-menu">
					<li><?php echo HTML::a(\Yii::t('app', 'About Us'), '/about', []); ?></li>
					<li><?php echo HTML::a(\Yii::t('app', 'Blog'), '/blogs', []); ?></li>
					<li><?php echo HTML::a(\Yii::t('app', 'For Academics'), '/forAcademics', []); ?></li>
					<li><?php echo HTML::a(\Yii::t('app', 'For NGOs and Non-Profits'), '/forNgosProfits', []); ?></li>
					<li><?php echo HTML::a(\Yii::t('app', 'Privacy'), '/privacy', []); ?></li>
					<li><?php echo HTML::a(\Yii::t('app', 'Terms'), '/terms', []); ?></li>
					<li><?php echo HTML::a(\Yii::t('app', 'FAQ'), '/faq', []); ?></li>
					<li><?php echo HTML::a(\Yii::t('app', 'Contact Us'), '/contact', []); ?></li>
				</div>
				<div class="col-sm-3">
					<ul class="social-networks">
						<li>
							<a target="_blank" href="https://www.facebook.com/braincloudsolutions" class="fb-ic"></a>
						</li>
						<li>
							<a target="_blank" href="https://www.linkedin.com/company/braincloud-solutions" class="in-ic"></a>
						</li>
						<li>
							<a target="_blank" href="https://twitter.com/BrainCsolutions" class="twt-ic"></a>
						</li>
					</div>
				</div>
			</ul>
		</div>
	</div>
</footer>

<?php $this->endBody() ?>
<script>
    function notification(){
        $.get( "/api/notification", function( data ) {
            $('#notification_count').hide();
        });
    }
</script>
</body>
</html>
<?php $this->endPage() ?>
