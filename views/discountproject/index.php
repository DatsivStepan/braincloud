<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DiscountProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Discount Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discount-project-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Discount Project', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'count',
            'discount',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
