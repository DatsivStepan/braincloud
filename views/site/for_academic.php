<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'For Academics';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="for-academics-page containerBlock">
	<div class="container">
		<?=$content?>
	</div>
</div>
