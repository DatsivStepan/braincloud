<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Privacy';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="privacy-page">
	<?=$content?>
	<!-- <div class="container">
		<h1 class="page-title">Privacy Policy</h1>
		<hr class="title-hr">
		<h4 class="page-subtitle">Privacy rights of our customers and stormers are highly respected and we try our<br /> best to protect your private information by adopting the following measures:</h4>
	</div>
	<div class="list-wrapper-top">
		<div class="container">
			<div class="row">
				<div class="col-md-offset-1 col-md-10">
					<ol class="list-privacy">
						<li>We use industry standard secure socket layer encryption (SSL) to protect the data transfer between the Site and our servers; however, your browser must support encryption protection to take advantage of this service.</li>
						<li>In the interest of protecting customer’s right to privacy, we do not have cookies on our site.</li>
						<li>Any information collected via <a href="https://braincloud.solutions">www.braincloud.solutions</a> (the “Site”) are voluntarily submitted to us by our customers and stormers when register as user, update user profile, or request information from us at the “Site”, including contact information, prior education or experience, field of expertise, project information and other similar information in case of a stormer; company information, contact details, project information and other similar information in case of customer (“Your Information”). Your Information is used to respond your requests, communicate with you about updates of the “Site” or project related issues. Except for emails, which is used as means of verification when register your account, you have the right to choose revealing Your Information or stay anonymously at the Site.</li>
						<li>If as a stormer, you choose to sign up to the Site using a social networking site sign-on, we might collect Your Information directly from the social networking site, depending on their policies, and in such case, it is considered that the information we acquired are voluntarily submitted by you, and therefore in accordance with this policy. After you participate and finish a paid project at the Site, we will process payments through a trusted third party, in which case, the transaction will be governed by the privacy policy of that third party; we do not receive or store payment information on the servers we use.</li>
						<li>In case of businesses, the IP of the project you posted belongs to you. All attachment you uploaded as support information of your launched projects will be deleted from our site after <b>90</b> days. We will not keep record of such documents at our server</li>
						<li>Your Information will not be shared to a third party for any profitable purpose. In case we do need to provide Your Information in response to duly authorized information request of governmental authorities or when required by law, we will provide written explanations in advance and keep you informed. Sometimes we will cooperate with educational institution on their academic researches and studies, in which cases, private information of individual users won’t be shared.</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	<div class="list-wrapper-bottom">
		<div class="container">
			<div class="row">
				<div class="col-md-offset-1 col-md-10">
					<h4 class="list-header">You may also help us protecting your own privacy rights, please be aware that:</h4>
					<hr class="title-hr">
					<ol class="list-privacy">
						<li>Sharing information or the activities you participate at the Site on your personal social networking sites which is visible to the public, the terms listed in this Privacy Policy will not apply.</li>
						<li>As one of the service provided by the Site, it might contain links to sites that contain the information we believe can be useful to our users, however, this Privacy Policy does not apply to any third party sites.</li>
						<li>The Site is not designed for being used by, or attracting individuals under the age of 18 (see “terms and conditions”), however, since age is not a mandatory option one should submit to the Site, we will not take any responsibility if any individuals intentionally violate this term.</li>
						<li>In connection with the sale, or business transfer of the Site to which Your Information is related, we may also provide it to the related party, in which case we will require Your Information be treated in accordance with this Privacy Policy.</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	<div class="bottom-information">
		<div class="container">
			<div class="row">
				<div class="col-md-offset-1 col-md-10">
					<p>
						With the development of the functions at the Site, we might update and upgrade our Privacy Policy, when such change happens, a written notification will be sent to all users. Unless stated otherwise, the current Privacy Policy applies to all Your Information we have.
					</p>
					<p>
						Should you have any concerns about Your Information or this Privacy Policy, please <a href="mailto:admin@braincloud.solutions">contact us</a>.
					</p>
				</div>
			</div>
		</div>
	</div> -->
</div>