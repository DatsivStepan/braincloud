<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\assets\TagsinputAsset;
use dosamigos\multiselect\MultiSelect;

TagsinputAsset::register($this);

//$this->registerJsFile('/js/jquery-ui.js',  ['position' => yii\web\View::POS_END]);

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;

?>

<div style="display:none;">
	<div class="table table-striped" class="files" id="previews">
		<div id="template" class="file-row">
			<!-- This is used as the file preview template -->
			<div class="row">
				<div class="col-sm-12">
					<span class="preview"><img data-dz-thumbnail /></span>
					<p class="name" data-dz-name></p>
					<strong class="error text-danger" data-dz-errormessage></strong>
				</div>
				<div class="col-sm-12">
					<p class="size" data-dz-size></p>
					<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
						<div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
					</div>
				</div>
				<div class="col-sm-12">
					<button data-dz-remove class="btn delete">
						<i class="glyphicon glyphicon-trash"></i>
						<span>Delete</span>
					</button>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="registration-step">
	<?php if((($role == 'stormer') && ($step == 'step_1')) || ($role == 'customer')){ ?>
		<?php
			$form = ActiveForm::begin(['id' => 'form-signup',
			'options' => ['class' => 'form-horizontal'],
				'enableClientValidation' => false,
			'fieldConfig' => [
					'template' => "{label}\n{input}\n{error}",
					'labelOptions' => ['class' => 'control-label'],
			],
		]); ?>
		<?php if($role == 'customer'){ ?>
			<div class="title-wrap">
				<h1 class="page-title">Basic Contact Information</h1>
				<hr>
				<h3 class="details big">This information is private and secure, and is not present on your profile</h3>
			</div>
		<?php }else{ ?>
			<div class="title-wrap">
				<h1 class="page-title">Welcome to BrainCloud Solutions.</h1>
				<h4 class="page-subtitle">We’re very happy that you’ve decided to come on board, there are a few things we need from you first. Get yourself a coffee, get dinner in the oven, and give us a little bit of your time to help you settle in.</h4>
				<hr>
				<h3 class="details stormer">First, some basic information:</h3>
			</div>
		<?php }
	?>

		<div class="form-wrap">
			<div class="container containerBlock">
				<div class="registration-field">
					<div class="row">
						<?= $form->field($newModel, 'users_type')->hiddenInput(['value' => $user_type])->label(false); ?>
						<div class="col-sm-6 col-lg-4">
							<?= $form->field($newModel, 'name')->textInput(['placeholder' => 'Contact name'])->label(false); ?>
						</div>
						<div class="col-sm-6 col-lg-4">
							<?= $form->field($newModel, 'username')->textInput(['placeholder' => 'Username'])->label(false); ?>
						</div>
						<div class="col-sm-6 col-lg-4">
							<?= $form->field($newModel, 'email')->input('email')->textInput(['placeholder' => 'Contact email'])->label(false); ?>
						</div>
						<div class="col-sm-6 col-lg-offset-3 col-lg-3">
							<?= $form->field($newModel, 'password')->passwordInput(['placeholder' => 'Password'])->label(false); ?>
						</div>
						<div class="col-sm-offset-6 col-sm-6 col-lg-offset-0 col-lg-3">
							<?= $form->field($newModel, 'password_repeat')->passwordInput(['placeholder' => 'Password Repeat'])->label(false); ?>
						</div>

						<div class="col-sm-6 col-lg-3">
							<input type="text" name="ref_code" value="" placeholder="Refferal code" class="form-control" />
						</div>

					</div>
				</div>
			</div>

			<?php if($role == 'customer'){ ?>

				<div class="customer-registration">
					<div class="container">
						<?= $form->field($newModel, 'accept_terms')->checkbox()->label(HTML::a(\Yii::t('app', 'Accept terms and agreement'), '#', ['onclick' => 'accept_terms(true)']));  ?>
					</div>
					<div class="title-wrap">
						<div class="container">
							<h1 class="page-title">Business Information</h1>
							<hr>
							<h3 class="details big">
								This information is what will be on your profile. Customer profiles are not available for public browsing. They are only seen when stormers you hired want to learn more about your company. This information will never be packaged and sold to any third party.
								<a class="anonymise_me">Is it a secret? Anonymise me</a>
							</h3>
							<div class="col-sm-12">
								<a class="btn busy_button" >Busy? Do this later
									<div class="busy_block">
										<div class="form-group">
											<div class="col-sm-6">
												<?= Html::submitButton('Take a look around first', ['class' => 'btn btn-busy', 'name' => 'take_look_basic']) ?>
											</div>
											<div class="col-sm-6">
												<?= Html::submitButton('Post a project now', ['class' => 'btn btn-busy', 'name' => 'post_project_basic']) ?>
											</div>
										</div>
									</div>
								</a>
							</div>
						</div>
					</div>

					<div class="container">
						<div class="business-info-form">
							<div class="row">
								<div class="col-md-6">
									<?= $form->field($modelCustomer, 'business_name')->textInput(array('placeholder'=>'Business Name'))->label(false); ?>
								</div>
								<div class="col-md-6">
									<div class="row">
										<div class="col-sm-4 col-md-6 col-lg-5">
											<label class="control-label">Add logo or profile image</label>
										</div>
										<div class="col-sm-8 col-md-6 col-lg-7 containerUpload">
											<div id="cropContaineroutput"></div>
										</div>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-md-6">
									<?= $form->field($modelCustomer, 'image_name')->hiddenInput()->label(false); ?>
									<?php
//                                    echo  $form->field($modelCustomer, 'country')->dropDownList($arrayCountry,['class'=>'form-control','prompt'=>'Select Country'])->label(false);
									?>
                                    <?php
                                    echo \kartik\typeahead\Typeahead::widget([
                                      'name' => 'country_head',
                                      'options' => [
                                        'placeholder' => 'Where in the world are you?',
                                        'class' => 'form-control',
                                      ],
                                      'pluginOptions' => ['highlight'=>true],
                                      'dataset' => [
                                        [
                                          'remote' => [
                                            'url' => \yii\helpers\Url::to(['site/krainy-list']) . '?q=%QUERY',
                                            'wildcard' => '%QUERY'
                                          ]
                                        ]
                                      ]
                                    ]);
                                    ?>
									<?php
//                                        echo $form->field($modelCustomer, 'city')->label(false)->dropDownList($arraySity,['class'=>'selectCity form-control','prompt'=>'First select a country']);
                                        ?>
                                    <?php
                                    echo \kartik\typeahead\Typeahead::widget([
                                        'name' => 'city_head',
                                        'options' => [
                                            'placeholder' => 'Select Your City',
                                            'class' => 'form-control',
                                        ],
                                        'pluginOptions' => ['highlight'=>true],
                                        'dataset' => [
                                            [
                                                'remote' => [
                                                    'url' => \yii\helpers\Url::to(['site/country-list']) . '?q=%QUERY',
                                                    'wildcard' => '%QUERY'
                                                ]
                                            ]
                                        ]
                                    ]);
                                    ?>
                                    <?= $form->field($modelCustomer, 'business_type')->textInput(array('placeholder'=>'Business Type'))->label(false); ?>
									<?= $form->field($modelCustomer, 'ask')->radioList(array('1'=>'Yes', '2' =>'No'))->label('Is it okay for stormers to ask you questions during a project?'); ?>
								</div>
								<div class="col-md-6">
									<?= $form->field($modelCustomer, 'business_services')->textArea(['rows' => '5','placeholder' => 'Business Services'])->label(false); ?>
									<?= $form->field($modelCustomer, 'more_information')->textArea(['rows' => '5', 'placeholder' => 'More Information',])->label(false); ?>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<a class="btn busy_button continue" >Continue
										<div class="busy_block">
											<div class="form-group">
												<div class="col-sm-6">
													<?= Html::submitButton('Take a look around first', ['class' => 'btn btn-busy', 'name' => 'take_look_all']) ?>
												</div>
												<div class="col-sm-6">
													<?= Html::submitButton('Post a project now', ['class' => 'btn btn-busy', 'name' => 'post_project_all']) ?>
												</div>
											</div>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>

				</div>
			<?php }else{ ?>
				<div class="social-icon-wrap">
					<span class="social-label">Log in with:</span>
					<a class="fb-ic" title='Login with facebook' href="/api/social?servise=facebook&type=register&user=stormer"></a>
					<a class="twt-ic" title='Login with twitter' href="/api/social?servise=twitter&type=register&user=stormer"></a>
					<a class="gp-ic" title='Login with Google+' href="/api/social?servise=google&type=register&user=stormer"></a>
				</div>
				<?= Html::submitButton('Next', ['class' => 'btn btn-next', 'name' => 'saveStormer']) ?>
			<?php } ?>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
		<?php }elseif(($role == 'stormer') && ($step == 'step_2')){ ?>

			<div class="getting-wrap">
				<div class="container">
					<h1 class="page-title" id="focus">Getting To Know You</h1>
					<h4 class="page-subtitle">Let’s get some info to get to know you better.</h4>
					<hr>
					<div class="form-group field_customer_tags required">
						<label class="control-label" for="user-username">Tags
							<a class="infoButtonShow">!</a>
							<div class="infoBlockShow" style="display: none;">
								<div class="arrow"></div>
								<p class="getting-description">
									Tags are important, as they allow us to find people with particular skills and knowledge. Please list each one of your skills and ranges of knowledge, separated by a comma, feel free to include business as well as non-business related fields.</p>
								<p class="getting-description">
									Which tags to include? Imagine you were asked for an opinion on a subject, include the tags you would feel comfortable participating in a conversation about, with peers that have indepth, related knowledge.
									Try to include as many as you can, and rephrase them in different ways so as to be seen in a keyword search.
								</p>
								<p class="getting-description">
									As an example, here are some tags of BrainCloud’s owner/animal mascot: Innovation, open-innovation, management, marketing, entrepreneurship, start-ups, motivation, strategies, leadership, human resources, training, teaching, public relations, photography, gastronomy, beer, cooking, cycling, archery, Chinese language, chocolate.
								</p>
								<p class="getting-description">
									Please do not include any tags that you are not that familiar with, you can succeed and do perfectly well on our platform with a finite range of knowledge. You don’t have to be a walking Wikipedia to make the world a better place!
								</p>
							</div>
						</label>
						<input type="text" data-role="tagsinput" id="my_input" name="tagsi">
						<div class="error_text" style="display:none;">
							<p class="help-block help-block-error">Tags cannot be blank.</p>
						</div>
						<span class="hint">Press enter after each tag word(s)</span>
					</div>
				</div>
			</div>
			<?php
				$form = ActiveForm::begin(['id' => 'form-signup',
				'options' => ['class' => 'form-horizontal'],
				'fieldConfig' => [
						'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-7\">{error}</div>",
						'labelOptions' => ['class' => 'col-lg-2 control-label'],
				],
			]); ?>
				<input type="hidden" id="value_post" name="value_post">
				<div class="getting-bottom-field">
					<div class="container">
						<div class="row">
							<div class="col-lg-5">
								<div class="form-group field_customer_category required">
									<div class="col-md-6 col-lg-7">
										<label class="control-label" for="user-username">Select your category of knowledge
										<a class="infoButtonShow">!</a>
										<div class="infoBlockShow" style="display: none;">
											<div class="arrow"></div>
											<p class="getting-description">
												It may feel like you’re repeating yourself a little, but we promise, this is the only time. 
												To help us find you via classification, select the category you know most about, even if 
												you only know a specific range of knowledge within that area. Add as many as you like. 
												These are all business related categories. If you don’t know that much about any, please 
												don’t select any. Misrepresenting yourself will come out sooner or later, and is a breach 
												of our terms and conditions, which may get your account suspended.
											</p>
										</div>
										</label>
									</div>
									<div class="col-md-6 col-lg-5 category_input_block" >
										<?php
											echo MultiSelect::widget([
												'id'=>"multiXX",
												'options' => ['multiple'=>"multiple"], // for the actual multiselect
												'data' => ['Marketing'=> ['Advertising' => 'Advertising','Market research and analysis' => 'Market research and analysis', 'Packaging and design' => 'Packaging and design','Branding' =>  'Branding', 'External communications' => 'External communications', 'Promotions and pricing' => 'Promotions and pricing'],
															'Management'=>[ 'Human resource management' => 'Human resource management', 'Engagement' => 'Engagement', 'Internal communications' => 'Internal communications', 'Project management' => 'Project management', 'Growth and development' => 'Growth and development', 'Organizational design' => 'Organizational design'], // data as array
															'Sales'=>['The sales process' => 'The sales process', 'Sales management' =>  'Sales management', 'Retail' => 'Retail', 'B2B selling' => 'B2B selling', 'Industry analysis' => 'Industry analysis', 'Industry analysis' => 'Industry analysis']], // data as array
												'value' => [], // if preselected
												'name' => 'multtiSelect', // name for the form
												"clientOptions" =>
													[
														"includeSelectAllOption" => true,
														'numberDisplayed' => 0
													],
											]);
										?>
									</div>
									<div class="col-sm-12 error_text" style="display:none;">
										<p class="help-block help-block-error">Category cannot be blank.</p>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="col-lg-1">
								<div class="separator">
									or
								</div>
							</div>
							<div class="col-lg-6">
								<div class="input-group another-category">
									<input type="text" class="form-control add_category_input" placeholder="Enter another category">
									<input type="hidden" id="count_your_catecory" name="count_your_catecory" value="1" >
									<span class="input-group-btn">
										<a class="btn add_category_button">Add another category</a>
									</span>
								</div>
							</div>
						</div>

						<input type="submit" class='btn btn-next' name="step_2_next" value="Next">
					</div>
				</div>

			<?php ActiveForm::end(); ?>
		<?php }elseif(($role == 'stormer') && ($step == 'step_3')){ ?>

			<div class="title-wrap">
				<div class="container">
					<h1 class="page-title">Introduce Yourself</h1>
					<hr>
					<h4 class="page-subtitle">Tell Us Something about Who You Are</h4>
				</div>
			</div>
			<?php
				$form = ActiveForm::begin(['id' => 'form-signup',
				'options' => ['class' => 'form-horizontal introduce-yourself'],
				
			]); ?>
				<div class="form-top-part">
					<div class="container">
						<?php

							if (!$user->email or $email_input){
								?>
									<div class="col-md-6">
										<?php
											echo $form->field($user, 'email')->input('email')->textInput(['placeholder'=>'Contact email'])->label(false);
										?>
									</div>
								<?php
							}
							 ?>
							<div class="col-md-6">
								<?php
//                                  echo $form->field($modelStormerInfo, 'country')->dropDownList($arrayCountry,['class'=>'form-control','prompt'=>'Where in the world are you?'])->label(false);
                                ?>
                                <?php
                                echo \kartik\typeahead\Typeahead::widget([
                                  'name' => 'country_head',
                                  'options' => [
                                    'placeholder' => 'Where in the world are you?',
                                    'class' => 'form-control',
                                  ],
                                  'pluginOptions' => ['highlight'=>true],
                                  'dataset' => [
                                    [
                                      'remote' => [
                                        'url' => \yii\helpers\Url::to(['site/krainy-list']) . '?q=%QUERY',
                                        'wildcard' => '%QUERY'
                                      ]
                                    ]
                                  ]
                                ]);
                                ?>
                            </div>
							<div class="col-md-6">
								<?php
//                                    echo $form->field($modelStormerInfo, 'city')->dropDownList([],['class'=>'selectCity form-control','prompt'=>'Select City/County/State'])->label(false);
                                ?>
                                <div class="form-group">
                                	<?php
                                	echo \kartik\typeahead\Typeahead::widget([
                                	    'name' => 'city_head',
                                	    'options' => [
                                	        'placeholder' => 'Select Your City',
                                	        'class' => 'selectCity form-control',
                                	    ],
                                	    'pluginOptions' => ['highlight'=>true],
                                	    'dataset' => [
                                	        [
                                	            'remote' => [
                                	                'url' => \yii\helpers\Url::to(['site/country-list']) . '?q=%QUERY',
                                	                'wildcard' => '%QUERY'
                                	            ]
                                	        ]
                                	    ]
                                	]);
                                	?>
                            	</div>
							</div>
							<div class="clearfix"></div>
							<div class="col-md-6 gender-select text-left">
								<?= $form->field($modelStormerInfo, 'gender')->radioList([1 => 'Male', 0 => 'Female']); ?>
								<a class="btn btn-show-question dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Why are we asking this?</a>
								<ul class="dropdown-menu question" aria-labelledby="dropdownMenu1">
									To ensure our system is approachable to all genders, we need to measure the gender of our teams. This helps us become more gender-neutral. This information is private and will not be present on your profile.</p>
								</ul>
							</div>
						<div class="col-sm-12">
							<!--<span class="titleDesign"  data-toggle="tooltip" title="<p style='font-size:14px;'>To ensure our system is approachable to all genders, we need to measure the gender of our teams. This helps us become more gender-neutral. This information is private and will not be present on your profile.</p>" >Why are we asking this?</span>-->
						</div>
					</div>
				</div>
				<div class="form-bottom-part">
					<div class="container">
						<h1 class="page-title">Give us a brief introduction
							<a class="titleDesign" data-placement="bottom" data-toggle="tooltip" title="
								<p>You may be selected for jobs based off this introduction, try to make it as interesting as you can. 
									Tell us something about your specialities. Keep things factual and do your best not to 
									make any spelling or grammar mistakes.</p>" >i</a>
						</h1>
						<hr>
						<div class="row">
							<div class='col-sm-12'>
								<?= $form->field($modelStormerInfo, 'introduction')->textInput(['class'=>'form-control pull-right'])->label(false); ?>
							</div>
							<div class="col-sm-12">
								<div class="row">
									<div class="col-md-4">
										<?= $form->field($modelStormerInfo, 'linkedin_link')->textInput(['placeholder'=>'Linkedin Link'])->label(false); ?>
									</div>
									<div class="col-md-4">
										<?= $form->field($modelStormerInfo, 'twitter_link')->textInput(['placeholder'=>'Twitter Link'])->label(false); ?>
									</div>
									<div class="col-md-4">
										<?= $form->field($modelStormerInfo, 'facebook_link')->textInput(['placeholder'=>'Facebook Link'])->label(false); ?>
									</div>
								</div>
							</div>
							<div class='col-sm-12'>
								<div class="row">
									<div class="col-md-6">
										<a class="clicable btn btn-add-photo">
											<i class="glyphicon glyphicon-plus"></i>
											Add logo or profile image
										</a>
										<div id="previewsS">

										</div>
									</div>
									<div class="col-md-6">
										<?= $form->field($modelStormerInfo, 'where_here_about_us')->dropDownList(['Where did you hear about us?','Google','Other search engine','Recommended by a friend','Recommended by a website'])->label(false); ?>
										<?= $form->field($modelStormerInfo, 'image_src')->hiddenInput()->label(false); ?>
										<div class="which_website" style="display:none;">
											<?= $form->field($modelStormerInfo, 'which_website')->textInput(['placeholder'=>'Which Website?'])->label(false); ?>
										</div>
									</div>
								</div>
							</div>
							<!--<div class="col-sm-12">
								<div class="col-sm-5 ">
									<div class="col-sm-5">
										<label class="pull-right" style="margin-right:20px;">Add a profile photo</label>
									</div>
									<div class="col-sm-7 pull-right containerUpload">
										<div id="cropContaineroutputFoto"></div>
									</div>
								</div>
							</div>-->
							<div class="col-sm-12">
							</div>
							<div class="col-sm-12 text-center">
								<?= $form->field($modelStormerInfo, 'accept_term')->checkbox()->label(HTML::a(\Yii::t('app', 'Accept terms and agreement'), '#', ['onclick' => 'accept_terms(false)']));  ?>
							</div>
							<?= Html::submitButton('Start your adventure ', ['class' => 'btn start']) ?>
						</div>
					</div>
				</div>
			<?php ActiveForm::end(); ?>
		<?php }?>

	</div>
</div>
<div class="modal accept-modal fade" id="myModalCustomer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"></h4>
				<hr>
			</div>
			<div class="modal-body">
				<?=$modal_terms_customer->content?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-accept" onclick="accept_terms_ok()">Yes</button>
				<button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="modal accept-modal fade" id="myModalStormer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"></h4>
				<hr>
			</div>
			<div class="modal-body">
				<?=$modal_terms_stormer->content?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-accept" onclick="accept_terms_ok()">Yes</button>
				<button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<script>
	function accept_terms(t){
		if (t){
			$('#myModalCustomer').modal('show');

		} else {
			$('#myModalStormer').modal('show');
		}

	}

	function accept_terms_ok(){
		$("#user-accept_terms").attr('checked',true);
		$("#stormerinfo-accept_term").attr('checked',true);
		$('#myModalStormer').modal('hide');
		$('#myModalCustomer').modal('hide');
	}
</script>
