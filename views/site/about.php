<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=$content?>
<!-- <div class="about-team containerBlock">
	<div class="container text-center">
		<h1 class="page-title">The Team</h1>
		<hr class="title-hr" />
		<h4 class="page-subtitle">BrainCloud is the brainchild of Chris Lawrence and Estela Zhang. Built and refined by the geniuses at <a href="http://www.roketdev.com/" target="_blank">RoketDev</a>, the months of hard work and development resulted in our first Beta version, which was released in spring, 2017.</h4>
		<div class="row">
			<div class="col-md-6">
				<div class="about-team-block left">
					<div class="team-avatar">
						<img src="../../image/team-av1.jpg" alt="team_img">
					</div>
					<div class="name">
						Chris Lawrence
					</div>
					<hr>
					<div class="about">
						Throughout his varied career, more noticeably as a teacher/trainer and entrepreneur, 
						Chris has learned (through a healthy dose of failure) that there are few superhero businessmen in this world, 
						and success is the result of teamwork. The idea of providing an innovation team anytime, 
						anywhere came from that basic concept, and was refined into BrainCloud through his own frustration of 
						realising the lack of affordable resources for small businesses.
					</div>
					<div class="link-wrap">
						<span>To lern more about Chris, click here to see Chris on <a href="https://nl.linkedin.com/in/ChrisMLawrence" target="_blank">Linkedin</a></span>
						<span>To send Chris an email: <a href="mailto:chris.lawrence@braincloud.solutions">chris.lawrence at braincloud.solutions</a></span>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="about-team-block right">
					<div class="team-avatar">
						<img src="../../image/team-av2.jpg" alt="team_img">
					</div>
					<div class="name">
						Estela Zhang
					</div>
					<hr>
					<div class="about">
						Estela’s career working as a HR and training executive helps her to connect with people. 
						Giving up her position as a top HR exec at a global company in China, she helped found, 
						develop and create BrainCloud in the Netherlands. Estela has always believed in providing 
						the right tools to the right people, which laid out the founding concepts for BrainCloud. 
						Estela has two Master degrees, one in HR Management, and the other in Innovation and Entrepreneurship, 
						both of which helped refine BrainCloud from an idea hub, into a fully-operational business tool.
					</div>
					<div class="link-wrap">
						<span>To learn more about Estela, click here to see her on <a href="https://nl.linkedin.com/in/EstelaZhang" target="_blank">Linkedin</a></span>
						<span>To send Estela an email: <a href="mailto:zhang.jun@braincloud.solutions">zhang.jun@braincloud.solutions</a></span>
					</div>
				</div>
			</div>
		</div>
		<div class="bottom-title-wrap">
			<h1 class="page-title">Legal Information</h1>
			<hr class="title-hr" />
			<h4 class="page-subtitle">BrainCloud Solutions is a legally registered entity, based in Haarlem, Netherlands.</h4>
			<h4 class="page-subtitle">To learn more about your legal rights whilst using our site:</h4>
			<a href="/privacy">Click here to read our Privacy Policy</a>
			<a href="/terms">Click here to read our Terms and Conditions of Use.</a>
		</div>
	</div>
</div>
<div class="about-page containerBlock">
	<div class="container">
		<h1 class="page-title">About BrainCloud</h1>
		<div class="row">
			<div class="col-md-6 col-lg-7">
				<h4 class="page-subtitle">BrainCloud</h4>
				<div class="col-md-8">
					<div class="row">
						<hr class="title-hr" />
					</div>
				</div>
				<div class="clearfix"></div>
				<p class="about-text-block">
					There’s nothing we can’t reach in life. Wherever we come from, whatever cards we’re dealt with. 
					Finding new ways to reach new limits is how we become something more than what we are. This is why 
					we’re here; to challenge innovation. Truly great change doesn’t come from one mind; it isn’t limited 
					to fortune 500 companies; 
					<br />
					<br />
					it won’t be limited to the barriers of our own knowledge and imagination; it’s unstoppable 
					and fearless; it’s enjoyable and operational; it stretches limits and builds bridges to 
					doing things in ways we would never have done. Break out of the box and join us for change 
					that grows with you. We will never get tired, we will never get bored, and we will never 
					stay the same. This is our promise; this is every inch of who we are.
				</p>
			</div>
			<div class="col-md-6 col-lg-5">
				<img class="about-image img-responsive" src="../../image/about_img.jpg" alt="about">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="category">
					<h6>Company’s Mission</h6>
					<hr>
					<p>
						Our mission is to supersede expectations and to constantly challenge how innovation is done. <br />
						We are bound by the prospect that innovation providers should seek the most innovative 
						methods to bring solutions to others. We expect more, and so should you.
					</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="category">
					<h6>Our Vision</h6>
					<hr>
					<p>
						Growth should never stop. Our vision is growth without limits, beyond business solutions, 
						to the fundamental ways we see collective collaboration and personal value to innovate. 
					</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="category">
					<h6>How we work:</h6>
					<hr>
					<p>
						BrainCloud is a totally unique platform utilising the combination of open-innovation, 
						computer-mediated teams, and divergent and convergent brainstorming; aimed at reducing 
						psychological limits and organizing the ideation process to maximise both open creativity 
						and targeted problem solving. <br>
						For more information on our methodology,<br><a href="/site/ourmethodology">click here</a>
					</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h4 class="page-subtitle bottom">Case Studies and Academic Support:</h4>
				<div class="col-md-4">
					<div class="row">
						<hr class="title-hr" />
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="about-text-block">
					Every system we have introduced into our platform is the result of study 
					and analysis of innovation studies, success stories and known problems.
					<br />
					<br />
					Case Studies and Academic Support
					<br />
					<br />
					Every system we have introduced into our platform is the result of study and analysis of innovation studies, success stories and known problems.
					<br />
					<br />
					Feel free to explore the following information:
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<h4 class="page-subtitle bottom">Links:</h4>
				<div class="col-md-4">
					<div class="row">
						<hr class="title-hr" />
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="about-text-block">
					<span class="head">Title:</span> <h4 class="link-title">Open Innovation with Customers: Crowdsourcing and Co-Creation at Threadless</h4>
					<br />
					<span class="head">Link:</span> <a class="link-info" href=" http://papers.ssrn.com/sol3/papers.cfm?abstract_id=1688018" target="_blank"> http://papers.ssrn.com/sol3/papers.cfm?abstract_id=1688018</a>
					<br />
					<span class="head">Author:</span> <span class="author">Frank T. Piller</span>
					<br />
					<br />
					<span class="head">Title:</span> <h4 class="link-title">Six Interesting Open-Innovation Examples</h4>
					<br />
					<span class="head">Link:</span> <a class="link-info" href="/SixInterestingOpen.docx" download target="_blank">Six Open Innovation Examples</a>
					<br />
					<span class="head">Author:</span> <span class="author">Chris Lawrence</span>
					<br />
					<br />
					<span class="head">Title:</span> <h4 class="link-title">Design for Innovation</h4>
					<br />
					<span class="head">Link:</span> <a class="link-info" href="https://ec.europa.eu/growth/industry/innovation/policy/design_en" target="_blank">https://ec.europa.eu/growth/industry/innovation/policy/design_en</a>
					<br />
					<span class="head">Author:</span> <span class="author">Business Innovation Observatory, European Commission</span>
					<br />
					<br />
					If you wish to contribute to, or if you are currently studying innovation or management 
					science then please click <a href="/forAcademics">here</a>.
					<br />
					<br />
					To see some of our not-for-profit social innovation programs, click <a href="http://braincloud.team-kaktus.tk/customer/beinspired/Peer">here</a>.
					<br />
					<br />
					To learn more about our innovation methodology, click <a href="/site/ourmethodology">here</a>.
					<br />
					<br />
					We support the academic world. If you’re studying innovation or business management and require some assistance to make the next big break-through, feel free to <a href="/forAcademics">contact us</a>.
					<br />
					<br />
					Join our facebook, linked-in and twitter community here:
					<span class="social-wrap">
						<a class="social fb-ic" title='Facebook' href="https://www.facebook.com/braincloudsolutions/" target="_blank"></a>
						<a class="social in-ic" title='Linked-in' href="https://www.linkedin.com/company/braincloud-solutions" target="_blank"></a>
						<a class="social twt-ic" title='Twitter' href="https://twitter.com/braincsolutions" target="_blank"></a>
					</span>
				</div>
			</div>
		</div>
	</div>
</div> -->
