<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\widgets\LinkPager;
use dosamigos\tinymce\TinyMce;

$this->title = 'Blog';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact containerBlock">
    <div class="row">
        <div class="col-sm-12">
            <h2><?= $modelNews->title; ?>
                <!--  <a data-toggle="modal" class="pull-right" data-target="#myModal<?= $modelNews->id; ?>"><i class='fa fa-pencil-square-o fa-1x' style="color:#3187bf;"></i></a>-->
            </h2>
            <hr>
            <p><span class="glyphicon glyphicon-time"></span><?= $modelNews->date_create; ?></p>
            <hr>
            <div><?php echo $modelNews->content; ?></div>
        </div>
    </div> 
    <div class="col-sm-12">
        <hr>
        <div class="well">
            <h4>Leave Comment</h4>
            <div clas="col-sm-12">    
                <?php $form = ActiveForm::begin(['id' => 'form-signup', 'options' => ['class' => 'form-horizontal']]); ?>
                <?= $form->field($newComment, 'content')->textarea(); ?>
                <div class="col-sm-12" style="height: 0px;">
                    <?= $form->field($newComment, 'news_id')->hiddenInput(['value' => $modelNews->id])->label(false); ?>
                    <?= $form->field($newComment, 'user_id')->hiddenInput(['value' => $user_id])->label(false); ?>
                </div>
                <?= Html::submitButton('Add comment', ['class' => 'btn btn-primary', 'name' => 'saveNews']); ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <hr>
        <div class="col-lg-9 col-md-10 col-sm-12 col-xs-12" style="margin-left: 40px">
            <?php foreach ($modelComment as $comment) { ?>
                <div class="media">
                    <a class="pull-left" href="#">
                        <img class="media-object" src="http://placehold.it/64x64" alt="">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">
                            <small><?= $comment['date_create']; ?></small>
                            <?php $form = ActiveForm::begin(['id' => 'formDelete' . $comment['id'], 'options' => ['class' => 'form-horizontal']]); ?>
                            <input type="hidden" name="comment_id" value="<?= $comment['id']; ?>">
                            <?= Html::submitButton('<span class="glyphicon glyphicon-remove pull-right" style="color:#d9534f;"></span>', ['class' => 'pull-right', 'style' => 'border:0px solid transparent;background-color:transparent;', 'name' => 'deleteComment']); ?>
                            <?php ActiveForm::end(); ?>
                        </h4>
                        <?= $comment['content']; ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>