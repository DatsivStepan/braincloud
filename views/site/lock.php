<?php
use yii\helpers\Html;
$this->title = 'Oh dear, oh dear!';
/**
 *
 */?>
<div class="lock-page">
	<div class="container">
		<h1 class="page-title">Oh dear, oh dear!</h1>
		<!-- <h4 class="page-subtitle"><? //=$ban->date?></h4> -->
		<div class="content">
			This page is here because the team believes there has been a serious breach of our terms and conditions. 
			This could simply be a case of mistaken identity or a technical glitch, if you believe it is, please get in 
			touch straightaway: <a href="mailto:support@braincloud.solutions">support@braincloud.solutions</a> 
			and we will do our best to rectify this immediately.
			<br />
			<br />
			Until we hear from you, we’re sorry to say that there has been a temporary block on your account. 
			If you have any active projects, they will only be removed if they are the reason you have reached this page. 
			<br />
			<br />
			Click <a href="stormer/terms_conditions">here</a> to review the terms and conditions.
		</div>
	</div>
</div>

