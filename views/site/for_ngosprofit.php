<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'BrainCloud and Non-Profits';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ngo-page containerBlock">
	<div class="container">
		<?= $content; ?>
		<!-- <h1 class="page-title text-center">BrainCloud and Non-Profits</h1>
		<hr>
		<div class="about-text-block">
			<br />
			<p>If you`re a legal none-profit entity and want to make the world a better place, then help us to help you.</p>
			<p>We are seeking active profit-free partnership with non-profit organizations, to provide solution and assistance to improving the quality of life for all.</p>
			<p>If you aim is to improve society and the standard of living for all human beings,</p>
			<p>especially those currently neglected and requiring more awareness and attention then we share the same goals and benefits.</p>
			<p>if you are having technical or administration problems related to various elements of your organization, that perhaps your internal or external teams are struggling to solve, requiring the assistance of a larger-budget team to innovate, then we can help.</p>
			<p>Click <a href="http://braincloud.team-kaktus.tk/contact">here</a> to get in contact now.</p>
		</div> -->
	</div>
</div>
