<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

//$this->title = 'Signup';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="join-team-page">
	<div class="container">
		<h2 class="page-title">Join the Team</h2>
	</div>
	<div class="join-team-representation">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6 no-padding">
					<img class="representation-img img-responsive pull-right" src="../../image/join-team-1.png" alt="">
				</div>
				<div class="col-md-6">
					<div class="representation animated fadeIn wow">
						<h4 class="title">
							We are team that is true to ourselves. We believe that true innovation means breaking down traditions.Everything can be improved, so why do it the same way as everyone else? We believe in making a difference and contributing towards a better society.
						</h4>
						<hr>
						<h5 class="subtitle">
							Why can’t everything be fun? Why can’t it be rewarding?Join us, and let’s remove limits and redefine expectations with high-quality, ever-improving change. For anyone that has the intention to become something more. Take a risk; we support you.
						</h5>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-md-push-6 no-padding">
					<img class="representation-img img-responsive pull-left" src="../../image/join-team-2.png" alt="">
				</div>
				<div class="col-md-6 col-md-pull-6">
					<div class="representation animated fadeIn wow">
						<h4 class="title">Develop your creative and innovative skills in a fun environment</h4>
						<hr>
						<h5 class="subtitle">It’s all play and no work! Who says changing the world can’t be fun?</h5>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 no-padding">
					<img class="representation-img img-responsive pull-right" src="../../image/join-team-3.png" alt="">
				</div>
				<div class="col-md-6">
					<div class="representation animated fadeIn wow">
						<h4 class="title">Achieve Something Big; Work With Businesses around the Globe to Earn a Second Income</h4>
						<hr>
						<h5 class="subtitle">Get paid and be appreciated for every idea you give to an organization, with a flexible schedule that can fit around any other job!</h5>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-md-push-6 no-padding">
					<img class="representation-img img-responsive pull-left" src="../../image/join-team-4.png" alt="">
				</div>
				<div class="col-md-6 col-md-pull-6">
					<div class="representation animated fadeIn wow">
						<h4 class="title">Make a Difference to the World</h4>
						<hr>
						<h5 class="subtitle">Support our non-profit social innovation projects and make the world a better place</h5>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 no-padding">
					<img class="representation-img img-responsive pull-right" src="../../image/join-team-5.png" alt="">
				</div>
				<div class="col-md-6">
					<div class="representation animated fadeIn wow">
						<h4 class="title">Open Doors</h4>
						<hr>
						<h5 class="subtitle">You never know where the next big opportunity will come from; show off your brain power and solve challenges to gain attention.</h5>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-md-push-6 no-padding">
					<img class="representation-img img-responsive pull-left" src="../../image/join-team-6.png" alt="">
				</div>
				<div class="col-md-6 col-md-pull-6">
					<div class="representation animated fadeIn wow">
						<h4 class="title">Innovate your <br /> own life</h4>
						<hr>
						<h5 class="subtitle">Post your own personal challenges and be rewarded</h5>
						<?php echo HTML::a(\Yii::t('app', 'Join Now'), '/signup/stormer/step_1', array('class'=> 'btn btn-join')); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>