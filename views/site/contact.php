<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact Us';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-us-page">
	<div class="title-wrap">
		<div class="container-fluid">
			<h1 class="page-title"><?= Html::encode($this->title) ?></h1>
			<h4 class="page-subtitle">Having a question or a problem? Read on! We’re here to help!</h4>
			<hr />
			<h4 class="c-us-details">We have addressed various concerns and commonly asked questions below.<br />Feel free to take a quick look to see if the answer to your question is already here.</h4>
			<h3 class="link-to-faq"><?php echo HTML::a(\Yii::t('app', 'FAQ'), '/faq', []); ?> link to the main FAQ</h3>
		</div>
	</div>
	<div class="question-form-wrap">
		<?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>
			<div class="container">
				<div class="alert alert-info alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					Thank you for contacting us. We will respond to you as soon as possible.
				</div>
			</div>
		<?php else: ?>

			<div class="container">
				<p class="question-form-title">
					Question still not answered? Ask Away!
				</p>

				<?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

					<div class="row">
						<div class="col-sm-12">
							<div class="row">
								<div class="col-md-4">
									<?= $form->field($model, 'name')->textInput(['autofocus' => true, 'placeholder' => 'Contact name'])->label(''); ?>
								</div>
								<div class="col-md-4">
									<?= $form->field($model, 'email')->textInput(['placeholder' => 'Contact email'])->label(''); ?>
								</div>
								<div class="col-md-4">
									<?= $form->field($model, 'subject')->textInput(['placeholder' => 'Subject of your concern'])->label(''); ?>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="row">
								<div class="col-sm-8">
									<?= $form->field($model, 'body')->textArea(['rows' => 6, 'placeholder' => 'Descriptions'])->label(false); ?>
									<?= $form->field($model, 'send_me')->checkbox()->label('Send a copy to me'); ?>
								</div>
								<div class="col-sm-4">
									<?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
										'template' => '{image}
													{input}
													',
									]) ?>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group text-center">
								<?= Html::submitButton('Submit', ['class' => 'btn', 'name' => 'contact-button']) ?>
							</div>
						</div>
					</div>

				<?php ActiveForm::end(); ?>

			</div>
		<?php endif; ?>
	</div>
</div>
