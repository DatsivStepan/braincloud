<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Our Methodology';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about containerBlock">
	<div class="container">
		<h1 class="page-title"><?= Html::encode($this->title) ?></h1>
		<hr>
	</div>

	<div class="content grey">
		<div class="container">
			<div class="row">
				<div class="col-md-offset-2 col-md-8">
					<div class="row">
						<div class="col-sm-4 col-md-4 list-col">
							<ol class="list-met">
								<li>Develop</li>
								<li>Open Selection</li>
								<li>Remove Barriers</li>
								<li>Diverge</li>
								<li>Combine</li>
								<li>Converge</li>
								<li>Coordinate</li>
							</ol>
						</div>
						<div class="col-sm-8 col-md-8">
							<img class="img-responsive method-img" src="/image/Method2.png" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="content">
		<div class="container">
			<h3 class="title no-margin-top">Develop</h3>
			<p>
				The ability to create is a muscle that can be and must be exercised to maximize results. Our 
				methodology focuses on engaging our community ofproblem solvers to develop and stimulate their 
				overall creative potential in anon-stop series of challenges and controlled exercises, that are well 
				founded in literature to provide focused creative development. <span class="numbers">1, 2, 3, 4</span></p>
			<p>
				Our customers are able to see which of our problem solvers have engaged in the most amount of 
				practice and training, with a simple point system, as well as the number of prior-recognition awards 
				they have received for their ideas, and select those that are the strongest to work on their project.
			</p>
			<img class="img-responsive ourmeth-img" src="/image/image_1.jpg" alt="">
		</div>
	</div>
	<div class="content grey">
		<div class="container">
			<h3 class="title no-margin-top">Open Selection</h3>
			<p>
				The power of crowds and internet-mediated technologies are very known factors in the innovation 
				world. 5 The result of studies and research demonstrate that under the right conditions, drawing 
				together people with a wide-range of knowledge and specialties to solve problems will outperform 
				not only any one individual within the team, but also most likely, the organization’s own internal 
				team; providing faster and more innovative solutions to problems. <span class="numbers">6, 7, 8, 9, 10, 11</span>
			</p>
			<p>
				In 2003, the term ‘Open Innovation’ was created by Dr. Henry Chesbrough, as a result of his own 
				research that dated back over twenty years. 12 Since then, top organizations worldwide, such as NASA, 
				Apple, Procter and Gambel, Philips and Siemenshave allcapitalized on opening the doors of 
				innovation to people around the world. <span class="numbers">13, 14</span>
			</p>
			<h5 class="more"><b>For more info:</b></h5>
			<ol class="list-met">
				<li>
					P&G : <a href="http://www.pgconnectdevelop.com/home/stories.html" target="_blank">http://www.pgconnectdevelop.com/home/stories.html</a>
				</li>
				<li>
					NASA: <a href="http://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/20100042384.pdf" target="_blank">http://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/20100042384.pdf</a>
				</li>
				<li>
					Philips: <a href="http://www.research.philips.com/open-innovation/" target="_blank">http://www.research.philips.com/open-innovation/</a>
				</li>
			</ol>

			<p>
				The heart of our methodology isbased on applying “out of the box thinking” to creative, incremental 
				and radical innovationgoals. 15 Supported by gathering and developing teams of engaged and 
				rewarded problem solvers with a wide range of knowledge, we aim to uncover maximal innovation 
				potential. <span class="numbers">16, 17</span>
			</p>
		</div>
	</div>

	<div class="content">
		<div class="container">
				<h3 class="title no-margin-top">Remove Barriers</h3>

				<h5><b>We confront several well documented titans that inhibit the creative innovation process:</b></h5>
				<br />
				<ul class="list-met">
					<li>
						Social Loafing; allowing others in the team to achieve results. <span class="numbers">18, 19, 20</span>
					</li>
					<li>
						Social Anxiety; a fear of expressing ideas in front of others. <span class="numbers">21, 22, 23</span>
					</li>
					<li>
						Production Blocking; not being able to express one’s ideas due to other, more dominant 
						contributors. <span class="numbers">24, 25</span>
					</li>
					<li>
						Downward Matching; using the least productive contributor as a benchmark for 
						performance. <span class="numbers">26, 27</span>
					</li>
					<li>
						Latter idea generation reduction; motivation for expressing as well as the number of ideas 
						reduce towards the end of an innovation session. <span class="numbers">28, 29, 30</span>
					</li>
					<li>
						Category Convergence; the group focuses their ideas on a limited number of potential 
						categories. <span class="numbers">31, 32</span>
					</li>
				</ul>

				<p>
					These problems are tackled by our methodology in several ways: To reduce anxiety, we created a 
					fun, stress-free environment; our online platform requires equal contribution and prevents 
					production blocking; the systemprovides benchmarks of standards and guided assistance for problem 
					solvers to reduce downward matching and encourages a constant standard of quality; and creating a 
					blind divergence phase eliminates fixed categories. <span class="numbers">33, 34, 35, 36, 37</span>
				</p>
				<img class="img-responsive ourmeth-img two" src="/image/image_2.jpg" alt="">
		</div>
	</div>
	<div class="content grey">
		<div class="container">
				<h5 class="big-title">Our innovation system is built on four main stages:</h5>

				<h3 class="title">Divergence</h3>
				<p>
					The best way to leave the box behind, is not to be in it at all. Divergent innovation means to think 
					radically, unleashing creativity to push an ideation challenge to new limits. <span class="numbers">38, 39, 40</span>
				</p>

				<h3 class="title">Idea Sharing</h3>
				<p>
					After the initial creative idea development phase, our stormers share and combine ideas, this has the 
					benefit of further idea extension, as well asself-motivated refinement of idea quality, from social and 
					cognitive associations. <span class="numbers">41, 42, 43</span>
				</p>

				<h3 class="title">Convergence</h3>
				<p>
					After we’ve left behind everything we know, the convergent stage draws a link between the ideas 0 
					and functional strategies. An essential step in the creative thinking and innovation process. 44
					One could say that divergent and convergent thinking are two inseparable sides of the innovation 
					coin, and both are required in order to produce creative yet practical results. <span class="numbers">45, 46</span>
				</p>

				<h3 class="title">Coordinate</h3>
				<p>
					Studies tell us that many managers tend to select the ideas they most believe in, rather than ideas 
					that are the most creative and impacting. 47 Thus, we help you organize the ideas and adapt them, so 
					that you are able to leave the box behind and develop a set of strategies that you may have never 
					considered. <span class="numbers">48, 49, 50</span>
				</p>
				<p>
					You can also choose to develop and enhance ideas that you feel have potential, to further extend 
					the limitless possibilities of your results.
				</p>
		</div>
	</div>
	<div class="content">
		<div class="container">
				<h3 class="title no-margin-top">References and further reading:</h3>
				<ol class="list-met">
					<li>
						(Edited by) Plattner, Meinel, Leifer (2015): Design Thinking Research: Building Innovators
					</li>
					<li>
						Scott, Lertitz& Mumford (2004): The effectiveness of creativity training: A quantitative review 
					</li>
					<li>
						Scott, Lertitz & Mumford (2011): Types of creativity training: Approaches and their effectiveness
					</li>
					<li>
						Wang & Horng (2002): The effects of creative problem solving training on creativity, 
						cognitive type and R&D performance
					</li>
					<li>
						Surowiecki (2004): wisdom of crowds
					</li>
					<li>
						Raymond (2001). The cathedral & the bazaar musings on Linux and open source by an 
						accidental revolutionary
					</li>
					<li>
						Howe(2009): Crowdsourcing: Why the Power of the Crowd is Driving the Future of Business
					</li>
					<li>
						Oinas-Kukkonen, Harri (2008). Network analysis and crowds of people as sources of new organisational knowledge
					</li>
					<li>
						Travis(2008): Science by the Masses
					</li>
					<li>
						O'Neil(2010): Shirky and Sanger, or the costs of crowdsourcing
					</li>
					<li>
						Seltzer & Mahmoudi(2012): Citizen participation, open innovation, and crowdsourcing: Challenges and opportunities for planning
					</li>
					<li>
						Chesbrough (2003): Open Innovation: The New Imperative for Creating and Profiting from Technology
					</li>
					<li>
						Lakhani, Lifshitz-Assaf, Tushman (2012): Open Innovation and Organizational Boundaries: 
						The Impact of Task Decomposition and Knowledge Distribution on the Locus of Innovation
					</li>
					<li>
						Enkel, Gassmann, Chesbrough (2009): Open R&D and open innovation: exploring the phenomenon 
					</li>
					<li>
						(Edited by) Sternberg (1999): Handbook of Creativity
					</li>
					<li>
						Wylant (2008): Design Thinking and the Experience of Innovation
					</li>
					<li>
						Amabile (2001): Motivational synergy: Toward new conceptualizations of intrinsic and extrinsic motivation in the workplace
					</li>
					<li>
						Borgatta, & Bales (1953): Interaction of individuals in reconstituted groups 
					</li>
					<li>
						Diehl & Stroebe(1987): Productivity Loss In Brainstorming Groups: Toward the Solution of a Riddle
					</li>
					<li>
						Karau & Williams (1993): Social loafing: A meta-analytic review and theoretical integration.
					</li>
					<li>
						Camacho & Paulus (1995): The role of social anxiousness in group brainstorming.
					</li>
					<li>
						Collaros & Anderson (1969): Effect of perceived expertness upon creativity of members of brainstorming groups.
					</li>
					<li>
						Harahi & Graham (1975): Task and task consequences as factors in individual and group brainstorming
					</li>
					<li>
						Diehl & Stroebe (1991): Productivity loss in idea-generating groups: Tracking down the blocking effect.
					</li>
					<li>
						Nijstad, Stroebe, Lodewijkx (2003): Production blocking and idea generation: Does blocking interfere with cognitive processes?
					</li>
					<li>
						Paulus et al. (2002); Social and cognitive influences in group brainstorming: Predicting production gains and losses
					</li>
					<li>
						Paulus & Dzindolet (1993): Social influence processes in group brainstorming.
					</li>
					<li>
						Coskun (2000): The effects of out-group comparison, social context, intrinsic motivation, and 
						collective identity in brainstorming groups
					</li>
					<li>
						Nijstad et al. (2003): Production blocking and idea generation: Does blocking interfere with cognitive processes?
					</li>
					<li>
						Paulus et al. (2002): Social and cognitive influences in group brainstorming: Predicting production gains and losses
					</li>
					<li>
						Brown, Tumeo, Larey, Paulus (1998): Modeling cognitive interactions during group brainstorming
					</li>
					<li>
						Larey & Paulus (1999): Group preference and convergent tendencies in small groups: 
						A content analysis of group brainstorming performance
					</li>
					<li>
						Schawlow (1997): Motivating creativity in organizations
					</li>
					<li>
						Gallupe, Bastianutti & Cooper (1991): Unblocking brainstorms.
					</li>
					<li>
						Nunamaker, Briggs, Mittleman (1995): Electronic meeting systems: Ten years of lessons earned.
					</li>
					<li>
						Valacich, Dennis, Conolly (1994). Idea generation in computer based groups: A new ending to 
						an old story. Organizational Behavior and Human Decision Processes
					</li>
					<li>
						Gurteen (1998): Knowledge, Creativity and Innovation
					</li>
					<li>
						Basadur & Hausdorf (1996): Measuring Divergent Thinking Attitudes Related to Creative 
						Problem Solving and Innovation Management
					</li>
					<li>
						Paulus (2000): Groups, Teams, and Creativity: The Creative Potential of Idea‐generating Groups
					</li>
					<li>
						Coskun (2005): cognitive stimulation with convergent and divergent thinking exercises in brainwriting
					</li>
					<li>
						Dugosh & Paulus (2004): Cognitive and social comparisons in brainstorming
					</li>
					<li>
						Kohn, Paulus, Choi (2010): Building on the ideas of others: an examination of the idea combination process
					</li>
					<li>
						Michinov& Primois (2004): Improving productivity and creativity in online groups through 
						social comparison process: New evidence for asynchronous electronic brainstorming 
					</li>
					<li>
						Basadur & Gelad(2006): The Role of Knowledge Management in the Innovation Process (The four stage innovation process)
					</li>
					<li>
						Cropley (2006): In Praise of Convergent Thinking
					</li>
					<li>
						Basadur & Runco, (2000): Understanding how creative thinking skills, attitudes and behaviors work together: A causal process model
					</li>
					<li>
						Rietzschel, Nijstad, Stroebe (2010): The selection of creative ideas after individual idea 
						generation: Choosing between creativity and impact
					</li>
					<li>
						Leifer, O'Connor, & Rice (2001): Implementing radical innovation In mature firms: The role of hubs
					</li>
					<li>
						Beach (1993): Broadening the definition of decision making: The Role of Pre-choice Screening of Options
					</li>
					<li>
						Hastie &Dawes (2010): Rational choice in an uncertain world: The psychology of judgment and decision making (2nd edition).
					</li>
				</ol>
				<br />
				<br />
				<img class="img-responsive method-img" src="/image/Method2.png" alt="">
				<div class="col-sm-12">
					<a href="../../signup/customer/step_1" class="btn btn-back" >Back to customer registration</a>
				</div>
		</div>
	</div>
</div>