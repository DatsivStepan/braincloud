<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
	<div class="title-wrap">
		<div class="container">
			<h1 class="page-title"><?= Html::encode($this->title) ?></h1>
			<hr>
			<h4 class="page-subtitle">Please fill out the following fields to login:</h4>
		</div>
	</div>
	<?php if (Yii::$app->request->get('ms')){?>
		<div class="alert alert-warning alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Warning!</strong> You have not been registered through social networks.
		</div>
	<?php }?>
	<div class="form-wrap">
		<?php $form = ActiveForm::begin([
			'id' => 'login-form',
			'options' => ['class' => 'form-horizontal'],
			'fieldConfig' => [
				'template' => "{label}\n{input}\n{error}",
				'labelOptions' => ['class' => 'control-label'],
			],
		]); ?>
		<div class="container">
			<div class="row">
				<div class="col-md-offset-2 col-md-8">
					<div class="row">
						<div class="col-sm-6">
							<?= $form->field($model, 'username')->textInput(['placeholder' => 'Username'])->label(false); ?>
						</div>
						<div class="col-sm-6">
							<?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Password'])->label(false); ?>
						</div>
						<div class="col-sm-12">
							<div class="row">
								<div class="col-sm-6 text-left">
									<?= $form->field($model, 'rememberMe')->checkbox([
										'template' => "{input} {label}\n{error}",
									]) ?>
								</div>
								<div class="col-sm-6 text-right">
									<a class="forgot-pass" href="#" onclick="forgot()">Forgot your password?</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="social-icon-wrap">
			<span class="social-label">Log in with:</span>
			<a class="fb-ic" title='Login with facebook' href="/api/social?servise=facebook&type=login" style='color:#337ab7;'></a>
			<a class="twt-ic" title='Login with twitter' href="/api/social?servise=twitter&type=login" style='color:#337ab7;'></a>
			<a class="gp-ic" title='Login with Google+' href="/api/social?servise=google&type=login"></a>
		</div>

		<?= Html::submitButton('Login', ['class' => 'btn btn-login', 'name' => 'login-button']) ?>
		<?php ActiveForm::end(); ?>
	</div>
</div>
<div class="modal fade login-modal" id="myModalForgot" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Forgot</h4>
				<hr>
			</div>
			<div class="modal-body">
				<p class="capture">Please fill out the following email:</p>
				<div class="col-md-offset-1 col-md-10">
					<label class="control-label">Email</label>
					<input class="form-control" type="email" id="email_forgot">
				</div>
				<div class="clearfix"></div>
				<br />
				<br />
			</div>
			<div class="modal-footer">
				<div class="row">
					<div class="col-sm-12">
						<button type="button" class="btn btn-login" onclick="send_forgot()">Send</button>
						<button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	function forgot(){
		$('#myModalForgot').modal('show');
	}
	function send_forgot(){
		$.post('/api/forgot',{email:$('#email_forgot').val()}, function(data) {
			if (data.status){
				$('#myModalForgot').modal('hide');
				swal("", data.msg, "success");
			} else {
				swal("", data.msg, "error")
			}
		});

	}
</script>
