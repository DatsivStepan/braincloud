<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\widgets\LinkPager;

$this->title = 'Blog';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact blog-page containerBlock">
	<div class="container">
		<h1 class="page-title">Blog</h1>
		<div class="row">
			<div class="col-sm-12">
				<?php foreach($modelNews as $news){ ?>

					<div class="new-container">
						
						<?php echo HTML::a(\Yii::t('app', $news->title), Url::home().'blog/'.$news->id,['class' => 'newsTitle']); ?>
						<hr>
						
						<div class="new-content">
							
							<!-- <div class="post-image-test">
								<img class="img-responsive" src="../../web/image/new-test.jpg" alt="post-image">
							</div> -->

							<?php echo $news->content; ?>
							
							<div class="post-data">
								Posted by Braincloud on <?php echo $news->date_create; ?>
							</div>
						</div>
					</div>

				<?php } ?>
				<div class="text-center">
					<?= LinkPager::widget(['pagination'=>$pagination]); ?>
				</div>
			</div>
		</div>
	</div>
</div>
