<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

use app\assets\HomeAsset;
HomeAsset::register($this);

$this->title = 'BrainCloud';
?>

<div id="large-header" class="large-header">
	<div class="home-titles-wrapper">
		<div class="titles-wrap">
			<h1 class="home-tittle" >Growth Without Barriers</h1>
			<h3 class="home-subtitle">Business model innovation has never been so affordable and easy</h3>
			<div class="btn-wrap">
				<a href="signup/stormer" class="btn btn-stormer">Join Our Team</a>
				<a href="signup/customer/step_1" class="btn btn-customer">Our Services</a>
			</div>
		</div>
	</div>
	<div id="large-header2" class="large-header2 lamp">
		<canvas id="demo-canvas"></canvas>
	</div>
</div>

	<div class="modal fade login-modal" id="myModalCompany" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Login</h4>
					<hr>
				</div>
				<div class="modal-body">
					<p class="capture">Please fill out the following fields to login:</p>
					<div class="col-md-offset-1 col-md-10">
						<?php $form = ActiveForm::begin([
							'id' => 'login-form',
							'action' => '/site/login',
							'options' => ['class' => 'form-horizontal'],
							'fieldConfig' => [
								'template' => "{label}{input}{error}",
								'labelOptions' => ['class' => 'control-label'],
							],
						]); ?>
						<?= $form->field($newModelLogin, 'username')->textInput(['autofocus' => true]) ?>
						<?= $form->field($newModelLogin, 'password')->passwordInput() ?>
						<?= $form->field($newModelLogin, 'rememberMe')->checkbox([
							'template' => "{input} {label}{error}",
						]) ?>
						<a class="forgot-pass" href="#" onclick="forgot()">Forgot your password?</a>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="modal-footer">
					<div class="row">
						<div class="col-sm-6 text-left login-with-wrap">
							<a class="login-with fb-ic" title='Login with facebook'  href="/api/social?servise=facebook&type=login"></a>
							<a class="login-with twt-ic" title='Login with twitter'  href="/api/social?servise=twitter&type=login"></a>
							<a class="login-with gp-ic" title='Login with Google+'  href="/api/social?servise=google&type=login"></a>
						</div>
						<div class="col-sm-6">
							<?= Html::submitButton('Login', ['class' => 'btn btn-login', 'name' => 'login-button']) ?>
							<button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
						</div>
					</div>
					<?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
	</div>

<div class="modal fade login-modal" id="myModalForgot" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Forgot</h4>
				<hr>
			</div>
			<div class="modal-body">
				<p class="capture">Please fill out the following email:</p>
				<div class="col-md-offset-1 col-md-10">
					<label class="control-label">Email</label>
					<input class="form-control" type="email" id="email_forgot">
				</div>
				<div class="clearfix"></div>
				<br />
				<br />
			</div>
			<div class="modal-footer">
				<div class="row">
					<div class="col-sm-12">
						<button type="button" class="btn btn-login" onclick="send_forgot()">Send</button>
						<button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade login-modal" id="myModalForgot" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Forgot</h4>
				<hr>
			</div>
			<div class="modal-body">
				<p class="capture">Please fill out the following email:</p>
				<div class="col-md-offset-1 col-md-10">
					<label>Email</label>
					<input type="email" id="email_forgot">
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="modal-footer">
				<div class="row">
					<div class="col-sm-6">
						<button type="button" class="btn btn-login" onclick="send_forgot()">Send</button>
						<button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	function forgot(){
		$('#myModalCompany').modal('hide');
		$('#myModalForgot').modal('show');
	}
	function send_forgot(){
		$.post('/api/forgot',{email:$('#email_forgot').val()}, function(data) {
			if (data.status){
				$('#myModalForgot').modal('hide');
				swal("", data.msg, "success");
			} else {
				swal("", data.msg, "error")
			}
		});

	}
</script>
