<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

//$this->title = 'Signup';
//$this->params['breadcrumbs'][] = $this->title;
//$this->registerJsFile('/js/jquery-ui.js');
//$this->registerJsFile('/js/customer_step_1.js');
$this->registerJsFile('/js/jquery-ui.js', ['depends' => \yii\web\JqueryAsset::className()]);
$this->registerJsFile('/js/customer_step_1.js', ['depends' => \yii\web\JqueryAsset::className()]);
?>

<div class="join-team-page customer">
	<div class="container">
		<h2 class="page-title">Take A Risk and Grow; We Support You</h2>
	</div>
	<div class="join-team-representation">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6 no-padding">
					<img class="representation-img img-responsive pull-right" src="../../image/join-team-7.png" alt="">
				</div>
				<div class="col-md-6">
					<div class="representation animated fadeIn wow">
						<button class="btn btn-join buttom_bottom">Instant pricing that you control</button>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-md-push-6 no-padding">
					<img class="representation-img img-responsive pull-left" src="../../image/join-team-8.png" alt="">
				</div>
				<div class="col-md-6 col-md-pull-6">
					<div class="representation animated fadeIn wow">
						<h4 class="title">24/7 Teams of Unlimited Scope</h4>
						<h5 class="subtitle">Adaptable innovation teams ready to go whenever you are</h5>
						<hr>
						<h5 class="content">Join us on a journey to redefine the way innovation is done. Our synergized teams of problem solvers are ready any time, in a self-managed platform that engages both yourself and your team in whatever mission you set for them.</h5>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 no-padding">
					<img class="representation-img img-responsive pull-right" src="../../image/join-team-9.png" alt="">
				</div>
				<div class="col-md-6">
					<div class="representation animated fadeIn wow">
						<h4 class="title">Create or Target</h4>
						<h5 class="subtitle">Fully-customizable system that fits almost all innovation needs</h5>
						<hr>
						<h5 class="content">Moderate, unify and manage your team; organize and develop ideas and solutions, structure results in order of practicality; further develop answers with new teams, all in a beautiful and engaging interface.</h5>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-md-push-6 no-padding">
					<img class="representation-img img-responsive pull-left" src="../../image/join-team-10.png" alt="">
				</div>
				<div class="col-md-6 col-md-pull-6">
					<div class="representation animated fadeIn wow">
						<h4 class="title">Secure and Anonymous</h4>
						<h5 class="subtitle">Protected IP that belongs to you</h5>
						<hr>
						<h5 class="content">Our system ensures intellectual property remains secure, we also offer anonymous entry for clients that would rather not disclose business information.</h5>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 no-padding">
					<img class="representation-img img-responsive pull-right" src="../../image/join-team-11.png" alt="">
				</div>
				<div class="col-md-6">
					<div class="representation animated fadeIn wow">
						<h4 class="title">Modern Approach</h4>
						<h5 class="subtitle">The latest methodology supported by innovation research</h5>
						<hr>
						<h5 class="content">Our defined methodology tackles psychological limits, maximises innovation potential, and produces a wide range of prospective solutions, as well as creative ideas, that you own completely, and can be applied to every element of the business.</h5>
						<?php echo HTML::a(\Yii::t('app', 'Click here to learn more about our methodology'), '/site/ourmethodology', array('class'=> 'btn btn-join big')); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="add-team-wrap">
		<div class="container">
			<h2 class="add-team-title">Not Just For Big Players</h2>
			<h3 class="add-team-subtitle">Inexpensive and flexible pricing system</h3>
			<div class="col-md-offset-1 col-md-10 text-left">
				<span class="range-title">
					Select the size of your team
				</span>
				<div class="range-wrap">
					<div id="slider_team_size"></div>
				</div>
				<span class="range-title">
					Select the scope of your project (ideas to be generated per stormer)<a class="titleDesign" data-toggle="tooltip" title="<h4>Ideas VS Solutions</h4><p>Our system employs both the divergent creative stage of the ideation process, as well as the practical convergent stage. Therefore with a little help, every idea is a potential solution.</p>" >i</a>
				</span>
				<div class="range-wrap">
					<div id="slider_scope_project"></div>
				</div>
				<div class="price-wrap">
					<h4 class="price-title">Price($)</h4>
					<span class="sliderPrice">0</span>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="btn-wrap">
				<div class="col-sm-4 text-left">
					<h6 class="titleDesign"  data-toggle="tooltip" title="<h4>Size Vs Scope </h4><p style='font-size:14px;'>Research is quite inconclusive whether a larger team producing fewer ideas will out, equal or under perform a smaller team with more ideas. For best results, we recommend a team large enough to apply a wide range of knowledge, and a scope large enough to provide a wide range of answers. </p>" >Why those numbers?</h6>
					<a class="btn btn-help our_recomendation_customer">Our Recommendation</a>
				</div>
				<div class="col-sm-4">
					<h6 class="text-left">Click here to contact us</h6>
					<?php echo HTML::a(\Yii::t('app', 'Not Sure?'), '/contact', ['class' => 'btn btn-help', 'style' => 'btn btn-help']); ?>
				</div>
				<div class="col-sm-4 text-right">
					<h6 class="text-left">Send this quote via email</h6>
					<?php $form = ActiveForm::begin(['id' => 'contact-form', 'action' => '/site/emailquote?reg_customer=true', 'method' => 'GET']); ?>
						<input type='hidden' name='scope_project'>
						<input type='hidden' name='team_size'>
						<input type='hidden' name='count_price'>
						<?= Html::submitButton('Email Quote', ['class' => 'btn btn-help', 'name' => 'contact-button']) ?>
					<?php ActiveForm::end(); ?>
				</div>
				<div class="clearfix"></div>
			</div>
			<?php echo HTML::a(\Yii::t('app', 'Take The Next Step'), '/signup/customer/step_2', array('class'=> 'btn btn-join')); ?>
			
		</div>
	</div>
</div>