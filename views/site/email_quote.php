<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Email quote';
$this->params['breadcrumbs'][] = $this->title;
$b_text = "
Dear ";
$e_text = "
Thank you for requesting a quote regarding our services at BrainCloud Solutions.
We welcome you to join us on a journey to redefine the way innovation is done. Our synergized teams of problem solvers are ready any time.
You selected:
A ".$_GET['team_size']." person team
Working together to provide: ". $_GET['scope_project']." ideas each to solve your mission.
The price, including all costs is $".$_GET['count_price']." (for outside of Netherlands).
For customers inside of Netherlands, an extra 21% VAT surcharge must be made.
Our prices are flexible and you can adjust the system to meet your budget and needs.
Click here to learn more: Braincloud.solution/services

Kind Regards,
BrainCloud team";
?>
<style>
.row-centered {
	text-align:center;
}
</style>

<div class="email-quote-page">
	<div class="title-wrap">
		<div class="container-fluid">
			<h2 class="page-title">Send an email to yourself or someone else with your quote</h2>
			<?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>
				<div class="alert alert-success">
					Thank you for contacting us. We will respond to you as soon as possible.
				</div>
				<?php if (Yii::$app->request->get('reg_customer')) {?>
				<script>
					function func(){
						window.location = "/signup/customer/step_2";
					}
					setTimeout(func, 3000);
				</script>
				<?php }?>
			<?php else: ?>
		</div>
	</div>
	<div class="container containerBlock">
		<div class="email-quote-form-wrap">
			<?php $form = ActiveForm::begin(['id' => 'form-signup',
				'options' => ['class' => 'form-horizontal'],
				//'fieldConfig' => [
				//        'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-7\">{error}</div>",
				//        'labelOptions' => ['class' => 'col-lg-2 control-label'],
				//],
				]); ?>

					<!-- <label onkeypress="">Recipient Name</label> -->
					<div class="col-md-4 top-input">
						<?=$form->field($model,'name')->input('text',['class'=>'form-control','placeholder'=>'Recipient Name','onkeyup'=>'text_m(this)'])->label(false)?>
					</div>

						<!-- <label>Recipient Email</label> -->
					<div class="col-md-4 center-input">
						<?=$form->field($model,'email')->input('text',['class'=>'form-control','placeholder'=>'Recipient Email'])->label(false)?>
					</div>

						<!-- <label>Sender’s Email</label> -->
					<div class="col-md-4 bottom-input">
						<?=$form->field($model,'send_email')->input('text',['class'=>'form-control','placeholder'=>'Sender’s Email'])->label(false)?>
					</div>

						<!-- <label>Message</label> -->
						<?=$form->field($model,'body')->textarea([
							'class'=>'form-control',
							'rows'=>13,
							'value'=>$b_text.'Recipient Name'.$e_text,
							'id'=>'text_message'
						])->label(false)?>

					<div class="captcha-wrap">
						<label>Code</label>
						<?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
							//'template' => '{image}{input}',
							'template' => "{image}{input}",
						])->label(false) ?>
					</div>

					<div class="form-group">
						<?= Html::submitButton('Send', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
					</div>

			<?php ActiveForm::end(); ?>
			<?php endif; ?>
		</div>
	</div>
</div>

<script>

	function text_m(name){
//        alert('sd');
		console.log(name.value)
		$('#text_message').text("   Dear " +name.value+
			"\nThank you for requesting a quote regarding our services at BrainCloud Solutions.\n"+
		"We welcome you to join us on a journey to redefine the way innovation is done. Our synergized teams of problem solvers are ready any time.\n"+
		"You selected:\n"+
		"A "+"<?=$_GET['team_size']?>"+" person team\n"+
		"Working together to provide: "+"<?= $_GET['scope_project']?>"+" ideas each to solve your mission.\n"+
		"The price, including all costs is $"+"<?=$_GET['count_price']?>"+" (for outside of Netherlands).\n"+
		"For customers inside of Netherlands, an extra 21% VAT surcharge must be made.\n"+
		"Our prices are flexible and you can adjust the system to meet your budget and needs.\n"+
		"Click here to learn more: Braincloud.solution/services\n"+
		"\nKind Regards,\n"+
		"BrainCloud team\n");
	}
</script>
