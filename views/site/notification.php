<?php
/**
 *
 */?>

<div class="notifications-page">
	<div class="container containerBlock">
		<h1 class="page-title">Notifications</h1>
		<div class="table-wrap">
			<?= \kartik\grid\GridView::widget([
				'dataProvider' => $dataProvider,
				'columns' => [
					['class' => 'yii\grid\SerialColumn'],
					'message',
					'created_at:datetime',
				],
			]); ?>
		</div>
	</div>
</div>
