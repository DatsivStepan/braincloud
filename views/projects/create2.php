<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\assets\Project2Asset;
Project2Asset::register($this);

?>

<div class="post-project-page">
	<div class="projects-create">
		<div class="title-wrap">
			<h1 class="page-title">Post Project <span>Stage II</span></h1>
		</div>
		<div class="post-project-form-wrap">
			<div class="container">
				<?php $form = ActiveForm::begin(); ?>
				
					<?php if($type == 'target2'){ ?>
						<div class="target2">
							<div class="row">
								<div class="col-sm-12 target-block">
									<?= $form->field($model, 'perspectives')->textarea(['rows' => 6, 'placeholder' => ''])->label('Define perspectives') ?>
									<a class="avtomaticalPerspectiveQuestion btn btn-target pull-right">Automate</a>
									<div class="clearfix"></div>
								</div>
								<div class="col-md-6 target-block">
									<div class="row">
										<div class="col-sm-9 col-md-8 col-lg-9">
											<?= $form->field($model, 'question_convergent')->dropDownList($question_convergent,['prompt' => 'Select Convergent Questions','class'=>'form-control question_convergent']) ?>
										</div>
										<div class="col-sm-3 col-md-4 col-lg-3">
											<a class="avtomaticalConvergent btn btn-target">Automate</a>
											<div class="clearfix"></div>
										</div>
										<div class="col-sm-12">
											<p class="example-header">Example:</p>
											<textarea class="example_convergent" readonly ></textarea>
										</div>
									</div>
								</div>
								<div class="col-md-6 target-block">
									<div class="row">
										<div class="col-sm-9 col-md-8 col-lg-9">
											<label>Divergent
												<a class="titleDesignProject" data-placement="bottom" data-toggle="tooltip" title="<p style='font-size:14px;'>
												The instructions are the heart of an innovation system. You can set the system to ask any questions you feel would represent your particular project. Hover your mouse over the question titles for examples. You can also press the automate button, which will set the system to a general set of questions that give the best all-round results. 
												 </p>" >i</a>
											</label>
											<?= $form->field($model, 'question_divergent')->dropDownList($question_divergent,['prompt' => 'Select Divergent Questions','class'=>'form-control question_divergent'])->label(false) ?>
										</div>
										<div class="col-sm-3 col-md-4 col-lg-3">
											<a class="avtomaticalDivergent btn btn-target">Automate</a>
											<div class="clearfix"></div>
										</div>
										<div class="col-sm-12">
											<p class="example-header">Example:</p>
											<textarea class="example_divergent" readonly /></textarea>
										</div>
									</div>
								</div>
								<div class="col-sm-12 target-block">
									<div class="col-sm-6 no-padding">
										<label>Custom questions
											<a class="titleDesignProject" data-placement="bottom" data-toggle="tooltip" title="<p style='font-size:14px;'>
													Here, you can add any extra question that you feel is necessary.<br />
													Eg. “How would that impact the business?”<br />
													This is not a compulsory section, the system will still perform very well by leaving this blank. 
											</p>" >i</a>
										</label>
										<?= $form->field($model, 'question_custom')->textInput()->label(false); ?>
									</div>
								</div>
							</div>
						</div>
					<?php } ?>

					<div class="dropzonefile">
						Put your Files Here
					</div>
					<div id="actions" class="row">
						<div class="col-sm-12 button-wrap">
							<!-- The fileinput-button span is used to style the file input field as button -->
							<span class="btn btn-add-image fileinput-button">
								<i class="glyphicon glyphicon-plus"></i>
								<span>Add image or files...</span>
							</span>
							<button type="reset" class="btn btn-cancel cancel">
									<i class="glyphicon glyphicon-ban-circle"></i>
									<span>Cancel upload</span>
							</button>
						</div>
						<div class="col-sm-12">
							<!-- The global file processing state -->
							<span class="fileupload-process">
								<div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
									<div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
								</div>
							</span>
						</div>
					</div>
					<div class="table table-striped files" id="previews">
						<div id="template" class="file-row">
							<!-- This is used as the file preview template -->
							<div class="img-preview">
								<span class="preview"><img data-dz-thumbnail /></span>
								<p class="name" data-dz-name></p>
								<strong class="error text-danger" data-dz-errormessage></strong>
									<p class="size" data-dz-size></p>
							</div>
							<div class="add-description text-left">
								<input type="text" class="form-control exp" placeholder="Brief description of file" name="Files[explanation][]" value="" required="">
								<input type="hidden" class="path" name="Files[path][]" value="" >
								<span class="instruction">
									In the interest of data protection, as well as server space, any file you upload will be deleted automatically, 2 weeks after the project has ended.
									<a class="infoButtonShow">Support..</a>
									<div class="infoBlockShow" style="display: none;">
										Add images or files to help your stormers better understand 
										the project. If you’re having difficulty or technical problems,
										please <a href="#" onclick="send_mail_admin()">click here</a> and we’ll be in contact as soon as we possibly can.
									</div>
								</span>
							</div>
							<div class="delete-wrap">
								<a data-dz-remove class="btn delete">
									<i class="glyphicon glyphicon-trash"></i>
									<span>Delete</span>
								</a>
							</div>
							<div>
								<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
									<div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="row">
							<div class="col-sm-6 no-padding">
								<label>Add links</label>
								<input type="text" class="form-control" name="link_name" placeholder="Link">
							</div>
							<div class="col-sm-6 no-padding">
								<a id="add_link" class="btn btn-add-link">
									<i class="glyphicon glyphicon-plus"></i>
									Add
								</a>
							</div>
						</div>
						<div class="col-sm-12 linkContainer">
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="form-group">
						<?= Html::submitButton('Create', ['class' => 'btn btn-primary create']) ?>
					</div>

				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>
