<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use app\assets\ProjectAsset;
ProjectAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Projects */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="projects-form">

	<?php $form = ActiveForm::begin(); ?>
		
		<div class="col-sm-12">
			<div class="row">
				<div class="col-md-6">
					<label>Project Title
						<a class="titleDesignProject" data-placement="bottom" data-toggle="tooltip" title="
							<p>
								Reduce barriers in your project as much as you can by removing unnecessary industry lingo, and breaking down the problem to its simplest determining factor.
							</p>
							<p>
								Eg. Rather than: “Methods of Catching Institutional Memory and Knowledge”
							</p>
							<p>
								Instead: “Ways to share and record the knowledge of employees”
							</p>">i</a>
					</label>
					<?= $form->field($model, 'title')->textInput(['maxlength' => true, 'placeholder'=>'Eg. An Amazing New Toothbrush'])->label(false); ?>
					
					<label>Project Description
						<a class="titleDesignProject" data-placement="bottom" data-toggle="tooltip" title="
							<p>
								Describe the situation you want your team to focus on as clearly as you can. To get really creative answers, try to open your project to people with different fields of knowledge.
							</p>
							Consider:
								<ol>
									<li>
										Can you remove any industry or knowledge-specific lingo?<br>
									</li>
									<li>
										Are the instructions clear? If you asked someone with no prior knowledge to the project or situation to read this description, would they understand?<br>
									</li>
									<li>
										Are there any other limiting factors you can remove? The more limits you place on the project, the more limited your range of answers will be. Knowledge can sometimes be removed for the creative-thinking stage, and then applied again for the application stage.<br>
									</li>
								</ol>
							" >i</a>
					</label>
					<?= $form->field($model, 'description')->textarea(['rows' => 6, 'placeholder' => 'Eg. Can we think of new ways to brush our teeth...'])->label(false); ?>
					
					<label>Project Goals
						<a class="titleDesignProject" data-placement="bottom" data-toggle="tooltip" title="
							<p>
									What goals do you want your team to accomplish during the innovation process? Remember not to communicate your evaluation criteria here. Stormers shouldn’t know how you’ll be judging their answers, as that could greatly influence their ability to exceed expectations with really creative ideas.
								</p>
								<p>
									Eg. Rather than: “Solutions should be web-based and easy to implement”
								</p>
									Instead: “Consider how to apply the ideas and the channels that will be used”
								</p>" >i</a>
					</label>
					<?= $form->field($model, 'goals')->textarea(['rows' => 6, 'placeholder' => 'Eg. Unlike anything currently available!'])->label(false); ?>
				</div>
				
				<div class="col-md-6">
					<label>Evaluation Benchmark
						<a class="titleDesignProject" data-placement="bottom" data-toggle="tooltip" title="
							<p>
									What do you consider to be a successful solution? This is not communicated to the stormers, but back to yourself during the organization stage, at the end of your project.
								</p>
								<p>
									Your evaluation benchmark is important to consider before the project begins, as that allows you to identify which solutions are in “the box”, and which are not.
								</p>" >i</a>
					</label>
					<?= $form->field($model, 'evaluation_benchmark')->textarea(['rows' => 6, 'placeholder' => 'Eg. Cost effective and improves cleaning function...'])->label(false); ?>
				
					<div id="datetimepicker1" class="date">
						<label>Timeline for Completion
							<a class="infoButtonShow">i</a>
							<div class="infoBlockShow">
								<div class="arrow"></div>
								<p>Take as much time as you need. The minimum project time is 5 days. If this is too long for you, please let us know. We are currently measuring both demand for, as well as practical applications of shorter innovation challenges to ensure quality is not compromised.</p>
								<p>If you think the deadline is too far in advance, <a class="not_corect_timeline">click here</a>.</p>
							</div>
						</label>
					   <!-- <input data-format="yyyy-MM-dd hh:mm:ss" type="text" class='form-control from'></input> -->
					   <?= $form->field($model, 'end_date')->textInput(['class' => "date add-on  form-control","data-format"=> "yyyy-MM-dd hh:mm:ss"])->label(false) ?>
					</div>
					<div class="form-group">
						<?= $form->field($model, 'category')->dropDownList($categories) ?>
						<label>Define Your Own Innovation Category:
							<a class="titleDesignProject" data-placement="bottom" data-toggle="tooltip" title="
							<p>
								The project category tells the system what kind of innovation process to use. You can also control that manually on the next page. Selecting a category will also show you possible scope for that group.
							</p>
							<p>
								If you don’t feel any of these match the problem you’re presenting, then write your own. We’ll help you set the system on the next page.
							</p>" >i</a>
						</label>
						<?= $form->field($model, 'tmpCategory')->textInput(['placeholder'=>'Eg. Mechanical Engineering'])->label(false); ?>
					</div>
					
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	
		<div class="col-sm-12 bottom-blok">
			<div class="row">
				<div class="col-md-push-6 col-md-6">
					<div class="col-sm-8 col-sm-offset-2 text-center">
						<img class="category_icon" src="../../../image/project/target/employee_engagement.png">
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="col-md-pull-6 col-md-6">
					<div class="form-group">
						<label class="label-ngo">Project NGO
							<a class="titleDesignProject" data-placement="bottom" data-toggle="tooltip" title="
							<p>
								If you are a Non-Governmental Organization and would like to qualify for the possibility of a free project, please click here. An administrator will get in touch shortly.
							</p>">i</a>
						</label>
						<?= $form->field($model, 'ngo')->checkbox(['label' => null]) ?>
					</div>
					<div class="modal-button-wrap">
						Try to consider how your project will influence other factors.
						You can always break down a larger mission into several smaller ones. 
						<a href="#" data-toggle="modal" data-target="#myModal">Click here</a> for more information on pricing on our packages.
					</div>
					<div class="form-group text-center">
						<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-create' : 'btn btn-crate']) ?>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<?=$form->field($model,'size_team')->hiddenInput(['id'=>'size_team'])->label(false)?>
		<?=$form->field($model,'count_idea')->hiddenInput(['id'=>'count_idea'])->label(false)?>

	<?php ActiveForm::end(); ?>

</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Not Just For Big Players</h4>
				<hr>
			</div>
			<div class="modal-body">
				<div class="customerInfoTextBlock">
					<div class="col-md-offset-2 col-md-8">
						<h3 class="title-team">Inexpensive and flexible pricing system</h3>
						<div class="range-title">
							Select the size of your team
						</div>
						<div class="range-wrap">
							<div id="slider_team_size"></div>
						</div>
						<div class="range-title">
							Select the scope of your project (ideas to be generated per stormer)<a style="cursor:pointer;" class="titleDesign" data-placement="bottom" data-toggle="tooltip" title="<h4>Ideas VS Solutions</h4><p style='font-size:14px;'>Our system employs both the divergent creative stage of the ideation process, as well as the practical convergent stage. Therefore with a little help, every idea is a potential solution.</p>" >i</a>
						</div>
						<div class="range-wrap">
							<div id="slider_scope_project"></div>
						</div>
						<div class="price-wrap">
							<h4 class="price-title">Price($)</h4>
							<span class="sliderPrice">0</span>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="btn-wrap">
						<div class="col-sm-12">
							<div class="row">
								<div class="col-sm-12 col-md-4">
									<h6 class="titleDesign"  data-toggle="tooltip" title="<h4>Size Vs Scope </h4><p style='font-size:14px;'>Research is quite inconclusive whether a larger team producing fewer ideas will out, equal or under perform a smaller team with more ideas. For best results, we recommend a team large enough to apply a wide range of knowledge, and a scope large enough to provide a wide range of answers. </p>" >Why those numbers?</h6>
									<a class="btn btn-help our_recomendation_customer">Our Recommendation</a>
								</div>
								<div class="col-sm-12 col-md-4">
									<h6>Click here to contact us</h6>
									<?php echo HTML::a(\Yii::t('app', 'Not Sure?'), '/contact', ['class' => 'btn btn-help']); ?>
								</div>
								<div class="col-sm-12 col-md-4">
									<h6>Send this quote via email</h6>
	
									<?php $form = ActiveForm::begin(['id' => 'contact-form', 'action' => '/site/emailquote', 'method' => 'GET']); ?>
									<input type='hidden' name='scope_project'>
									<input type='hidden' name='team_size'>
									<input type='hidden' name='count_price'>
									<?= Html::submitButton('Email Quote', ['class' => 'btn btn-help', 'name' => 'contact-button']) ?>
									<?php ActiveForm::end(); ?>
	
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-save" data-dismiss="modal">Save</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->