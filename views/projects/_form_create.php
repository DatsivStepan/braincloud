<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\assets\ProjectAsset;
ProjectAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Projects */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="projects-form">

	<?php $form = ActiveForm::begin(); ?>

	<div class="row">
		<div class="col-sm-12">
			<div class="row">
				<div class="col-md-6">
					<label>Project Title
						<a class="titleDesignProject" data-placement="bottom" data-toggle="tooltip" title="
							<p>
								Reduce barriers in your project as much as you can by removing unnecessary industry lingo, and breaking down the problem to its simplest determining factor.
							</p>
							<p>
								Eg. Rather than: “Methods of Catching Institutional Memory and Knowledge”
							</p>
							<p>
								Instead: “Ways to share and record the knowledge of employees”
							</p>">i</a>
					</label>
					<?= $form->field($model, 'title')->textInput(['maxlength' => true, 'placeholder'=>'Eg. An Amazing New Toothbrush'])->label(false); ?>
					
					<label>Project Description
							<a class="titleDesignProject" data-placement="bottom" data-toggle="tooltip" title="
								<p>
									Describe the situation you want your team to focus on as clearly as you can. To get really creative answers, try to open your project to people with different fields of knowledge.
								</p>
								Consider:
									<ol>
										<li>
											Can you remove any industry or knowledge-specific lingo?<br>
										</li>
										<li>
											Are the instructions clear? If you asked someone with no prior knowledge to the project or situation to read this description, would they understand?<br>
										</li>
										<li>
											Are there any other limiting factors you can remove? The more limits you place on the project, the more limited your range of answers will be. Knowledge can sometimes be removed for the creative-thinking stage, and then applied again for the application stage.<br>
										</li>
									</ol>
							" >i</a>
					</label>
					<?= $form->field($model, 'description')->textarea(['rows' => 6, 'placeholder' => 'Eg. Can we think of new ways to brush our teeth...'])->label(false); ?>
					
					<label>Project Goals
							<a class="titleDesignProject" data-placement="bottom" data-toggle="tooltip" title="
								<p>
									What goals do you want your team to accomplish during the innovation process? Remember not to communicate your evaluation criteria here. Stormers shouldn’t know how you’ll be judging their answers, as that could greatly influence their ability to exceed expectations with really creative ideas.
								</p>
								<p>
									Eg. Rather than: “Solutions should be web-based and easy to implement”
								</p>
									Instead: “Consider how to apply the ideas and the channels that will be used”
								</p>" >i</a>
					</label>
					<?= $form->field($model, 'goals')->textarea(['rows' => 6, 'placeholder' => 'Eg. Unlike anything currently available!'])->label(false) ?>
				</div>
				<div class="col-md-6">
					<label>Evaluation Benchmark
							<a class="titleDesignProject" data-placement="bottom" data-toggle="tooltip" title="
								<p>
									What do you consider to be a successful solution? This is not communicated to the stormers, but back to yourself during the organization stage, at the end of your project.
								</p>
								<p>
									Your evaluation benchmark is important to consider before the project begins, as that allows you to identify which solutions are in “the box”, and which are not.
								</p>" >i</a>
					</label>
					<?= $form->field($model, 'evaluation_benchmark')->textarea(['rows' => 6, 'placeholder' => 'Eg. Cost effective and improves cleaning function...'])->label(false) ?>
					
					<div id="datetimepicker1" class="date">
						<label>Timeline for Completion
							<a class="infoButtonShow">i</a>
							<div class="infoBlockShow">
								<div class="arrow"></div>
								<p>Take as much time as you need. The minimum project time is 5 days. If this is too long for you, please let us know. We are currently measuring both demand for, as well as practical applications of shorter innovation challenges to ensure quality is not compromised.</p>
								<p>If you think the deadline is too far in advance, <a class="not_corect_timeline">click here</a>.</p>
							</div>
						</label>
						<!-- <input data-format="yyyy-MM-dd hh:mm:ss" type="text" class='form-control from'></input> -->
						<?= $form->field($model, 'end_date')->textInput(['class' => "date add-on  form-control","data-format"=> "yyyy-MM-dd hh:mm:ss"])->label(false); ?>
					</div>

					<label class="label-ngo"> Project NGO
						<a class="titleDesignProject" data-placement="bottom" data-toggle="tooltip" title="
						<p>
							If you are a Non-Governmental Organization and would like to qualify for the possibility of a free project, please click here. An administrator will get in touch shortly.
						</p>" >i</a>
					</label>
					<?= $form->field($model, 'ngo')->checkbox(['label' => null]) ?>
					<div class="form-group">
						<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-create create' : 'btn btn-create']) ?>
					</div>
					
				</div>
			</div>
		</div>
	</div>

	<?php ActiveForm::end(); ?>

</div>