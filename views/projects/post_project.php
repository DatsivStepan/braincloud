<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Select Your Project Type';
$this->params['breadcrumbs'][] = ['label' => 'Profile', 'url' => ['customer/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="select-project-type-page">
	<div class="container text-center">
		<div class="projects-index">
			<h1 class="page-title"><?= Html::encode($this->title) ?></h1>
			<hr>
			<div class="button-wrap">
				<div class="row">
					<div class="col-sm-6">
						<?php
							if($token)
								echo Html::a('Create', ['customer/post-project/create','token'=>$token], ['class' => 'btn btn-create']);
							else
								echo Html::a('Create', ['customer/post-project/create'], ['class' => 'btn btn-create']);
						?>
						<div class="create-info">
							Let your stormers loose on a<br />creative challenge. <a class="titleDesignProject" data-placement="bottom" data-toggle="tooltip" title="<p>Remove barriers and limits. Simply set a goal and allow your stormers to get really creative! Ideal for open-ideation and creative projects. </p>" >More</a>
						</div>
					</div>
					<div class="col-sm-6">
						<?php
							if($token)
								echo Html::a('Target', ['customer/post-project/target','token'=>$token], ['class' => 'btn btn-target']);
							else
								echo Html::a('Target', ['customer/post-project/target'], ['class' => 'btn btn-target']);
						?>
						<div class="create-info">
							Focus your stormers on a<br />targeted mission.  <a class="titleDesignProject" data-placement="bottom" data-toggle="tooltip" title="<p>Produce targeted focus. Break the project down, introduce perspectives and define a problem category. Ideal for challenges that require a specific set of solutions. </p>" >More</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
