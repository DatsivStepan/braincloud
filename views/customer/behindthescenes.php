<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use app\assets\BehindthescenesAsset;
BehindthescenesAsset::register($this);

$this->title = 'Behind The Scenes';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="behind-the-scenes">
	<div class="container containerBlock">
		<input type="hidden" id="scenarios_user_type" data-type="stormer">
		<h1 class="page-title">Your Project: Behind The Scenes</h1>
		<h4 class="page-subtitle">Select an idea to flag </h4>
		<?= HTML::a('Exit',Url::home().'customer/myprojects',['class'=>'btn btn-exit']); ?>
		<?php //= 'Size team - '.$projectModel->size_team; ?>
		<?php //= 'Count Idea - '.$projectModel->count_idea; ?><br />
		<?php foreach($arrayIdeas as $key => $ideas){ ?>
			<h2><?= \Yii::$app->user->identity->getUsername($key); ?></h2>
			<?php 
			$count_star = '';
			if($ideas['rating'] != null){
				$count_star = $ideas['rating']['count_star'];
			}else{
				$count_star = 0;
			} ?>
			<input class="rating_stormer<?= $key; ?>" data-stormer_id="<?= $key; ?>" data-project_id="<?= $projectModel->id; ?>" data-customer_id="<?= \Yii::$app->user->id; ?>" value="<?= $count_star; ?>" type="number">
			<div class="col-sm-12" style="padding-left:100px;">
				<?php foreach($ideas['idea'] as $idea){ ?>
					<div class="well">
						<span class='fa fa-flag pull-right fa-2x flagIcon' style='cursor:pointer;' data-idea_id='<?= $idea['id']; ?>'></span>
						<?= $idea['ideas_description']; ?><br />
						<hr style='margin-top: 5px;margin-bottom: 5px;'>
						<div class='flagBlock<?= $idea['id']; ?>' style='display:none;'>
							<h4>Reason for flagging </h4>
							<?php if($idea['flagging'] == 0){ ?>
							<div class="radio">
								<label>
									<input type="radio" name="reason_for_flagging" value="1">
									Offensive/gratuitous
								</label>
							</div>
							<div class="radio">
								<label>
									<input type="radio" name="reason_for_flagging" value="2">
									Repeated idea
								</label>
							</div>
							<div class="radio">
								<label>
									<input type="radio" name="reason_for_flagging" value="3">
									<input type='text' name='reason_for_flagging_other' placeholder="Other(please state)" class='form-control'>
								</label>
							</div>
							<button class='btn btn-primary pull-right SendFlag' data-idea_id='<?= $idea['id']; ?>'>Send</button>
							<?php }else{ ?>
								<span>Ideas flagging</span>
							<?php } ?>
							<div style='clear:both;'></div>
						</div>
					</div>
				<?php } ?>
			</div>
		<?php } ?>
	</div>
	
</div>
