<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\Alert;
use app\assets\ReferalAsset;
ReferalAsset::register($this);

?>
<div class="telltheworld-page">
	<div class="title-wrap">
		<h1 class="page-title">Tell the World about BrainCloud</h1>
		<h4 class="page-subtitle">Let people know about us and earn extra points!</h4>
		<hr>
	</div>
	<div class="container containerBlock">
		<div class="row">
			<div class="col-sm-push-4 col-sm-8">
				<div class="content">
					<h3 class="details">
						Be proud that you’re working with the most innovative innovation system around, and earn loyalty points for letting people now!
						<br />
						<br />
						Earn 5 points for every account you post on!
					</h3>
					<h3 class="details">
						This is the message that will be posted, no other message will be posted at any other time without your prior consent.
					</h3>
				</div>
			</div>
			<div class="col-sm-pull-8 col-sm-4">
				<img class="tell-img img-responsive" src="/image/BusinessSocialMedia.jpg" width="300">
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="social-icon-wrap">
			<h3 class="details">Share With</h3>
			<a class="fb-ic" title='Share with facebook' onclick="add_point_social('<?='http://www.facebook.com/share.php?u='.\yii\helpers\Url::home(true).'tells_word.html'?>')" target="_blank"  href="<?='http://www.facebook.com/share.php?u='.\yii\helpers\Url::home(true).'tells_word.html'?>"></a>
			<a class="twt-ic" title='Share with twitter' onclick="add_point_social('<?='https://twitter.com/intent/tweet?url='.\yii\helpers\Url::home(true).'tells_word.html&via=braincloud'?>')" target="_blank" href="<?='https://twitter.com/intent/tweet?url='.\yii\helpers\Url::home(true).'tells_word.html&via=braincloud'?>"></a>
			<a class="gp-ic" title='Share with Google+' onclick="add_point_social('<?='https://plus.google.com/share?url='.\yii\helpers\Url::home(true)?>')" target="_blank" href="<?='https://plus.google.com/share?url='.\yii\helpers\Url::home(true)?>"></a>
			<a class="in-ic" onclick="add_point_social('<?='https://www.linkedin.com/shareArticle?mini=true&url='.\yii\helpers\Url::home(true).'&title=Braincloud&summary=&source='?>')" target="_blank" href="<?='https://www.linkedin.com/shareArticle?mini=true&url='.\yii\helpers\Url::home(true).'&title=Braincloud&summary=&source='?>"></a>
		</div>
	</div>
</div>
<script>
	function add_point_social(url){
		$.post(
			'/api/addpoints',
			{type:'social', point:5},
			function($result){
				if($result){
//					window.open(url,'_blank');
					location.reload();
				}else{
					alert('error');
				}
			}
		)
	}
</script>