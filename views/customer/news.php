<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\Alert;

if($status == 'all'){ 
	$this->title = 'News';
	$this->params['breadcrumbs'][] = ['label' => 'Profile Home', 'url' => [$home_profile_src]];
	$this->params['breadcrumbs'][] = $this->title = 'News';
}else{
	$this->title = 'News';
	$this->params['breadcrumbs'][] = ['label' => 'Profile Home', 'url' => [$home_profile_src]];
	$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => [$home_news_link]];
	$this->params['breadcrumbs'][] = $this->title = $modelNews->title;
}
?>
<div class="container blog-page containerBlock">
	<h1 class="page-title">Important Notifications and News </h1>
	<?php if($status == 'all'){ ?>
		<div class="row">
			<div class="col-sm-12">
				<?php foreach($modelNews as $news){ ?>
					<div class="new-container">
						<?php echo HTML::a(\Yii::t('app', $news->title), '/customer/news/'.$news->id,['class' => 'newsTitle']); ?>
						<hr>
						<div class="new-content">
							<?php echo $news->content; ?>
							
							<div class="post-data">
								Posted by Braincloud on <?php echo $news->date_create; ?>
							</div>
						</div>
					</div>
				<?php } ?>
				<div class="text-center">
					<?= LinkPager::widget(['pagination'=>$pagination]); ?>
				</div>
			</div>
		</div>
	<?php }else{ ?>
		<div class="row">
			<div class="col-sm-12">
				<?php
					if(Yii::$app->session->hasFlash('comment_added')):
						echo Alert::widget([
							'options' => [
								'class' => 'alert-info',
							],
							'body' => 'Comment added',
						]);
					endif; 
					if(Yii::$app->session->hasFlash('comment_not_added')):
						echo Alert::widget([
							'options' => [
								'class' => 'alert-error',
							],
							'body' => 'Comment not added',
						]);
					endif; 
				?>
				<?php
					if(Yii::$app->session->hasFlash('comment_delete')):
						echo Alert::widget([
							'options' => [
								'class' => 'alert-info',
							],
							'body' => 'Comment delete',
						]);
					endif; 
					if(Yii::$app->session->hasFlash('comment_not_delete')):
						echo Alert::widget([
							'options' => [
								'class' => 'alert-error',
							],
							'body' => 'Comment not delete',
						]);
					endif; 
				?>
			</div>
		</div>
		<div class="post-open">
			<div class="row">
				<div class="col-sm-12">
					<h2 class="news-title"><?= $modelNews->title; ?></h2>
					<div class="date">
						<span class="date-icon glyphicon glyphicon-time"></span><?= $modelNews->date_create; ?>
					</div>
					<hr class="hr-date">
					<div class="new-content"><?php echo $modelNews->content; ?></div>
				</div>
				<div class="col-sm-12">
					<div class="comment-block">
						<h4 class="head">Leave Comment</h4>
						<div clas="col-sm-12">
							<?php $form = ActiveForm::begin(['id' => 'form-signup', 'options' => ['class' => 'form-horizontal form-add-comment']]); ?>
								<?= $form->field($newComment, 'content')->textarea(); ?>
								<div class="col-sm-12" style="height: 0px;">
									<?= $form->field($newComment, 'news_id')->hiddenInput(['value' => $modelNews->id])->label(false); ?>
									<?= $form->field($newComment, 'user_id')->hiddenInput(['value' => Yii::$app->user->identity->id])->label(false); ?>
								</div>
								<?= Html::submitButton('Add comment', ['class' => 'btn btn-comment', 'name' => 'saveNews']); ?>
							<?php ActiveForm::end(); ?>
						</div>
					</div>
					<div class="col-sm-12">
						<?php foreach($modelComment as $comment){ ?>
							<div class="media">
								<a class="pull-left" href="#">
									<img class="media-object" src="http://placehold.it/64x64" alt="">
								</a>
								<div class="media-body">
									<h4 class="media-heading">
									<div class="comment-date"><?= $comment->date_create; ?></div>
									<?php if($comment->user_id == Yii::$app->user->identity->id){ ?>    
										<?php $form = ActiveForm::begin(['id' => 'formDelete'.$comment->id, 'options' => ['class' => 'form-horizontal']]); ?>
											<input type="hidden" name="comment_id" value="<?= $comment->id; ?>">
											<?= Html::submitButton('<span class="glyphicon glyphicon-remove pull-right""></span>', ['class' => 'pull-right delete-comment', 'name' => 'deleteComment']); ?>
										<?php ActiveForm::end(); ?>
									<?php } ?>
									</h4>
										<?= $comment->content; ?>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>
</div>
