<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/**
 *
 */?>

<div class="customer-message-page">
	<div class="container containerBlock">
		<h1 class="page-title"><?=$project->title?></h1>
		<h4 class="page-subtitle"><?=$project->description?></h4>
		<?php
			if($aler){
				?>
				<div class="alert alert-info alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					You created message!
				</div>
				<?php
			}
		?>
		<div class="panel">
			<div class="panel-heading">
				<button class="btn btn-new-message" onclick="add()">New message</button>
				<h3 class="panel-title">Messages:</h3>
			</div>
			<div class="panel-body">
				<?php foreach($messages as $item){
					if ($item->type_user =='customer'){
						?>
						<div class="row">
							<div class="col-md-8">
								<div class="row">
									<div class="alert alert-success left-col" role="alert">
										<p class="author">My message <?= $item->status?'<span class="label label-success">read</span>':'<span class="label label-danger">unread</span>'?></p>
										<p class="message-content"><?=$item->message?></p>
										<p>
											<span class="date"><?= Yii::$app->formatter->asDatetime($item->created_at)?></span>
											<span class="clearfix"></span>
										</p>
									</div>
								</div>
							</div>
						</div>
					<?php }else {
						$item->status=1;
						$item->save();
						?>
						<div class="row">
							<div class="col-md-offset-4 col-md-8">
								<div class="row">
									<div class="alert alert-info right-col" role="alert">
										<p class="author">Stormer</p>
										<p class="message-content"><?=$item->message?></p>
										<p>
											<span class="date"><?= Yii::$app->formatter->asDatetime($item->created_at)?></span>
											<span class="clearfix"></span>
										</p>
									</div>
								</div>
							</div>
						</div>

					<?php } }?>
					<div class="row">
						<?php
						echo \yii\widgets\LinkPager::widget([
							'pagination' => $pages,
						]);?>
					</div>
			</div>
		</div>
	</div>
</div>

<div class="modal modal-message fade" tabindex="-1" role="dialog" id="myModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">New message</h4>
			</div>
			<?php $form = ActiveForm::begin(); ?>
			<div class="modal-body">
				<textarea class="form-control"  name="message" rows="3"></textarea>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-send">Send</button>
			</div>
			<?php ActiveForm::end(); ?>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
	function add(){
		$('#myModal').modal('show');
	}
</script>