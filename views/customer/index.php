<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Home profile';
$this->params['breadcrumbs'][] = $this->title;

/*$points_message = \app\models\PointsMessage::find()->where(['user_id'=>Yii::$app->user->id, 'show'=>0])->all();
foreach($points_message as $item){
	$this->registerJs(
		"$().toastmessage('showToast', {
			text     : '$item->message',
			sticky   : true,
			position : 'top-right',
			type     : 'success',

		});"
	);
	$item->show = 1;
	$item->save();
}*/

?>

<div class="dashboard-page">
	<div class="container dashboard-section">
		<h1 class="page-title">Customer Home Page</h1>
		<div class="profile-menu">
			<div class="row">
				<div class="col-md-4 dashboard-left-col">
					<div class="row">
						<div class="col-xs-6 col-md-12 dashboard-block">
							<div class="profile-user">
								<div class="table">
									<div class="table-cell">
										<div class="profile-photo-wrap">
											<?php if($customerInfo['image_name'] == ''){ ?>
												<img src='../../image/default-avatar.png'>
											<?php }else{ ?>
												<img src='<?= Url::home().'image/users_images/'.$customerInfo['image_name']; ?>'>
											<?php } ?>
										</div>
									</div>
									<div class="table-cell">
										<div class="user-info">
											<?php echo HTML::a(\Yii::t('app', $userModel->username), '/customer/profile/'.$userModel->id, ['class' => 'profile-link']); ?>
											<div class="other-info">
												<div class="col-xs-6">
													Name of business:
												</div>
												<div class="col-xs-6">
													<span><?php echo $customerInfo['business_name'];  ?></span>
												</div>
												<div class="col-xs-6">
													Business Type:
												</div>
												<div class="col-xs-6">
													<span><?php echo $customerInfo['business_type'];  ?></span>
												</div>
												<div class="clearfix"></div>
												<?php if($customerInfo->country):?>
													<div class="col-xs-6">
														Location:
													</div>
													<div class="col-xs-6">
														<span><?php echo $customerInfo->locationC->country_name;  ?> (<?php echo $customerInfo->locationS->city_name;  ?>)</span>
													</div>
												<?php endif?>
													<div class="col-xs-6">
														Level:
													</div>
													<div class="col-xs-6">
														<span><?php echo $userModel->level.' ('.\app\models\Points::find()->where(['user_id'=>Yii::$app->user->id])->sum('point').' points)';  ?></span>
													</div>
												<div class="clearfix"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-md-12 dashboard-block active-project customer">
							<div class="animate-block">
								<div class="menu-item-content">
									<div id="active_project">
											<?php
											if ($active_project)
												foreach($active_project as $item){
													echo HTML::a(\Yii::t('app', $item->title), '/customer/behindthescenes/'.$item->id,['class'=>'']);
												}
											else
												echo HTML::a(\Yii::t('app', '<h5>Post a Project</h5>'), '/customer/post-project',['class'=>'']);
											?>
									</div>
									<a id="load_more" <?=$page?'':'style="display:none"'?>><h5 onclick="add_active_project()">Load more</h5></a>
								</div>
								<div class="menu-item-wallaper active-prj">
									<div class="wallaper-content">
										<?php echo HTML::a(\Yii::t('app', '<h2>Active Projects</h2>'), '/customer/myprojects/current',['class'=>'']); ?>
										<!-- <h2>Active Projects</h2> -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-8 dashboard-right-col">
					<div class="row">
						<div class="col-xs-6 col-sm-4 dashboard-block less">
							<div class="animate-block">
								<div class="menu-item-content">
									<?php  echo HTML::a(\Yii::t('app', '<h4>Print and Review</h4>'), '/customer/invoices',['class'=>'']); ?>
								</div>
								<div class="menu-item-wallaper invoices">
									<div class="wallaper-content">
										<h2>Invoices</h2>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-sm-4 dashboard-block less">
							<div class="animate-block">
								<div class="menu-item-content">
									<?php  echo HTML::a(\Yii::t('app', '<h4>Not-for-Profit Projects</h4>'), '/customer/beinspired/Peer',['class'=>'']); ?>
								</div>
								<div class="menu-item-wallaper be-inspired">
									<div class="wallaper-content">
										<h2>Be Inspired</h2>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-sm-4 dashboard-block less">
							<div class="animate-block">
								<div class="menu-item-content">
									<?php echo HTML::a(\Yii::t('app', '<h4>News & Notifications</h4>'), '/customer/news'); ?>
								</div>
								<div class="menu-item-wallaper news-block">
									<div class="wallaper-content">
										<h2>Stay Informed</h2>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-12 dashboard-block bigger">
							<div class="animate-block">
								<div class="menu-item-content">
									<?php echo HTML::a(\Yii::t('app', '<h4>Options</h4>'), '/customer/myprojects/current',['class'=>'']); ?>
								</div>
								<div class="menu-item-wallaper my-project">
									<div class="wallaper-content">
										<h2>My Projects</h2>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-sm- dashboard-block less">
							<div class="animate-block">
								<div class="menu-item-content">
									<?php echo HTML::a(\Yii::t('app', '<h4>Post a Project</h4>'), '/customer/post-project',['class'=>'']); ?>
								</div>
								<div class="menu-item-wallaper post-project">
									<div class="wallaper-content">
										<h2>Innovate</h2>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-sm- dashboard-block less">
							<div class="animate-block">
								<div class="menu-item-content">
									<?php echo HTML::a(\Yii::t('app', '<h4>Referral Page</h4>'), '/customer/referalpage',['class'=>'']); ?>
								</div>
								<div class="menu-item-wallaper referal">
									<div class="wallaper-content">
										<h2>Recommend a customer</h2>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-sm-4 dashboard-block less">
							<div class="animate-block">
								<div class="menu-item-content">
									<?php  echo HTML::a(\Yii::t('app', '<h4>Send a Message</h4>'), '/contact',['class'=>'']); ?>
								</div>
								<div class="menu-item-wallaper contact-us">
									<div class="wallaper-content">
										<h2>Contact Us</h2>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-sm-4 dashboard-block less">
							<div class="animate-block">
								<div class="menu-item-content">
									<?php echo HTML::a(\Yii::t('app', '<h4>Get Help</h4>'), '/customer/supportpage',['class'=>'']); ?>
								</div>
								<div class="menu-item-wallaper support-page">
									<div class="wallaper-content">
										<h2>Customer Support</h2>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-sm-4 dashboard-block less">
							<div class="animate-block">
								<div class="menu-item-content">
									<?php echo HTML::a(\Yii::t('app', '<h4>Spread the Word</h4>'), '/customer/telltheworld',['class'=>'']); ?>
								</div>
								<div class="menu-item-wallaper tell-world">
									<div class="wallaper-content">
										<h2>Tell the World</h2>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 dashboard-block less">
							<div class="animate-block">
								<div class="menu-item-content">
									<?php echo HTML::a(\Yii::t('app', '<h4>Core Beliefs</h4>'), '/customer/about',['class'=>'']); ?>
								</div>
								<div class="menu-item-wallaper about-us">
									<div class="wallaper-content">
										<h2>About Us</h2>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-sm-6 dashboard-block less">
							<div class="animate-block">
								<div class="menu-item-content">
									<?php  echo HTML::a(\Yii::t('app', '<h4>Terms and Conditions</h4>'), '/customer/terms_conditions',['class'=>'']); ?>
								</div>
								<div class="menu-item-wallaper terms">
									<div class="wallaper-content">
										<h2>Terms and Conditions</h2>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	var page = 0;
	function add_active_project(){
//       alert(page);
		page++;
		$.ajax({
			type: 'POST',
			url: '/api/test',
			data: {page:page},
			success: function(response){
				if(response.status){
					console.log(response.data);
					$.each(response.data, function(index, value) {
						console.log(value);
						$('#active_project').append('<a href="/customer/behindthescenes/'+value.id+'">'+value.title+'</a>');
					});
					$('#list_project').css('height',500+'px');
					// $('#active_project').css('height',400+'px');
					if (!response.page){
						$('#load_more').hide();
						$('#post-project').show();
					}
				}
			}
		});

	}
</script>
