<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\Alert;

$this->title = 'My Projects: past projects';
?>
<div class="my-project-customer-page">
	<div class="container containerBlock">
		<h1 class="page-title">My Projects</h1>
		<h4 class="page-subtitle">Information related to your ongoing and past projects. </h4>
		<div class="projects-tab-wrapper">
			<div class="row">
				<div class="col-sm-12">
					<ul class="nav nav-tabs">
						<li role="presentation">
							<?= HTML::a('Current Projects',Url::home().'customer/myprojects/current'); ?>
						</li>
						<li role="presentation" class="active">
							<?= HTML::a('Past Projects',Url::home().'customer/myprojects/past'); ?>
						</li>
					</ul>
					<hr class="nav-line" />
				</div>
				<div class="col-sm-12">
				<h2 class="tab-title">Past Projects</h2>
				<div class="row">
					<?php foreach($modelProjectsClose as $projects){?>
						<div class="col-sm-12">
							<h3 class="project-name"><?= $projects->title; ?></h3>
							<hr class="progress-hr">
							<div class="project-container">
								<div class="col-sm-12">
									<div class="row">
										<div class="col-sm-2">
											<h4 class="status">Status:</h4>
										</div>
										<div class="col-sm-10">
											<span class="status-project">Closed</span>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-2">
											<h4 class="team">Your Team:</h4>
										</div>
										<div class="col-sm-6">
											<?php foreach($projects->team as $userModel){
												echo '<span class="team-members">'.$userModel['username'].'</span>';
											}; ?>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="row">
										<div class="col-md-12 left-buttons">
											<a class="btn add-star" href="/customer/organizeidea/<?=$projects->id?>"><span aria-hidden="true" class="glyphicon glyphicon-star"></span> Star Your Stormers</a>
											<?= HTML::a('Organize and Export Ideas','/customer/organizeidea/'.$projects->id,['class' => 'btn btn-organize']); ?>
										</div>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					<?php } ?>
					<div class="col-sm-12 text-center">
						<?= LinkPager::widget(['pagination'=>$pagination]); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>