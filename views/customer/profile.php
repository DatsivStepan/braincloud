<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use app\assets\ProfilecustomerAsset;
ProfilecustomerAsset::register($this);

$this->title = 'Profile page';
$this->params['breadcrumbs'][] = ['label' => 'Profile Home', 'url' => [$home_profile_src]];
$this->params['breadcrumbs'][] = $this->title = 'Profile page';
?>
<div class="profile-page">
	<div class="container containerBlock">
		<?php
			if(Yii::$app->session->hasFlash('profile_update')):
				echo Alert::widget([
					'options' => [
						'class' => 'alert-info',
					],
					'body' => 'Profile updated',
				]);
			endif; 
			if(Yii::$app->session->hasFlash('profile_notupdate')):
				echo Alert::widget([
					'options' => [
						'class' => 'alert-error',
					],
					'body' => 'Profile not updated',
				]);
			endif; 
		?>
		<h1 class="page-title">Your Profile</h1>
		<hr>
		<div class="profile-container">
			<div class="row">
				<div class="col-sm-3">
					<div class="profile-avatar-wrap">
						<img class="img-responsive profilePhoto" src="<?= $customerInfo['image_name'] ? Url::home().'image/users_images/'.$customerInfo['image_name'] : '/image/default-avatar.png'; ?>">
						<a class="btn btn-change-avatar clicable">Change avatar</a>
						<div id="previewsS" style="display:none;"></div>
					</div>
				</div>
				<div class="col-sm-9">
					<div class="col-sm-12">
						<?php $form = ActiveForm::begin(['id' => 'form-signup', 'options' => ['class' => 'form-horizontal']]); ?>
						<?= $form->field($userModel,'name')->input('text',['disabled'=>'disabled'])->label('Contact name')?>
						<?= $form->field($userModel,'username')->input('text',['disabled'=>'disabled'])->label('Contact login')?>
						<?= $form->field($customerInfo, 'image_name')->hiddenInput()->label(false); ?>
							<?= $form->field($customerInfo, 'business_name')->textInput(['placeholder' => 'Business Name'])->label('Name of Business'); ?>
							<?= $form->field($customerInfo, 'country')->dropDownList($arrayCountry,['class'=>'selectCountry form-control','prompt'=>'Select country?'])->label('Country'); ?>
							<?php
//                                echo  $form->field($customerInfo, 'city')->dropDownList($arrayCity,['class'=>'selectCity form-control','prompt'=>'Select Your City'])->label('City');
                            ?>
                        <?php
                        echo \kartik\typeahead\Typeahead::widget([
                            'name' => 'city_head',
                            'options' => [
                                'placeholder' => 'Select Your City',
                                'class' => 'selectCity form-control',
                            ],
                            'value'=>$city_name->city_name,
                            'pluginOptions' => ['highlight'=>true],
                            'dataset' => [
                                [
                                    'remote' => [
                                        'url' => Url::to(['site/country-list']) . '?q=%QUERY',
                                        'wildcard' => '%QUERY'
                                    ]
                                ]
                            ]
                        ]);
                        ?>
							<?= $form->field($customerInfo, 'business_type')->textInput(['placeholder' => 'Business Type'])->label('Type of Business'); ?>
							<?= $form->field($customerInfo, 'business_services')->textArea(['rows' => '3'])->label('Brief summary of business services'); ?>
							<?= $form->field($customerInfo, 'more_information')->textArea(['rows' => '5'])->label('More information: Tell us more about who you are!'); ?>
							<?= $form->field($customerInfo, 'ask')->radioList(array('1'=>'Yes', 2=>'No'))->label('Is it okay for stormers to ask you questions during a project?'); ?>
							<?= $form->field($userModel,'email')->label(false)?>
							<div class="row">
								<div class="panel panel-info" style="display: none;" id="email_notification">
									<div class="panel-heading">
										<h3 class="panel-title">Email notification settings:</h3>
									</div>
									<div class="panel-body">
										<?= $form->field($customerInfo, 'notifications')->radioList(array('1'=>'Yes', 2=>'No'))->label('Email all notifications'); ?>
										<?= $form->field($customerInfo, 'notification_project')->radioList(array('1'=>'Yes', 2=>'No'))->label('Email me with notifications only related to my project'); ?>
										<?= $form->field($customerInfo, 'notifications_off')->radioList(array('1'=>'Yes', 2=>'No'))->label('Switch off all notifications'); ?>
									</div>
								</div>
							</div>
							<div class="text-right">
								<div class="row">
									<a class="btn btn-email-panel" id="email_notification_btn" onclick="email_notification()">Email notification settings</a>
									<?= Html::submitButton('Save', ['class' => 'btn btn-save', 'name' => 'saveCustomer']) ?>
								</div>
							</div>
						<?php ActiveForm::end(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>

	function email_notification(){
		$('#email_notification').show();
		$('#email_notification_btn').hide();
	}
</script>
