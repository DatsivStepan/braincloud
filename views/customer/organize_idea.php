<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use app\assets\OrganizeAsset;
OrganizeAsset::register($this);


$this->title = 'Organize and export ideas';
$this->params['breadcrumbs'][] = $this->title;
$count_ide = \app\models\Ideas::find()->where(['project_id'=>$project_id])->count();
$time = $count_ide*30;
?>

<div class="organizeidea-page">
	<?php
	
	if($message){
		$this->registerJs(
			"$('document').ready(function(){
				   swal({
						title:'',
						text: 'You have $count_ide ideas and potential solutions. If you were to look at each one and consider the value, then it could take you $time to complete. We highly recommend using our quick and easy idea organization system. You can still export all your ideas, they’ll just much better organized, and much more guaranteed to end in usable solutions. Simply click on an idea to begin. Organizing all your ideas will take between 30m to an hour, and is time very well invested! You can always leave at any time and come back to complete it later. ',
						confirmButtonText: 'Close',
					});
				});"
		);
		$project_my = \app\models\Projects::findOne($project_id);
		$project_my->show_message = 1;
		$project_my->scenario = 'update_message';
		$project_my->save();
	}
	if($message_procent and ($procent == 100)){
		$this->registerJs(
			"$('document').ready(function(){
				   swal({
						title:'',
						text: 'Congratulations! Your ideas are now organized! You can toggle on and off your idea categories. The ‘what you know works’ category is the easiest to implement, but the most exciting category is the ‘out of the box category’, and worth giving a little extra attention to! Don’t forget you can also select any idea category and develop them further with a new, extended project, with a simple click of the button.',
						confirmButtonText: 'Close',
					});
				});"
		);
		$project_my = \app\models\Projects::findOne($project_id);
		$project_my->show_message = 2;
		$project_my->scenario = 'update_message';
		$project_my->save();
	}
	?>
	
	<style>
		.summary{
			display: none;
		}
	</style>
	<div class="container containerBlock organizePage">
		<h1 class="page-title">Organize and Export Ideas</h1>
		<hr>
		<h4 class="page-subtitle">Instructions: Organization is <?=$procent?>% complete, click on any idea string to begin. Scroll your mouse over the symbol to see the question.</h4>
		<div class="button-wrap">
			<?php
			$gridColumns = [
				['class' => 'kartik\grid\SerialColumn'],
				[
					'label'=>'Stormers',
					'content' => function($date){
						return $date->Stormerv($date->project_id,$date->user->id);
					}
				],
				'ideas_description',
//                'organize.category',
//                'organize.cat_child',
				[
					'label'=>'Category',
					'content' => function($date){
						$mod = \app\models\OrganizeIdea::find()->where(['idea_id'=>$date->id])->one();
						return ($mod)?$mod->category:'not set';
					}
				],
				[
					'label'=>'Subcategory',
					'content' => function($date){
						$mod = \app\models\OrganizeIdea::find()->where(['idea_id'=>$date->id])->one();
						return ($mod)?$mod->cat_child:'not set';
					}
				]
			];


			echo ExportMenu::widget([
				'dataProvider' => $dataProvider,
				'showColumnSelector'=>true,
				'columns' => $gridColumns,
				'columnSelectorOptions'=>[
					'label' => 'Colums',
					'class' => 'btn-cols',
				],
				'fontAwesome' => true,
				'hiddenColumns'=>[0],
				'dropdownOptions' => [
					'label' => 'Export now',
					'class' => 'btn btn-export',
				],
				'filename' => 'ideas',
				'asDropdown'=>true,
				'exportConfig' =>[
					ExportMenu::FORMAT_TEXT => false,
					ExportMenu::FORMAT_PDF => [
						'label' =>  'PDF',
						'icon' => 'file-pdf-o',
						'iconOptions' => ['class' => 'text-danger'],
						'linkOptions' => [],
						'options' => ['title' => 'Portable Document Format'],
						'alertMsg' => 'The PDF export file will be generated for download.',
						'mime' => 'application/pdf',
						'extension' => 'pdf',
						'writer' => 'PDF'
					],
					ExportMenu::FORMAT_HTML =>false,
					ExportMenu::FORMAT_CSV => false,
					ExportMenu::FORMAT_EXCEL => false,
					ExportMenu::FORMAT_EXCEL_X => [
						'label' =>  'Excel',
						'icon' =>  'file-excel-o' ,
						'iconOptions' => ['class' => 'text-success'],
						'linkOptions' => [],
						'options' => ['title' => 'Microsoft Excel 2007+ (xlsx)'],
						'alertMsg' =>  'The EXCEL 2007+ (xlsx) export file will be generated for download.',
						'mime' => 'application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
						'extension' => 'xlsx',
						'writer' => 'Excel2007'
					],
				]
			]);
			?>
<!--            <button class="btn btn-default exportButton">Export now</button>-->
			<button class="btn implement_ideas" onclick="implement(<?=$project_id?>)">Implement ideas</button>
			<?= HTML::a('Come back later','/',['class'=>'btn btn-later']); ?>
			<?php 
				if($countIdeaNotOrganize == '0'){ ?>
					<?= HTML::a('Develop Selected Categories Further','/customer/developfurther/'.$project_id,['class'=>'btn btn-develop']); ?>
			 <?php   }
			?>
		</div>
		<div class="table-wrap">
			<?php
			echo GridView::widget([
	
				'dataProvider' => $dataProvider,
				'filterModel' => $searchModel,
				'columns' => [
					['class' => 'kartik\grid\SerialColumn'],
	//                'user.username',
					[
						'label'=>'Stormers',
						'content' => function($date){
							return $date->Stormerv($date->project_id,$date->user->id);
						}
					],
					'ideas_description',
	//            'organization.category',
					[
						'attribute' => 'category',
						'value' =>  'organization.category',
						'filter' => Html::activeDropDownList(
							$searchModel,
							'category',
							['out of box thinking'=>'out of box thinking','what you know works'=>'what you know works'],
							['class'=>'form-control', 'prompt' => '']
						)
					],
					[
						'attribute' => 'cat_child',
						'value' =>  'organization.cat_child',
						'format' => 'html',
						'filter' => Html::activeDropDownList(
							$searchModel,
							'cat_child',
							[
								'can be implemented with little expense to resources'=>'Can be implemented with little expense to resources',
								'needs developing - develop further'=>'needs developing - develop further',
								'not usable at this time'=>'not usable at this time'
							],
							['class'=>'form-control', 'prompt' => '']
						)
					],
					[
						'label'=>'Create project',
						'format' => 'html',
						'value' => function($model){
							return "<a href='/customer/developyour?title=".$model->ideas_description."&id=".$model->project_id."' class='btn btn-primary'>create</a>";
						}
					]
	//            'organization.cat_child',
	
	//                [
	//                    'label'=>'Category',
	//                    'content' => function($date){
	//                        $mod = \app\models\OrganizeIdea::find()->where(['idea_id'=>$date->id])->one();
	//                        return ($mod)?$mod->category:'not set';
	//                    },
	//
	//                ],
	//                [
	//                    'label'=>'Subcategory',
	//                    'content' => function($date){
	//                        $mod = \app\models\OrganizeIdea::find()->where(['idea_id'=>$date->id])->one();
	//                        return ($mod)?$mod->cat_child:'not set';
	//                    }
	//                ]
				]
			]);
			?>
		</div>
	</div>
</div>

<div class="modal fade" id="organizeInstruction" role="dialog">       
	<div class="modal-dialog" style="width:80%;">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<h1 style="text-align: center">Organization Instructions for Customer</h1>
				<h3 style="text-decoration: underline;">Assumptions</h3> 
				<p>Consider the assumptions in your position and experiment with them. 
				For example, your goal is to develop ideas for a new water bottle packaging. When judging the ideas, there could be several assumptions that may exist in your line of thought: Packaging: Do all the ideas need to relate to the packaging or can they relate to something else? A different brand element perhaps? Product: Do all the ideas need to be used on the product? Could they be used on another product? Or on the website? As a printed ad? The packaging materials: Does it need to be printed on the originally conceived packaging? Can it be printed directly on the bottle? Can the bottle have no packaging and instead have a nice design? Can the packaging be removable?</p>
				<h3 style="text-decoration: underline;">Escape Thinking.</h3>
				<p>Trace all the assumptions in the line of thought, to the goal you are trying to achieve, then reverse each stage and look at your goal from a new perspective. 
				For example, you are designing your product packaging to be desirable and attractive therefore selling more. Your assumption may be ‘People buy water for themselves’. This naturally leads to consider who else the product may be purchased for. Mothers and children perhaps? Next, reverse the assumption to ‘People do NOT buy water for drinking at all’. What will this lead to? You may end up with people buy water as plant feeder, for collection purposes, etc. </p>
				<h3 style="text-decoration: underline;">Counteraction Busting.</h3> 
				<p>What counteracting forces are you facing in your scenario to prevent you reaching your goal? 
				What can you do so that the counteraction no longer exists or the counteraction is no longer an issue?</p>
				<h3 style="text-decoration: underline;">Resource Availability.</h3>
				<p>What if money, time, people, supplies are not issues at all? What if you could ask for whatever you wanted and have it happen? How would that change your game plan?</p>
				<h3 style="text-decoration: underline;">Drivers Analysis.</h3> 
				<p>What are the forces that help drive you forward in your situation? What forces are acting against you? Think about how you can magnify the former and reduce/eliminate the latter.</p>
				<h3 style="text-decoration: underline;">Integration</h3>
				<p>How can this idea be combined with other ideas to create a more complete or comprehensive understanding of a potential solution? </p>
				<h3 style="text-decoration: underline;">Classification</h3>
				<p>How can these different ideas be grouped together into a more general category? How can these separate solutions be reorganized or rearranged to produce a more comprehensive understanding of the “big picture?”</p>
				<h3 style="text-decoration: underline;">Induction</h3>
				<p>What are the broader implications of the ideas? What patterns or themes are emerging? What can be extrapolated or extended from these concepts that may have more general or universal value?</p>
			</div>
		</div>
	</div>
</div>