<?php
/**
 *
 * @var Field reference $content*/

use yii\helpers\Html;

$this->title = 'Customer terms';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="terms-page containerBlock">
	<?=$content?>

	<!-- <div class="container">
		<h1 class="page-title">Terms and Conditions</h1>
		
		<div class="white">
			<h4 class="subtitle blue">Welcome to BrainCloud.Solutions</h4>
			<hr class="hr blue">
			<p>
				Thank you for using the products and services (“Services”) provided by this website. 
				This website is operated by BrainCloud Solutions (“BrainCloud”).
			</p>
			<p>
				All information, tools, and services offered by this site to its user, you, are based upon 
				your acceptance of all terms, conditions, policies, and notices (“Terms of Service”, “Terms”) 
				stated here. These terms and conditions govern your use of this website. By visiting this 
				website and/or our Services, or purchasing our Services, you are agreeing to these Terms, 
				including any additional terms and conditions, and policies referenced herein and/or 
				available by hyperlink. Please read them carefully. Moreover, we strive to provide our 
				users with better services, therefore, new functions might be installed, upgraded, or 
				changed, and in which case, additional terms or service requirements (including age requirements) may apply. 
				These additional Terms will be available with the relevant Services and become part of your 
				agreement with us if you choose to use these Services. You can review the most current version 
				of the Terms at any time from the link “Terms” in the footer of this website. If you disagree 
				with these Terms or any part of these Terms, please do not continue using our website and/or our Services.
			</p>
			<p>
				Throughout the site, the terms “we,” “us,” and “our” refer to BrainCloud Solutions, whilst 
				“you” and “your” refer to the user. The terms “user” and “users” refer to all users, including 
				without limitation users who are employees, browsers, vendors, customers, merchants, and/ or 
				contributors of content. The terms “customer” and “customers” refer to the individual(s) or organization(s) 
				who register as a “customer” at our website. The terms “stormer” and “stormers” refer to the individual(s) 
				who register as a “stormer” at our website.
				<br />
				Some of the Terms apply to all users, whereas some apply to certain type of users, which are be specified in the following contents.
			</p>
		</div>
		<div class="grey">
			<h4 class="subtitle">Using Our Services</h4>
			<hr class="hr">
			<p>
				Users must be over the legal age of 18 to use this website. By using this website, and agreeing to 
				these Terms, you warrant and represent that you are at least 18 years old.
			</p>
			<p>
				Do not misuse our Services. You may not interfere with our Services, try to access them 
				using a method other than the instructions and/or interfaces we provided. You may 
				not use our Services for any unlawful, illegal, fraudulent, harmful, or unauthorized 
				purpose; or in connection with any unlawful, illegal, fraudulent, harmful, or unauthorized 
				purposes and/or activities. Nor may you, in the use of the Service, violate any laws in 
				your jurisdiction (including but not limited to copyright laws). You must not use this 
				website to copy, store, host, transmit, send, use, publish, or distribute any spyware, 
				computer viruses, worms, Trojan horse, keystroke logger, rootkit, or any code/software 
				of a malicious and/or destructive nature. Any use of our system for purposes of spam 
				will be removed, and the user’s accounts blocked. A breach or violation of any of the 
				Terms will result in an immediate suspension or termination of the Services we provide, 
				as well as the user’s account blocked.
			</p>
			<p>
				Using our website does not give you ownership of any intellectual property rights in 
				our Services or the content you access, unless you are a project creator. You agree 
				not to reproduce, duplicate, copy, sell, resell, or exploit any portion of the 
				Services or content provided by our website without express written permission by us, 
				unless it is content within a project that you have created. Without permission from a 
				project owner or by law, you may not republish material from this website 
				(including republication on another website); sell, rent, or sub-license material 
				from the website, show any material from the website in public; reproduce, duplicate, 
				copy or otherwise exploit material on this website for a commercial purpose; edit or 
				otherwise modify any material on the website; or redistribute material from this website 
				(except for content specifically and expressly made available for redistribution). 
				These terms do not grant you the right to use any branding or logos appeared when 
				using our Services. Do not remove, obscure, or alter any legal notices displayed in 
				or along with our Services.
			</p>
			<p>
				Our website may present some content that does not belong to us, in which case, the 
				content is the sole responsibility of the entity that makes it available. We may review 
				content, and may remove or refuse to display anything that we reasonably believe violates 
				our policies or the laws. However, that does not necessarily mean that we review all content, 
				so please do not assume that we do. Some of our Services are available on mobile devices. 
				Do not use such Services in a way that distracts you and prevents you from obeying traffic or safety laws.
			</p>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="grey left">
					<h4 class="subtitle">Your Account</h4>
					<hr class="hr">
					<p>
						Some services and/or functions are only accessible with a registered account. You may register 
						as a “customer” or a “stormer” based on your purpose of using our Services, or an account 
						may be assigned to you by an administrator. In any case, you need a user ID and password. 
						It is users’ responsibility to ensure the confidentiality and safety of his/her own user 
						ID and password. Users are responsible for the activities that happen on or through their 
						respective accounts.
					</p>
					<p>
						Our <a href="/privacy">privacy policy</a> explains how we handle your personal data and protect 
						your privacy when you use our Services.
					</p>
				</div>
			</div>
			<div class="col-md-6">
				<div class="grey right">

					<h4 class="subtitle">Rights and Liabilities as Customers:</h4>
					<hr class="hr">
					<ul>
						<li>
							During the entire use of our Services, no derogatory discrimination is permitted. 
							Customers must not post any project that could affect the high reputation of our 
							business; no racism, sexism or any other form of derogatory discrimination is allowed. 
							Customers are not permitted to generalize based on race or gender, they must select 
							their team with performance in mind. The stormer(s) should be respected, and not 
							treated in any derogatory manner.
						</li>
						<li>
							Customers should understand that although we will do our utmost to satisfy our customers’
						</li>
						<li class="shadow-block"></li>
					</ul>
					<a class="read-more" href="" data-toggle="modal" data-target="#customer-modal">Read more</a>
					<div class="modal fade" id="customer-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title" id="myModalLabel">Rights and Liabilities as Customers:</h4>
									<hr>
								</div>
								<div class="modal-body">
									<ul>
										<li>During the entire use of our Services, no derogatory discrimination is permitted. 
											Customers must not post any project that could affect the high reputation of our 
											business; no racism, sexism or any other form of derogatory discrimination is allowed. 
											Customers are not permitted to generalize based on race or gender, they must select 
											their team with performance in mind. The stormer(s) should be respected, and not 
											treated in any derogatory manner.
										</li>
										<li>Customers should understand that although we will do our utmost to satisfy our customers’ 
											needs, we could not guarantee that a solution to a problem will be found. It is the customers’ 
											responsibility to provide as detailed instruction as possible to guide stormers to generate 
											desired solutions. Furthermore, the system is designed to maximize the quality of ideas generated. 
											If the system is not respected and/or followed, we cannot be responsible for the quality of the 
											ideas or the ideation process.
										</li>
										<li>Some data may be monitored to ensure we can adapt our platform to meet customer needs, 
											but they will never be given or sold to a third party. Stormers are held legally responsible 
											if they breach the confidentiality agreements, or the terms and conditions that protect your data. 
											Although we take every possible precaution, we cannot be held responsible for breach of 
											protocol with intellectual property.
										</li>
										<li>We request our stormers check and confirm that they have not repeated any divergent ideas 
											at the whole picture phase. If in the event they have unknowingly done so, it is the customer’s 
											responsibility to moderate the ideas, and flag any repeated ideas generated. 
											A modification request will then be sent by admin accordingly. Moreover, as the 
											convergent phase of the ideation process is hidden to the stormers, they can only 
											see their own ideas. Although the occurrence is rare, there might still be chances 
											when ideas are repeated during this phase. If there were, please flag the ideas as early as possible, 
											and a modification request will be sent by admin. Admin is unable to send a modification request if 
											the ideas have not first been flagged by the customer. It is the customer’s responsibility to moderate the project.
										</li>
										<li>There should be a 48-hour modification request buffer. Which could mean if the modifications 
											requested within 48 hours of, or after the original set deadline, the deadline may need to be extended 
											by 48 hours before being completed.
										</li>
										<li>The ideas generated for the customer is the customer’s property, therefore, in order to protect our 
											customers, unless instructed otherwise, it will be removed from our website after a certain 
											period of time (the specific date of which will be indicated when the project is completed). 
											It is the customers’ responsibility to download/print the generated solutions from our 
											website within the specified expiring date, and preserve the information at their own places.
										</li>
										<li>Customers are welcomed to contact our stormer(s) they have worked with, to discuss 
											further opportunities. However, when contacting our stormer(s), customers are not allowed 
											to attempt to remove him/her/them from our platform, or prevent him/her/them from continuing 
											as a stormer. At the same time, we are not held responsible for any agreement made between 
											yourself and the stormer outside the range of our system.
										</li>
										<li>Other than as expressly set out in the Terms, we do not make any specific promises about the 
											Services and to the extent permitted by law, we exclude all warranties. When permitted 
											by law, we will not be responsible for lost profits, revenues, or data, financial losses 
											or indirect, special, consequential, exemplary, or punitive damages. To the extent permitted 
											by law, our total liability, for any claims under these terms, including for any implied warranties, 
											is limited to the amount you paid us to use the Services.
										</li>
									</ul>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
		<div class="white">
			<h4 class="subtitle blue">Warranties and Disclaimers</h4>
			<hr class="hr blue">
			<p>
				In order to satisfy the needs of our customers, changes and improvements may be applied to our Services, 
				functionalities or features maybe added, altered or removed, and in rare occasions, the 
				we might suspend or stop a Service altogether. In such cases, a reasonable advance notice 
				will be provided to you, to allow you to retrieve any data that you own out of the Service 
				if necessary. We will not be liable for any loss or damage that is not reasonably foreseeable.
			</p>
			<p>
				We do not guarantee, represent or warrant that your use of our services will be uninterrupted, timely, 
				secure or error-free. We do not warrant that the results that may be obtained from the use of the service 
				will be accurate or reliable.
			</p>
			<p>
				You expressly agree that your use of, or inability to use, the service is at your sole risk. The service 
				and all products and services delivered to you through the service are provided “as is” and “as available” 
				for your use, without any representation, warranties or conditions of any kind, either express or implied, 
				including all implied warranties or conditions of merchantability, merchantable quality, fitness for a 
				particular purpose, durability, title, and non-infringement.
			</p>
			<p>
				If you are using the Services for a personal purpose, then nothing in these terms or any additional 
				terms limits any consumer legal rights, which may not be waived by contract.
			</p>
			<p>
				If you are using our Services on behalf of a business, that business accepts these terms. 
				It will hold harmless and indemnify us and our affiliates, officers, agents, and employees 
				from any claim, suit or action arising from, or related to the use of the Services or violation 
				of these terms, including any liability or expense arising from claims, losses, damages, suits, 
				judgments, litigation costs and attorneys’ fees.
			</p>
			<p>
				In no case shall us or our employees, be liable for any injury, loss, claim, or any direct, indirect, incidental, 
				punitive, special, or consequential damages of any kind, including, without limitation lost profits, lost revenue, 
				lost savings, loss of data, replacement costs, or any similar damages, arising from your use of our Services, 
				or for any other claim related in any way to your use of the Services, including, but not limited to, any errors 
				or omissions in any content, or any loss or damage of any kind incurred as a result of the use of the service or 
				any content (or product) posted, transmitted, or otherwise made available via the Services, even if advised of 
				their possibility. Due to that some countries or jurisdictions do not allow the exclusion or the limitation of 
				liability for consequential or incidental damages, in such states or jurisdictions, our liability shall be 
				limited to the maximum extent permitted by law.
			</p>
		</div>
		<div class="grey">
			<h4 class="subtitle">Modifications and Changes</h4>
			<hr class="hr">
			<p>
				We may modify the Terms or any additional terms that apply to a Service to reflect changes to the law 
				or changes to our Services. It is your responsibility to check the terms regularly. We will post notice 
				of modifications to these terms on this page and any additional terms in the applicable Service. Normally, 
				changes will not apply retroactively and will become effective fourteen days after they are posted. 
				Nevertheless, changes addressing new functions for a Service or changes made for legal reasons will 
				be effective immediately. If you do not agree to the modified terms for a Service, you should 
				discontinue your use of the Service.
			</p>
			<p>
				In case violation of the Terms by user(s) occurs and no immediate action is taken by us, this 
				doesn’t mean that we are giving up any rights that we may have. In case that a particular 
				term is not enforceable, it will not affect the execution of any other terms.
			</p>
			<p>
				In some countries, the court will not apply European law to some types of disputes. 
				If you reside in one of those countries, then where European law is excluded from applying, 
				your country’s laws will apply to such disputes related to these terms. Otherwise, 
				you agree that the laws of European Union will apply to any disputes arising out of 
				or relating to these terms or the Services. 
				Should you have any questions regarding the Terms, place <a href="/contact">contact us</a>.
			</p>
		</div>
	</div> -->
</div>