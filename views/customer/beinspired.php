<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\Alert;
use app\assets\CustomerprojectAsset;
CustomerprojectAsset::register($this);

$this->title = 'My Projects: current projects';
?>
<div class="be-inspired-page">
	<div class="container containerBlock">
		<h1 class="page-title">Be inspired</h1>
		<div class="projects-tab-wrapper">
			<div class="row">
				<div class="col-sm-12">
					<ul class="nav nav-tabs">
						<li role="presentation" <?=($type == 'Peer')?'class="active"':''?>>
							<?= HTML::a('Peer Projects',Url::home().'customer/beinspired/Peer'); ?>
						</li>
						<li role="presentation" <?=($type == 'Open')?'class="active"':''?>>
							<?= HTML::a('Open Projects',Url::home().'customer/beinspired/Open'); ?>
						</li>
						<li role="presentation" <?=($type == 'NGO')?'class="active"':''?>>
							<?= HTML::a('NGO Projects',Url::home().'customer/beinspired/NGO'); ?>
						</li>
					</ul>
					<hr class="nav-line">
				</div>
				<div class="col-sm-12">
				<!--            <h2 style="text-align: center">Current Projects</h2>-->
					<div class="row">
						<?php //= ; ?>
						<?php foreach($modelProjectsOpen as $projects){?>
							<div class="col-sm-12">
								<h3 class="project-name"><?= $projects->title; ?>
									<span class="progress-count pull-right">
										<?php
										if($projects->count_idea != 0 ){
											$one_procent=100/($projects->count_idea*$projects->size_team);
											if( $one_procent*\app\models\Ideas::find()->where(['project_id'=>$projects->id])->count() <= 100){
												echo Yii::$app->formatter->asPercent(($one_procent*\app\models\Ideas::find()->where(['project_id'=>$projects->id])->count())/100, 1);;
											}else{
												echo '100%';
											};
										}else{
											echo '0%';
										}
		
										?>
									</span>
								</h3>
								<hr class="progress-hr">
								<!--hidden project ideas data-->
								<input type="hidden" name="ideaUserCount<?= $projects->id; ?>" value="<?= $projects->countIdeaUser; ?>">
								<input type="hidden" name="ideaCount<?= $projects->id; ?>" value="<?= $projects->count_idea; ?>">
								<input type="hidden" name="sizeTeam<?= $projects->id; ?>" value="<?= $projects->size_team; ?>">
								<input type="hidden" name="endDate<?= $projects->id; ?>" value="<?= $projects->end_date; ?>">
								<!--hidden project ideas data-->
								<div class="project-container">
									<div class="col-sm-12">
										<div class="row">
											<div class="col-sm-2">
												<h4 class="status">Status:</h4>
											</div>
											<div class="col-sm-10">
												<span class="status-project"><?php if($projects->active != 0){ echo 'Active'; }else{ echo 'Not active'; } ?></span>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-2">
												<h4 class="team">Your Team:</h4>
											</div>
											<div class="col-sm-6">
												<?php foreach($projects->team as $userModel){
													echo '<span class="team-members">'.$userModel['username'].'</span>';
												}; ?>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="row">
											<div class="col-md-6 left-buttons">
												<?= HTML::a('Behind the Scenes','/customer/behindthescenes/'.$projects->id.'?beinspired=true',['class'=>'btn btn-behind']); ?>
											</div>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						<?php } ?>
						<div class="col-sm-12 text-center">
							<?= LinkPager::widget(['pagination'=>$pagination]); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
