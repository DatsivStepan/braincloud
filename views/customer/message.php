<?php
/**
 *
 */?>
<div class="customer-message-page">
	<div class="container containerBlock">
		<h1 class="page-title"><?=$project->title?></h1>
		<h4 class="page-subtitle"><?=$project->description?></h4>
		<div class="panel">
			<div class="panel-heading">
				<h3 class="panel-title">Message</h3>
			</div>
			<div class="panel-body">
				<?php foreach($project_stormer as $item){?>
					<div class="alert alert-info" role="alert">
						<?=$item->user['username']?>
						<a href="/customer/message/<?=$project->id?>/<?=$item->stormer_id?>" class="btn btn-message" type="button">
							Messages <span class="badge">new <?= \app\models\Message::find()->where(['stormer_id'=>$item->stormer_id,'project_id'=>$project->id,'status'=>0,'type_user'=>'stormer'])->count()?></span>
						</a>
						<div class="clearfix"></div>
					</div>
				<?php }?>
			</div>
		</div>
	</div>
</div>
