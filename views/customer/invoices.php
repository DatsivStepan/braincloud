<?php
use kartik\grid\GridView;
use kartik\export\ExportMenu;
/**
 *
 */?>
<div class="invoices-page">
	<div class="container containerBlock">
		<h1 class="page-title">Here you can find and print any previous purchase records.</h1>
		<hr>
		<h4 class="page-subtitle">If there is any extra information you need for your records, please get in contact, and we will do our best to satisfy your needs.</h4>

		<div class="button-wrap">
			<?php
				$gridColumns = [
					['class' => 'kartik\grid\SerialColumn'],
					'othe',
					'project.size_team',
					'project.count_idea',
					'project.discount',
	//                'price',
					[
						'label'=>'Price ($)',
						'attribute'=>'price',

					],
					'updated_at:date',
				];


				echo ExportMenu::widget([
					'dataProvider' => $dataProvider,
					'showColumnSelector'=>true,
					'columns' => $gridColumns,
					'columnSelectorOptions'=>[
						'label' => 'Colums',
						'class' => 'btn btn-cols',
					],
					'fontAwesome' => true,
					'hiddenColumns'=>[0],
					'dropdownOptions' => [
						'label' => 'Export now',
						'class' => 'btn btn-export',
					],
					'filename' => 'invoices',
					'asDropdown'=>true,
					'target' => ExportMenu::TARGET_BLANK,
					'exportConfig' =>[
						ExportMenu::FORMAT_TEXT => false,
						ExportMenu::FORMAT_PDF => [
							'label' =>  'PDF',
							'icon' => 'file-pdf-o',
							'iconOptions' => ['class' => 'text-danger'],
							'linkOptions' => [],
							'options' => ['title' => 'Portable Document Format'],
							'alertMsg' => 'The PDF export file will be generated for download.',
							'mime' => 'application/pdf',
							'extension' => 'pdf',
							'writer' => 'PDF'
						],
						ExportMenu::FORMAT_HTML =>false,
						ExportMenu::FORMAT_CSV => false,
						ExportMenu::FORMAT_EXCEL => false,
						ExportMenu::FORMAT_EXCEL_X => [
							'label' =>  'Excel',
							'icon' =>  'file-excel-o' ,
							'iconOptions' => ['class' => 'text-success'],
							'linkOptions' => [],
							'options' => ['title' => 'Microsoft Excel 2007+ (xlsx)'],
							'alertMsg' =>  'The EXCEL 2007+ (xlsx) export file will be generated for download.',
							'mime' => 'application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
							'extension' => 'xlsx',
							'writer' => 'Excel2007'
						],
					]
				]);?>
				<a href="/contact" class="btn btn-contact">Contact Us</a>
		</div>
		<div class="table-wrap">
			<?php

				array_push($gridColumns,[
					'class' => '\kartik\grid\ActionColumn',
					'template' => '{update}',
					'buttons' => [
						'update' => function ($url,$model) {
							return \yii\helpers\Html::a(
								'<span class="glyphicon glyphicon-download-alt"></span>',
								'/api/invoice?id='.$model->id);
						},
					],
				]);
				echo GridView::widget([

					'dataProvider' => $dataProvider,
	//                    'filterModel' => $searchModel,
					'columns' => $gridColumns,
				]);
			?>
		</div>
	</div>
</div>