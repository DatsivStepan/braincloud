<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\assets\OrganizeIdeaAsset;
OrganizeIdeaAsset::register($this);

//$this->title = 'Stormer';
//$this->params['breadcrumbs'][] = $this->title;
?>
<style>
	#jsm_container{
		width:100%;
		height:800px;
	}
	.hiddenText {
		display:none
	}
</style>
<script>var project_id = <?=$project_id?></script>
<script>var mindM = <?=$mindM?></script>
<script>var count_idea = <?=$count_idea?></script>
<script>var count_idea_user = <?=$count_idea_user?></script>

<div class="behind-the-scenes">
	<div class="container">
		<h1 class="page-title">Implement ideas</h1>
		<hr>
	</div>
	<div class="content">
		<div class="container">
			<div class="benchmark-title"><?=$project->evaluation_benchmark?></div>
		</div>
		<div id="jsm_container">

		</div>
	</div>
</div>
<!-- <div class="modal " id="myModal" name="modal"  role="dialog">
	 <div class="modal-dialog" style="margin-top: 100px">
		 <div class="modal-content">
			 <div class="col-sm-12" style="margin-top: 20px">
			 Welcome! In order to get the finest experience, please follow the instructions as best you can. If you get stuck, click the ‘Tips and Help’ button. (You can leave and save your work anytime, and come back to it later, although there is a time bonus for completing it in the first 48 hours!) if customer projects.
Your quota is x ideas. Please don’t forget that one idea includes all three stages of the ideation. Your ideas will not be counted if all three stages are not completed.
Good luck!
</div>
			 <div class="modal-footer">
				 <button type="button" class="btn btn-primary" data-dismiss="modal">Start Now!</button>
			 </div>
		 </div>
	 </div>
 </div> -->
<div class="modal fade" id="organizeIdea" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal formOrganize_idea']]); ?>

			   <input type="hidden" name="Organizeidea[idea_id]" id="idea_id" value="">
				<label style="text-align: left" class="control-label">How close is this to your original assessment benchmark</label>
				<input class="question_1" type="number" />
				<?= $form->field($newOrganizeIdea, 'question_1')->hiddenInput()->label(false); ?>

				<label style="text-align: left" class="control-label">Affordability for using the idea</label>
				<input class="question_2" type="number" />
				<?= $form->field($newOrganizeIdea, 'question_2')->hiddenInput()->label(false); ?>

				<label style="text-align: left" class="control-label">Can it be adapted or made cheaper?</label>
				<input class="question_3" type="number" />
				<?= $form->field($newOrganizeIdea, 'question_3')->hiddenInput()->label(false); ?>

				<label style="text-align: left" class="control-label">Skill of current employees for using the idea</label>
				<input class="question_4" type="number" />
				<?= $form->field($newOrganizeIdea, 'question_4')->hiddenInput()->label(false); ?>

				<label style="text-align: left" class="control-label">Can it be adapted or made simpler?</label>
				<input class="question_5" type="number" />
				<?= $form->field($newOrganizeIdea, 'question_5')->hiddenInput()->label(false); ?>

				<?= Html::submitButton('Organize', ['class' => 'btn btn-primary buttonCloseProjectRanting', 'name' => 'buttonCloseProjectRanting']); ?>
				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>
