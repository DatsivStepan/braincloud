<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use dosamigos\tinymce\TinyMce;
use dosamigos\multiselect\MultiSelect;
use yii\bootstrap\Alert;
use app\assets\SelectstormerAsset;
SelectstormerAsset::register($this);

$this->title = 'Develop Your Ideas Further';
$this->params['breadcrumbs'][] = 'Develop Your Ideas Further';

?>
<script>var discount = <?=$discount?>;</script>
<style>
    .multiselectClass{

    }
    .textStormer2{
        text-indent: 3em;
        text-align: justify;
        margin-bottom:0px;
    }
    .textStormer3{
        text-align: justify;
        text-indent: 3em;
    }
    .textStormer4{
        text-indent: 3em;
        text-align: justify;
    }
</style>

<div class="container containerBlock">

    <div class="panel panel-default">
        <div class="panel-heading" style="padding:5px !important;">

            <h4 style="display:inline-block;margin-right: 10px;margin-top:5px;margin-bottom:5px;">Develop Your Ideas Further</h4><p>To develop your ideas, simply select how many more ideas you want developing from the idea category and the size of your team, the project is then immediately live, and should be ready in five days:</p></div>
        <div class="panel-body" <?php if($displayArrow){ echo 'style="display:none;"'; } ?>>
            <span id="slider_scope_projectV" style="display:none;"><?php echo '5' ?></span>
            <span id="slider_team_sizeV" style="display:none;"><?php echo '5'; ?></span>
            <div class="col-sm-12" style="text-align: center">
                Select the size of your team
            </div>
            <div class="col-sm-12" style="padding-top: 20px;padding-bottom:20px;">
                <div id="slider_team_size_selectStormerpage"></div>
            </div>

            <div class="col-sm-12" style="text-align: center">
                Select the scope of your project
                <a class="infoButtonShow" style="cursor:pointer;"><b>?</b></a>
                <div class="infoBlockShow">
                    <h5>Ideas VS Solutions</h5>
                    <h5>Our system employs both the divergent creative
                        stage of the ideation process, as well as the practical convergent stage.
                        Therefore with a little help, every idea is a potential solution.</h5>
                </div>
            </div>
            <div class="col-sm-12" style="padding-top: 20px;padding-bottom:20px;">
                <div id="slider_scope_project_selectStormerpage"></div>
            </div>
            <div class="col-sm-12"  style="text-align: center">
                <h4>Price($)</h4>
                <h4 style="color:#27a9e2;" class="sliderPrice">0</h4>
                <div style=" <?= $discount?'':'display:none'?>">
                    <h4>Discount(<?=$discount?>%)</h4>
                    <h4>Total($)</h4>
                    <h4 style="color:#27a9e2;" class="totalPrice"></h4>
                </div>
            </div>
            <div class="col-sm-12" style="margin-bottom:20px;margin-top:30px;text-align:center;">
                <?php $form = ActiveForm::begin(['id' => 'form-signup',
                    'options' => ['class' => 'form-horizontal'],
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-7\">{error}</div>",
                        'labelOptions' => ['class' => 'col-lg-2 control-label'],
                    ],
                ]); ?>
                <input type="hidden" name="slider_team_size">
                <input type="hidden" name="slider_scope_project">
                <?= Html::submitButton('Take The Next Step ', ['class' => 'btn btn-success', 'name' => 'confirmSliderSelect','style' => 'padding-left:30px;padding-right:30px']); ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <?php if(!$valid){?>
        <div class="panel-body">
            This project has already created.
        </div>
        <?php }?>
    </div>

    <!--    <div class="row customerInfoTextBlock" style="text-align:center;">
        </div>-->
</div>
