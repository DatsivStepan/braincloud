<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


$this->title = 'Develop Your Ideas Further';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="developfurther-page">
	<div class="container containerBlock">
		<h1 class="page-title">Develop Your Ideas Further</h1>
		<h4 class="page-subtitle">
			To develop your ideas, simply select how many more ideas you want developing from the idea category and the size of 
			your team, the project is then immediately live, and should be ready in five days
		</h4>
		<?= HTML::a('Take The Next Step','/customer',['class'=>'btn btn-next']); ?>
	</div>
</div>
