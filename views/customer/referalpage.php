<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\Alert;
use app\assets\ReferalAsset;
ReferalAsset::register($this);

?>

<div class="referal-page">
	<div class="title-wrap">
		<div class="container-fluid">
			<h1 class="page-title">Recommend a Customer </h1>
			<hr>
			<h4 class="details">Do you think our products might be suitable for someone you know? Send them a message or give them your reference number to type in when they make a purchase. Earn points for both yourself and them for every referral that becomes a customer.</h4>
			<br>
			<h4 class="details"> Your referal key: <?=$user->referal_key?> </h4>
		</div>
	</div>
	<div class="container containerBlock">
		<div class="referal-form-wrap">
			<?php $form = ActiveForm::begin(['id' => 'cusromer_referal',
					'options' => ['class' => 'form-horizontal'],
					'fieldConfig' => [
							'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-7\">{error}</div>",
							'labelOptions' => ['class' => 'col-lg-2 control-label'],
					],
				]); ?>
				<div class="col-md-4 top-input">
					<?= Html::input('text','recipient_name','',['class'=>'form-control','placeholder'=>'Recipient Name']); ?>
				</div>
				<div class="col-md-4 center-input">
					<?= Html::input('email','recipient_email','',['class'=>'form-control','placeholder'=>'Recipient Email']); ?>
				</div>
				<div class="col-md-4 bottom-input">
					<?= Html::input('text','senders_email','',['class'=>'form-control','placeholder'=>'Sender’s Email']); ?>
				</div>
				<div class="clearfix"></div>
				<p id="messageTamplate">
					Dear <span class="RecipientName">Recipient name</span>,<br>
					I currently use the innovation platform here at BrainCloud, and thought it might be useful for you as well. <br>
					The platform offers personalised innovation teams, affordable business solutions and creative ideation processes. Everything is done online within a relatively quick timeframe. 
					Click here to find out more: <a href="">braincloud.solutions/services</a>
					if you feel this is something that interests you, use the link to register, to earn extra 25 loyalty points<br>
					Kind regards,<br>
					<?= \Yii::$app->user->identity->username;?><br>
				</p>
				<input type="submit" name="referalButton" class="btn btn-send">
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>