<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\assets\PaymentAsset;

PaymentAsset::register($this);
$this->title = 'Profile page';
// $this->params['breadcrumbs'][] = ['label' => 'Profile Home', 'url' => [$home_profile_src]];
$this->params['breadcrumbs'][] = $this->title = 'Payment';
?>
<div class="container containerBlock">
    <div class="col-sm-6 col-sm-offset-3">
        <h1>Payment Page </h1>
        <div class="col-sm-12" style="text-align: center">
            <?php
                switch ($mProject->active) {
                  case '0':?>
                  <form method="post" id="paymentf" action="https://www.sandbox.paypal.com/cgi-bin/webscr">
                      <input type="hidden" name="cmd" value="_xclick">
                      <input type="hidden" name="business" value="<?= \Yii::$app->params['paypalEmail'] ?>">
                      <input type="hidden" name="item_name" value="Project <?= $mProject->title ?>">
                      <input type="hidden" name="item_number" value="<?= $mProject->id ?>">
                      <input type="hidden" name="amount" value="<?= $price ?>">
                      <input type="hidden" name="no_shipping" value="1">
                      <input type="hidden" name="notify_url" value="<?= $aUrls['notify_url'] ?>">
                      <input type="hidden" name="cancel_return" value="<?= $aUrls['cancel_url'] ?>">
                      <input type="hidden" name="return" value="<?= $aUrls['return_url'] ?>">
                      <h3 style="text-align: center">Price : <?= $price ?></h3>
                      <input type="submit" value="Pay now(Paypal)" class="btn btn-primary" style="margin">
                  </form>
                    <?php break;
                  case '1':?>
                  <h2>Successfuly completed purchase</h2>
                  <?= Html::a("Go home", '/', ['class' => "btn btn-primary"] ) ?>
                    <?php break;
                  case '2':?>
                  <h2>Waiting for purchase confirmation from paypal</h2>
                  <?= Html::a("Refresh", '#', ['class' => "btn btn-primary"] ) ?>
                    <?php break;
                } 
            ?>
        </div>
    </div>
</div>
<script type="text/javascript">
  var url = "<?= $aUrls['sended'] ?>"
</script>
