<?php
/**
 *
 */
?>

<h1><?=$message_send?></h1>


<?php
if($message)
    $this->registerJs(
        "//$('document').ready(function(){
            swal({
                title: 'Got it!',
                 text: 'Thank you! The project is now launched and awaiting response from the stormers. In the event that any are unavailable or do not respond within 48 hours, stormer’s of similar level or category will be chosen automatically. You will be notified if such a situation occurs.  You can check your project status in the ‘My Projects’ page at any time. Any notifications will arrive on your ‘news’ page. You can select your email notification settings on your profile page, if you wish to receive emails.',
                 type: 'success',
                  showCancelButton: false,
                  confirmButtonColor: '#3085d6',
                 confirmButtonText: 'Take Me Home',
                  closeOnConfirm: false
                },
                function(){
                  location.href = '/'
                });
           "
    );?>