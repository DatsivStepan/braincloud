<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use dosamigos\tinymce\TinyMce;
use dosamigos\multiselect\MultiSelect;
use yii\bootstrap\Alert;
use app\assets\SelectstormerAsset;
SelectstormerAsset::register($this);

	$this->title = 'Select your team';
	$this->params['breadcrumbs'][] = 'Select Your Stormers';

?>
<script>var discount = <?=$discount?>;</script>
<script>var selecte_stormers = <?=$modelProject->size_team?>;</script>
<style>
	.multiselectClass{
		
	}
	.textStormer2{
		/*text-indent: 3em;*/
		text-align: justify;
		margin-bottom:0px;
	}
	.textStormer3{
		text-align: justify;
		/*text-indent: 3em;*/
	}
	.textStormer4{
		/*text-indent: 3em;*/
		text-align: justify;
	}
</style>
<?php  
	$displayArrow = false;
	if(($modelProject->size_team != null) && ($modelProject->count_idea != null)){
		$displayArrow = true;
	} ?>

<div class="selectstormer-page">
	<div class="container containerBlock">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 <?php if($displayArrow){ echo 'class="showScole"'; }else{ echo 'class="showScole"'; } ?>>Second thoughts about the size and scope of your project? Click here to change it now.</h4>
				<hr>
			</div>
			<div class="panel-body" <?php if($displayArrow){ echo 'style="display:none;"'; } ?>>
				<span id="slider_scope_projectV" style="display:none;"><?php if($modelProject->count_idea == null){ echo '5'; }else{ echo $modelProject->count_idea; } ?></span>
				<span id="slider_team_sizeV" style="display:none;"><?php if($modelProject->size_team == null){ echo '5'; }else{ echo $modelProject->size_team; } ?></span>
				<div class="col-md-offset-1 col-md-10 text-left">
					<span class="range-title">
						Select the size of your team
					</span>
					<div class="range-wrap">
						<div id="slider_team_size_selectStormerpage"></div>
					</div>
					<span class="range-title">
						Select the scope of your project
						<a class="infoButtonShow">i</a>
						<div class="infoBlockShow">
							<h5>Ideas VS Solutions</h5>
							<p>Our system employs both the divergent creative 
								stage of the ideation process, as well as the practical convergent stage. 
								Therefore with a little help, every idea is a potential solution.</p>
						</div>
					</span>
					<div class="range-wrap">
						<div id="slider_scope_project_selectStormerpage"></div>
					</div>
					<div class="price-wrap">
						<h4 class="price-title">Price($)</h4>
						<h4 class="sliderPrice">0</h4>
						<div style="<?= $discount?'':'display:none'?>">
							<h4 class="discount-title">Discount(<?=$discount?>%)</h4>
							<h4 class="disc-price-title">Total($)</h4>
							<h4 class="totalPrice">0</h4>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="btn-wrap">
					<div class="col-sm-4 text-left">
						<h6 class="titleDesign"  data-toggle="tooltip" title="<h4>Size Vs Scope </h4><p style='font-size:14px;'>Research is quite inconclusive whether a larger team producing fewer ideas will out, equal or under perform a smaller team with more ideas. For best results, we recommend a team large enough to apply a wide range of knowledge, and a scope large enough to provide a wide range of answers. </p>" >Why those numbers?</h6>
						<a class="btn btn-help our_recomendation">Our Recommendation</a>
					</div>
					<div class="col-sm-4 text-center">
						<h6 class="text-left">Click here to contact us</h6>
						<?php echo HTML::a(\Yii::t('app', 'Not Sure?'), '/contact', ['class' => 'btn btn-help']); ?>
					</div>
					<div class="col-sm-4 text-right">
						<h6>Send this quote via email</h6>
						<?php $form = ActiveForm::begin(['id' => 'contact-form', 'action' => '/site/emailquote', 'method' => 'GET']); ?>
							<input type='hidden' name='scope_project'>
							<input type='hidden' name='team_size'>
							<input type='hidden' name='count_price'>
							<?= Html::submitButton('Email Quote', ['class' => 'btn btn-help', 'name' => 'contact-button']) ?>
						<?php ActiveForm::end(); ?>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="col-sm-12" style="margin-bottom:20px;margin-top:30px;text-align:center;">
					<?php $form = ActiveForm::begin(['id' => 'form-signup',
						'options' => ['class' => 'form-horizontal'],
						'fieldConfig' => [
								'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-7\">{error}</div>",
								'labelOptions' => ['class' => 'col-lg-2 control-label'],
						],
					]); ?>
						<input type="hidden" name="slider_team_size">
						<input type="hidden" name="slider_scope_project">
						<?= Html::submitButton('Confirm! ', ['class' => 'btn btn-success', 'name' => 'confirmSliderSelect']); ?>
					<?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
<!--    <div class="row customerInfoTextBlock" style="text-align:center;">
	</div>-->
		<div class="select-team">
			<?php  if(($modelProject->size_team != null) && ($modelProject->count_idea != null)){ ?>
					<input type="hidden" value="<?=$modelProject->id?>" id="select_st_project_id" />
				<div class="row">
					<div class="col-sm-12">
						<h3 class="page-title">Select Your Team</h3>
						<hr>
					</div>
				</div>
				<div class="col-md-offset-1 col-md-10">
					<p class="title-details">Although it’s tempting to want to find a team of industry-based experts. Truly Innovative solutions come when your team’s knowledge range is diversified. The innovation potential is also greater in a team that frequently practices their problem-solving and creativity skills.</p>
					<p class="title-details">For that reason, our system prioritises degree of engagement. The stormers earn points by participating in innovation challenges, and earns stars by having their ideas selected as the most valuable for the organization</p>
				</div>
				<!-- begin -->
				<input type="hidden" name="size_team_c" value="<?= $modelProject->size_team; ?>">
				<!-- end -->
				<div class="row">
					<div class="col-md-8">
						<h4 class="filters-head-left">Filters</h4>
						<form id="filterStormer">
							<hr class="left">
							<div class="col-sm-12">
								<div class="row">
									<div class="col-sm-5 col-md-5 col-lg-4 filters-view-count">
										<select class="form-control" name="changeViewCount" id="changeViewCount">
											<option value="100">100</option>
											<option value="500">500</option>
											<option value="1000">1000</option>
										</select>
										<a class="infoButtonShow">i</a>
										<div class="infoBlockShow">
											<p class="textStormer3">
												Click on a person's name to view their profile. Please also consider how viewing profiles may reduce the objectivity of your decision</p>
										</div>
									</div>
									<div class="col-sm-7 col-md-7 col-lg-8">
										<p class="description">Eight of Ten: Because we truly believe that open-innovation requires a wide range of minds, the system allows for the causation of human genius by randomly selecting two people out of each ten stormers to be part of all projects. The other eight are entirely your choice. </p>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="row select-options">
									<div class="col-lg-3 col-md-6">
										<?= Html::dropDownList('filterStormerCountry', null,$arrayCountry,['class'=>'form-control','prompt' => ' Select Country ','id' => 'filterStormerCountry']); ?>
									</div>
									<div class="col-lg-3 col-md-6">
										<select class="form-control" name="filterStormerCity" id="filterStormerCity">
											<option value="0">First select a country</option>
										</select>
									</div>
									<div class="col-lg-3 col-md-6">
										<input type="text" class="form-control" name="filterStormerUsername" id="filterStormerUsername" placeholder="Identify stormers through keyword">
										<a class="infoButtonShow">i</a>
										<div class="infoBlockShow">
											<p>Type in any keywords related to skills, knowledge and experience that you think 
												is relevant for your project. You can search for anything you like, 
												however tags are set by the stormers and are uncategorized. 
												For categories of skills and knowledge, use the category selection tool below</p>
										</div>
									</div>
									<div class="col-lg-3 col-md-6 filterCategoryBlock">
										<?php
											echo MultiSelect::widget([
												'id'=>"filterStormerCategory",
												"options" => ['style'=>"width:100%;", 'multiple'=>"multiple"], // for the actual multiselect
												'data' => ['Marketing'=> ['Advertising' => 'Advertising','Market research and analysis' => 'Market research and analysis', 'Packaging and design' => 'Packaging and design','Branding' =>  'Branding', 'External communications' => 'External communications', 'Promotions and pricing' => 'Promotions and pricing'],
															'Management'=>[ 'Human resource management' => 'Human resource management', 'Engagement' => 'Engagement', 'Internal communications' => 'Internal communications', 'Project management' => 'Project management', 'Growth and development' => 'Growth and development', 'Organizational design' => 'Organizational design'], // data as array
															'Sales'=>['The sales process' => 'The sales process', 'Sales management' =>  'Sales management', 'Retail' => 'Retail', 'B2B selling' => 'B2B selling', 'Industry analysis' => 'Industry analysis', 'Industry analysis' => 'Industry analysis']], // data as array
				//                                 'value' => [], // if preselected
												'name' => 'multtiSelect', // name for the form
												"clientOptions" =>
													[
														'numberDisplayed' => 0
													],
											]); 
										?>
										<a class="infoButtonShow">i</a>
										<div class="infoBlockShow">
											<p>Select a business category to find top performing stormers with that knowledge range. 
												Don’t be afraid to mix it up a little by selecting stormers from different categories, 
												or choosing some top stormers to be part of a knowledge-based team.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
						</form>
					</div>
					<div class="col-md-4">
						<h4 class="filters-head-right">Randomize my team</h4>
						<div class="right-options">
							<hr class="right">
							<input type="radio" name="random_my_team" value="100"><span>Randomize from top 100</span><br>
							<input type="radio" name="random_my_team" value="500"><span>Randomize from top 500</span><br>
							<input type="radio" name="random_my_team" value="1000"><span>Randomize from top 1000</span><br>
							<input type="radio" name="random_my_team" value="4"><span>Randomize from top performing stormers within geographical region</span>
							<a class="infoButtonShow">i</a>
							<div class="infoBlockShow">
								<p>For innovation that’s truly out of the box, let our system find you a great team 
								from our selection of top stormers. 
								No need to look through profiles or pick through names. </p>
							</div>
						</div>
						<h4 class="you-stormers-head">Your stormers</h4>
						<h5>You have <span id="youHaveStormers"><?=$modelProject->size_team?></span> stormers remaining</h5>
						<div class="" id="boxWithYourStormers">
						</div>
					</div>
				</div>
				<div class="col-sm-12">
						<?php $form = ActiveForm::begin(['id' => 'form-signup',
							'options' => ['class' => 'form-horizontal'],
							'fieldConfig' => [
									'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-7\">{error}</div>",
									'labelOptions' => ['class' => 'col-lg-2 control-label'],
							],
						]); ?>
						<input type="hidden" name="FormSelectStormer">
						<?= Html::submitButton('Launch the project! ', ['class' => 'btn btn-launch', 'name' => 'saveSelectStormer']) ?>
					<?php ActiveForm::end(); ?>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="stormers-rating">
							Top 100 stormers in order of points and stars. Click to select
							<a class="infoButtonShow">i</a>
							<div class="infoBlockShow">
								<p>
									Click on a person's name to view their profile. Please also consider how viewing profiles may reduce the objectivity of your decision. 
								</p>
							</div>
						</div>
						<div class="" id="boxWithAllStormers">
							<?php foreach($modelStormers as $stormer){ ?>
								<div class="stormer<?= $stormer['stormer_id']; ?> stormer-item" id="allStormer<?= $stormer['stormer_id']; ?>">
									<input type="checkbox" name="stormerCheckboxSelect" data-id="<?= $stormer['stormer_id']; ?>" class="form-control stormerCheckbox<?= $stormer['stormer_id']; ?> pull-right">
									<div class="col-sm-2">
										<!-- default-avatar.png -->
										<?php if($stormer['image_src'] == ''){ ?>
											<img class="stormer-avatar img-responsive" src="../../../image/default-avatar.png">
										<?php }else{ ?>
											<img class="stormer-avatar img-responsive" src="<?= $stormer['image_src'];  ?>">
										<?php } ?>
									</div>
									<div class="col-sm-10">
										<div class="row">
											<div class="col-xs-12">
												<h3 class="stormer-name"><?= $stormer['username'];  ?></h3>
											</div>
											<!-- <div class="col-xs-2">
												<div class="row">
												</div>
											</div> -->
											<div class="col-sm-12 stormerUserInfo" style="display:none;">
												<table class="table">
													<tr>
														<td>Country</td>
														<td><?= $stormer['country_name']; ?></td>
													</tr>
													<tr>
														<td>Email</td>
														<td><?= $stormer['email'];  ?></td>
													</tr>
													<tr>
														<td>Category</td>
														<td>
															<?php
																$arrayCategory = json_decode($stormer['category']);
																if($arrayCategory != null){
																	$stringCategory = implode(",", $arrayCategory);
																	echo $stringCategory;
																}
															?>
														</td>
													</tr>
													<tr>
														<td>Tags</td>
														<td><?php
															$arrayTags = json_decode($stormer['tags']);
															$stringTags = implode(",", $arrayTags);
															echo $stringTags;
															?>
														</td>
													</tr>
													<tr>
														<td>Introduction</td>
														<td colspan="3"><?= $stormer['introduction'];  ?></td>
													</tr>
													<tr>
														<td>Star:</td>
														<td colspan="3"><?=$stormer['global_stars']?></td>
													</tr>
												</table>
												<div class="row">
													<div class="col-sm-12 text-right">
														<div class="row">
															<?php if($stormer['facebook_link'] != ''){ ?>
																<a href="<?= $stormer['facebook_link'] ?>" class="fb-ic social_icon_style">
																</a>
															<?php } ?>
															<?php if($stormer['twitter_link'] != ''){ ?>
																<a href="<?= $stormer['twitter_link']; ?>" class="in-ic social_icon_style">
																</a>
															<?php } ?>
															<?php if($stormer['linkedin_link'] != ''){ ?>
																<a href="<?= $stormer['linkedin_link']; ?>" class="twt-ic social_icon_style">
																</a>
															<?php } ?>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-12">
												<a class="btn btn-show-info stormerUsername pull-right">Show more info</a>
											</div>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
			<?php }  ?>
		</div>
	</div>
</div>
