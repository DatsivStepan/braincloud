<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\Alert;
use app\assets\ReferalAsset;
ReferalAsset::register($this);

?>
<div class="referal-page">
	<div class="title-wrap">
		<div class="container-fluid">
			<h1 class="page-title">Generate a Lead</h1>
			<hr>
			<h4 class="details">Have a lead in mind? Send an email. Remember, each customer that joins using your referral code will earn 25 loyalty points, which earns them discounts on future purchases.</h4>
			<br/>
		</div>
	</div>
	<div class="container containerBlock">
		<div class="referal-form-wrap">
			<?php $form = ActiveForm::begin(['id' => 'cusromer_referal',
					'options' => ['class' => 'form-horizontal'],
					'fieldConfig' => [
							'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-7\">{error}</div>",
							'labelOptions' => ['class' => 'col-lg-2 control-label'],
					],
				]); ?>
				<div class="col-md-4 top-input">
					<?= Html::input('text','recipient_name','',['class'=>'form-control','placeholder'=>'Recipient Name']); ?>
				</div>
				<div class="col-md-4 center-input">
					<?= Html::input('email','recipient_email','',['class'=>'form-control','placeholder'=>'Recipient Email']); ?>
				</div>
				<div class="col-md-4 bottom-input">
					<?= Html::input('text','senders_email','',['class'=>'form-control','placeholder'=>'Sender’s Email']); ?>
				</div>
				<div class="clearfix"></div>
				<p id="messageTamplate">
					Dear <span class="RecipientName">Recipient name</span>,<br>
					I am currently a member of BrainCloud Solution’s innovation platform, and thought it might be useful for you. <br>
					The platform offers personalised innovation teams, affordable business solutions and creative ideation processes. Everything is done online within a relatively quick timeframe.<br>
					Click here to find out more: <a href="">braincloud.solutions/services</a>
					If you feel this is something that interests you, type in the following code during purchase to earn extra loyalty points: 25<br>
					Kind regards,<br>
					<?= \Yii::$app->user->identity->username;?><br>
				</p>
				<input type="submit" name="referalstormerButton" class="btn btn-send" value="Send">
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>