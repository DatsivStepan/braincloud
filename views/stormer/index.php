<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;

$this->title = 'Stormer';
$this->params['breadcrumbs'][] = $this->title;
/*if(Yii::$app->session->hasFlash('EMAIL_sended')): */?><!--
<?php
/*echo Alert::widget([
  'options' => [
	'class' => 'alert-info',
  ],
  'body' => \Yii::t('app','Для того щоб активувати аккаунт, увійдіть на вказаний e-mail!'),
]);
endif;
if(Yii::$app->session->hasFlash('Email_error')): */?>
--><?php
/*echo Alert::widget([
  'options' => [
	'class' => 'alert-warning',
  ],
  'body' => \Yii::t('app','Проблеми з вашею електронною адресою, або її не їснує, попробуйте знову, або зверніться до адміністрації сайту!'),
]);
endif;*/



/*$points_message = \app\models\PointsMessage::find()->where(['user_id'=>Yii::$app->user->id, 'show'=>0])->all();
foreach($points_message as $item){
	$this->registerJs(
		"$().toastmessage('showToast', {
			text     : '$item->message',
			sticky   : true,
			position : 'top-right',
			type     : 'success',

		});"
	);
	$item->show = 1;
	$item->save();
}*/



?>

<div class="dashboard-page">
	<div class="container dashboard-section">
		<?php
		if (($active_status == 'active') && ($points <350)){
			$one_show = \app\models\OneShowMessage::find()->where(['user_id'=>Yii::$app->user->id, 'type'=>'no_point'])->one();
			if ($one_show){
				$p = 350 - $points;
				echo Alert::widget([
						'options' => [
								'class' => 'alert-info',
						],
						'body' => '<span class="bold">Only ' . $p . ' points to go before all features are unlocked!</span>',
				]);
			} else {
				$one_show = new \app\models\OneShowMessage();
				$one_show->user_id = Yii::$app->user->id;
				$one_show->type = 'no_point';
				$one_show->save();
				$p = 350 - $points;
				echo Alert::widget([
						'options' => [
								'class' => 'alert-info',
						],
						'body' => '<span class="bold">Welcome to BrainCloud!</span><br /> We know you\'re probably eager to start earning money, although quality is essential as we progress in our working relationship! Since quality is built and crafted through practice, we give you the challenge to participate in peer projects, including establishing your own, to get to know the system. Once you\'ve earned 350 points, we\'ll add you to the customer database!<br /><span class="bold">Only ' . $p . ' points to go before all features are unlocked!</span>',
				]);
			}
		}
		?>
		<h1 class="page-title">Stormer Home Page</h1>
		<?php if($active_status == 'active'){ ?>
<!--		--><?php //if(false){ ?>
			<div class="profile-menu">
				<div class="row">
					<div class="col-md-4 dashboard-left-col">
						<div class="row">
							<div class="col-xs-6 col-md-12 dashboard-block">
								<div class="profile-user">
									<div class="table">
										<div class="table-cell">
											<div class="profile-photo-wrap">
												<?php if($stormerInfoModule->image_src == ''){ ?>
													<img src='/image/default-avatar.png'>
												<?php }else{ ?>
													<img src='/image/users_images/<?= $stormerInfoModule->image_src; ?>'>
												<?php } ?>
											</div>
										</div>
										<div class="table-cell">
											<div class="user-info">
												<?php echo HTML::a(\Yii::t('app', $userModel->username), '/stormer/profile/'.$userModel->id, ['class' => 'profile-link']); ?>
												<div class="other-info">
													<!-- <div class="col-xs-6">
														First line of introduction:
													</div> -->
													<div class="col-xs-12 no-padding introduction">
														<?php echo $stormerInfoModule->introduction;  ?>
													</div>
													<div class="col-xs-6">
														Country:
													</div>
													<div class="col-xs-6">
														<span><?php echo $stormerInfoModule->locationC->country_name;  ?></span>
													</div>
													<div class="col-xs-6">
														City:
													</div>
													<div class="col-xs-6">
														<span><?php echo $stormerInfoModule->locationS->city_name;  ?></span>
													</div>
													<div class="col-xs-6">
														Level:
													</div>
													<div class="col-xs-6">
														<span><?=$userModel->level?></span>
													</div>
													<div class="col-xs-6">
														Stars:
													</div>
													<div class="col-xs-6">
														<span><?=\app\models\Userrating::find()->where(['stormer_id'=>Yii::$app->user->id])->sum('count_star')?></span>
													</div>
													<div class="clearfix"></div>
													<?php //var_dump($stormerInfoModule); ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-6 col-md-12 dashboard-block active-project">
								<div class="animate-block">
									<div class="menu-item-content">
										<div id="active_project">
											<?php
											if ($active_project)
												foreach($active_project as $item){
													echo HTML::a(\Yii::t('app', $item->title), '/stormer/behindthescenes/'.$item->id,['class'=>'']);
												}
											else
												echo HTML::a(\Yii::t('app', '<h5 class="">Post a Project</h5>'), '/stormer/postproject',['class'=>'btn-post']);
											?>
										</div>
										<a id="load_more" <?=$page?'':'style="display:none"'?>><h5 onclick="add_active_project()">Load more</h5></a>
									</div>
									<div class="menu-item-wallaper active-prj">
										<div class="wallaper-content">
											<?php //echo HTML::a(\Yii::t('app', '<h2>Active Projects</h2>'), '/stormer/myprojects',['class'=>'']); ?>
											<h2>Active Projects</h2>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-8 dashboard-right-col">
						<div class="row">
							<div class="col-xs-6 col-sm-4 dashboard-block less join-proj">
								<div class="animate-block">
									<div class="menu-item-content">
										<?php echo HTML::a(\Yii::t('app', '<h4>Change the World</h4>'), '/stormer/projectintroduction'); ?>
									</div>
									<div class="menu-item-wallaper animated join-project">
										<div class="wallaper-content">
											<h2>Join a Project</h2>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-6 col-sm-4 dashboard-block less">
								<div class="animate-block">
									<div class="menu-item-content">
										<?php echo HTML::a(\Yii::t('app', '<h4>Post a Project</h4>'), '/stormer/postproject'); ?>
									</div>
									<div class="menu-item-wallaper post-project">
										<div class="wallaper-content">
											<h2>Innovate</h2>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-6 col-sm-4 dashboard-block less">
								<div class="animate-block">
									<div class="menu-item-content">
										<?php echo HTML::a(\Yii::t('app', '<h4>News & Notifications</h4>'), '/stormer/news/'); ?>
									</div>
									<div class="menu-item-wallaper news-block">
										<div class="wallaper-content">
											<h2>Stay Informed</h2>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-6 col-sm-12 dashboard-block bigger">
								<div class="animate-block">
									<div class="menu-item-content">
										<?php echo HTML::a(\Yii::t('app', '<h4>Review</h4>'), '/stormer/myprojects'); ?>
									</div>
									<div class="menu-item-wallaper my-project">
										<div class="wallaper-content">
											<h2>My Projects</h2>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-6 col-sm-4 dashboard-block less">
								<div class="animate-block">
									<div class="menu-item-content">
										<?php echo HTML::a(\Yii::t('app', '<h4>Help Us Grow </h4>'), '/stormer/joinsalesteam'); ?>
									</div>
									<div class="menu-item-wallaper join-team">
										<div class="wallaper-content">
											<?php if($stormerInfoModule->sales_team){?>
												<h2>My sales performance</h2>
											<?php }else{?>
												<h2>Join Our Sales Team</h2>
											<?php }?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-6 col-sm-4 dashboard-block less">
								<div class="animate-block">
									<div class="menu-item-content">
										<?php echo HTML::a(\Yii::t('app', '<h4>How Do I Compare?</h4>'), '/stormer/rankings'); ?>
									</div>
									<div class="menu-item-wallaper my-ranking">
										<div class="wallaper-content">
											<h2>My Rankings</h2>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-6 col-sm-4 dashboard-block less">
								<div class="animate-block">
									<div class="menu-item-content">
										<?php echo HTML::a(\Yii::t('app', '<h4>Stormer Benchmark Certificates</h4>'), '/stormer/stormer_benchmark'); ?>
									</div>
									<div class="menu-item-wallaper stormer-benchmark">
										<div class="wallaper-content">
											<h2>Stormer Benchmark Certificates</h2>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-6 col-sm-12 dashboard-block bigger">
								<div class="animate-block">
									<div class="menu-item-content">
										<?php echo HTML::a(\Yii::t('app', '<h4>Income and Bank Info</h4>'), '/stormer/earnings'); ?>
									</div>
									<div class="menu-item-wallaper your-earning">
										<div class="wallaper-content">
											<h2>Your Earnings</h2>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-6 col-sm-4 dashboard-block less">
								<div class="animate-block">
									<div class="menu-item-content">
										<?php  echo HTML::a(\Yii::t('app', '<h4>Send a Message</h4>'), '/contact',['class'=>'']); ?>
									</div>
									<div class="menu-item-wallaper contact-us">
										<div class="wallaper-content">
											<h2>Contact Us</h2>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-6 col-sm-4 dashboard-block less">
								<div class="animate-block">
									<div class="menu-item-content">
										<?php echo HTML::a(\Yii::t('app', '<h4>Get Help</h4>'), '/stormer/supportpage'); ?>
									</div>
									<div class="menu-item-wallaper support-page">
										<div class="wallaper-content">
											<h2>Stormer Support</h2>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-6 col-sm-4 dashboard-block less">
								<div class="animate-block">
									<div class="menu-item-content">
										<?php echo HTML::a(\Yii::t('app', '<h4>Spread the Love!</h4>'), '/stormer/telltheworld'); ?>
									</div>
									<div class="menu-item-wallaper tell-world">
										<div class="wallaper-content">
											<h2>Tell the World</h2>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-6 col-sm-6 dashboard-block less">
								<div class="animate-block">
									<div class="menu-item-content">
										<?php echo HTML::a(\Yii::t('app', '<h4>Who We Are</h4>'), '/stormer/about'); ?>
									</div>
									<div class="menu-item-wallaper about-us">
										<div class="wallaper-content">
											<h2>About Us</h2>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 dashboard-block less">
								<div class="animate-block">
									<div class="menu-item-content">
										<?php echo HTML::a(\Yii::t('app', '<h4>Terms and Conditions</h4>'), '/stormer/terms_conditions'); ?>
									</div>
									<div class="menu-item-wallaper terms">
										<div class="wallaper-content">
											<h2>Terms and Conditions</h2>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php } else{ ?>
			<h3 class="no-active-profile">An activation link was sent to your email, please check and confirm.</h3>
			<p class="text-center">
				<button type="button" class="btn btn-activation" data-toggle="modal" data-target="#myModal">
					Click here to resend activation email
				</button>
			</p>

			<!-- Modal -->
			<div class="modal activation-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">Modal title</h4>
							<hr>
						</div>
						<div class="modal-body">
							<div class="col-md-offset-1 col-md-10">
								<form onsubmit="send_activate_code(event)">
									<div class="form-group">
										<label class="control-label" for="exampleInputEmail1">Email address</label>
										<input type="email" class="form-control" id="send_email" placeholder="Email">
									</div>
									<button type="submit" class="btn btn-send">Send activation code</button>
								</form>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-close" data-dismiss="modal">Close</button>

						</div>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>
</div>
<script>
	var page = 0;
	function send_activate_code(e){
		e.preventDefault();
		$.post('/api/send_code',{email: $('#send_email').val()}, function(data){
			if (data){
				$('#myModal').modal('hide');
				swal(
						'',
						'Activation code send!',
						'success'
				)
			}
			else
				swal(
						'',
						'Error!',
						'error'
				)
		});
	}
	function add_active_project(){
//       alert(page);
		page++;
		$.ajax({
			type: 'POST',
			url: '/api/test',
			data: {page:page},
			success: function(response){
				if(response.status){
					console.log(response.data);
					$.each(response.data, function(index, value) {
						console.log(value);
						$('#active_project').append('<a href="/stormer/behindthescenes/'+value.id+'">'+value.title+'</a>');
					});
					$('#list_project').css('height',500+'px');
					// $('#active_project').css('height',400+'px');
					if (!response.page){
						$('#load_more').hide();
						$('#post-project').show();
					}
				}
			}
		});

	}
</script>
