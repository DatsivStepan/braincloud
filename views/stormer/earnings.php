<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

$this->title = 'Your Earnings';

?>

<!--<div class="row containerBlock">
	<h4>
		Please note that completed projects that have not been marked as closed by the customer will not have been paid in to your account. 
		If you feel there is an issue or problem with your payment,<a href="/stormer/">get in contact</a>.
		Payment is made monthly, and is calculated from the first day to the last day of the month. 
		The transfer is then made automatically before the fifth working day of the next month.
	</h4>

	<div>
	</div>
</div>-->

<div class="your-earnings-page">
	<div class="container containerBlock">
		<h1 class="page-title">Your Earnings</h1>
		<h4 class="title-description">
			Here are your current earnings and completed project records as well as your payment information.
			<br />
			<br />
			Please ensure all information is correct.</h4>
		
		<h3 class="description">
			Please note that completed projects that have not been marked as closed by the customer will 
			not have been paid in to your account. If you feel there is an issue or problem with your payment, <?php echo HTML::a(\Yii::t('app', 'get in contact'), '/contact', []); ?>
			<br />
			<br />
			Payment is made monthly, and is calculated from the first day to the last day of the month. 
			The transfer is then made automatically before the fifth working day of the next month. 
		</h3>
			<?php if(!$user->paypal){?>
			<div role="alert" class="alert alert-danger alert-dismissible fade in">
				<h4 class="w-head">WARNING!</h4>
				<p class="w-subhead">For funds enter your paypal email.</p>
				<div class="col-md-offset-2 col-md-8 form-wrap">
					<?php $form = ActiveForm::begin(); ?>
	
					<?= $form->field($user, 'paypal')->label('Paypal email'); ?>
	
					<div class="form-group">
						<?= Html::submitButton($user->isNewRecord ? 'Create' : 'Update', ['class' => $user->isNewRecord ? 'btn btn-create' : 'btn btn-update']) ?>
					</div>

					<?php ActiveForm::end(); ?>
				</div>
				<div class="clearfix"></div>
			</div>
			<?php }else{?>
				<button class="btn btn-paypal" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
					Manage Payment Methods
				</button>
				<div class="collapse" id="collapseExample">
					<div class="col-md-offset-2 col-md-8 form-wrap form-collapse">
						<?php $form = ActiveForm::begin(); ?>

							<?= $form->field($user, 'paypal')->label('Paypal email'); ?>

							<div class="form-group">
								<?= Html::submitButton($user->isNewRecord ? 'Create' : 'Update', ['class' => $user->isNewRecord ? 'btn btn-create' : 'btn btn-update']) ?>
							</div>

						<?php ActiveForm::end(); ?>
					</div>
					<div class="clearfix"></div>
				</div>
			<?php }?>

		<h4 class="balance">
			Your current month’s earnings are $<?=$price?>. Increase your rank in the system to get noticed and earn more!
		</h4>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel-wrapper">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<?php $k=0;  foreach($payments as $payment){ $k++;?>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingTwo<?=$k?>">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo<?=$k?>" aria-expanded="false" aria-controls="collapseTwo<?=$k?>">
										<table class="table">
											<tbody>
											<tr>
												<td>Date: <?=$payment->date_for?></td>
												<td>Referal price: $<?=$payment->referal_price?></td>
												<td>Total price: $<?=$payment->price?></td>
												<td>Status: <?= $payment->status?'PAID':'NOT PAID' ?></td>
											</tr>
											</tbody>
										</table>
									</a>
								</h4>
							</div>
							<div id="collapseTwo<?=$k?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo<?=$k?>">
								<div class="panel-body">
									<a class="btn-export" href="/api/earning?id=<?=$payment->id?>"><span aria-hidden="true" class="glyphicon glyphicon-download-alt"></span>Export</a>
									<?php
	
									echo '<table class="table table-bordered payment-table">';
									echo '<thead><th>Project name</th><th>Count ideas</th><th>Price ($)</th></thead><tbody>';
									$projects = \app\models\Projects::find()->where(['id'=>array_unique( json_decode($payment->projects))])->all();
		//     	                      var_dump($projects);
									foreach($projects as $project){
										echo '<tr><td>'.$project->title.'</td><td>'.\app\models\Ideas::find()->where(['project_id'=>$project->id,'stormer_id'=>Yii::$app->user->id])->count().'</td><td>'.\app\models\Ideas::find()->where(['project_id'=>$project->id,'stormer_id'=>Yii::$app->user->id])->count()*$payment->price_idea.'</td></tr>';
									}
									echo '</tbody></table>';
									?>
								</div>
							</div>
						</div>
						<?php }?>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
