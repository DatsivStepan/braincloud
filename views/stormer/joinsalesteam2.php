<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Join the sales team 2';
?>
<div class="join-sales-page">
	<div class="container containerBlock">
		<h1 class="page-title">Join the sales team</h1>
		<hr>
		<h3 class="page-subtitle">Welcome on board as a member BrainCloud’s powerful sales force!</h3>
		<h4 class="description">
			As a member of the sales team, you will be treated as a member of the BrainCloud family; earn big money, points and help define the quality of the community.
		</h4>
		<div class="panels-wrap">
			<div class="row">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
                        	<h3 class="panel-title">My Sales Target <?=date('M',strtotime($sales_targe->date_for))?>. - <?=date('M',strtotime($sales_targe->date_to))?>.</h3>
                    	</div>
                    	<div class="panel-body">
                    	    <p>Number of Customers: <?=$sales_targe->namber_customer?></p>
                    	    <p>Sales Amount: $<?=$sales_targe->sales_amount?> </p>
                    	    <button class="btn btn-update" onclick="update_modal()">Update</button>
                    	</div>
                	</div>
                </div>
            	<?=\app\widgets\TargeYearWidget::widget()?>
            	<?= \app\widgets\TargeMonthForWidget::widget()?>
            	<?php \yii\widgets\Pjax::begin(); ?>
            	<?= \app\widgets\TargeMonthForToWidget::widget()?>
            	<?php \yii\widgets\Pjax::end(); ?>
            	<div class="col-sm-12">
					To download our training package, <?php echo HTML::a(\Yii::t('app', 'click here'), '/SalesTraining.pdf',['class' => 'join-team-link','download'=>true]); ?><br />
					<?php echo HTML::a(\Yii::t('app', 'Need some advice or help? Get in contact'), '/contact',['class' => 'join-team-link']); ?><br />
					<?php echo HTML::a(\Yii::t('app', 'Think you have a lead already? Send them an email with your activation code'), '/stormer/referalpage_sales',['class' => 'join-team-link']); ?>
		<!--            --><?php //echo HTML::a(\Yii::t('app', 'Generate a new code for another lead'), '#',['class' => 'btn btn-primary']); ?>
				</div>
        	</div>
        </div>
        <h3 class="join-table-head">
        	Your lead generation report
        </h3>
        <hr>
        <div class="table-wrap">
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<?php foreach ($sailsTeams as $sailsTeam){
					if ($sailsTeam->status){ ?>
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingTwo<?=$sailsTeam->id?>">
							<h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo<?=$sailsTeam->id?>" aria-expanded="false" aria-controls="collapseTwo<?=$sailsTeam->id?>">
									<table class="table">
										<tbody>
											<tr>
											<tr>
												<td>

													Name
													<a class="infoButtonShow">i</a>
													<div class="infoBlockShow">
														<div class="arrow"></div>
														<h5>This is the name of a lead you have previously sent an email to</h5>
													</div>
													:
													<?=$sailsTeam->name?>
												</td>
												<td>

													Email
													<a class="infoButtonShow">i</a>
													<div class="infoBlockShow">
														<div class="arrow"></div>
														<h5>The is the email address of the a lead you have sent an email to</h5>
													</div>
													:
													<?=$sailsTeam->email?>
												</td>
												<td>

													Status
													<a class="infoButtonShow">i</a>
													<div class="infoBlockShow">
														<div class="arrow"></div>
														<h5>The is the email address of the a lead you have sent an email to</h5>
													</div>
													:

													<?= ($sailsTeam->status) ? 'Active member' : 'Pending'?>
												</td>
												<td>Total purchase: <?=\app\models\Transaction::find()->where(['user_id'=>$sailsTeam->customer_id, 'complete'=>1])->sum('price')?></td>
												<td>Total price(%): <?=\app\models\Transaction::find()->where(['user_id'=>$sailsTeam->customer_id, 'complete'=>1])->sum('price')?></td>
												<td class="action-column"></td>
											</tr>
										</tbody>
									</table>
								</a>
							</h4>
						</div>
						<div id="collapseTwo<?=$sailsTeam->id?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo<?=$sailsTeam->id?>">
							<div class="panel-body">
								<?php
								$trans = \app\models\Transaction::find()->where(['user_id'=>$sailsTeam->customer_id, 'complete'=>1])->all();
								if ($trans){?>
								<table class="table table-bordered payment-table">
									<thead>
										<th>Discription</th>
										<th>Price</th>
									</thead>
									<tbody>
									<?php foreach($trans as $tran){?>
										<tr>
											<td><?=$tran->othe?></td>
											<td><?=$tran->price?></td>
										</tr>
									<?php }?>
									</tbody>
								</table>
									<?php }?>
							</div>
						</div>
					</div>

				<?php } else {?>
				<table class="table">
					<tbody>
					<tr>
						<td>

							Name
							<a class="infoButtonShow">i</a>
							<div class="infoBlockShow">
								<div class="arrow"></div>
								<h5>This is the name of a lead you have previously sent an email to</h5>
							</div>
							:
							<?=$sailsTeam->name?>
						</td>
						<td>

							Email
							<a class="infoButtonShow">i</a>
							<div class="infoBlockShow">
								<div class="arrow"></div>
								<h5>The is the email address of the a lead you have sent an email to</h5>
							</div>
							:
							<?=$sailsTeam->email?>
						</td>
						<td>

							Status
							<a class="infoButtonShow">i</a>
							<div class="infoBlockShow">
								<div class="arrow"></div>
								<h5>The is the email address of the a lead you have sent an email to</h5>
							</div>
							:

							<?= ($sailsTeam->status) ? 'Active member' : 'Pending'?>
						</td>
						<td>Total purchase: <?=\app\models\Transaction::find()->where(['user_id'=>$sailsTeam->customer_id, 'complete'=>1])->sum('price')?></td>
						<td>Total price(%): <?=\app\models\Transaction::find()->where(['user_id'=>$sailsTeam->customer_id, 'complete'=>1])->sum('price')?></td>
						<td class="action-column"><button style="z-index: 999999" type="button" id="button_pay_2" class="btn btn-send" onclick="send_now('<?=$sailsTeam->name?>','<?=$sailsTeam->email?>','<?=Yii::$app->user->identity->email?>')">Send</button></td>
					</tr>
					</tbody>
				</table>
				<?php } }?>
			</div>

		</div>
	</div>
</div>

<div id="myModal" class="modal update-modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal">×</button>
				<h4 class="modal-title">Update</h4>
				<hr>
			</div>
			<?php $form = ActiveForm::begin()?>
			<div class="modal-body">
				<?php
					echo $form->field($sales_targe,'namber_customer');
					echo $form->field($sales_targe,'sales_amount');
				?>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-update" name="active" value="true">Submit</button>
				<button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
			</div><?php ActiveForm::end(); ?>
		</div>
	</div>
</div>
<script>
    function send_now(recipient_name,recipient_email,senders_email){
        $.ajax({
            url:'/customer/sendreferal',
            method:'post',
            data:{recipient_name:recipient_name,recipient_email:recipient_email,senders_email:senders_email},
            dataType:'json',
        }).done(function(responce){
            if(responce.status == 'send'){
                swal("Good job!", "Thank you! Your message has been sent!", "success")
            }else{

            }
        });
    }
    function update_modal(){
        $('#myModal').modal('show');
    }
</script>
