<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\helpers\Url;

//$this->title = 'Stormer';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="my-project-stormer-page">
	<div class="container containerBlock">
		<?php
			if(Yii::$app->session->hasFlash('denied')):
				echo Alert::widget([
					'options' => [
						'class' => 'alert-info',
					],
					'body' => 'Denied',
				]);
			endif; 
			if(Yii::$app->session->hasFlash('not_denied')):
				echo Alert::widget([
					'options' => [
						'class' => 'alert-error',
					],
					'body' => 'Problem',
				]);
			endif; 
		?>
		<h1 class="page-title">My Projects</h1>
		<h4 class="page-subtitle">Information related to your projects.</h4>
		<div class="button-wrap">
			<div class="btn-group" role="group" aria-label="...">
                <div class="btn-group" role="group">
                    <a href="<?= Url::home(); ?>stormer/myprojects" class="btn btn-introd ">
                        My Projects <span class="badge"><?=\app\models\Projects::find()->where(['owner_id' => \Yii::$app->user->id])->count()?></span>
                    </a>
                </div>
                <div class="btn-group" role="group">
                    <a href="<?= Url::home(); ?>stormer/projectconfirmed?type=customer" class="btn btn-introd">
                        Customer projects <span class="badge"><?=\app\models\Projectstormer::find()->joinWith('project')->where(['project_stormer.stormer_id' => \Yii::$app->user->id,'project_stormer.confirmation'=>1,'project_stormer.status'=>0,'projects.project_type'=>'customer'])->count()?></span>
                    </a>
                </div>
                <div class="btn-group" role="group">
                    <a href="<?= Url::home(); ?>stormer/projectconfirmed?type=peer" class="btn btn-introd">
                        Peer projects <span class="badge"><?=\app\models\Projectstormer::find()->joinWith('project')->where(['project_stormer.stormer_id' => \Yii::$app->user->id,'project_stormer.confirmation'=>1,'project_stormer.status'=>0,'projects.project_type'=>'Peer'])->count()?></span>
                    </a>
                </div>
                <div class="btn-group" role="group">
                    <a href="<?= Url::home(); ?>stormer/projectconfirmed?type=open" class="btn btn-introd">
                        Open projects <span class="badge"><?=\app\models\Projectstormer::find()->joinWith('project')->where(['project_stormer.stormer_id' => \Yii::$app->user->id,'project_stormer.confirmation'=>1,'project_stormer.status'=>0,'projects.project_type'=>'Open'])->count()?></span>
                    </a>
                </div>
                <div class="btn-group" role="group">
                    <a href="<?= Url::home(); ?>stormer/projectconfirmed?type=ngo" class="btn btn-introd">
                        NGO projects <span class="badge"><?=\app\models\Projectstormer::find()->joinWith('project')->where(['project_stormer.stormer_id' => \Yii::$app->user->id,'project_stormer.confirmation'=>1,'project_stormer.status'=>0,'projects.project_type'=>'NGO'])->count()?></span>
                    </a>
                </div>
                <div class="btn-group" role="group">
                    <a href="/stormer/past_project" class="btn btn-introd active">
                        Finished Projects <span class="badge"><?=\app\models\Projectstormer::find()->where(['stormer_id' => \Yii::$app->user->id,'confirmation'=>1,'status'=>1])->count()?></span>
                    </a>
                </div>
			</div>
		</div>
		<div class="table-wrap">
			<?php foreach($modelProjects as $projects){ ?>
				<?php //var_dump($projects); ?>
					<table class="table">
						<tr>
							<td>Title</td>
							<td><?= $projects['title']; ?></td>
						</tr>
						<tr>
							<td>Project Information </td>
							<td><?= $projects['description']; ?></td>
						</tr>
						<tr>
							<td>Project type </td>
							<td><?= $projects['project_type']; ?></td>
						</tr>
						<tr>
							<td>Project Goals </td>
							<td><?= $projects['goals']; ?></td>
						</tr>
						<tr>
							<td>Project category </td>
							<td><?= $projects['category']; ?></td>
						</tr>
						<tr>
							<td>Links </td>
							<td>
								<?php
								$data = \app\models\ProjectData::find()->where(['project_id'=>$projects['project_id'],'type'=>2])->all();
								foreach($data as $item){?>
									<span class="label"><?=$item->data?></span>
								<?php }?>
							</td>
						</tr>
						<tr>
							<td>Files </td>
							<td>
								<?php
								$data = \app\models\ProjectData::find()->where(['project_id'=>$projects['project_id'],'type'=>1])->all();
								foreach($data as $item){?>
									<a href="/<?=$item->data?>"> <span aria-hidden="true" class="glyphicon glyphicon-save-file"></span><?=$item->description?></a>
								<?php }?>
							</td>
						</tr>
						<tr>
							<td>Timeline for Completion</td>
							<td><?= $projects['end_date']; ?></td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<?php
								if ($projects['status']==1)
									echo HTML::a('Open project', '/stormer/project_completed/'.$projects['project_id'],['class' => 'btn btn-open']);
								else
									echo HTML::a('Open project', '/stormer/divergent/'.$projects['project_id'],['class' => 'btn btn-open']);
	
								$user = \app\models\User::find()->where(['id'=>$projects['owner_id']])->one();
								if ($user)
									if($user->users_type == 'customer'){
										$customer_info = \app\models\Customerinfo::find()->where(['customer_id'=>$projects['owner_id']])->one();
										if($customer_info->ask == '1'){
											echo HTML::a('<span class="glyphicon glyphicon-envelope"></span> Message <span class="badge">new '. \app\models\Message::find()->where(['project_id'=>$projects['project_id'],'status'=>0,'type_user'=>'customer'])->count().'</span>','/stormer/message/'.$projects['project_id'],['class'=>'btn btn-message']);
										}
									}
	
	//                                var_dump($projects);
	
								?>
							</td>
						</tr>
					</table>
			<?php } ?>
		</div>
	</div>
</div>