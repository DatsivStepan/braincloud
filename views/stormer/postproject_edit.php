<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\Alert;
use app\assets\ProjectAsset;
ProjectAsset::register($this);

?>
<div class="post-project-page">
	<div class="title-wrap">
		<h1 class="page-title">Post a Project and Become an Active Member of the Community </h1>
		<h4 class="page-subtitle">
			Please remember that projects should be related to non-workplace/profit related issues. To read terms and agreements, please <?php echo HTML::a(\Yii::t('app', 'click here'), '/terms', []); ?>.</h4>
		<hr class="hr">
		<h4 class="title-details">
			If you’re a workaholic and have no idea what could be innovated in your life outside of work, <a href="/stormer/beinspired/Peer">click here for some inspiration</a>
		</h4>
	</div>
	<div class="post-project-form-wrap project-edit">
		<div class="container">
			<?php $form = ActiveForm::begin(); ?>
			<?= $form->field($modelNewProject, 'title')->textInput(['maxlength' => true])->label('Project title'); ?>
			<label>
				Project Description
				<a class="infoButtonShow">i</a>
				<div class="infoBlockShow">
					<h5>Describe the situation you want your team to focus on as clearly as you can. To get really creative answers, try to open your project to people with different fields of knowledge. </h5>
					<p>Consider:</p>
					<p>1.Can you remove any knowledge-specific lingo?</p>
					<p>2.Are the instructions clear? If you asked someone with no prior knowledge to the project or situation to read this description, would they understand?</p>
					<p>3.Are there any other limiting factors you can remove? The more limits you place on the project, the more limited your range of answers will be. Knowledge can sometimes be removed for the creative-thinking stage, and then applied again for the application stage.</p>
				</div>
			</label>
			<div class="content"><?= $form->field($modelNewProject, 'description')->textarea(['rows' => 6])->label(false); ?></div>
			<div class="dropzonefile">
				Put your Files Here
			</div>
			<div id="actions" class="row text-left">
				<div class="col-sm-4 col-md-3">
					<!-- The fileinput-button span is used to style the file input field as button -->
					<span class="btn btn-add-image fileinput-button">
						<i class="glyphicon glyphicon-plus"></i>
						<span>Add image or files...</span>
					</span>
				</div>
				<div class="col-sm-4 col-md-3">
					<button type="reset" class="btn btn-cancel cancel">
						<i class="glyphicon glyphicon-ban-circle"></i>
						<span>Cancel upload</span>
					</button>
				</div>
				<div class="col-sm-4 col-md-6">
					<!-- The global file processing state -->
					<span class="fileupload-process">
						<div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
							<div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
						</div>
					</span>
				</div>
			</div>
			<div class="table table-striped files" id="previews">
				<div id="template" class="file-row">
					<!-- This is used as the file preview template -->
					<div class="img-preview">
						<span class="preview"><img data-dz-thumbnail /></span>
						<p class="name" data-dz-name></p>
						<strong class="error text-danger" data-dz-errormessage></strong>
						<p class="size" data-dz-size></p>
					</div>
					<div class="add-description text-left">
						<input type="text" class="form-control exp" placeholder="Brief description of file" name="Files[explanation][]" value="" required="">
						<input type="hidden" class="path" name="Files[path][]" value="" >
						<span class="instruction">
							In the interest of data protection, as well as server space, any file you upload will be deleted automatically, 2 weeks after the project has ended.
							<a class="infoButtonShow">Support..</a>
							<div class="infoBlockShow">
								Add images or files to help your stormers better understand
								the project. If you’re having difficulty or technical problems,
								please <a href="#">click here</a> and we’ll be in contact as soon as we possibly can.
							</div>
						</span>
					</div>
					<div class="delete-wrap">
						<a data-dz-remove class="btn delete">
							<i class="glyphicon glyphicon-trash"></i>
							<span>Delete</span>
						</a>
					</div>
					<div>
						<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
							<div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
						</div>
					</div>
				</div>
				<?php foreach($projectDate as $item){
					if($item->type==1){?>
					<div id="date<?=$item->id?>" class="file-row dz-processing dz-success dz-complete">
					<!-- This is used as the file preview template -->
					<div class="img-preview">
						<span class="preview"><img data-dz-thumbnail="" src="/<?=$item->data?>" width="200px"></span>
					</div>
					<div class="add-description text-left">
						<input type="text" class="form-control exp" placeholder="Brief description of file" name="Files[explanation][]" value="<?=$item->description?>" required="">
						<input type="hidden" class="path" name="Files[path][]" value="<?=$item->data?>">
						<span class="instruction">
							In the interest of data protection, as well as server space, any file you upload will be deleted automatically, 2 weeks after the project has ended.
							<a class="infoButtonShow">Support..</a>
							<div class="infoBlockShow">
								Add images or files to help your stormers better understand
								the project. If you’re having difficulty or technical problems,
								please <a href="#">click here</a> and we’ll be in contact as soon as we possibly can.
							</div>
						</span>
					</div>
					<div class="delete-wrap">
						<a data-dz-remove="" class="btn delete" onclick="delete_data('date<?=$item->id?>')">
							<i class="glyphicon glyphicon-trash"></i>
							<span>Delete</span>
						</a>
					</div>
					<div>
						<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
							<div class="progress-bar progress-bar-success" style="width: 100%;" data-dz-uploadprogress=""></div>
						</div>
					</div>
				</div>
				<?php }}?>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<label>Add links</label>
					<input type="text" class="form-control" name="link_name" placeholder="Link">
				</div>
				<div class="col-sm-6">
					<a id="add_link" class="btn btn-add-link">
						<i class="glyphicon glyphicon-plus"></i>
						Add</a>
				</div>
			</div>
			<div class="linkContainer">
				<?php foreach($projectDate as $item){
					if($item->type==2){
					?>
						<div class="row">
							<div class="col-sm-6">
								<input type="hidden" name="Link[link][]" class="form-control" value="<?=$item->data?>">
								<h4 class="link-edit"><?=$item->data?></h4>
							</div>
							<div class="clearfix"></div>
							<div class="col-sm-6">
								<label>Brief description of link <a class="infoButtonShow">Support..</a>
									<div class="infoBlockShow">Add links to support your project. Don’t forget to mention why you’re including these links.</div>
								</label>
								<input type="text" name="Link[brief_link][]" class="form-control" placeholder="Brief description" required="" value="<?=$item->description?>">
							</div>
							<div class="col-sm-6">
								<a data-dz-remove="" class="btn deleteLink" onclick="delete_link(this)">
									<i class="glyphicon glyphicon-trash"></i>
									<span>Delete</span>
								</a>
							</div>
						</div>
				<?php }}?>
			</div>
			<div class="clearfix"></div>
			<div class="form-group">
				<?= Html::submitButton($modelNewProject->isNewRecord ? 'Review and Confirm' : 'Update', ['class' => $modelNewProject->isNewRecord ? 'btn btn-success create' : 'btn btn-update']) ?>
			</div>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>

<script>
	function delete_data(id){
		$('#'+id).remove();
	}
	function delete_link(elment){
		$( elment ).parent().parent().remove();
	}
</script>