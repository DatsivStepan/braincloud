<?php
/**
 *
 * @var PsiWhiteSpace $user
 */?>
<div class="behind-the-scenes-last">
<div class="container">
			<h1 class="page-title">aaand we’re done!</h1>
			<h3 class="title-description">We have received <?=$idea?> ideas <?php if($projectModel->project_type == 'customer'){ ?>, and earned $<?=$price_idea*$idea?><?php } ?>. You’ve met your quota! Well done!</h3>
			<p class="description">We are now awaiting confirmation from the customer to review and close the project. It is possible that we may have to request modifications, so please keep a close eye on your emails and notifications.</p>
			<a class="btn btn-home" href="/">Take Me Home</a>
		</div>

	</div>
</div>
