<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\Alert;
use app\assets\ProjectAsset;
ProjectAsset::register($this);

?>
<div class="post-project-page">
	<div class="title-wrap">
		<h1 class="page-title">Post a Project and Become an Active Member of the Community</h1>
		<h4 class="page-subtitle">
			Please remember that projects should be related to non-workplace/profit related issues. To read terms and agreements, please <?php echo HTML::a(\Yii::t('app', 'click here'), '/terms', []); ?>.</h4>
		</h4>
		<hr class="hr">
		<h4 class="title-details">
			If you’re a workaholic and have no idea what could be innovated in your life outside of work, <a href="/stormer/beinspired/Peer">click here for some inspiration</a>
		</h4>
	</div>
	<div class="post-project-form-wrap project-info">
		<div class="container">
			<label class="project-title-label">Project Title <span>&nbsp;(Eg. Living Room Design)</span></label>
			<h3 class="project-title"><?=$project->title?></h3>
			<label>
				Project Description
				<a class="infoButtonShow">i</a>
				<div class="infoBlockShow">
					<h5>Describe the situation you want your team to focus on as clearly as you can. To get really creative answers, try to open your project to people with different fields of knowledge. </h5>
					<p>Consider:</p>
					<p>1.Can you remove any knowledge-specific lingo?</p>
					<p>2.Are the instructions clear? If you asked someone with no prior knowledge to the project or situation to read this description, would they understand?</p>
					<p>3.Are there any other limiting factors you can remove? The more limits you place on the project, the more limited your range of answers will be. Knowledge can sometimes be removed for the creative-thinking stage, and then applied again for the application stage.</p>
				</div>
				&nbsp;<span>(Eg. Should I make all my walls the same color, or …)</span>
			</label>
			<div class="content"><?=$project->description?></div>
			<?php if ($projectData){?>
			<div class="dropzonefile text-center">
				Put your Files Here
			</div>
			<?php }?>
			<div class="table table-striped files" id="previews">
				<?php foreach($projectData as $item){
					if($item->type == 1){
					?>
				<div id="" class="file-row dz-processing dz-image-preview dz-success dz-complete AAA">
					<!-- This is used as the file preview template -->
					<div class="project-img-wrap">
						<span class="preview"><img data-dz-thumbnail="" alt="" src="/<?=$item->data?>"></span>
					</div>
					<div>
						<?=$item->description?>
					</div>
				</div>
				<?php } }?>
			</div>
			<?php if ($projectData){?>
			<div class="dropzonefile">
				Lisnks
			</div>
			<?php }?>
			<div class="table table-striped files" id="previews">
				<?php foreach($projectData as $item){
					if($item->type == 2){
						?>
						<div id="" class="file-row dz-processing dz-image-preview dz-success dz-complete">
							<!-- This is used as the file preview template -->
							<div class="link">
								<?=$item->data?>
							</div>
							<div class="link-description">
								<?=$item->description?>
							</div>
						</div>
					<?php } }?>
			</div>
			<div class="form-group text-center">
				<a href="/stormer/project-edit/<?=$project->id?>" class="btn btn-edit">Edit</a>
				<a href="/stormer/create-peer/<?=$project->id?>" class="btn btn-create">Create</a>
			</div>
		</div>
	</div>
	</div>
</div>
