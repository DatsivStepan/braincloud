<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use app\assets\ProfilestormerAsset;
ProfilestormerAsset::register($this);

$this->title = 'Profile page';
$this->params['breadcrumbs'][] = ['label' => 'Profile Home', 'url' => [$home_profile_src]];
$this->params['breadcrumbs'][] = $this->title = 'Profile page';
?>

<div class="profile-page">
	<div class="container containerBlock">
		<?php if(Yii::$app->session->hasFlash('profile_update')):
				echo Alert::widget([
					'options' => [
						'class' => 'alert-info',
					],
					'body' => 'Profile updated',
				]);
			endif; 
			if(Yii::$app->session->hasFlash('profile_notupdate')):
				echo Alert::widget([
					'options' => [
						'class' => 'alert-error',
					],
					'body' => 'Profile not updated',
				]);
			endif; 
		?>
		<h1 class="page-title">Your Profile</h1>
		<hr>
		<div class="profile-container">
			<div class="row">
				<div class="col-sm-3">
					<div class="profile-avatar-wrap">
						<img class="img-responsive profilePhoto" src="<?= $stormerInfoModel['image_src']?  Url::home().'image/users_images/'.$stormerInfoModel['image_src']: '/image/default-avatar.png'; ?>">
						<a class="btn btn-change-avatar clicable">Change avatar</a>
						<div id="previewsS" style="display:none;"></div>
					</div>
				</div>
				<div class="col-sm-9">
					<div class="col-sm-12">
						<?php $form = ActiveForm::begin(['id' => 'form-signup', 'options' => ['class' => 'form-horizontal'] ]); ?>
							<?= $form->field($userModel,'name')->input('text',['disabled'=>'disabled'])->label('Contact name')?>
							<?= $form->field($userModel,'username')->input('text',['disabled'=>'disabled'])->label('Contact login')?>
							<?= $form->field($stormerInfoModel, 'country')->dropDownList($arrayCountry,['class'=>'selectCountry form-control','prompt'=>'Select Country'])->label('Where in the world are you?'); ?>
<!--							--><?php //echo $form->field($stormerInfoModel, 'city')->dropDownList($arrayCity,['class'=>'selectCity form-control','prompt'=>'Select Your City'])->label(false); ?>
                            <?php
                            echo \kartik\typeahead\Typeahead::widget([
                                'name' => 'city_head',
                                'options' => [
                                    'placeholder' => 'Select Your City',
                                    'class' => 'selectCity form-control',
                                ],
                                'value'=>$city_name->city_name,
                                'pluginOptions' => ['highlight'=>true],
                                'dataset' => [
                                    [
                                        'remote' => [
                                            'url' => Url::to(['site/country-list']) . '?q=%QUERY',
                                            'wildcard' => '%QUERY'
                                        ]
                                    ]
                                ]
                            ]);
                            ?>
							<div class="gender-wrap">
								<?= $form->field($stormerInfoModel, 'gender')->radioList([1 => 'Male', 0 => 'Female']); ?>
								<a class="btn btn-show-question dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Why are we asking this?</a>
								<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
									To ensure our system is approachable to all genders, we need to measure the gender of our teams. This helps us become more gender-neutral. This information is private and will not be present on your profile</p>
								</ul>
							</div>
							<div class="row">
								<div class="col-sm-12" style="height:0px;">
									<?= $form->field($stormerInfoModel, 'image_src')->hiddenInput()->label(false); ?>
								</div>
								<div class='col-sm-12 introduction-wrap'>
									<div class="row">
										<label>Give us a brief introduction
											<a class="titleDesign" data-placement="bottom" data-toggle="tooltip" title="<p>You may be selected for jobs based off this introduction, try to make it as interesting as you can. Tell us something about your specialities. Keep things factual and do your best not to make any spelling or grammar mistakes.</p>" >i</a>
										</label>
										<div class='col-sm-12'>
											<?= $form->field($stormerInfoModel, 'introduction')->textInput(['class'=>'form-control pull-right'])->label(false); ?>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="social-networks">
									<?= $form->field($stormerInfoModel, 'linkedin_link'); ?>
									<?= $form->field($stormerInfoModel, 'twitter_link'); ?>
									<?= $form->field($stormerInfoModel, 'facebook_link'); ?>
								</div>
							</div>
							<?= $form->field($userModel,'email')->textInput(['placeholder'=>'Contact email'])->label('Contact email')?>
							<div class="panel panel-info" style="display: none;" id="email_notification">
								<div class="panel-heading">
									<h3 class="panel-title">Email notification settings:</h3>
								</div>
								<div class="panel-body">
									<?= $form->field($stormerInfoModel, 'notifications')->radioList(array('1'=>'Yes', 2=>'No'))->label('Email all notifications'); ?>
									<?= $form->field($stormerInfoModel, 'notification_project')->radioList(array('1'=>'Yes', 2=>'No'))->label('Email me with notifications only related to my project'); ?>
									<?= $form->field($stormerInfoModel, 'notifications_off')->radioList(array('1'=>'Yes', 2=>'No'))->label('Switch off all notifications'); ?>
									<?= $form->field($stormerInfoModel, 'show_email_customer_option')->radioList(array('1'=>'Yes', 2=>'No'),['id'=>'show_email', 'onclick'=>'show_email()'])->label('Show emails from corporate clients'); ?>
									<div id="email_stormer" style="display: <?= ($stormerInfoModel->show_email_customer_option == 1)? 'block' :'none' ?>">
										<?= $form->field($stormerInfoModel, 'show_email_customer')->label('Email for customer'); ?>
									</div>
								</div>
							</div>
							<div class="text-right">
								<div class="row">
									<a class="btn btn-email-panel" id="email_notification_btn" onclick="email_notification()">Email notification settings</a>
									<?= Html::submitButton('Save', ['class' => 'btn btn-save', 'name' => 'saveStormer']) ?>
								</div>
							</div>
						<?php ActiveForm::end(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>

	function email_notification(){
		$('#email_notification').show();
		$('#email_notification_btn').hide();
	}

	function show_email(){
		if ($('#show_email input:checked').val() == 1){
			$('#email_stormer').show();
		} else {
			$('#email_stormer').hide();
		}
	}
</script>
