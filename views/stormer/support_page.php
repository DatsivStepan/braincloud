<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\Alert;
use app\assets\ReferalAsset;
ReferalAsset::register($this);

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faq-page">
	<div class="title-wrap text-center">
		<div class="container">
			<h1 class="page-title">Stormer Support</h1>
			<h4 class="page-subtitle">Having a question or a problem? Read on! We’re here to help!</h4>
			<hr class="title-hr" />
			<h3 class="faq-details">We have addressed various concerns and commonly asked questions below. Feel free to take a quick look to see if the answer to your question is already here. </h3>
		</div>
	</div>
	<div class="container">
		<div class="question-list-wrap">
			<div class="container">
				<h2 class="question-wrap-header">FAQs</h2>
				<div class="row">
					<?php foreach($modelFaqs as $faq){ ?>
						<div class="col-md-12">
							<div class="question">
								<div class="question-title"><?= $faq->question; ?></div>
								<hr />
								<div class="answer">
									<?= $faq->answer; ?>
									<span class="arrow"></span>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
