<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Join the sales team';
?>
<div class="join-sales-page-1">
	<div class="container text-center">
		<h1 class="page-title">Join the sales team</h1>
		<hr>
		<h4 class="page-subtitle">As we are currently in early stages of development, we are seeking a strong sales team to join us in our progress and expansion.<br>Interested? Read on!</h4>
		<p class="content">
			Earn 10% of the sales revenue as commission, as well as 150 Stormer points for each sale, to get more noticed on the platform!<br><br>
			Work anytime, anywhere, with no stress or targets.<br><br>
			Get support and advice, as well as training.<br><br>
			Become a leading member of the community!
		</p>

		<?php
			$form=ActiveForm::begin();

				echo Html::submitButton('Join Now', ['class' => 'btn btn-join', 'name'=>'join']);

			ActiveForm::end();
		?>

	</div>
</div>
