<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
//use app\assets\WholepictureAsset;
//WholepictureAsset::register($this);
use app\assets\BehindthescenesAsset;
BehindthescenesAsset::register($this);

$this->title = 'Behind The Scenes';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
	#jsm_container{
		width:100%;
		height:800px;
	}
	.hiddenText {
		display:none
	}
</style>
<?php
	$one_show_ms= \app\models\OneShowMessage::find()->where([
		'user_id'=>Yii::$app->user->id,
		'option'=>$project_id,
		'type'=>'scena'
	])->one();
	if ($one_show_ms){
		echo '<script>var one_show_ms = false;</script>';
	} else {
		$one_show_ms_new = new \app\models\OneShowMessage();
		$one_show_ms_new->user_id= Yii::$app->user->id;
		$one_show_ms_new->option= $project_id;
		$one_show_ms_new->type= 'scena';
		$one_show_ms_new->save();
		echo '<script>var one_show_ms = true;</script>';
	}
?>
<script>var project_id = <?=$project_id?></script>
<script>var mindM = <?=$mindM?></script>
<script>var count_idea = <?=$count_idea?></script>

<div class="behind-the-scenes">
	<div class="container">
		<input type="hidden" id="scenarios_user_type" data-type="stormer">
		<h1 class="page-title">Your Project: Behind The Scenes</h1>
		<hr>
	</div>
	<div class="content">
		<div class="container">
			<?php if (!Yii::$app->request->get('beinspired')) {?>
			<h4 class="page-subtitle">Select an idea to flag </h4>
			<?= HTML::a('Exit',\yii\helpers\Url::home().'stormer/myprojects',['class'=>'btn btn-exit']); ?>
			<button class='btn btn-flag' id="flag_idea">Flag</button>
			<button class='btn btn-star' id="star_stormer">Give Star</button>
			<?php } else {?>
				<?= HTML::a('Exit',\yii\helpers\Url::home().'stormer/beinspired/Peer',['class'=>'btn btn-exit']); ?>
			<?php }?>
		</div>
		<div id="jsm_container">
		</div>
	</div>
</div>
<?php
foreach($ideas as $idea){
	$ranting = \app\models\Userrating::find()->where(['project_id'=>$project_id, 'stormer_id'=>$idea->stormer_id])->one();
	if($ranting){
		$count_star= $ranting->count_star;
	}else
		$count_star=0;
	?>
	<div class="modal fade in" id="stormer<?=$idea->id?>" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<input class="rating_stormer<?= $idea->id; ?>"  type="number">
				</div>
			</div>
		</div>
	</div>
	<div class="modal modal-behind fade" id="idea<?=$idea->id?>" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><?=$idea->ideas_description?></h4>
					<hr>
				</div>
				<div class="modal-body">
					<div class='flagBlock<?= $idea['id']; ?>'>

						<?php if($idea->flagging == 0){ ?>
							<label class="control-label">Reason for flagging </label>
							<div class="col-sm-12">
								<div class="radio">
									<label>
										<input type="radio" name="reason_for_flagging" value="1">
										Offensive/gratuitous
									</label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="reason_for_flagging" value="2">
										Repeated idea
									</label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="reason_for_flagging" value="3">
										<input type='text' name='reason_for_flagging_other' placeholder="Other(please state)" class='form-control'>
									</label>
								</div>
							</div>
							<div class="clearfix"></div>
							<button class='btn btn-send SendFlag' data-idea_id='<?= $idea['id']; ?>'>Submit</button>
						<?php }else{ ?>
							<span class="flagging">Ideas flagging</span>
						<?php } ?>
						<div style='clear:both;'></div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php }?>



<?php

$stormers = \app\models\Projectstormer::find()->where(['project_id'=>$project_id])->all();
foreach($stormers as $stormer){

	$ranting = \app\models\Userrating::find()->where(['project_id'=>$project_id, 'stormer_id'=>$stormer->stormer_id])->one();
	if($ranting){
		$count_star= $ranting->count_star;
	}else
		$count_star=0;
	?>
	<div class="modal modal-behind fade" id="stormer<?=$stormer->stormer_id?>" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<input class="rating_stormer<?= $stormer->stormer_id; ?>"  data-stormer_id="<?= $stormer->stormer_id ?>" data-project_id="<?= $project_id ?>" data-customer_id="<?= \Yii::$app->user->id; ?>" value="<?= $count_star; ?>" type="number">
				</div>
			</div>
		</div>
	</div>
<?php }?>

<div class="modal modal-behind fade" id="root" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><?=$projectMy->title?></h4>
				<hr>
			</div>
			<div class="modal-body">
				<p class="description"><?=$projectMy->description?></p>
			</div>
		</div>
	</div>
</div>
