<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\assets\WholepictureAsset;
WholepictureAsset::register($this);

//$this->title = 'Stormer';
//$this->params['breadcrumbs'][] = $this->title;
?>
<style>
#jsm_container{
		float:left;
		width:100%;
		height:800px;
}
.hiddenText {
		display:none
}
</style>

<script>var project_id = <?=$project_id?></script>
<script>var mindM = <?=$mindM?></script>
<script>var count_idea = <?=$count_idea?></script>
<script>var count_idea_p = <?=$count_idea?></script>
<script>var count_idea_user = <?=$count_idea_user?></script>
<script>var user_id = <?=Yii::$app->user->id?></script>
<script>var instruction = 0;</script>

<div class="behind-the-scenes">
	<div class="container">
		<h1 class="page-title">Take a Look at the Whole Picture</h1>
		<hr>
		<h4 class="page-subtitle instructions">Stage Instructions: Look at all the ideas generated so far, if any of your ideas are similar to others, either change them or remove them. Once you have some open spaces, follow the instructions to fill them back in again.</h4>
	</div>
	<div class="content">
		<div class="container-fluid">
			<p class="perspective">Consider this from a <?=$project->perspectives?> perspective</p>
			<button class="btn btn-next-stage saveD" data-id="stage">Next Stage</button>
			<button class="btn btn-save-exit saveD" data-id="exit">Save and exit</button>
			<button class="btn btn-inspire" onclick="inspire()" id="inspire_bt">Inspire me</button>
			<div class="row">
				<div class="col-sm-offset-2 col-sm-8 col-md-offset-0 col-md-5 col-lg-4 col-md-push-7 col-lg-push-8 right-col">
					<div id="instruction">
						<h3 class="instruction-title">Innovation instructions</h3>
						<div class="buttons">
							<button class="btn btn-next-ins" style="float: left" onclick="innovation_next(2)">next</button>
							<div class="toggle-button toggle-button--aava" onclick="innovation_show()">
								<input type="checkbox" id="innovation_show12" checked="checked">
								<label for="toggleButton" id="lable_ch" data-on-text="On" data-off-text="Off"></label>
								<div class="toggle-button__icon"></div>
							</div>
						</div>
						<div id="instruction_show">

						</div>
					</div>
					<div id="inspire" style="display: none">
						<h3>Innovation Tips</h3>
						<div id="inspire_show">

						</div>
						<!--<button class="btn btn-primary" onclick="inspire_next()">back next</button>-->
					</div>
					<!--<div id="update" style="display:none;">
						<h3 class="add-idea-title">Update idea</h3>
						<textarea placeholder="Idea" rows="4" class="idea-sescription form-control" ></textarea>
						<!--<a href="" class="btn btn-primary add-node form-controll">Add</a>-->
<!--						<a href="#" class="btn btn-update-idea form-controll">Update</a>-->
<!--					</div>-->
					<div id="show_idea" style="display: none;">
						<h3 class="add-idea-title">Idea</h3>
						<div id="idea_text" style="background-color: white; color: #717171; padding: 5px"></div>
					</div>
					<!--<a href="" class="btn btn-danger remove-node form-controll">Remove selected node</a> -->
				</div>
				<div class="clearfix visible-sm"></div>
				<div class="col-md-7 col-lg-8 col-md-pull-5 col-lg-pull-4 no-padding-md">
					<div id="jsm_container">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- <div class="modal " id="myModal" name="modal"  role="dialog">
		 <div class="modal-dialog" style="margin-top: 100px">
				 <div class="modal-content">
						 <div class="col-sm-12" style="margin-top: 20px">
						 Welcome! In order to get the finest experience, please follow the instructions as best you can. If you get stuck, click the ‘Tips and Help’ button. (You can leave and save your work anytime, and come back to it later, although there is a time bonus for completing it in the first 48 hours!) if customer projects.
Your quota is x ideas. Please don’t forget that one idea includes all three stages of the ideation. Your ideas will not be counted if all three stages are not completed.
Good luck!
</div>
						 <div class="modal-footer">
								 <button type="button" class="btn btn-primary" data-dismiss="modal">Start Now!</button>
						 </div>
				 </div>
		 </div>
 </div> -->
<div class="modal modal-behind fade" id="root" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><?=$project->title?></h4>
				<hr>
			</div>
			<div class="modal-body">
				<p class="description"><?=$project->description?></p>
			</div>
		</div>
	</div>
</div>
<div class="modal modal-behind fade" id="idea_view" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Idea</h4>
                <hr>
            </div>
            <div class="modal-body">
                <p class="description" id="description_idea"></p>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-behind fade" id="idea_up" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update idea</h4>
                <hr>
            </div>
            <div class="modal-body">
                <textarea rows="4" class="form-control" id="idea_input_update" ></textarea>
                <input type="hidden" id="nodeid">
                <button class="btn form-controll" id="update">Update</button>
            </div>
        </div>
    </div>
</div>