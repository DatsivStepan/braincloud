<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\helpers\Url;
use app\assets\ProjectAsset;
ProjectAsset::register($this);

//$this->title = 'Stormer';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-project-page">
    <div class="title-wrap">
        <h1 class="page-title">Project Introduction</h1>
    </div>


        <div class="post-project-form-wrap project-info">
            <div class="container">
                <label class="project-title-label">Project Title</label>
                <h3 class="project-title"><?=$project->title?></h3>
                <label>
                    Project Description
                </label>
                <div class="content"><?=$project->description?></div>
                <div class="table table-striped files" id="previews">
                    <?php foreach($projectData as $item){
                        if($item->type == 1){
                            ?>
                            <div id="" class="file-row dz-processing dz-image-preview dz-success dz-complete AAA">
                                <!-- This is used as the file preview template -->
                                <div class="project-img-wrap">
                                    <span class="preview"><img data-dz-thumbnail="" alt="" src="/<?=$item->data?>"></span>
                                </div>
                                <div>
                                    <?=$item->description?>
                                </div>
                            </div>
                        <?php } }?>
                </div>
                <?php if ($projectData){?>
                    <div class="dropzonefile">
                        Lisnks
                    </div>
                <?php }?>
                <div class="table table-striped files" id="previews">
                    <?php foreach($projectData as $item){
                        if($item->type == 2){
                            ?>
                            <div id="" class="file-row dz-processing dz-image-preview dz-success dz-complete">
                                <!-- This is used as the file preview template -->
                                <div class="link">
                                    <?=$item->data?>
                                </div>
                                <div class="link-description">
                                    <?=$item->description?>
                                </div>
                            </div>
                        <?php } }?>
                </div>
                <div class="form-group text-center">
                    <a href="/stormer/divergent/<?=$project->id?>" class="btn btn-edit">Start</a>
                </div>
            </div>
        </div>

</div>