<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\helpers\Url;

//$this->title = 'Stormer';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-introduction-page">
	<div class="container containerBlock">
		<?php
			if(Yii::$app->session->hasFlash('denied')):
				echo Alert::widget([
					'options' => [
						'class' => 'alert-info',
					],
					'body' => 'Denied',
				]);
			endif; 
			if(Yii::$app->session->hasFlash('not_denied')):
				echo Alert::widget([
					'options' => [
						'class' => 'alert-error',
					],
					'body' => 'Problem',
				]);
			endif; 
		?>
		<h1 class="page-title">Join a Project</h1>
		<h4 class="page-subtitle">Here is a list of all the available projects. Feel free to explore and join any which inspire you. Nothing there? Make your own on the "Innovate" page on your dashboard.</h4>

        <?php
        $points = \app\models\Points::find()->where(['user_id'=>Yii::$app->user->id])->sum('point');
        if ($points<350) {
          $p = 350 - $points;
          echo Alert::widget([
            'options' => [
              'class' => 'alert-info',
            ],
            'body' => '<span class="bold">Only ' . $p . ' points to go before all features are unlocked!</span>',
          ]);
        }
        ?>
		
		<div class="tab-panels-wrap">
			<div class="panel panel-default">
				<div class="panel-body">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab">All</a></li>
						<li role="presentation"><a href="#customer" aria-controls="customer" role="tab" data-toggle="tab">Customer</a></li>
						<li role="presentation"><a href="#open" aria-controls="open" role="tab" data-toggle="tab">Open</a></li>
						<li role="presentation"><a href="#peer" aria-controls="peer" role="tab" data-toggle="tab">Peer</a></li>
						<li role="presentation"><a href="#ngo" aria-controls="ngo" role="tab" data-toggle="tab">NGO</a></li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="all">
							<?php foreach($modelProjects as $projects){ ?>
								<?php if (true){?>
									<?php if ((time() < strtotime($projects['end_date'])) and ($projects['project_type'] == 'customer') ) { ?>
										<table class="table">
											<tr>
												<td>Title</td>
												<td><?= $projects['title']; ?></td>
											</tr>
											<tr>
												<td>Project Information </td>
												<td><?= $projects['description']; ?></td>
											</tr>
											<tr>
												<td>Project type </td>
												<td><?= $projects['project_type']; ?></td>
											</tr>
											<tr>
												<td>Project Goals </td>
												<td><?= $projects['goals']; ?></td>
											</tr>
											<tr>
												<td>Project category </td>
												<td><?= $projects['category']; ?></td>
											</tr>
											<tr>
												<td>Timeline for Completion</td>
												<td><?= $projects['end_date']; ?></td>
											</tr>
											<tr>
												<td>
													<?php
					//											echo HTML::a('Let’s give this a shot!', '/stormer/divergent/'.$projects['project_id'],['class' => 'btn btn-action']);
													echo HTML::a('Let’s give this a shot!', '/stormer/confidentialityagreement?id='.$projects['project_id'],['class' => 'btn btn-action']);
													?>
												</td>
												<td>

													<?php $form = ActiveForm::begin(); ?>
													<input type="hidden" name="project_id" value="<?= $projects['project_id']; ?>">
													<?= Html::submitButton('I don’t think this project is for me', ['class' => 'btn btn-refusal', 'name' => 'vidmov']); ?>
													<?php ActiveForm::end(); ?>
												</td>
											</tr>
										</table>
									<?php } else { ?>
										<table class="table">
											<tr>
												<td>Title</td>
												<td><?= $projects['title']; ?></td>
											</tr>
											<tr>
												<td>Project Information </td>
												<td><?= $projects['description']; ?></td>
											</tr>
											<tr>
												<td>Project type </td>
												<td><?= $projects['project_type']; ?></td>
											</tr>
											<tr>
												<td>Project Goals </td>
												<td><?= $projects['goals']; ?></td>
											</tr>
											<tr>
												<td>Project category </td>
												<td><?= $projects['category']; ?></td>
											</tr>
											<!--						<tr>-->
											<!--							<td>Timeline for Completion</td>-->
											<!--							<td>--><?//= $projects['end_date']; ?><!--</td>-->
											<!--						</tr>-->
											<tr>
												<td>

													<?php
													echo HTML::a('Join Project', '/stormer/confidentialityagreement?id='.$projects['project_id'],['class' => 'btn btn-action']);
		//											echo HTML::a('Let’s give this a shot!', '/stormer/divergent/'.$projects['project_id'],['class' => 'btn btn-action']);
													?>
												</td>
												<td>

												</td>
											</tr>
										</table>
									<?php }	} }?>
						</div>
						<div role="tabpanel" class="tab-pane" id="customer">
							<?php foreach($modelProjects as $projects){ ?>
								<?php if ($projects['project_type'] == 'customer'){?>
								<?php if ((time() < strtotime($projects['end_date'])) and ($projects['project_type'] == 'customer') ) { ?>
									<table class="table">
										<tr>
											<td>Title</td>
											<td><?= $projects['title']; ?></td>
										</tr>
										<tr>
											<td>Project Information </td>
											<td><?= $projects['description']; ?></td>
										</tr>
										<tr>
											<td>Project Goals </td>
											<td><?= $projects['goals']; ?></td>
										</tr>
										<tr>
											<td>Project category </td>
											<td><?= $projects['category']; ?></td>
										</tr>
										<tr>
											<td>Timeline for Completion</td>
											<td><?= $projects['end_date']; ?></td>
										</tr>
										<tr>
											<td>
												<?php echo HTML::a('Let’s give this a shot!', '/stormer/confidentialityagreement?id='.$projects['project_id'],['class' => 'btn btn-action']); ?>
											</td>
											<td>

												<?php $form = ActiveForm::begin(); ?>
												<input type="hidden" name="project_id" value="<?= $projects['project_id']; ?>">
												<?= Html::submitButton('I don’t think this project is for me', ['class' => 'btn btn-refusal', 'name' => 'vidmov']); ?>
												<?php ActiveForm::end(); ?>
											</td>
										</tr>
									</table>
								<?php } else { ?>
									<table class="table">
										<tr>
											<td>Title</td>
											<td><?= $projects['title']; ?></td>
										</tr>
										<tr>
											<td>Project Information </td>
											<td><?= $projects['description']; ?></td>
										</tr>
										<tr>
											<td>Project Goals </td>
											<td><?= $projects['goals']; ?></td>
										</tr>
										<tr>
											<td>Project category </td>
											<td><?= $projects['category']; ?></td>
										</tr>
										<!--						<tr>-->
										<!--							<td>Timeline for Completion</td>-->
										<!--							<td>--><?//= $projects['end_date']; ?><!--</td>-->
										<!--						</tr>-->
										<tr>
											<td>
												<?php echo HTML::a('Let’s give this a shot!', '/stormer/confidentialityagreement?id='.$projects['project_id'],['class' => 'btn btn-action']); ?>
											</td>
											<td>

												<?php $form = ActiveForm::begin(); ?>
												<input type="hidden" name="project_id" value="<?= $projects['project_id']; ?>">
												<?= Html::submitButton('I don’t think this project is for me', ['class' => 'btn btn-refusal', 'name' => 'vidmov']); ?>
												<?php ActiveForm::end(); ?>
											</td>
										</tr>
									</table>
								<?php }	} }?>
						</div>
						<div role="tabpanel" class="tab-pane" id="open">
							<?php foreach($modelProjects as $projects){ ?>
								<?php if ($projects['project_type'] == 'Open'){?>
									<?php if ((time() < strtotime($projects['end_date'])) and ($projects['project_type'] == 'customer') ) { ?>
										<table class="table">
											<tr>
												<td>Title</td>
												<td><?= $projects['title']; ?></td>
											</tr>
											<tr>
												<td>Project Information </td>
												<td><?= $projects['description']; ?></td>
											</tr>
											<tr>
												<td>Project Goals </td>
												<td><?= $projects['goals']; ?></td>
											</tr>
											<tr>
												<td>Project category </td>
												<td><?= $projects['category']; ?></td>
											</tr>
											<tr>
												<td>Timeline for Completion</td>
												<td><?= $projects['end_date']; ?></td>
											</tr>
											<tr>
												<td>
													<?php echo HTML::a('Join Project', '/stormer/confidentialityagreement?id='.$projects['project_id'],['class' => 'btn btn-action']); ?>
												</td>
												<td>

												</td>
											</tr>
										</table>
									<?php } else { ?>
										<table class="table">
											<tr>
												<td>Title</td>
												<td><?= $projects['title']; ?></td>
											</tr>
											<tr>
												<td>Project Information </td>
												<td><?= $projects['description']; ?></td>
											</tr>
											<tr>
												<td>Project Goals </td>
												<td><?= $projects['goals']; ?></td>
											</tr>
											<tr>
												<td>Project category </td>
												<td><?= $projects['category']; ?></td>
											</tr>
											<!--						<tr>-->
											<!--							<td>Timeline for Completion</td>-->
											<!--							<td>--><?//= $projects['end_date']; ?><!--</td>-->
											<!--						</tr>-->
											<tr>
												<td>
													<?php echo HTML::a('Join Project', '/stormer/confidentialityagreement?id='.$projects['project_id'],['class' => 'btn btn-action']); ?>
												</td>
												<td>

												</td>
											</tr>
										</table>
									<?php }	} }?>
						</div>
						<div role="tabpanel" class="tab-pane" id="peer">
							<?php foreach($modelProjects as $projects){ ?>
								<?php if ($projects['project_type'] == 'Peer'){?>
									<?php if ((time() < strtotime($projects['end_date'])) and ($projects['project_type'] == 'customer') ) { ?>
										<table class="table">
											<tr>
												<td>Title</td>
												<td><?= $projects['title']; ?></td>
											</tr>
											<tr>
												<td>Project Information </td>
												<td><?= $projects['description']; ?></td>
											</tr>
											<tr>
												<td>Project Goals </td>
												<td><?= $projects['goals']; ?></td>
											</tr>
											<tr>
												<td>Project category </td>
												<td><?= $projects['category']; ?></td>
											</tr>
											<tr>
												<td>Timeline for Completion</td>
												<td><?= $projects['end_date']; ?></td>
											</tr>
											<tr>
												<td>
													<?php echo HTML::a('Join Project', '/stormer/confidentialityagreement?id='.$projects['project_id'],['class' => 'btn btn-action']); ?>
												</td>
												<td>

												</td>
											</tr>
										</table>
									<?php } else { ?>
										<table class="table">
											<tr>
												<td>Title</td>
												<td><?= $projects['title']; ?></td>
											</tr>
											<tr>
												<td>Project Information </td>
												<td><?= $projects['description']; ?></td>
											</tr>
											<tr>
												<td>Project Goals </td>
												<td><?= $projects['goals']; ?></td>
											</tr>
											<tr>
												<td>Project category </td>
												<td><?= $projects['category']; ?></td>
											</tr>
											<!--						<tr>-->
											<!--							<td>Timeline for Completion</td>-->
											<!--							<td>--><?//= $projects['end_date']; ?><!--</td>-->
											<!--						</tr>-->
											<tr>
												<td>
													<?php echo HTML::a('Join Project', '/stormer/confidentialityagreement?id='.$projects['project_id'],['class' => 'btn btn-action']); ?>
												</td>
												<td>

												</td>
											</tr>
										</table>
									<?php }	} }?>
						</div>
						<div role="tabpanel" class="tab-pane" id="ngo">
							<?php foreach($modelProjects as $projects){ ?>
								<?php if ($projects['project_type'] == 'NGO'){?>
									<?php if ((time() < strtotime($projects['end_date'])) and ($projects['project_type'] == 'customer') ) { ?>
										<table class="table">
											<tr>
												<td>Title</td>
												<td><?= $projects['title']; ?></td>
											</tr>
											<tr>
												<td>Project Information </td>
												<td><?= $projects['description']; ?></td>
											</tr>
											<tr>
												<td>Project Goals </td>
												<td><?= $projects['goals']; ?></td>
											</tr>
											<tr>
												<td>Project category </td>
												<td><?= $projects['category']; ?></td>
											</tr>
											<tr>
												<td>Timeline for Completion</td>
												<td><?= $projects['end_date']; ?></td>
											</tr>
											<tr>
												<td>
													<?php echo HTML::a('Join Project', '/stormer/confidentialityagreement?id='.$projects['project_id'],['class' => 'btn btn-action']); ?>
												</td>
												<td>

												</td>
											</tr>
										</table>
									<?php } else { ?>
										<table class="table">
											<tr>
												<td>Title</td>
												<td><?= $projects['title']; ?></td>
											</tr>
											<tr>
												<td>Project Information </td>
												<td><?= $projects['description']; ?></td>
											</tr>
											<tr>
												<td>Project Goals </td>
												<td><?= $projects['goals']; ?></td>
											</tr>
											<tr>
												<td>Project category </td>
												<td><?= $projects['category']; ?></td>
											</tr>
											<!--						<tr>-->
											<!--							<td>Timeline for Completion</td>-->
											<!--							<td>--><?//= $projects['end_date']; ?><!--</td>-->
											<!--						</tr>-->
											<tr>
												<td>
													<?php echo HTML::a('Join Project', '/stormer/confidentialityagreement?id='.$projects['project_id'],['class' => 'btn btn-action']); ?>
												</td>
												<td>

												</td>
											</tr>
										</table>
									<?php }	} }?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>