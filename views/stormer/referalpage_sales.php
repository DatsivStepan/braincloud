<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\Alert;
use app\assets\ReferalAsset;
ReferalAsset::register($this);
$user = \app\models\User::findOne(Yii::$app->user->id);
if ($user->referal_key){
	$referal_key = $user->referal_key;
}else{
	$user->scenario='referal_key';
	$user->referal_key=$user->referal();
	$referal_key = $user->referal_key;
	$user->save();
}
?>
<div class="referal-page">
	<div class="title-wrap">
		<div class="container-fluid">
			<h1 class="page-title">Generate a Lead</h1>
			<h4 class="page-subtitle">Have a lead in mind? Send an email.</h4>
			<hr class="hr">
			<h4 class="details">Remember, each customer that joins using your referral email will earn 25 loyalty points, which earns them discounts on future purchases.</h4>
		</div>
	</div>
	<div class="container containerBlock">
		<div class="referal-form-wrap">
			<?php $form = ActiveForm::begin(['id' => 'cusromer_referal',
				'options' => ['class' => 'form-horizontal'],
				'fieldConfig' => [
					'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-7\">{error}</div>",
					'labelOptions' => ['class' => 'col-lg-2 control-label'],
				],
				]); ?>
				<div class="col-md-4 top-input">
					<?= Html::input('text','recipient_name','',['class'=>'form-control','placeholder'=>'Recipient Name']); ?>
				</div>
				<div class="col-md-4 center-input">
					<?= Html::input('email','recipient_email','',['class'=>'form-control','placeholder'=>'Recipient Email']); ?>
				</div>
				<div class="col-md-4 bottom-input">
					<?= Html::input('text','senders_email','',['class'=>'form-control','placeholder'=>'Sender’s Email']); ?>
				</div>
				<label class="control-label">Message</label>
				<div class="clearfix"></div>
				<p id="messageTamplate" contenteditable="true">
					Dear <span class="RecipientName">Recipient name</span>,<br>
					I am currently a member of BrainCloud Solution’s innovation platform, and thought it might be useful for you. <br>
					The platform offers personalised innovation teams, affordable business solutions and creative ideation processes. Everything is done online within a relatively quick timeframe.<br>
					Click here to find out more: <a href="">braincloud.solutions/services</a>
					If you feel this is something that interests you, type in the following code during purchase to earn extra loyalty points: 25<br>
					Referal key: <?=$referal_key?><br>
					Kind regards,<br>
					<?= \Yii::$app->user->identity->username;?><br>
				</p>
				<input type="submit" name="referalstormersalesButton" class="btn btn-send" value="Send">
				<a href="/stormer/joinsalesteam2" class="btn btn-back">Back</a>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>
