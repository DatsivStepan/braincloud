<?php
/**
 *
 */
?>
<div class="stormer-benchmark-page">
	<div class="container">
			<h1 class="page-title">Congratulations!</h1>
				<p class="details">
					This certifies that <?=$user->name?> has reached level <?=$user->level?> in our innovation system.<br>
				</p>
				<br />
				<p class="content">
					<?php
					if ($user->level <20)
						echo 'Congratulations! You are now an Ns (Nimbostratus) Stormer!<br>';
					elseif ($user->level <30)
						echo 'Congratulations! You are now a St (Stratus) Stormer!<br>';
					elseif ($user->level <40)
						echo 'Congratulations! You are now a Cu (Cumulus) Stormer!<br>';
					elseif ($user->level <50)
						echo 'Congratulations! You are now a Sc (Stratocumulus) Stormer!<br>';
					elseif ($user->level <60)
						echo 'Congratulations! You are now an As (Altostratus) Stormer!<br>';
					elseif ($user->level <70)
						echo 'Congratulations! You are now an Ac (Altocumulus) Stormer!<br>';
					elseif ($user->level <80)
						echo 'Congratulations! You are now a Ci (Cirrus) Stormer!<br>';
					elseif ($user->level <90)
						echo 'Congratulations! You are now a Cc (Cirrocumulus) Stormer!<br>';
					elseif ($user->level <100)
						echo 'Congratulations! You are now a Cs (Cirrostratus) Stormer!<br>';
					elseif ($user->level == 100)
						echo 'Congratulations! You are now a Cb (Cumulonimbus) Stormer!<br>';
					?>
					<br />
					This certifies that that the bearer has handled a wide range of difficult and challenging innovation problems. 
					Although there is always room to grow, this is a declaration of the bearer’s amazing achievements so far.
					<br>
					<br>
					If the contents of the certificate need to be independently verified, feel free to contact us at <a href="mailto:support@braincloud.solutions">support@braincloud.solutions</a>.
					<br>
					<br>
					Chris Lawrence signature<br>
					CEO BrainCloud Solutions<br>
					To save this certificate as a pdf click here: <a href="/api/stormer_benchmark">Downloads pdf version</a><br>
				</p>
			</div>
		</div>
	</div>
</div>
