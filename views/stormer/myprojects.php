<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\Alert;
use yii\helpers\Url;

$this->title = 'My projects';

?>
<div class="my-project-stormer-page">
	<div class="container">
		<h1 class="page-title">My Projects</h1>
		<h4 class="page-subtitle">Information related to your projects.</h4>
		<div class="button-wrap">
			<div class="btn-group" role="group" aria-label="...">
				<div class="btn-group" role="group">
					<a href="<?= Url::home(); ?>stormer/myprojects" class="btn btn-introd active">
                        My Projects <span class="badge"><?=\app\models\Projects::find()->where(['owner_id' => \Yii::$app->user->id])->count()?></span>
                    </a>
				</div>
                <div class="btn-group" role="group">
                    <a href="<?= Url::home(); ?>stormer/projectconfirmed?type=customer" class="btn btn-introd">
                        Customer projects <span class="badge"><?=\app\models\Projectstormer::find()->joinWith('project')->where(['project_stormer.stormer_id' => \Yii::$app->user->id,'project_stormer.confirmation'=>1,'project_stormer.status'=>0,'projects.project_type'=>'customer'])->count()?></span>
                    </a>
                </div>
                <div class="btn-group" role="group">
                    <a href="<?= Url::home(); ?>stormer/projectconfirmed?type=peer" class="btn btn-introd">
                        Peer projects <span class="badge"><?=\app\models\Projectstormer::find()->joinWith('project')->where(['project_stormer.stormer_id' => \Yii::$app->user->id,'project_stormer.confirmation'=>1,'project_stormer.status'=>0,'projects.project_type'=>'Peer'])->count()?></span>
                    </a>
                </div>
                <div class="btn-group" role="group">
                    <a href="<?= Url::home(); ?>stormer/projectconfirmed?type=open" class="btn btn-introd">
                        Open projects <span class="badge"><?=\app\models\Projectstormer::find()->joinWith('project')->where(['project_stormer.stormer_id' => \Yii::$app->user->id,'project_stormer.confirmation'=>1,'project_stormer.status'=>0,'projects.project_type'=>'Open'])->count()?></span>
                    </a>
                </div>
                <div class="btn-group" role="group">
                    <a href="<?= Url::home(); ?>stormer/projectconfirmed?type=ngo" class="btn btn-introd">
                        NGO projects <span class="badge"><?=\app\models\Projectstormer::find()->joinWith('project')->where(['project_stormer.stormer_id' => \Yii::$app->user->id,'project_stormer.confirmation'=>1,'project_stormer.status'=>0,'projects.project_type'=>'NGO'])->count()?></span>
                    </a>
                </div>
				<div class="btn-group" role="group">
					<a href="/stormer/past_project" class="btn btn-introd">
                        Finished Projects <span class="badge"><?=\app\models\Projectstormer::find()->where(['stormer_id' => \Yii::$app->user->id,'confirmation'=>1,'status'=>1])->count()?></span>
                    </a>
				</div>
			</div>
		</div>

		<div class="project-list-wrap">
			<div class="container-fluid">
				<div class="row list-project">
						<?php foreach($modelProjects as $projects){ ?>
							<div class="project-container">
								<h3 class="project-title"><?= $projects->title; ?></h3>
								<hr>
								<div class="content">
									<div class="col-md-9">
										<div class="description">
											<div><?= $projects->description; ?></div>
										</div>
									</div>
									<div class="col-md-offset-9 col-md-3">
										<div class="btn-wrap">
											<?= HTML::a('Behind the Scenes','/stormer/behindthescenes/'.$projects->id,['class'=>'btn btn-prj']); ?>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						<?php } ?>
						<div class="col-sm-12 text-center">
							<?= LinkPager::widget(['pagination'=>$pagination]); ?>
						</div>
					</div>
			</div>
		</div>
	</div>
</div>