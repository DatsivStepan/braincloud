<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\Alert;
use app\assets\ProjectAsset;
ProjectAsset::register($this);

?>

<div class="awaiting-approval-page">
	<div class="container">
		<h1 class="page-title">Awaiting Approval</h1>
		<hr>
		<h4 class="page-subtitle">Thanks for contributing to the community, your project has been sent to the administrators for approval, you will be notified once the admin has made a decision, and the project will go live immediately.</h4>
		<?= HTML::a('Take Me Home',Url::home().'stormer/'.\Yii::$app->user->identity->username,['class' => 'btn btn-take']); ?>
	</div>
</div>
