<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Stormer';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="stormer-last-register-steps">
	<div class="container containerBlock">
	<?php if($step == 3){ ?>
		<h1 class="page-title">Get a Head Start </h1>
		<h4 class="page-subtitle">Earn some <span class="titleDesign" data-placement="bottom" data-toggle="tooltip" title="<h4>What are points? </h4><p>Content: Points are how you will be noticed and found on the system. You earn points by solving problems. You’ll be rewarded points for participating in social innovation projects, peer projects, posting projects, and of course, taking part in customer projects, as well as other extras you can do here and there. The points are a declaration of your problem solving ability; the more you earn, the more you’ll be selected by customers to participate in paid projects, and the more money you’ll earn.</p>">Points</span> and Give Yourself a Competitive Advantage</h4>
		<p class="content">
			To get you started, we have 3 random puzzles to give to you. Your accurateness and speed to 
			solve them is being tested. You have nothing to lose by participating, but if you would rather 
			not do them, you can choose to skip them.
		</p>
		<div class="form-wrap">
			<div class='row'>
				<div class="col-md-offset-2 col-md-4">
					<?php
						$form = ActiveForm::begin(['id' => 'form-signup',
							'options' => ['class' => 'form-horizontal'],
							'fieldConfig' => [
								'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-7\">{error}</div>",
								'labelOptions' => ['class' => 'col-lg-2 control-label'],
							],
						]);
					?>
					<?= Html::submitButton('Skip', ['class' => 'btn btn-skip', 'name' => 'skip']) ?>
					<?php ActiveForm::end(); ?>
				</div>
				<div class="col-md-4">
					<button class='btn btn-challenge ChallengeButton'>I Love Challenge!</button>
				</div>
			</div>
		</div>


		<div class="ChallengeBlok" style="display:none;">
			<span class="challengeStep" style="display:none;">0</span>
			<div class="row">
				<div class='col-sm-12'>
					<div class="col-sm-12 challenge"></div>
					<span class="result1" style="display:none;"></span>
					<span class="result2" style="display:none;"></span>
					<span class="result3" style="display:none;"></span>
					<div class="ChallengeFinish" style="display:none;">
						<?php
						$form = ActiveForm::begin(['id' => 'form-signup',
							'options' => ['class' => 'form-horizontal'],
							'fieldConfig' => [
									'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-7\">{error}</div>",
									'labelOptions' => ['class' => 'col-lg-2 control-label'],
							],
						]); ?>
							<p>Congratulations! You now have <span id="view_point"> </span> points! Don’t stop now, continue your world domination by earning more!</p>
							<input type="hidden" name="challengePoint">
						<?= Html::submitButton('Next Challenge!', ['class' => 'btn btn-next-challenge', 'name' => 'skip']) ?>
						<?php ActiveForm::end(); ?>
					</div>
				</div>
			</div>
		</div>
		<?php }elseif($step == '4'){?>
			<h1 class="page-title">Recommend a Stormer</h1>
			<h4 class="page-subtitle">Know any good problem-solvers? Invite them and earn 20 points for each person that joins.</h4>
			<div class="form-wrap">
				<div class='row'>
					<div class="col-md-offset-3 col-md-6">
						<?php
							$form = ActiveForm::begin(['id' => 'form-signup',
								'options' => ['class' => 'form-horizontal','enctype' => 'multipart/form-data'],
								'fieldConfig' => [
										'template' => "{label}\n<div class=\"col-lg-12\">{input}</div>\n<div class=\"col-lg-12\">{error}</div>",
										'labelOptions' => ['class' => 'col-lg-2 control-label'],
								],
							]); ?>
							<div class="form-group">
								<label for="exampleInputEmail1">Email address</label>
								<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email" name="email">
							</div>
							<div class="form-group">
								<label for="exampleInputFile">Upload Contact List (<a href="/Example.csv">dowload exemample file</a>)</label>

							   <?= $form->field($file,'file')->fileInput()->label(false)?>
							</div>
		<!--                <div class="form-group">-->
		<!--                    <label for="exampleInputSocial">Social media friends list</label>-->
		<!--                    <textarea class="form-control" rows="3" name="social_f" id="exampleInputSocial"></textarea>-->
		<!--                </div>-->
							<?= Html::submitButton('Next', ['class' => 'btn btn-next', 'name' => 'recommend_a_stormer']) ?>
							<?php ActiveForm::end(); ?>
					</div>
				</div>
			</div>
		<?php }elseif($step == '5'){?>
			<h1 class="page-title">Tell Your Friends about Us</h1>
			<h4 class="page-subtitle">Let your friends know about us by posting this message on your social media page. Earn 5 points for each time and account you post it.</h4>
			<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-offset-3 col-sm-6">
						<div class="tell-friends">
							<img class="img-responsive final-img" src="/image/StormerSocialMedia_small.jpg"/>
							<div class="social-icon-wrap">
								<h3 class="details">Share With</h3>
								<a class="fb-ic" title='Share with facebook' onclick="add_point_social('<?='http://www.facebook.com/share.php?u='.\yii\helpers\Url::home(true).'tell_word.html'?>')"  href="<?='http://www.facebook.com/share.php?u='.\yii\helpers\Url::home(true).'social_reg.html'?>" target="_blank"></a>
								<a class="twt-ic" title='Share with twitter' onclick="add_point_social('<?='https://twitter.com/intent/tweet?url='.\yii\helpers\Url::home(true).'tell_word.html&via=braincloud'?>')" href="<?='https://twitter.com/intent/tweet?url='.\yii\helpers\Url::home(true).'social_reg.html&via=braincloud'?>" target="_blank"></a>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="col-sm-12">
						<?php if (!$sum_point) {?>
							<div class="message-wrap">
								Oh dear! It seems that you didn't get any correct! But that doesn't matter! As they say, "a journey of a thousand miles begins with a single step!" Take your next step, and continue on your path to world domination!
							</div>
						<?php } elseif ($sum_point>=15) {?>
							<div class="message-wrap">
								Alright! You now have <?=$sum_point?> points! You are now a fully-pledged stormer! That should give you a head start! Now let’s get you solving some real problems!
							</div>
						<?php } elseif ($sum_point<15) {?>
							<div class="message-wrap">
								You have <?=$sum_point?> points. A little bit of a slow start, but that doesn’t mean anything! You can still participate in our events and stormer challenges to get yourself in the top stormers!
							</div>
						<?php } ?>
					</div>
		
					<?php
					$form = ActiveForm::begin(['id' => 'form-signup',
						'options' => ['class' => 'form-horizontal'],
						'fieldConfig' => [
							'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-7\">{error}</div>",
							'labelOptions' => ['class' => 'col-lg-2 control-label'],
						],
					]); ?>
					<?= Html::submitButton('Next!', ['class' => 'btn btn-next', 'name' => 'end_signup']) ?>
					<?php ActiveForm::end(); ?>
				</div>
			</div>

		<?php } ?>
	</div>
	
</div>
<script>
	function add_point_social(url){
		$.post(
				'/api/addpoints',
				{type:'social_tell_word', point:5},
				function($result){
					if($result){
//						window.open(url,'_blank');
						location.reload();
					}else{
						alert('error');
					}
				}
		)
	}
</script>