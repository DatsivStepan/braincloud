<?php
/**
 *
 */
//var_dump($frends_user)
?>
<style>
	.summary{
		display: none;
	}
</style>

<div class="my-rankings-page">
	<div class="container containerBlock">
		<h1 class="page-title">My Rankings</h1>
		<hr>
		<h4 class="details">My global position <?=$position_all?></h4>
		<h4 class="details">My geographical position <?=$position_location?></h4>
<!--		<h4 class="details">My friends --><?//=$position_frend?><!--</h4>-->
	<!--        <a href="--><?//=$loginUrl?><!--">Frends</a>-->
		<div class="tab-panels-wrap">
			<div class="panel panel-default">
				<div class="panel-body">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab">All stormer rankings</a></li>
						<li role="presentation"><a href="#week" aria-controls="week" role="tab" data-toggle="tab">Week stormer rankings</a></li>
						<li role="presentation"><a href="#day" aria-controls="day" role="tab" data-toggle="tab">Day stormer rankings</a></li>
						<li role="presentation"><a href="#location" aria-controls="location" role="tab" data-toggle="tab">Location stormer rankings</a></li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="all">
							<?= \kartik\grid\GridView::widget([
								'dataProvider' => $dataProvider,
								'rowOptions' => function ($model) {
									if ($model->id == Yii::$app->user->id) {
										return ['style' => 'background-color:#dff0d8;'];
									}
								},
								'columns' => [
									['class' => 'yii\grid\SerialColumn'],
									'username',
									'level',
									[
										'label'=>'Points',
										'content'=> function($model){
											$sum = 0;
											foreach($model->points as $point){
												$sum+=$point->point;
											}
											return $sum;
										}
									]
								],
							]); ?>
						</div>
						<div role="tabpanel" class="tab-pane" id="week">
							<table class="table table-bordered">
								<thead>
								<tr>
									<th>#</th>
									<th>Username</th>
									<th>Level</th>
									<th>Points</th>
								</tr>
								</thead>
								<tbody>
								<?php
								$k=0;
								foreach($week_user as $user){
									$k++;
									?>
								<tr <?=($user['id']==Yii::$app->user->id)?'style="background-color:#dff0d8;"':''?>>
									<th scope="row"><?=$k?></th>
									<td><?=$user['username']?></td>
									<td><?=$user['level']?></td>
									<td><?=$user['sum_point']?></td>
								</tr>
								<?php }?>
								</tbody>
							</table>
						</div>
						<div role="tabpanel" class="tab-pane" id="day">
							<table class="table table-bordered">
								<thead>
								<tr>
									<th>#</th>
									<th>Username</th>
									<th>Level</th>
									<th>Points</th>
								</tr>
								</thead>
								<tbody>
								<?php
								$k=0;
								foreach($day_user as $user){
									$k++;
									?>
									<tr <?=($user['id']==Yii::$app->user->id)?'style="background-color:#dff0d8;"':''?>>
										<th scope="row"><?=$k?></th>
										<td><?=$user['username']?></td>
										<td><?=$user['level']?></td>
										<td><?=$user['sum_point']?></td>
									</tr>
								<?php }?>
								</tbody>
							</table>
						</div>
						<div role="tabpanel" class="tab-pane" id="location">
							<table class="table table-bordered">
								<thead>
								<tr>
									<th>#</th>
									<th>Username</th>
									<th>Level</th>
									<th>Points</th>
								</tr>
								</thead>
								<tbody>
								<?php
								$k=0;
								foreach($location_user as $user){
									$k++;
									?>
									<tr <?=($user['id']==Yii::$app->user->id)?'style="background-color:#dff0d8;"':''?>>
										<th scope="row"><?=$k?></th>
										<td><?=$user['username']?></td>
										<td><?=$user['level']?></td>
										<td><?=$user['sum_point']?></td>
									</tr>
								<?php }?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
