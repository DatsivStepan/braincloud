<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\Alert;
use app\assets\ProjectAsset;
ProjectAsset::register($this);

?>
<div class="post-project-page">
	<div class="title-wrap">
		<h1 class="page-title">Post a Project and Become an Active Member of the Community</h1>
		<h4 class="page-subtitle">Please remember that projects should be related to non-workplace/profit related issues. To read terms and agreements, please <?php echo HTML::a(\Yii::t('app', 'click here'), '/terms', []); ?>.</h4>
		<hr class="hr">
		<h4 class="title-details">If you’re a workaholic and have no idea what could be innovated in your life outside of work, <a href="#" data-toggle="modal" data-target="#myModal">click here for some inspiration</a></h4>
	</div>
	<div class="post-project-form-wrap">
		<div class="container">
			<div class="row">
				<?php $form = ActiveForm::begin(); ?>
					<div class="col-md-6">
						<?= $form->field($modelNewProject, 'title')->textInput(['maxlength' => true, 'placeholder'=>'Eg. Living Room Design'])->label('Project Title'); ?>
						<div id="actions" class="row">
							<div class="col-sm-12 text-left">
								<!-- The fileinput-button span is used to style the file input field as button -->
								<div class="col-sm-4 col-md-6 col-lg-5 btn-col-left">
									<span class="btn btn-add-image fileinput-button">
										<i class="glyphicon glyphicon-plus"></i>
										<span>Add image or files...</span>
									</span>
								</div>
								<div class="col-sm-4 col-md-6 btn-col-right">
									<button type="reset" class="btn btn-cancel cancel">
										<i class="glyphicon glyphicon-ban-circle"></i>
										<span>Cancel upload</span>
									</button>
								</div>
							</div>
							<div class="col-sm-12">
								<!-- The global file processing state -->
								<span class="fileupload-process">
									<div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
										<div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
									</div>
								</span>
							</div>
						</div>
						<div class="dropzonefile">
							Put your Files Here
						</div>
					</div>
					<div class="col-md-6">
						<label>
							Project Description
							<a class="infoButtonShow">i</a>
							<div class="infoBlockShow">
								<div class="arrow"></div>
								<h5>Describe the situation you want your team to focus on as clearly as you can. To get really creative answers, try to open your project to people with different fields of knowledge. </h5>
								<p>Consider:</p>
								<p>1.Can you remove any knowledge-specific lingo?</p>
								<p>2.Are the instructions clear? If you asked someone with no prior knowledge to the project or situation to read this description, would they understand?</p>
								<p>3.Are there any other limiting factors you can remove? The more limits you place on the project, the more limited your range of answers will be. Knowledge can sometimes be removed for the creative-thinking stage, and then applied again for the application stage.</p>
							</div>
						</label>
						<?= $form->field($modelNewProject, 'description')->textarea(['rows' => 6, 'placeholder'=>'Eg. Should I make all my walls the same color, or …'])->label(false); ?>
					</div>
					<div class="col-md-12">
						<div class="table table-striped files create" id="previews">
							<div id="template" class="file-row">
								<!-- This is used as the file preview template -->
								<div class="img-preview">
									<span class="preview"><img data-dz-thumbnail /></span>
									<p class="name" data-dz-name></p>
									<strong class="error text-danger" data-dz-errormessage></strong>
									<p class="size" data-dz-size></p>
								</div>
								<div class="add-description text-left">
									<input type="text" class="form-control exp" placeholder="Brief description of file" name="Files[explanation][]" value="" required="">
									<input type="hidden" class="path" name="Files[path][]" value="" >
									<span class="instruction">
										In the interest of data protection, as well as server space, any file you upload will be deleted automatically, 2 weeks after the project has ended.
										<a class="infoButtonShow">Support..</a>
										<div class="infoBlockShow">
											Add images or files to help your stormers better understand 
											the project. If you’re having difficulty or technical problems,
											please <a href="#">click here</a> and we’ll be in contact as soon as we possibly can.
										</div>
									</span>
								</div>
								<div class="delete-wrap">
									<a data-dz-remove class="btn delete">
										<i class="glyphicon glyphicon-trash"></i>
										<span>Delete</span>
									</a>
								</div>
								<div>
									<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
										<div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="row">
							<div class="col-sm-6">
								<label>Add links</label>
								<input type="text" class="form-control" name="link_name" placeholder="Link">
							</div>
							<div class="col-sm-6">
								<a id="add_link" class="btn btn-add-link">
									<i class="glyphicon glyphicon-plus"></i>
									Add</a>
							</div>
							<div class="col-sm-12">
								<div class="linkContainer">
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="form-group">
						<?= Html::submitButton($modelNewProject->isNewRecord ? 'Review and Confirm' : 'Update', ['class' => $modelNewProject->isNewRecord ? 'btn btn-success create' : 'btn create']) ?>
					</div>
				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>
	<div class="modal modal-some-inspiration fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Some inspiration</h4>
					<hr>
				</div>
				<div class="modal-body">
					<p class="title">Stuck for Ideas? How about these as a little inspiration?</p>
					<br>
					<p>“What can I get for my wife’s birthday?”</p>
					<p>“Adding a hidden flavour layer to a pumpkin pie…”</p>
					<p>“Which colors or fairing design would suit my motorcycle the best?”</p>
					<p>“Which color should I paint my living and dining room?”</p>
					<p>“What would make an awesome birthday party for a 9-year old who loves Harry Potter?”</p>
					<p>“What’s the most fun thing I can do with a kid for less than $10?”</p>
					<p>“What’s the cheapest meals you can make?”</p>
					<p>“What would be the best way to use BrainCloud to improve my life?”</p>
					<br>
					<p>Got a better problem that needs solving? Post it now and have it solved by geniuses from around the world!</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
<script>
	function delete_data(id){
		$('#'+id).remove();
	}
	function delete_link(elment){
		$( elment ).parent().parent().remove();
	}
</script>
