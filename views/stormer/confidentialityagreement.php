<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

//$this->title = 'Stormer';
//$this->params['breadcrumbs'][] = $this->title;
?>

<div class="confidentiality-page">
	<div class="container">
		<h1 class="page-title">Confidentiality Agreement</h1>
		<hr>
		<h4 class="details">Before we look at the project details, there’s one legal requirement we must first agree on.</h4>
		<h3 class="header">Confidentiality agreement between client and stormer</h3>
		<p class="content">
			<a class="btn-modal" href="" data-toggle="modal" data-target="#modal-confidentiality">Confidentiality agreement</a>
		</p>
		<?php $form = ActiveForm::begin(['id' => 'form-signup', 'options' => ['class' => 'form-horizontal'] ]); ?>
			<?= Html::submitButton('Accept and Continue', ['class' => 'btn btn-accept', 'name' => 'Confirm']) ?>
		<?php ActiveForm::end(); ?>
	</div>
</div>

<div class="modal fade" id="modal-confidentiality" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Stormers Confidentiality Agreement</h4>
				<hr>
			</div>
			<div class="modal-body">
				<p>
					This agreement applies to all users registered stormers (the “Stormer”) at the website www.braincloud.solutions (the “Site”).
				</p>
				<p>
					In connection with terms and conditions agreed by the Stormer on participation in activities at the Site, 
					all information, including but not limited to the Site design, wording, images, tools, systems, programs, 
					software, documents, materials, methods of operation, business contacts, pricing lists and information 
					regarding projects the Stormer is participating (the Proprietary Information) will be made available to the Stormer.
				</p>
				<p>
					Stormer acknowledges the proprietary and confidential nature of the Proprietary Information.
				</p>
				<p>
					Stormer agrees, during and after the term of his/her being a registered stormer, 
					not to disclose to anyone or make use in any way of the proprietary information.
				</p>
				<p>
					Stormer also agrees not to copy, take or retain any of the proprietary information except as required in line with 
					stormer’s duties at the Site and except for any of the proprietary information that has lawfully entered public domain.
				</p>
				<p>
					In case the stormer fails to comply with this Agreement, the stormer takes full legal responsibilities.
				</p>
				<p>
					This Agreement expresses the complete understanding of both parties with respect to the subject matter and 
					supersedes all prior proposals, agreements, representations, and understandings.
				</p>
				<p>
					This Agreement shall remain in effect until the Proprietary Information no longer qualifies as one or until 
					the Site sends the Stormer written notice releasing the Stormer from this Agreement, whichever occurs first.
				</p>
				<p>
					If a court finds any provision of this Agreement invalid or unenforceable, the remainder of this Agreement 
					shall be interpreted so as best to effect the intent of the parties.
				</p>
				<p>
					The failure to exercise any right provided in this Agreement shall not be a waiver of prior or subsequent rights.
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->