<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
?>
    <?php    
    
        $class = (($this->context->getRoute() == 'admin/news') ||  ($this->context->getRoute() == 'admin/onenews'))?'active':''; 
        $class1 = ($this->context->getRoute() == 'admin/flag')?'active':'';
        $class2 = ($this->context->getRoute() == 'admin/pay_stormer')?'active':'';
        $class3 = ($this->context->getRoute() == 'admin/flaggingideas')?'active':''; 
        $class4 = ($this->context->getRoute() == 'admin/faqs')?'active':''; 
        $class5 = ($this->context->getRoute() == 'admin/pages')?'active':'';
        $class6 = ($this->context->getRoute() == 'admin/projectquestion')?'active':'';
        $class7 = ($this->context->getRoute() == 'admin/projects')?'active':'';
        $class7 = ($this->context->getRoute() == 'admin/projectsstormer')?'active':'';
        $class7 = ($this->context->getRoute() == 'admin/projectsngo')?'active':'';
        $class7 = ($this->context->getRoute() == 'admin/projectsopen')?'active':'';
        $class8 = ($this->context->getRoute() == 'admin/feedback')?'active':'';
        $class9 = ($this->context->getRoute() == 'admin/discountproject')?'active':'';
        $class10 = ($this->context->getRoute() == 'admin/contact')?'active':'';
        $class11 = ($this->context->getRoute() == 'admin/timeline_inquire')?'active':'';
    ?>
    
    <span class="menu-burger">
        Menu
        <i class="fa fa-caret-down" aria-hidden="true"></i>
    </span>
    <ul class="list-menu">
        <li class="<?= $class; ?>">
            <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-newspaper-o"></i><span class="item-text">News</span>'), '/admin/news'); ?>
        </li>

        <li class="<?= $class7; ?>">
            <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-server"></i><span class="item-text">Projects</span>'), '/admin/projects'); ?>
        </li>
        
<!--        <li class="--><?//= $class3; ?><!--">-->
<!--            --><?php //echo HTML::a(\Yii::t('app', '<i class="fa fa-comments"></i>Ideas'), '/admin/flaggingideas'); ?>
<!--        </li>-->
        
        <li class="<?= $class1; ?>">
            <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-flag"></i><span class="item-text">Flag</span>'), '/admin/flag'); ?>
        </li>
        
        <li class="<?= $class2; ?>">
            <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-user-plus"></i><span class="item-text">Pay stormer</span>'), '/admin/pay_stormer'); ?>
        </li>
        
        <li class="<?= $class4; ?>">
            <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-user-plus"></i><span class="item-text">Faqs</span>'), '/admin/faqs'); ?>
        </li>
        
        <li class="<?= $class5; ?>">
            <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-user-plus"></i><span class="item-text">Pages</span>'), '/admin/pages'); ?>
        </li>
        
        <li class="<?= $class6; ?>">
            <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-question" style="font-size:8px;"></i><span class="item-text">Project question</span>'), '/admin/projectquestion'); ?>
        </li>



        <li class="<?= $class8; ?>">
            <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-line-chart"></i><span class="item-text">Feedback</span>'), '/admin/feedback'); ?>
        </li>
        <li class="<?= $class9; ?>">
            <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-user-plus" style="font-size:8px;"></i><span class="item-text">Discount Project</span>'), '/admin/discountproject'); ?>
        </li>
        <li class="<?= $class10; ?>">
            <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-user-plus"></i><span class="item-text">Contact us</span>'), '/admin/contact'); ?>
        </li>
        <li class="<?= $class11; ?>">
            <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-user-plus"></i><span class="item-text">Timeline inquire</span>'), '/admin/timeline_inquire'); ?>
        </li>
    </ul>
            
            
            
