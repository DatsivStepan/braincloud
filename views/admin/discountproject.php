<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\assets\AdminAsset;
use yii\bootstrap\ActiveForm;
AdminAsset::register($this);
/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="dashboard-container">

    <div class="container">
        <div id="cssmenu">
            <?php echo $this->render('menu'); ?>
        </div>
        <div class="sub-nav hidden-sm hidden-xs">
            <ul>
                <li>
                    <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-home"></i> Home </a><a> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;">Discount Project'), '/admin/discountproject',['class'=>'AdminHomePageLink']); ?>
                </li>
            </ul>
        </div>

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
            <div class="projects-index">
                <p>
                    <?= Html::a('Create Discount Project', ['discountproject/create'], ['class' => 'btn btn-success']) ?>
                </p>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

//                        'id',
                        'count',
                        'discount',

                        ['class' => 'yii\grid\ActionColumn',
                            'template'=>'{update}{delete}',
                            'buttons' => [
                                'update' => function ($url, $model, $key) {
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>','/discountproject/update?id='.$model->id);
                                },
                                'delete' => function ($url, $model, $key) {
                                    return Html::a('<span class="glyphicon glyphicon-trash"></span>','/discountproject/delete?id='.$model->id);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
