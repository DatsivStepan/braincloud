<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\assets\Project2Asset;
Project2Asset::register($this);

?>
<div class="projects-create">
  <div class="containerBlock ">
      <h1>Post Project Stage II</h1>
      <?php $form = ActiveForm::begin(); ?>
      
      <?php if($type == 'target2'){ ?>
            <div class="col-sm-12" style="margin-top:20px">
                <?= $form->field($model, 'perspectives')->textarea(['rows' => 6]) ?>
                <a class="avtomaticalPerspectiveQuestion pull-right btn btn-primary pull-right">Avtomatical</a>
            </div>
            <div class="col-sm-12" style="margin-top:20px">
                <div class="col-sm-7">
                    <div class="col-sm-9">
                        <?= $form->field($model, 'question_convergent')->dropDownList($question_convergent,['prompt' => 'Select Convergent Questions','class'=>'form-control question_convergent']) ?>
                    </div>
                    <div class="col-sm-3">
                        <a class="avtomaticalConvergent btn btn-primary" style="margin-top:24px;">Avtomatical</a>
                    </div>
                </div>
                <div class="col-sm-5">
                  <p><b>Example</b></p>
                  <span class="example_convergent"><span>
                </div>
            </div>
            <div class="col-sm-12" style="margin-top:20px">
                <div class="col-sm-7">
                    <div class="col-sm-9">
                        <label>Question divergent
                            <a style="cursor:pointer;" class="titleDesignProject"  data-toggle="tooltip" title="<p style='font-size:14px;'>
                            The instructions are the heart of an innovation system. You can set the system to ask any questions you feel would represent your particular project. Hover your mouse over the question titles for examples. You can also press the automate button, which will set the system to a general set of questions that give the best all-round results. 
                           </p>" >Support..</a>
                        </label>
                        <?= $form->field($model, 'question_divergent')->dropDownList($question_divergent,['prompt' => 'Select Divergent Questions','class'=>'form-control question_divergent'])->label(false) ?>
                    </div>
                    <div class="col-sm-3">
                        <a class="avtomaticalDivergent btn btn-primary" style="margin-top:24px;">Avtomatical</a>
                    </div>
                </div>
                <div class="col-sm-5">
                  <p><b>Example</b></p>
                  <span class="example_divergent"><span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-12">
                <label>Custom questions
                  <a style="cursor:pointer;" class="titleDesignProject"  data-toggle="tooltip" title="<p style='font-size:14px;'>
                      Here, you can add any extra question that you feel is necessary.  
                      Eg. “How would that impact the business?”
                      This is not a compulsory section, the system will still perform very well by leaving this blank. 
                 </p>" >Support..</a>
                </label>
                <?= $form->field($model, 'question_custom')->textInput()->label(false); ?>
                </div>
            </div>
      <?php } ?>
            <div class="dropzonefile">
              Put your Files Here
            </div>
        <div id="actions" class="col-sm-12">
            <div class="col-lg-7">
               <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Add image or files...</span>
                </span>
                <button type="reset" class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel upload</span>
                </button>
            </div>

            <div class="col-lg-5">
               <!-- The global file processing state -->
                <span class="fileupload-process">
                    <div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                        <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                    </div>
                </span>
            </div>
        </div>


        <div class="table table-striped files" id="previews">
            <div id="template" class="file-row">
              <!-- This is used as the file preview template -->
              <div>
                  <span class="preview"><img data-dz-thumbnail /></span>
                  <p class="name" data-dz-name></p>
                  <strong class="error text-danger" data-dz-errormessage></strong>
                      <p class="size" data-dz-size></p>
              </div>
              <div>
                  <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                    <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                  </div>
              </div>
               <div>
                   <input type="text" class="form-control exp" placeholder="Brief description of file" name="Files[explanation][]" value="" required="">
                   <input type="hidden" class="path" name="Files[path][]" value="" >
                   In the interest of data protection, as well as server space, any file you upload will be deleted automatically, 2 weeks after the project has ended.
                   <a class="infoButtonShow">Support..
                   </a>
                   <div class="infoBlockShow" style="width:80% !important;">
                       Add images or files to help your stormers better understand 
                       the project. If you’re having difficulty or technical problems,
                       please <a href="#" onclick="send_mail_admin()">click here</a> and we’ll be in contact as soon as we possibly can.
                   </div>
               </div>
               <div>
                   <a data-dz-remove class="btn btn-danger delete">
                      <i class="glyphicon glyphicon-trash"></i>
                      <span>Delete</span>
                   </a>
               </div>
            </div>
        </div>

        <div class="col-sm-12">
            <h3>Add links</h3>
            <div class="col-sm-12">
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="link_name" placeholder="Link">
                </div>
                <div class="col-sm-4">
                    <a id="add_link" class="btn btn-success">+ Add</a>
                </div>
            </div>
            <div class="col-sm-12 linkContainer">
            </div>
        </div>
      
        <div class="form-group">
           <?= Html::submitButton('Create', ['class' => 'btn btn-primary create pull-right']) ?>
        </div>
      
      <?php ActiveForm::end(); ?>
      <div style="clear:both;"></div>
    </div>
</div>
