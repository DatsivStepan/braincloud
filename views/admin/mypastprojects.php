<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\Alert;

$this->title = 'My Projects: past projects';
?>
<div class="container containerBlock">
    <div class="col-sm-12" style="text-align: center">
        <h1>My Projects</h1>
        <h4>Information related to your ongoing and past projects. </h4>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <ul class="nav nav-tabs">
                <li role="presentation">
                    <?= HTML::a('Current Projects',Url::home().'admin/myprojects/current'); ?>
                </li>
                <li role="presentation" class="active">
                    <?= HTML::a('Past Projects',Url::home().'admin/myprojects/past'); ?>
                </li>
            </ul>
        </div>
        <div class="col-sm-12">
            <h2 style="text-align: center">Past Projects</h2>
            <div class="row">
                <?php foreach($modelProjectsClose as $projects){?>
                    <div class="col-sm-12" style="padding-bottom: 10px;margin-bottom: 10px; border:1px solid #e2e2e2;">
                        <h1><?= $projects->title; ?></h1>
                        <b>status</b> Closed <br>
                        <b>Your Team</b> - 
                        <?php foreach($projects->team as $userModel){
                            echo $userModel['username'].'; ';
                        }; ?><br>
                        <?= HTML::a('Organize and Export Ideas','/admin/organizeidea/'.$projects->id,['class' => 'btn btn-primary']); ?>
                    </div>
                <?php } ?>
                <div class="col-sm-12">
                    <?= LinkPager::widget(['pagination'=>$pagination]); ?>
                </div>
            </div>
        </div>
    </div>
</div>