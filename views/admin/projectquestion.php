<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\assets\AdminAsset;
use yii\widgets\LinkPager;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\Alert;

AdminAsset::register($this);

?>
<div class="dashboard-container">

      <div class="container">
        <div id="cssmenu">
            <?php echo $this->render('menu'); ?>
        </div>
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-home"></i> Home </a><a> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;">Project question'), '/admin/projectquestion',['class'=>'AdminHomePageLink']); ?>
            </li>
          </ul>
        </div>

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
          <?php
                if(Yii::$app->session->hasFlash('question_added')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-info',
                        ],
                        'body' => 'Question added',
                    ]);
                endif; 
                if(Yii::$app->session->hasFlash('question_not_added')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-error',
                        ],
                        'body' => 'Question not added',
                    ]);
                endif; 
            ?>
            
            <?php
                if(Yii::$app->session->hasFlash('question_update')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-info',
                        ],
                        'body' => 'Question update',
                    ]);
                endif; 
                if(Yii::$app->session->hasFlash('question_not_update')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-error',
                        ],
                        'body' => 'Question not update',
                    ]);
                endif; 
            ?>
            
            <?php
                if(Yii::$app->session->hasFlash('question_delete')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-info',
                        ],
                        'body' => 'Question delete',
                    ]);
                endif; 
                if(Yii::$app->session->hasFlash('question_not_delete')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-error',
                        ],
                        'body' => 'Question not delete',
                    ]);
                endif; 
            ?>
          <!-- Row Start -->
          <div class="row wrap" >
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="widget">
                <div class="widget-header">
                  <div class="title" style="height:40px;">
                    <i class="fa fa-arrow-down boxClick" data-action="show"> </i> Add Question
                    <span class="mini-title displayNone">
                        
                    </span>
                  </div>
                </div>
                <div class=" widget-body boxShow" <?=(Yii::$app->session->hasFlash('ErrorValidating'))?'':'style="display:none;"'?>>
                    <div class="row">
                        <div class="col-sm-12">
                            <?php $form = ActiveForm::begin(['id' => 'form-signup', 'options' => ['class' => 'form-horizontal']]); ?>
                                <?= $form->field($newModelProjectQuestion, 'question_title'); ?>
                                <?= $form->field($newModelProjectQuestion, 'question_text')->textarea(['rows' => 6]); ?>
                                <?= $form->field($newModelProjectQuestion, 'type')->dropDownList(['divergent' => 'divergent', 'convergent' => 'convergent', 'perspectives' => 'perspectives']); ?>
                                <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'save']) ?>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="row wrap" >
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                          <div class="title" style="height:40px;">
                            <i class="fa fa-arrow-down" data-action="show"> </i> All Question
                            <span class="mini-title displayNone">
                                
                            </span>
                          </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <table class="table">
                                    <tr>
                                        <td><b>#</b></td>
                                        <td><b>Question title</b></td>
                                        <td><b>Question text</b></td>
                                        <td><b>Type</b></td>
                                        <td></td>
                                    </tr>
                                <?php  foreach($modelProjectQuestion as $question){ ?>
                                    <tr>
                                        <td><?= $question['id']; ?></td>
                                        <td><?= $question['question_title']; ?></td>
                                        <td><?= substr($question['question_text'], 0, 400).'...'; ?></td>
                                        <td><?= $question['type']; ?></td>
                                        <td>
                                            <a class="pull-right" onclick="delete_question(<?=$question['id']?>)"><i class='fa fa-times fa-2x' style="color:red;"></i></a>
                                            <a data-toggle="modal" class="pull-right" data-target="#myModal<?= $question['id']; ?>"><i class='fa fa-edit fa-2x' style="color:#3187bf;"></i></a>
                                        </td>
                                    </tr>
                                    
                                    
                                    
                                            <div class="modal fade" id="myModal<?= $question['id']; ?>" role="dialog">       
                                                <div class="modal-dialog" style="width: 70%">
                                            <!-- Modal content-->
                                              <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title" style='color:black;'>Change question</h4>
                                                  </div>
                                                  <div class="modal-body" style='color:black;'>
                                                  <?php $form = ActiveForm::begin(['id' => 'UpdateNews', 'class' => 'form-control', 'method' => 'POST']);?>
                                                          <?= $form->field($question, 'id')->hiddenInput(['value' => $question['id']])->label(false); ?>
                                                          <?= $form->field($question, 'question_title'); ?>
                                                          <?= $form->field($question, 'question_text')->textarea(['rows' => 6]);?>
                                                          <?= $form->field($question, 'type')->dropDownList(['divergent' => 'divergent', 'convergent' => 'convergent', 'perspectives' => 'perspectives']); ?>
                                                          <div class="modal-footer">
                                                              <?php echo Html::submitButton(\Yii::t('app', 'Save'), ['name' => 'updateQuestion','class' => 'btn btn-primary']); ?>
                                                              <?php ActiveForm::end();  ?>
                                                              <button type="button" class="btn btn-danger" data-dismiss="modal"><?= \Yii::t('app','Cancel'); ?></button>
                                                          </div>
                                                  </div>
                                              </div>
                                          </div>
                                          </div> 
                                    
                                    
                                    
                                <?php }  ?>
                                </table>
                                <div class="col-sm-12">
                                    <?= LinkPager::widget(['pagination'=>$pagination]); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
       
          </div>
          <!-- Row End -->

          

        </div>

      </div>
    </div>

<script>
    function delete_question(id){
        swal({
                title: "Are you sure?",
                text: "Are you sure you want to delete this item?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function(){
                window.location = '/admin/delete_question?id='+id;
            });
    }
</script>