<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Projects */

$this->title = 'Target';
$this->params['breadcrumbs'][] = ['label' => 'Profile', 'url' => ['customer/index']];
$this->params['breadcrumbs'][] = ['label' => 'Post Project', 'url' => ['customer/post-project']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="target-project-step1">
	<div class="container">
		<h1 class="page-title">Post Your Project</h1>
		<h2 class="page-subtitle"><?= Html::encode($this->title) ?></h2>
		<hr>
	</div>
	<div class="projects-form-wrap">
		<div class="container">
			<?= $this->render('_form_target', [
				'model' => $model,
				'categories' => $categories,
			]); ?>
		</div>
	</div>
</div>
