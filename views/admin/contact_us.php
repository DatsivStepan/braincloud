<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\assets\AdminAsset;
use yii\bootstrap\ActiveForm;
AdminAsset::register($this);
/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="dashboard-container">

    <div class="container">
        <div id="cssmenu">
            <?php echo $this->render('menu'); ?>
        </div>
        <div class="sub-nav hidden-sm hidden-xs">
            <ul>
                <li>
                    <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-home"></i> Home </a><a> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;">Contact Us'), '/admin/projects',['class'=>'AdminHomePageLink']); ?>
                </li>
            </ul>
        </div>

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
            <div class="projects-index">

                <h1><?= Html::encode($this->title) ?></h1>
                <?php if($message){?>
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Goot!</strong> <?=$message_send?>
                    </div>
                <?php }?>
                <?php if($message_error){?>
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Error!</strong> <?=$message_send?>
                    </div>
                <?php }?>
                <div>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,

                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'username',
                            'email',
                            'subject_mail',
                            'descriptions',
                            'send_me:boolean',
                            'point:boolean',
                            ['class' => 'yii\grid\ActionColumn',
                                'template'=>'{update}{inspired}',
                                'buttons' => [
                                    'update' => function ($url, $model, $key) {
                                        return Html::a('<span class="glyphicon glyphicon-envelope"></span> ','#', [
                                            'onclick'=>'send_modal('.$model->id.',"'.$model->descriptions.'")'
                                        ]);
                                    },
                                    'inspired' => function ($url, $model, $key) {
                                        if ($model->subject_mail=='technical glitch'){
                                            return Html::a('<span class="glyphicon glyphicon-plus"></span>','#', [
                                                'onclick'=>'point_modal('.$model->id.',"'.$model->descriptions.'")'
                                            ]);
                                        }
                                    },
                                ],
                            ],
                        ],
                    ]); ?>

                </div>

            </div>
        </div>
    </div>
</div>
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><button class="close" type="button" data-dismiss="modal">×</button>
                <h4 class="modal-title">Send mail</h4>
            </div>
            <?php $form = ActiveForm::begin()?>
            <div class="modal-body">
                <input type="hidden" name="up_project_id" id="up_project_id" value="">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Descriptions:</label>
                    <textarea class="form-control " id="up_project_title" disabled></textarea>
                </div>
                <div class="form-group">
                    <label for="message-text" class="control-label">Message:</label>
                    <textarea name="message" class="form-control"></textarea>
                </div>
            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" name="send" value="true">Send</button>
            </div><?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<div id="inspired" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><button class="close" type="button" data-dismiss="modal">×</button>
                <h4 class="modal-title">Count point</h4>
            </div>
            <?php $form = ActiveForm::begin()?>
            <div class="modal-body">
                <input type="hidden" name="up_project_inspired_id" id="up_project_inspired_id" value="">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Descriptions:</label>
                    <textarea class="form-control " id="up_project_inspired_title" disabled></textarea>
                </div>
                <div class="form-group">
                    <label for="message-text" class="control-label">Count:</label>
                    <select name="up_project_inspired" class="form-control" >
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
                </div>
            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" name="point" value="true">Ok</button>
            </div><?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<script>
    function send_modal(project_id,title){
        $('#up_project_id').val(project_id);
        $('#up_project_title').text(title);
        $('#myModal').modal('show');
    }

    function point_modal(project_id,title){
        $('#up_project_inspired_id').val(project_id);
        $('#up_project_inspired_title').text(title);
        $('#inspired').modal('show');
    }
</script>