<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\Alert;
use app\assets\CustomerprojectAsset;
CustomerprojectAsset::register($this);

$this->title = 'My Projects: current projects';
?>
<div class="container containerBlock">
    <div class="col-sm-12" style="text-align: center">
        <h1>My Projects</h1>
        <h4>Information related to your ongoing and past projects. </h4>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <ul class="nav nav-tabs">
                <li role="presentation" class="active">
                    <?= HTML::a('Current Projects',Url::home().'admin/myprojects/current'); ?>
                </li>
                <li role="presentation">
                    <?= HTML::a('Past Projects',Url::home().'admin/myprojects/past'); ?>
                </li>
            </ul>
        </div>
        <div class="col-sm-12">
            <h2 style="text-align: center">Current Projects</h2>
            <div class="row">
                <?php //= ; ?>
                <?php foreach($modelProjectsOpen as $projects){?>
                    <div class="col-sm-12" style="padding-bottom: 10px;margin-bottom: 10px; border:1px solid #e2e2e2;">
                        <h3><?= $projects->title; ?>
                            <span class="pull-right">
                                <?php
                                if($projects->count_idea != 0 ){
                                    $one_procent=100/($projects->count_idea*$projects->size_team);
                                    if( $one_procent*\app\models\Ideas::find()->where(['project_id'=>$projects->id])->count() <= 100){
                                        echo Yii::$app->formatter->asPercent(($one_procent*\app\models\Ideas::find()->where(['project_id'=>$projects->id])->count())/100, 1);;
                                    }else{
                                        echo '100%';
                                    };
                                }else{
                                    echo '0%';
                                }

                                ?>
                            </span></h3>
                        
                        <hr>
                        <!--hidden project ideas data-->
                            <input type="hidden" name="ideaUserCount<?= $projects->id; ?>" value="<?= $projects->countIdeaUser; ?>">
                            <input type="hidden" name="ideaCount<?= $projects->id; ?>" value="<?= $projects->count_idea; ?>">
                            <input type="hidden" name="sizeTeam<?= $projects->id; ?>" value="<?= $projects->size_team; ?>">
                            <input type="hidden" name="endDate<?= $projects->id; ?>" value="<?= $projects->end_date; ?>">
                        <!--hidden project ideas data-->
                        
                        <div class="col-sm-12">
                            <h4 style="margin-top: 0px"><b>Status - </b><?php if($projects->active != 0){ echo 'Active'; }else{ echo 'Not active'; } ?></h4>
                        </div>
                        <div class="col-sm-12">
                            <b>Your Team</b> - 
                            <?php foreach($projects->team as $userModel){
                                echo $userModel['username'].'; ';
                            }; ?>
                        </div>
                        <div class="col-sm-12">
                            <?php if($projects->countIdeaUser > 0){ ?>
                                <?= HTML::a('Behind the Scenes','/customer/behindthescenes/'.$projects->id,['class'=>'btn btn-primary']); ?>
                            <?php } ?>
                            <?php if($projects->active == 0){ ?>

                            <?php }else{
                                $customer_info = \app\models\Customerinfo::find()->where(['customer_id'=>Yii::$app->user->id])->one();

                                if ($customer_info->ask == 1){
                                    echo HTML::a('<span class="glyphicon glyphicon-envelope"></span> Messages <span class="badge">new '. \app\models\Message::find()->where(['project_id'=>$projects->id,'status'=>0,'type_user'=>'stormer'])->count().'</span>','/customer/message/'.$projects->id,['class'=>'btn btn-success']);
                                }
                            }

                            ?>
                            <button class="btn btn-primary pull-right closeProject" data-project_id="<?= $projects->id; ?>">Confirm and Close the Project</button>
                            <!--<button class="btn btn-primary pull-right" data-toggle="modal" data-target="#closeProject<?php //= $projects->id; ?>">Confirm and Close the Project</button>-->
                            
                            <div class="modal fade" id="closeProject<?= $projects->id; ?>" role="dialog">       
                                <div class="modal-dialog">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title" style='color:black;'>Confirm and Close the Project</h4>
                                        </div>
                                        <div class="modal-body">
                                            <?php /* $form = ActiveForm::begin(['id' => 'formCloseProject', 'options' => ['class' => 'form-horizontal']]); ?>
                                                <input type="hidden" name="project_id" value="<?= $projects->id; ?>">
                                                <?= Html::submitButton('Close the Project', ['class' => 'btn btn-primary', 'name' => 'buttonCloseProject']); ?>
                                            <?php ActiveForm::end(); */ ?>
                                            <h3>Feedback Score</h3>
                                            <div class="col-sm-12">
                                                <?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal formFeedback']]); ?>
                                                    <?= $form->field($newFeedbackModal, 'project_id')->hiddenInput(['value' => $projects->id])->label(false); ?>
                                                    <label style="text-align: left" class="control-label">Please rate how you found the quality of ideas and solutions in this project (from 1-5 stars)</label>
                                                    <input class="ratingOne" type="number" />
                                                    <?= $form->field($newFeedbackModal, 'ranting_1')->hiddenInput()->label(false); ?>

                                                    <label style="text-align: left" class="control-label">Please rate how you found the quality of user experience (from 1-5 stars)</label>
                                                    <input class="ratingTwo" type="number" />
                                                    <?= $form->field($newFeedbackModal, 'ranting_2')->hiddenInput()->label(false); ?>

                                                    <label style="text-align: left" class="control-label">How likely are you to recommend us to another business, from a score of 1-10 (1 being very unlikely, 10 being very likely)</label>
                                                    <input class="ratingThree" type="number" />
                                                    <?= $form->field($newFeedbackModal, 'ranting_3')->hiddenInput()->label(false); ?>

                                                    <div class="col-sm-12">
                                                        <label>Tell us about your experience so far. We’d love to know! </label>
                                                        <?= $form->field($newFeedbackModal, 'about_experience')->textInput()->label(false); ?>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <label>Would you object to this review being used as a testimonial for our site?</label>
                                                        <?= $form->field($newFeedbackModal, 'question')->radioList(['1' => 'yes', '2' => 'I would or no', '3' => 'I wouldn’t' ])->label(false); ?>
                                                    </div>

                                            </div>
                                                <div style="clear: both"></div>
                                        </div>
                                        <div class="modal-footer">
                                                <a href="/customer/organizeidea/<?=$projects->id?>" type="button" class="btn btn-default" >Close</a>
                                                <?= Html::submitButton('Close the Project', ['class' => 'btn btn-primary buttonCloseProjectRanting', 'name' => 'buttonCloseProjectRanting']); ?>
                                            <?php ActiveForm::end(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            
                        </div>
                    </div>
                <?php } ?>
                <div class="col-sm-12">
                    <?= LinkPager::widget(['pagination'=>$pagination]); ?>
                </div>
            </div>
        </div>
    </div>
</div>