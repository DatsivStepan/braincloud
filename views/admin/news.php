<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\assets\AdminAsset;
use yii\widgets\LinkPager;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\Alert;

AdminAsset::register($this);
?>
<div class="dashboard-container">

      <div class="container">
        <div id="cssmenu">
            <?php echo $this->render('menu'); ?>
        </div>
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-home"></i> Home </a><a> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;">News'), '/admin/news',['class'=>'AdminHomePageLink']); ?>
            </li>
          </ul>
        </div>

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
          <?php
                if(Yii::$app->session->hasFlash('news_added')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-info',
                        ],
                        'body' => 'News added',
                    ]);
                endif; 
                if(Yii::$app->session->hasFlash('news_notadded')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-error',
                        ],
                        'body' => 'News not added',
                    ]);
                endif; 
            ?>
          <!-- Row Start -->
          <div class="row wrap" >
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="widget">
                <div class="widget-header">
                  <div class="title" style="height:40px;">
                    <i class="fa fa-arrow-down boxClick" data-action="show"> </i> Add News
                    <span class="mini-title displayNone">
                        
                    </span>
                  </div>
                </div>
                <div class=" widget-body boxShow" <?=(Yii::$app->session->hasFlash('ErrorValidating'))?'':'style="display:none;"'?>>
                    <div class="row">
                        <div class="col-sm-12">
                            <?php $form = ActiveForm::begin(['id' => 'form-signup', 'options' => ['class' => 'form-horizontal']]); ?>
                                <?= $form->field($newModelNews, 'title'); ?>
                                <?= $form->field($newModelNews, 'category')->dropDownList(['home'=> 'home', 'customer'=>'customer','stormer'=>'stormer']); ?>
                            <?= $form->field($newModelNews, 'notification')->dropDownList(['1'=>'Yes','2'=>'No','3'=>'Notification all users']); ?>
                                <?= $form->field($newModelNews, 'content')->widget(\dosamigos\ckeditor\CKEditor::className(), [
                                    'options' => ['rows' => 6],
                                    'preset' => 'full',
                                    'clientOptions' => [
                                        'language' => 'en',
                                    ]
                                ]) ?>
                                <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'saveNews']) ?>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="row wrap" >
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                          <div class="title" style="height:40px;">
                            <i class="fa fa-arrow-down" data-action="show"> </i> All news
                            <span class="mini-title displayNone">
                                
                            </span>
                          </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <?php foreach($modelNews as $news){ ?>
                                    
                                        <div class="row">
                                            <h2 style="text-align: center">
                                                <a href="" style="color: #337ab7 !important;">
                                                    <?php echo HTML::a(\Yii::t('app', $news->title), '/admin/onenews/'.$news->id,['class' => 'newsTitle']); ?>
                                                </a>
                                            </h2>
                                            <div class="col-sm-12">
                                                <?php echo $news->content; ?>
                                            </div>
                                            <div class="col-sm-12">
                                                <i><b>Posted by Braincloud on <?php echo $news->date_create; ?></b></i>
                                            </div>
                                        </div>
                                        <hr>
                                    
                                <?php } ?>
                                <div class="col-sm-12">
                                    <?= LinkPager::widget(['pagination'=>$pagination]); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
       
          </div>
          <!-- Row End -->

          

        </div>

      </div>
    </div>
 