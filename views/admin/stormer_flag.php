<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\assets\AdminAsset;
use yii\bootstrap\ActiveForm;
AdminAsset::register($this);
/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="dashboard-container">

    <div class="container">
        <div id="cssmenu">
            <?php echo $this->render('menu'); ?>
        </div>
        <div class="sub-nav hidden-sm hidden-xs">
            <ul>
                <li>
                    <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-home"></i> Home </a><a> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;">Projects'), '/admin/projects',['class'=>'AdminHomePageLink']); ?>
                </li>
            </ul>
        </div>

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
            <div class="projects-index">

                <h1><?= Html::encode($this->title) ?></h1>
                <?php if($message){?>
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Ok!</strong> <?=$message_send?>
                    </div>
                <?php }?>

                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="/admin/flag" >Ideas</a></li>
                        <li role="presentation"><a href="/admin/flag_users" >Users</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'label'=>'Customer name',
                            'value'=> 'user.username',
                        ],
                        [
                            'label'=>'Project title',
                            'value'=> 'idea.project.title',
                        ],
                        'idea.ideas_description',
                        [
                            'label'=>'Reason',
                            'value'=>function($model){
                                if ($model->why_flagging==1){
                                    return 'Offensive/gratuitous ';
                                }
                                if ($model->why_flagging==2){
                                    return 'Repeated idea';
                                }

                                return $model->why_flagging;
                            }
                        ],
                        [
                            'label'=>'Stormer name',
                            'value'=>'idea.user.username'
                        ],
                        [
                            'label'=> 'Status',
                            'value'=> function($model){
                                if ($model->idea->user->ban){
                                    if( $model->idea->user->ban->type == '2')
                                        return 'Warning';
                                    else
                                        return 'Banned';
                                }
                                return 'null';
//
                            }
                        ],
                        'created_at:datetime',

                        ['class' => 'yii\grid\ActionColumn',
                            'template'=>'{update}{delete}',
                            'buttons' => [
                                'update' => function ($url, $model, $key) {
                                    return Html::a('<span class="glyphicon glyphicon-lock"></span>  ','#', [
                                        'onclick'=>'update_modal('.$model->idea->user->id.',"'.$model->idea->user->username.'")'
                                    ]);
                                },
                                'delete' => function ($url, $model, $key) {
                                    return Html::a('  <span class="glyphicon glyphicon-remove-circle"></span>','/admin/delete_flag?id='.$model->id);
                                },
                            ],
                        ],
                    ],
                ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><button class="close" type="button" data-dismiss="modal">×</button>
                <h4 class="modal-title">Lock stormer</h4>
            </div>
            <?php $form = ActiveForm::begin()?>
            <div class="modal-body">
                <input type="hidden" name="stormer_id" id="stormer_id" value="">
                <input type="hidden" name="stormer_name" id="stormer_name" value="">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Stormer:</label>
                    <input type="text" name="name" class="form-control " id="name"  disabled>
                </div>
                <div class="form-group">
                    <label for="message-text" class="control-label">Type:</label>
                    <select name="type" class="form-control ">
                        <option value="1">Banned</option>
                        <option value="2">Warning</option>
                    </select>
                </div>
                <?php
                echo '<label>Lock Stormer Date</label>';
                echo \kartik\widgets\DatePicker::widget([
                    'name' => 'date',
                    'value' => date('d-M-Y', strtotime('+2 days')),
                    'options' => ['placeholder' => 'Select issue date ...'],
                    'pluginOptions' => [
                        'format' => 'dd-M-yyyy',
                        'todayHighlight' => true
                    ]
                ]);
                ?>
            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Ok</button>
            </div><?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<script>
    function update_modal(project_id,title){
        $('#stormer_id').val(project_id);
        $('#name').val(title);
        $('#stormer_name').val(title);
        $('#myModal').modal('show');
    }
</script>