<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\assets\AdminAsset;
use yii\bootstrap\ActiveForm;
AdminAsset::register($this);
/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $feedback app\models\Feedback */
?>
<div class="dashboard-container">

    <div class="container">
        <div id="cssmenu">
            <?php echo $this->render('menu'); ?>
        </div>
        <div class="sub-nav hidden-sm hidden-xs">
            <ul>
                <li>
                    <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-home"></i> Home </a><a> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;">Feedback'), '/admin/feedback',['class'=>'AdminHomePageLink']); ?>
                </li>
            </ul>
        </div>

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
            <div class="projects-index">

                <h1><?= Html::encode($this->title) ?></h1>
                <?php if($message){?>
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Update Got!</strong> <?=$message_send?>
                    </div>
                <?php }?>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">


                    <?php foreach($model as $feedback){ ?>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <?php if ($feedback->question <> 2){?>
                                        <button style="margin-top: -10px" class="btn btn-primary pull-right" onclick="update_modal(<?=$feedback->id?>,'<?=$feedback->project['title']?>')"><span class="glyphicon glyphicon-pencil"></span></button>
                                    <?php }?>
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo<?=$feedback->id?>" aria-expanded="false" aria-controls="collapseTwo">
                                        Project "<?=$feedback->project['title']?>",  Date: "<?= Yii::$app->formatter->asDatetime($feedback->created_at)?>", User "<?=$feedback->user['username']?>", Show for site:<?=($feedback->show_status)?'Yes':'No'?>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo<?=$feedback->id?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <div class="col-sm-12">

                                        <label style="text-align: left" class="control-label">Please rate how you found the quality of ideas and solutions in this project (from 1-5 stars)</label>
                                        <input class="ratingOne" type="number" value="<?=$feedback->ranting_1?>" />

                                        <label style="text-align: left" class="control-label">Please rate how you found the quality of user experience (from 1-5 stars)</label>
                                        <input class="ratingTwo" type="number" value="<?=$feedback->ranting_2?>"/>

                                        <label style="text-align: left" class="control-label">How likely are you to recommend us to another business, from a score of 1-10 (1 being very unlikely, 10 being very likely)</label>
                                        <input class="ratingThree" type="number" value="<?=$feedback->ranting_3?>" />


                                            <label>Tell us about your experience so far. We’d love to know! </label>
                                           <p> <?=$feedback->about_experience?></p>

                                            <label>Would you object to this review being used as a testimonial for our site?</label>
                                            <p><?php
                                            $mas[1]='yes, I would';
                                            $mas[2]='no, I wouldn’t';
                                                $mas[3]='';
                                            echo $mas[$feedback->question];
                                            ?></p>

                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php }?>
                </div>

                <div class="col-sm-12">
                    <?php echo \yii\widgets\LinkPager::widget([
                        'pagination' => $pages,
                    ]); ?>
                </div>

            </div>
        </div>
    </div>
</div>
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><button class="close" type="button" data-dismiss="modal">×</button>
                <h4 class="modal-title">Show feedback for site</h4>
            </div>
            <?php $form = ActiveForm::begin()?>
            <div class="modal-body">
                <input type="hidden" name="up_project_id" id="up_project_id" value="">
                <div class="form-group">
                    <label for="message-text" class="control-label">Show:</label>
                    <select name="up_project_active">
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
                </div>
            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Update</button>
            </div><?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<script>
    function update_modal(project_id,title){
        $('#up_project_id').val(project_id);
        $('#up_project_title').val(title);
        $('#myModal').modal('show');
    }
</script>