<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\assets\AdminAsset;
use yii\widgets\LinkPager;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\Alert;
use dosamigos\ckeditor\CKEditor;
AdminAsset::register($this);

?>
<div class="dashboard-container">

      <div class="container">
        <div id="cssmenu">
            <?php echo $this->render('menu'); ?>
        </div>
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-home"></i> Home </a><a> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;">News'), '/admin/news',['class'=>'AdminHomePageLink']); ?>
            </li>
          </ul>
        </div>

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
          <?php
                if(Yii::$app->session->hasFlash('pages_added')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-info',
                        ],
                        'body' => 'Page added',
                    ]);
                endif; 
                if(Yii::$app->session->hasFlash('pages_not_added')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-error',
                        ],
                        'body' => 'Page not added',
                    ]);
                endif; 
            ?>
            <?php
                if(Yii::$app->session->hasFlash('pages_update')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-info',
                        ],
                        'body' => 'Page update',
                    ]);
                endif; 
                if(Yii::$app->session->hasFlash('pages_not_update')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-error',
                        ],
                        'body' => 'Page not update',
                    ]);
                endif; 
            ?>
          <!-- Row Start -->
          <div class="row wrap" >
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="widget">
                <div class="widget-header">
                  <div class="title" style="height:40px;">
                    <i class="fa fa-arrow-down boxClick" data-action="show"> </i> Add Page
                    <span class="mini-title displayNone">
                        
                    </span>
                  </div>
                </div>
                <div class=" widget-body boxShow" <?=(Yii::$app->session->hasFlash('ErrorValidating'))?'':'style="display:none;"'?>>
                    <div class="row">
                        <div class="col-sm-12">
                            <?php  $form = ActiveForm::begin(['id' => 'form-signup', 'options' => ['class' => 'form-horizontal']]); ?>
                                <?= $form->field($newModelPages, 'page_url'); ?>
                                <?= $form->field($newModelPages, 'content')->widget(CKEditor::className(), [
                                        'options' => ['rows' => 6,'id' => 'new'],
                                        'preset' => 'full',
                                'clientOptions' => [
                                    'language' => 'en',
                                ]
                                    ]);
                                ?>
                            
                                <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'savePage']) ?>
                            <?php ActiveForm::end();   ?>
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="row wrap" >
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                          <div class="title" style="height:40px;">
                            <i class="fa fa-arrow-down" data-action="show"> </i> All Pages
                            <span class="mini-title displayNone">
                                
                            </span>
                          </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <table class="table">
                                    <tr>
                                        <td><b>#</b></td>
                                        <td><b>Question</b></td>
                                        <td><b>Answer</b></td>
                                        <td><b>Type</b></td>
                                        <td></td>
                                    </tr>
                                <?php foreach($modelPages as $pages){ ?>
                                    <tr>
                                        <td><?= $pages['id']; ?></td>
                                        <td><?= $pages['page_url']; ?></td>
                                        <td><?= substr($pages['content'], 0, 400).'...'; ?></td>
                                        <td>
                                            <a data-toggle="modal" class="pull-right" data-target="#myModal<?= $pages['id']; ?>"><i class='fa fa-edit fa-2x' style="color:#3187bf;"></i></a>
                                        </td>
                                    </tr>
                                    
                                    <div class="modal fade" id="myModal<?= $pages['id']; ?>" role="dialog">       
                                        <div class="modal-dialog" style="width: 70%">
                                            <!-- Modal content-->
                                              <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title" style='color:black;'>Change page</h4>
                                                  </div>
                                                  <div class="modal-body" style='color:black;'>
                                                    <?php $form = ActiveForm::begin(['id' => 'UpdateNews', 'class' => 'form-control', 'method' => 'POST']);?>
                                                      <?= $form->field($pages, 'id')->hiddenInput(['value' => $pages['id']])->label(false); ?>
                                                      <?= $form->field($pages, 'page_url')->textInput(['readonly' => true]); ?>
                                                      <?= $form->field($pages, 'content')->widget(CKEditor::className(), [
                                                                'options' => ['rows' => 6,'id' => $pages['id']],
                                                                'preset' => 'full',
                                                                  'clientOptions' => [
                                                                      'language' => 'en',
                                                                  ]
                                                            ]);
                                                      ?>
                                                      <div class="modal-footer">
                                                          <?php echo Html::submitButton(\Yii::t('app', 'Save'), ['name' => 'updateClass','class' => 'btn btn-primary']); ?>
                                                          <?php ActiveForm::end();  ?>
                                                          <button type="button" class="btn btn-danger" data-dismiss="modal"><?= \Yii::t('app','Cancel'); ?></button>
                                                      </div>
                                                  </div>
                                              </div>
                                        </div>
                                    </div> 
                                    
                                <?php } ?>
                                </table>
                                <div class="col-sm-12">
                                    <?= LinkPager::widget(['pagination'=>$pagination]); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
       
          </div>
          <!-- Row End -->

          

        </div>

      </div>
    </div>

