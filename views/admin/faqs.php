<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\assets\AdminAsset;
use yii\widgets\LinkPager;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\Alert;

AdminAsset::register($this);

?>
<div class="dashboard-container">

      <div class="container">
        <div id="cssmenu">
            <?php echo $this->render('menu'); ?>
        </div>
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-home"></i> Home </a><a> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;">News'), '/admin/news',['class'=>'AdminHomePageLink']); ?>
            </li>
          </ul>
        </div>

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
          <?php
                if(Yii::$app->session->hasFlash('faqs_added')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-info',
                        ],
                        'body' => 'Faq added',
                    ]);
                endif; 
                if(Yii::$app->session->hasFlash('faqs_not_added')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-error',
                        ],
                        'body' => 'Faq not added',
                    ]);
                endif; 
            ?>
            <?php
                if(Yii::$app->session->hasFlash('faqs_update')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-info',
                        ],
                        'body' => 'Faq update',
                    ]);
                endif; 
                if(Yii::$app->session->hasFlash('faqs_not_update')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-error',
                        ],
                        'body' => 'Faq not update',
                    ]);
                endif; 
            ?>
            <?php
                if(Yii::$app->session->hasFlash('faqs_delete')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-info',
                        ],
                        'body' => 'Faq delete',
                    ]);
                endif; 
                if(Yii::$app->session->hasFlash('faqs_not_delete')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-error',
                        ],
                        'body' => 'Faq not delete',
                    ]);
                endif; 
            ?>
          <!-- Row Start -->
          <div class="row wrap" >
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="widget">
                <div class="widget-header">
                  <div class="title" style="height:40px;">
                    <i class="fa fa-arrow-down boxClick" data-action="show"> </i> Add FAQ
                    <span class="mini-title displayNone">
                        
                    </span>
                  </div>
                </div>
                <div class=" widget-body boxShow" <?=(Yii::$app->session->hasFlash('ErrorValidating'))?'':'style="display:none;"'?>>
                    <div class="row">
                        <div class="col-sm-12">
                            <?php  $form = ActiveForm::begin(['id' => 'form-signup', 'options' => ['class' => 'form-horizontal']]); ?>
                                <?= $form->field($newModelFaqs, 'question'); ?>
                                <?php

//                            echo $form->field($newModelFaqs, 'answer')
////                                ->textarea(['rows' => 6])
//                                ->widget(\dosamigos\ckeditor\CKEditor::className(), [
//                                    'options' => ['rows' => 6],
//                                    'preset' => 'base',
//                                    'clientOptions' => [
//                                        'language' => 'en',
//                                    ]
//                                ]);
                            echo $form->field($newModelFaqs, 'answer')->widget(TinyMce::className(), [
                                'options' => ['rows' => 6],
                                'clientOptions' => [
                                    'plugins' => [
                                        "",
                                        "",
                                        ""
                                    ],
                                    'toolbar' => ""
                                ]
                            ]);

                            ?>
                                <?= $form->field($newModelFaqs, 'type')->dropDownList(['customer' => 'customer', 'stormer' => 'stormer', 'home' => 'home']); ?>
                                <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'saveFaq']) ?>
                            <?php ActiveForm::end();   ?>
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="row wrap" >
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                          <div class="title" style="height:40px;">
                            <i class="fa fa-arrow-down" data-action="show"> </i> All FAQS
                            <span class="mini-title displayNone">
                                
                            </span>
                          </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <table class="table">
                                    <tr>
                                        <td><b>#</b></td>
                                        <td><b>Question</b></td>
                                        <td><b>Answer</b></td>
                                        <td><b>Type</b></td>
                                        <td></td>
                                    </tr>
                                <?php foreach($modelFaqs as $faqs){ ?>
                                    <tr>
                                        <td><?= $faqs['id']; ?></td>
                                        <td><?= $faqs['question']; ?></td>
                                        <td><?= substr($faqs['answer'], 0, 400).'...'; ?></td>
                                        <td><?= $faqs['type']; ?></td>
                                        <td>
                                            <a data-toggle="modal" class="pull-right" data-target="#myModal<?= $faqs['id']; ?>"><i class='fa fa-edit fa-2x' style="color:#3187bf;"></i></a>
                                            <?= HTML::a("<i class='fa fa-times fa-2x' style='color:#d9534f;'></i>",'/admin/faqsdelete/'.$faqs['id']); ?>
                                        </td>
                                    </tr>
                                    
                                    
                                    
                            <div class="modal fade" id="myModal<?= $faqs['id']; ?>" role="dialog">       
                              <div class="modal-dialog" style="width: 70%">
                          <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title" style='color:black;'>Change news</h4>
                                </div>
                                <div class="modal-body" style='color:black;'>
                                <?php $form = ActiveForm::begin(['id' => 'UpdateNews', 'class' => 'form-control', 'method' => 'POST']);?>
                                        <?= $form->field($faqs, 'id')->hiddenInput(['value' => $faqs['id']])->label(false); ?>
                                        <?= $form->field($faqs, 'question'); ?>
                                        <?php
//                                        $form->field($faqs, 'answer')->textarea(['rows' => 6]);
                                          echo $form->field($faqs, 'answer')->widget(TinyMce::className(), [
                                            'options' => ['rows' => 6],
                                            'clientOptions' => [
                                                'plugins' => [
                                                    "",
                                                    "",
                                                    ""
                                                ],
                                                'toolbar' => ""
                                            ]
                                        ]);
                                        ?>
                                        <?= $form->field($faqs, 'type')->dropDownList(['customer' => 'customer', 'stormer' => 'stormer']); ?>
                                        <div class="modal-footer">
                                            <?php echo Html::submitButton(\Yii::t('app', 'Save'), ['name' => 'updateClass','class' => 'btn btn-primary']); ?>
                                            <?php ActiveForm::end();  ?>
                                            <button type="button" class="btn btn-danger" data-dismiss="modal"><?= \Yii::t('app','Cancel'); ?></button>
                                        </div>
                                </div>
                            </div>
                        </div>
                        </div> 
                                    
                                    
                                    
                                <?php } ?>
                                </table>
                                <div class="col-sm-12">
                                    <?= LinkPager::widget(['pagination'=>$pagination]); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
       
          </div>
          <!-- Row End -->

          

        </div>

      </div>
    </div>

