<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\assets\AdminAsset;
use yii\widgets\LinkPager;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\Alert;

AdminAsset::register($this);
?>
<div class="dashboard-container">

      <div class="container">
        <div id="cssmenu">
            <?php echo $this->render('menu'); ?>
        </div>
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-home"></i> Home </a><a> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;"><a href="../../../admin/news">News</a><i class="fa fa-arrow-right"> </i>'.$modelNews->title), '/admin/news',['class'=>'AdminHomePageLink']); ?>
            </li>
          </ul>
        </div>

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
            <?php
                if(Yii::$app->session->hasFlash('comment_added')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-info',
                        ],
                        'body' => 'Comment added',
                    ]);
                endif; 
                if(Yii::$app->session->hasFlash('comment_not_added')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-error',
                        ],
                        'body' => 'Comment not added',
                    ]);
                endif; 
            ?>
            <?php
                if(Yii::$app->session->hasFlash('comment_delete')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-info',
                        ],
                        'body' => 'Comment delete',
                    ]);
                endif; 
                if(Yii::$app->session->hasFlash('comment_not_delete')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-error',
                        ],
                        'body' => 'Comment not delete',
                    ]);
                endif; 
            ?>
            <?php
                if(Yii::$app->session->hasFlash('news_update')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-info',
                        ],
                        'body' => 'News update',
                    ]);
                endif; 
                if(Yii::$app->session->hasFlash('news_not_update')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-error',
                        ],
                        'body' => 'News not update',
                    ]);
                endif; 
            ?>
            <div class="row wrap" >
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                          <div class="title" style="height:40px;">
                            <i class="fa fa-arrow-down" data-action="show"> </i>News
                            <span class="mini-title displayNone">
                            </span>
                          </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h2><?= $modelNews->title; ?><a data-toggle="modal" class="pull-right" data-target="#myModal<?= $modelNews->id; ?>"><i class='fa fa-pencil-square-o fa-1x' style="color:#3187bf;"></i></a><a class="pull-right" onclick="delete_new(<?=$modelNews->id?>)"><i class='fa fa-times fa-1x' style="color:#3187bf;"></i></a></h2>
                                    <hr>
                                        <p><span class="glyphicon glyphicon-time"></span><?= $modelNews->date_create; ?></p>
                                    <hr>
                                    <div><?php echo $modelNews->content; ?></div>
                                </div>
                          <div class="modal fade" id="myModal<?= $modelNews->id; ?>" role="dialog">       
                              <div class="modal-dialog" style="width: 70%">
                          <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title" style='color:black;'>Change news</h4>
                                </div>
                                <div class="modal-body" style='color:black;'>
                                <?php $form = ActiveForm::begin(['id' => 'UpdateNews', 'class' => 'form-control', 'method' => 'POST']);?>
                                    <?= $form->field($modelNews, 'title'); ?>
                                    <?= $form->field($modelNews, 'category')->dropDownList(['home'=> 'home', 'customer'=>'customer','stormer'=>'stormer']); ?>
                                    <?= $form->field($modelNews, 'notification')->dropDownList(['1'=>'Yes','2'=>'No','3'=>'Notification all users']); ?>
                                    <?= $form->field($modelNews, 'content')->widget(\dosamigos\ckeditor\CKEditor::className(), [
                                        'options' => ['rows' => 6],
                                        'preset' => 'full',
                                        'clientOptions' => [

                                            'language' => 'en',
                                        ]

                                    ]) ?>
                                       <div class="modal-footer">
                                            <?php echo Html::submitButton(\Yii::t('app', 'Save'), array('class' => 'btn btn-primary')); ?>
                                            <?php ActiveForm::end(); ?>
                                            <button type="button" class="btn btn-danger" data-dismiss="modal"><?= \Yii::t('app','Cancel'); ?></button>
                                        </div>
                                </div>
                            </div>
                        </div>
                        </div> 
                                <div class="col-sm-12">
                                    <hr>
                                    <div class="well">
                                        <h4>Leave Comment</h4>
                                        <div clas="col-sm-12">    
                                            <?php $form = ActiveForm::begin(['id' => 'form-signup', 'options' => ['class' => 'form-horizontal']]); ?>
                                                <?= $form->field($newComment, 'content')->textarea(); ?>
                                                <div class="col-sm-12" style="height: 0px;">
                                                    <?= $form->field($newComment, 'news_id')->hiddenInput(['value' => $modelNews->id])->label(false); ?>
                                                    <?= $form->field($newComment, 'user_id')->hiddenInput(['value' => $user_id])->label(false); ?>
                                                </div>
                                                <?= Html::submitButton('Add comment', ['class' => 'btn btn-primary', 'name' => 'saveNews']); ?>
                                            <?php ActiveForm::end(); ?>
                                        </div>
                                    </div>
                                    <hr>
                                        <div class="col-lg-9 col-md-10 col-sm-12 col-xs-12" style="margin-left: 40px">
                                            <?php foreach($modelComment as $comment){ ?>
                                                <div class="media">
                                                    <a class="pull-left" href="#">
                                                        <img class="media-object" src="http://placehold.it/64x64" alt="">
                                                    </a>
                                                    <div class="media-body">
                                                        <h4 class="media-heading">
                                                            <small><?= $comment->date_create; ?></small>
                                                        <?php $form = ActiveForm::begin(['id' => 'formDelete'.$comment->id, 'options' => ['class' => 'form-horizontal']]); ?>
                                                            <input type="hidden" name="comment_id" value="<?= $comment->id; ?>">
                                                            <?= Html::submitButton('<span class="glyphicon glyphicon-remove pull-right" style="color:#d9534f;"></span>', ['class' => 'pull-right', 'style' => 'border:0px solid transparent;background-color:transparent;','name' => 'deleteComment']); ?>
                                                        <?php ActiveForm::end(); ?>
                                                        </h4>
                                                            <?= $comment->content; ?>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

      </div>
    </div>
 <script>
     function delete_new(id){
         swal({
                 title: "Are you sure?",
                 text: "Are you sure you want to delete this item?",
                 type: "warning",
                 showCancelButton: true,
                 confirmButtonColor: "#DD6B55",
                 confirmButtonText: "Yes, delete it!",
                 closeOnConfirm: false
             },
             function(){
                 window.location = '/admin/delete_new?id='+id;
             });
     }
 </script>