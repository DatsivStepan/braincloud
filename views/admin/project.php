<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\assets\AdminAsset;
use yii\widgets\LinkPager;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\Alert;

AdminAsset::register($this);
?>
<div class="dashboard-container">

      <div class="container">
        <div id="cssmenu">
            <?php echo $this->render('menu'); ?>
        </div>
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-home"></i> Home </a><a> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;">Project'), '#',['class'=>'AdminHomePageLink']); ?>
            </li>
          </ul>
        </div>

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
          <?php
                if(Yii::$app->session->hasFlash('news_added')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-info',
                        ],
                        'body' => 'News added',
                    ]);
                endif; 
                if(Yii::$app->session->hasFlash('news_notadded')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-error',
                        ],
                        'body' => 'News not added',
                    ]);
                endif; 
            ?>
          <!-- Row Start -->
          <div class="row wrap" >
            <div class="row wrap" >
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                          <div class="title" style="height:40px;">
                            <i class="fa fa-arrow-down" data-action="show"> </i> Project
                            <span class="mini-title displayNone">
                                
                            </span>
                          </div>
                        </div>
                        <div class=" widget-body">
                            <div class="col-sm-12">
                                <table class="table">
                                    <tr>
                                        <td colspan="2"><b>Title</b></td>
                                        <td colspan="2"><?= $modelProject->title; ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><b>Description</b></td>
                                        <td colspan="2"><?= $modelProject->description; ?></td>
                                    </tr>
                                    <tr>
                                        <td><b>Date end</b></td>
                                        <td><?= $modelProject->end_date; ?></td>
                                        <td><b>Date create</b></td>
                                        <td><?= $modelProject->create_at; ?></td>
                                    </tr>
                                </table>
                            </div>
                            <?php foreach($arrayIdeas as $key => $ideas){ ?>
                                <h2><?= \Yii::$app->user->identity->getUsername($key); ?></h2>
                                <div class="col-sm-12" style="padding-left:100px;">
                                    <?php foreach($ideas as $idea){ ?>
                                        <div class="well">
                                            <?= $idea['ideas_description']; ?><br />
                                        </div>
                                    <?php } ?>
                                </div>
                                <div style="clear:both;"></div>
                            <?php } ?>     
                        </div>
                    </div>
                </div>
            </div>
            
       
          </div>
          <!-- Row End -->

          

        </div>

      </div>
    </div>
 