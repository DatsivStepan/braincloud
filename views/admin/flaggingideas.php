<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\assets\AdminAsset;
use yii\widgets\LinkPager;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\Alert;

AdminAsset::register($this);
?>
<div class="dashboard-container">

      <div class="container">
        <div id="cssmenu">
            <?php echo $this->render('menu'); ?>
        </div>
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
            <li>
                <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-home"></i> Home </a><a> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;">Flagging ideas'), '/admin/flaggingideas',['class'=>'AdminHomePageLink']); ?>
            </li>
          </ul>
        </div>

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
          <?php
                if(Yii::$app->session->hasFlash('news_added')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-info',
                        ],
                        'body' => 'News added',
                    ]);
                endif; 
                if(Yii::$app->session->hasFlash('news_notadded')):
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-error',
                        ],
                        'body' => 'News not added',
                    ]);
                endif; 
            ?>
          <!-- Row Start -->
          <div class="row wrap" >
            <div class="row wrap" >
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                          <div class="title" style="height:40px;">
                            <i class="fa fa-arrow-down" data-action="show"> </i> Flagging ideas
                            <span class="mini-title displayNone">
                                
                            </span>
                          </div>
                        </div>
                        <div class=" widget-body">
                            <?php foreach($modelIdeas as $idea){ ?>
                                <div class="well blockIdea<?= $idea['id']; ?>">
                                    <span class="fa fa-times fa-2x pull-right ideaDelete" data-idea_id="<?= $idea['id']; ?>" style="cursor:pointer;color:#d9534f;"></span>
                                    <span class="fa fa-thumbs-up fa-2x pull-right ideaOk" data-idea_id="<?= $idea['id']; ?>" style="cursor:pointer;color:#449d44;"></span>
                                    <h3><b><?= \Yii::$app->user->identity->getUsername($idea['stormer_id']); ?></b></h3>
                                    <?= $idea['ideas_description']; ?>
                                    <hr style="margin-top: 5px;margin-bottom:5px;">
                                    <?= HTML::a('Check Project','/admin/project/'.$idea['project_id'],['class'=>'btn btn-info']); ?>
                                    <h4><?= \Yii::$app->user->identity->getUsername($idea['customer_id']); ?></h4>
                                    <?php switch ($idea['why_flagging']){ 
                                        case '1':
                                            echo 'Offensive/gratuitous';
                                            break;
                                        case '2':
                                            echo 'Repeated idea';
                                            break;
                                        default:
                                            echo $idea['why_flagging'];
                                            break;
                                    }?>
                                   
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            
       
          </div>
          <!-- Row End -->

          

        </div>

      </div>
    </div>
 