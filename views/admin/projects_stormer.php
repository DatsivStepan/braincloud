<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\assets\AdminAsset;
use yii\bootstrap\ActiveForm;
AdminAsset::register($this);
/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="dashboard-container">

    <div class="container">
        <div id="cssmenu">
            <?php echo $this->render('menu'); ?>
        </div>
        <div class="sub-nav hidden-sm hidden-xs">
            <ul>
                <li>
                    <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-home"></i> Home </a><a> <i class="fa fa-arrow-right"> </i> </a><a style="font-size:15px;padding:0px;">Projects'), '/admin/projects',['class'=>'AdminHomePageLink']); ?>
                </li>
            </ul>
        </div>

        <!-- Dashboard Wrapper Start -->
        <div class="dashboard-wrapper-lg">
            <div class="projects-index">

                <h1><?= Html::encode($this->title) ?></h1>
                <div class="row" style="margin-bottom: 10px">
                    <a href="/admin/postproject" class="btn btn-primary">Post Project</a>
                    <a href="/admin/myprojects/current" class="btn btn-primary">My Projects</a>
                </div>
                <?php if($message){?>
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Update Got!</strong> <?=$message_send?>
                    </div>
                <?php }?>
                <?php if($message_error){?>
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Error!</strong> <?=$message_send?>
                    </div>
                <?php }?>
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" ><a href="/admin/projects">All projects</a></li>
                        <li role="presentation" class="active"><a href="/admin/projectsstormer" >Peer projects</a></li>
                        <li role="presentation"><a href="/admin/projectsngo" >NGO projects</a></li>
                        <li role="presentation"><a href="/admin/projectsopen" >Open projects</a></li>
                        <li role="presentation"><a href="/admin/projects_customer" > Customer projects</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <?= GridView::widget([
                                'dataProvider' => $dataProvider,
                                'filterModel' => $searchModel,
                                'rowOptions' => function ($model, $key, $index, $grid)
                                {
                                    if($model->be_inspired == true) {
                                        return ['style' => 'background-color:#dff0d8;'];
                                    }
                                },
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],

//            'id',
//            'owner_id',
//            'title',

                                    //'type',
                                    'title',
                                    'description:ntext',
                                    'goals:ntext',
                                    'evaluation_benchmark:ntext',
                                    // 'end_date',
                                    // 'create_at',
                                    // 'perspectives:ntext',
                                    // 'category',
                                    // 'question_convergent:ntext',
                                    // 'question_divergent:ntext',
                                    // 'question_custom:ntext',
                                    // 'size_team',
                                    // 'count_idea',
                                    // 'status',
                                    [
                                        'label'=> 'Stormer name',
                                        'value'=> 'user.name',
                                    ],
                                    'active:boolean',
                                    'be_inspired:boolean',

                                    ['class' => 'yii\grid\ActionColumn',
                                        'template'=>'{update}{inspired}',
                                        'buttons' => [
                                            'update' => function ($url, $model, $key) {
                                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>','#', [
                                                    'onclick'=>'update_modal('.$model->id.',"'.$model->title.'")'
                                                ]);
                                            },
                                            'inspired' => function ($url, $model, $key) {
                                                return Html::a('<span class="glyphicon glyphicon-plus"></span>','#', [
                                                    'onclick'=>'inspired_modal('.$model->id.',"'.$model->title.'")'
                                                ]);
                                            },
                                        ],
                                    ],
                                ],
                            ]); ?>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><button class="close" type="button" data-dismiss="modal">×</button>
                <h4 class="modal-title">Update active project</h4>
            </div>
            <?php $form = ActiveForm::begin()?>
            <div class="modal-body">
                <input type="hidden" name="up_project_id" id="up_project_id" value="">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Poject:</label>
                    <input type="text" class="form-control " id="up_project_title" disabled>
                </div>
                <div class="form-group">
                    <label for="message-text" class="control-label">Active:</label>
                    <select name="up_project_active" class="form-control">
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
                </div>
            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" name="active" value="true">Update</button>
            </div><?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<div id="inspired" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><button class="close" type="button" data-dismiss="modal">×</button>
                <h4 class="modal-title">Add project to Be Inspired page</h4>
            </div>
            <?php $form = ActiveForm::begin()?>
            <div class="modal-body">
                <input type="hidden" name="up_project_inspired_id" id="up_project_inspired_id" value="">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Poject:</label>
                    <input type="text" class="form-control " id="up_project_inspired_title" disabled>
                </div>
                <div class="form-group">
                    <label for="message-text" class="control-label">Be Inspired:</label>
                    <select name="up_project_inspired" class="form-control" >
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
                </div>
            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" name="inspired" value="true">Update</button>
            </div><?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<script>
    function update_modal(project_id,title){
        $('#up_project_id').val(project_id);
        $('#up_project_title').val(title);
        $('#myModal').modal('show');
    }

    function inspired_modal(project_id,title){
        $('#up_project_inspired_id').val(project_id);
        $('#up_project_inspired_title').val(title);
        $('#inspired').modal('show');
    }
</script>