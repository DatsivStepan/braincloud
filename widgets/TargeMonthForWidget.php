<?php
namespace app\widgets;
use app\models\SalesTarget;
use app\models\SalesTeams;
use app\models\Transaction;
use yii\base\Widget;
use Yii;
use yii\db\Expression;

class TargeMonthForWidget extends  Widget {
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $sales_amount =0;


        $sales_targe = SalesTarget::find()
            ->where([
                'user_id'=>Yii::$app->user->id,
                'date_for'=>'25-'.date('m-Y'),
                'date_to'=>'25-'.date('m-Y',strtotime('next month')),
            ])
//                ->andWhere(['<','',''])
            ->one();
        $sales_teams = SalesTeams::find()
            ->where(['user_id'=>Yii::$app->user->id,'status'=>1])
            ->andWhere([
                '>','updated_at',strtotime($sales_targe->date_for)
            ])
            ->andWhere([
                '<','updated_at',strtotime($sales_targe->date_to)
            ])->all();

        foreach($sales_teams as $sales_team){
            if ($sales_team->customer_id){
                $trs = Transaction::find()
                    ->where(['user_id'=>$sales_team->customer_id,'complete'=>1])
                    ->andWhere([
                        '>','updated_at',strtotime($sales_targe->date_for)
                    ])
                    ->andWhere([
                        '<','updated_at',strtotime($sales_targe->date_to)
                    ])
                    ->all();
                foreach($trs as $tr){
                    $sales_amount+=$tr->price;
                }
            }
        }
        return $this->render('targe_month_for', [
            'sales_amount'=> $sales_amount,
            'sales_targe'=> $sales_targe,
            'count'=> \app\models\SalesTeams::find()
                ->where([
                    'user_id'=>Yii::$app->user->id,
                    'status'=>1,
                    'role'=>'customer'
                ])
                ->andWhere([
                    '>','updated_at',strtotime($sales_targe->date_for)
                ])
                ->andWhere([
                    '<','updated_at',strtotime($sales_targe->date_to)
                ])
                ->count(),
        ]);
    }
}
