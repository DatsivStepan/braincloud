<?php
namespace app\widgets;
use app\models\SalesTarget;
use app\models\SalesTeams;
use app\models\Transaction;
use yii\base\Widget;
use Yii;
use yii\db\Expression;
use yii\web\NotFoundHttpException;

class TargeMonthForToWidget extends  Widget {
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $sales_amount =0;


        if (Yii::$app->request->post('targe_month_for_to')){
            $sales_targe = SalesTarget::find()
                ->where([
                    'user_id'=>Yii::$app->user->id,
                    'date_to'=>'25-'.Yii::$app->request->post('mount').'-'.date('Y'),
                ])
//                ->andWhere(['<','',''])
                ->one();
            $mount= Yii::$app->request->post('mount');
        }else{
            $sales_targe = SalesTarget::find()
                ->where([
                    'user_id'=>Yii::$app->user->id,
                    'date_for'=>'25-'.date('m-Y'),
                    'date_to'=>'25-'.date('m-Y',strtotime('next month')),
                ])
//                ->andWhere(['<','',''])
                ->one();
            $mount = date('m',strtotime($sales_targe->date_for));
        }

        if ($sales_targe){
            $sales_teams = SalesTeams::find()
                ->where(['user_id'=>Yii::$app->user->id,'status'=>1])
                ->andWhere([
                    '>','updated_at',strtotime($sales_targe->date_for)
                ])
                ->andWhere([
                    '<','updated_at',strtotime($sales_targe->date_to)
                ])->all();

            foreach($sales_teams as $sales_team){
                if ($sales_team->customer_id){
                    $trs = Transaction::find()
                        ->where(['user_id'=>$sales_team->customer_id,'complete'=>1])
                        ->andWhere([
                            '>','updated_at',strtotime($sales_targe->date_for)
                        ])
                        ->andWhere([
                            '<','updated_at',strtotime($sales_targe->date_to)
                        ])
                        ->all();
                    foreach($trs as $tr){
                        $sales_amount+=$tr->price;
                    }
                }
            }
        }else{
            return $this->render('targe_month_for_to', [
              'error'=>true,
                'mount'=> $mount,
            ]);
        }

        return $this->render('targe_month_for_to', [
            'error'=>false,
            'sales_amount'=> $sales_amount,
            'sales_targe'=> $sales_targe,
            'mount'=> $mount,
            'count'=> \app\models\SalesTeams::find()
                ->where([
                    'user_id'=>Yii::$app->user->id,
                    'status'=>1,
                    'role'=>'customer'
                ])
                ->andWhere([
                    '>','updated_at',strtotime($sales_targe->date_for)
                ])
                ->andWhere([
                    '<','updated_at',strtotime($sales_targe->date_to)
                ])
                ->count(),
        ]);
    }
}
