<?php
/**
 *
 */
?>
<div class="col-sm-6">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"> My Sales Results for <?=date('M',strtotime($sales_targe->date_for))?>.</h3>
        </div>
        <div class="panel-body">
            <p>Number of Customers: <?=$count?></p>
            <p>Sales Amount: <?=$sales_amount?></p>
            <p>Points Earned:  <?=$count*150 ?></p>
            <p>Commission Earned: <?=$sales_amount*0.1?></p>
        </div>
    </div>
</div>
