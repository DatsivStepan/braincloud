<?php
/**
 *
 */
?>
<div class="col-sm-6">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"> Accumulated Sales Results for <?=date('Y')?></h3>
        </div>
        <div class="panel-body">
            <p>Number of Customers: <?=\app\models\SalesTeams::find()->where(['user_id'=>Yii::$app->user->id,'status'=>1,'role'=>'customer'])->andWhere(['>','updated_at',strtotime('01-01-'.date('Y',time()))])->count()?></p>
            <p>Sales Amount: <?=$sales_amount?></p>
            <p>Points Earned:  <?=\app\models\SalesTeams::find()->where(['user_id'=>Yii::$app->user->id,'status'=>1,'role'=>'customer'])->andWhere(['>','updated_at',strtotime('01-01-'.date('Y',time()))])->count()*150 ?></p>
            <p>Commission Earned: <?=$sales_amount*0.1?></p>
        </div>
    </div>
</div>
