<?php
/**
 *
 */
?>
<div class="col-sm-6">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"> My Sales Results for <?=date('M',strtotime('25-'.$mount.'-'.date('Y')))?>.</h3>
        </div>
        <div class="panel-body">

            <?php $form = \yii\widgets\ActiveForm::begin(); ?>
            <input type="hidden" name="targe_month_for_to" value="1">
            <?= \yii\bootstrap\Html::dropDownList('mount',$mount,[
                '01'=>'January',
                '02'=>'February',
                '03'=>'March',
                '04'=>'April',
                '05'=>'May',
                '06'=>'June',
                '07'=>'July',
                '08'=>'August',
                '09'=>'September',
                '10'=>'October',
                '11'=>'November',
                '12'=>'December',
            ],['onchange'=>"this.form.submit()", 'class'=>'form-control'])?>
            <?php \yii\widgets\ActiveForm::end(); ?>
            <?php if ($error){?>
                <p>Number of Customers: 0</p>
                <p>Sales Amount: 0</p>
                <p>Points Earned:  0</p>
                <p>Commission Earned: 0</p>
            <?php }else{ ?>
            <p>Number of Customers: <?=$count?></p>
            <p>Sales Amount: <?=$sales_amount?></p>
            <p>Points Earned:  <?=$count*150 ?></p>
            <p>Commission Earned: <?=$sales_amount*0.1?></p>
            <?php } ?>
        </div>
    </div>
</div>
