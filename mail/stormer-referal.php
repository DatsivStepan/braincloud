<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>
        Dear <?=$recipient_name?>,<br>
        I am currently a member of BrainCloud Solution’s innovation platform, and thought it might be useful for you. <br>
        The platform offers personalised innovation teams, affordable business solutions and creative ideation processes. Everything is done online within a relatively quick timeframe.<br>
        Click here to find out more: <?= Html::a('braincloud.solutions/services', \Yii::$app->urlManager->createAbsoluteUrl(['signup/stormer/step_1','ref'=>$referal_key])) ?><br>
        Click here <?= Html::a('braincloud.solutions/signup', \Yii::$app->urlManager->createAbsoluteUrl(['signup/stormer/step_1','ref'=>$referal_key])) ?> and enter kode: <?=$referal_key?><br>
        Kind regards,<br>
        <?= \Yii::$app->user->identity->username;?><br>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>