<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/activation', 'token' => $user->activation_token]);
?>
Hello <?= $user->username ?>,

Follow the link below to activate your account:

<?= $resetLink ?>
