<?php

namespace app\controllers;

use app\models\Message;
use app\models\PayStormer;
use app\models\Points;
use app\models\PointsSearch;
use app\models\PriceIdea;
use app\models\SalesTarget;
use app\models\SalesTeams;
use app\models\SalesTeamsSearch;
use app\models\Transaction;
use app\models\UploadCsv;
use app\models\UserBan;
use app\models\UserSearch;
use Facebook\Facebook;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\web\HttpException;
use Yii;
use yii\web\Controller;
use app\models\User;
use app\models\Ideas;
use yii\helpers\ArrayHelper;
use app\models\Puzzle;
use app\models\News;
use app\models\City;
use app\models\Country;
use app\models\Projects;
use app\models\Faqs;
use app\models\Pages;
use app\models\Userrating;
use app\models\Comment;
use app\models\Stormerinfo;
use app\models\Projectstormer;
use app\models\ProjectData;
use yii\db\Query;
use yii\helpers\Url;
use yii\web\UploadedFile;

class StormerController extends Controller
{
    public function rendomStormer($category, $project_id){
        $users = Stormerinfo::find()->all();
        if ($category){
            foreach($users as $user){
                $cats = json_decode($user->category);
                if ($cats) {
                    foreach($cats as $item){
                        if ($item == $category){
                            if (!Projectstormer::find()->where(['project_id'=>$project_id, 'stormer_id'=>$user->id])->one())
                                return $user->id;
                        }
                    }
                }

            }
            return 'not_category';
        } else {
            foreach($users as $user){
            if (!Projectstormer::find()->where(['project_id'=>$project_id, 'stormer_id'=>$user->id])->one())
                return $user->id;
            }
        }
        return false;
    }

    public function reloadStormer(){
        $projects = Projectstormer::find()->where(['confirmation'=>-1])->all();

        foreach($projects as $project){
            if (($project->created_at+43200) < time()){
                $pr = Projects::findOne($project->project_id);
                if ($pr->category){
                    $user_id = $this->rendomStormer($pr->category, $project->project_id);
                    if($user_id){
                        if ($user_id == 'not_category'){
                            $user_id = $this->rendomStormer(false, $project->project_id);
                            if ($user_id){
                                $modelProjectstormer = new Projectstormer();
                                $modelProjectstormer->scenario = 'add';
                                $modelProjectstormer->stormer_id = $project->project_id;
                                $modelProjectstormer->project_id = $user_id;
                                $modelProjectstormer->confirmation=-1;
                                if ($modelProjectstormer->save()) {
                                    $project->delete();
                                }
                            }
                        } else {
                            $modelProjectstormer = new Projectstormer();
                            $modelProjectstormer->scenario = 'add';
                            $modelProjectstormer->stormer_id = $project->project_id;
                            $modelProjectstormer->project_id = $user_id;
                            $modelProjectstormer->confirmation=-1;
                            if ($modelProjectstormer->save()) {
                                $project->delete();
                            }
                        }
                    }
                } else {
                    $user_id = $this->rendomStormer(false, $project->project_id);
                    if($user_id){
                        $modelProjectstormer = new Projectstormer();
                        $modelProjectstormer->scenario = 'add';
                        $modelProjectstormer->stormer_id = $project->project_id;
                        $modelProjectstormer->project_id = $user_id;
                        $modelProjectstormer->confirmation=-1;
                        if ($modelProjectstormer->save()) {
                            $project->delete();
                        }
                    }
                }
            }
        }
    }

    public function deleteProjectstormer(){
        $projects = Projects::find()->where(['active'=>1,'project_type'=>'customer'])->all();
        foreach ($projects as $project ){
            if ($project->end_date){
                if (strtotime($project->end_date) < time()){
                    Projectstormer::deleteAll(['confirmation'=>-1, 'project_id'=>$project->id]);
                }
            }
        }
    }

    public function behaviors()
    {
        $this->deleteProjectstormer();
        $this->reloadStormer();
        if (!Yii::$app->user->isGuest) {
            if (!Points::find()->where(['type' => 'selected_stormer_50'])->andWhere(['>', 'created_at', strtotime("last Monday")])->andWhere(['<', 'created_at', strtotime("Monday")])->one()) {

                $user = User::find()->select(['*', 'count_ideas' => '(SELECT COUNT(id) FROM ideas WHERE users.id=ideas.stormer_id AND created_at>' . strtotime("last Monday") . ' AND created_at<' . strtotime("Monday") . ')'])->where(['users_type' => 'stormer'])->orderBy(['count_ideas' => SORT_DESC])->one();

                $points = new Points();
                $points->point = 50;
                $points->type = 'selected_stormer_50';
                $points->date_week = strtotime("last Monday");
                $points->user_id = $user->id;
                $points->save();
            }

            if (!Points::find()->where(['type' => 'selected_team'])->andWhere(['>', 'created_at', strtotime("last Monday")])->andWhere(['<', 'created_at', strtotime("Monday")])->one()) {

                $project = Projects::find()
                    ->select(['*', 'sum_star' => '(SELECT sum(count_star) FROM stormer_rating WHERE projects.id=stormer_rating.project_id)'])
                    ->where(['active' => 1, 'project_type' => 'customer', 'status' => 0])
                    ->andWhere(['>', 'created_at', strtotime("last Monday")])
                    ->andWhere(['<', 'updated_at', strtotime("Monday")])
                    ->orderBy(['sum_star' => SORT_DESC])
                    ->one();
                if ($project) {
                    $groups = Ideas::find()->where(['project_id' => $project->id])->all();
                    foreach ($groups as $group) {
                        $points = new Points();
                        $points->point = 25;
                        $points->type = 'selected_team';
                        $points->date_week = strtotime("last Monday");
                        $points->user_id = $group->stormer_id;
                        $points->save();
                    }
                }


            }
            if (!Yii::$app->user->isGuest) {
                $ban = UserBan::find()->where(['stormer_id' => Yii::$app->user->id])->one();
                if ($ban) {
                    if (strtotime(date('d-M-Y')) <= strtotime($ban->date)) {
                        if ($ban->type == 1) {
                            $this->redirect('/lock');
                        } else {
                            if (Yii::$app->requestedAction->id == 'index') {
                                echo "<script>
                                        // $('.dashboard-page').hide();
                                        // showMessage();
                                        alert('Friendly warning');

                                       // swal({
                                       //     title: \"Friendly warning\",
                                       //     text: \"\",
                                       //     type: \"warning\",
                                       //     showCancelButton: false,
                                       //     confirmButtonColor: \"#4594de\",
                                       //     confirmButtonText: \"OK\",
                                       //     closeOnConfirm: true,
                                       // })

                                    </script>";
                            }

                        }
                    } else {
                        $ban->delete();
                    }

                }
            }


            //sesion avarat
            $session = Yii::$app->session;
            if ($session->isActive) {
                $u_i = Stormerinfo::find()->where(['stormer_id'=>Yii::$app->user->id])->one();
                $session['avatar']=$u_i->image_src;
            }else{
                $session->open();
                $u_i = Stormerinfo::find()->where(['stormer_id'=>Yii::$app->user->id])->one();
                $session['avatar']=$u_i->image_src;
            }





            $ideas = Ideas::find()
                ->where(['stormer_id' => Yii::$app->user->id])
                ->leftJoin('projects', ['ideas.project_id' => 'projects.id'])
                ->andWhere(['projects.project_type'=>'customer'])
                ->orderBy(['ideas.updated_at' => SORT_ASC])
                ->all();

            $ideas_pay = [];
            $project_pay = [];
            $date_m = false;
            $count = 0;
            foreach ($ideas as $idea) {
                    $d = date('m-Y', $idea->updated_at);
                    if ($date_m) {
                        if ($date_m <> $d) {
                            $this->add_pay_stormer($date_m, $count, $ideas_pay, $project_pay);
                            $count = 1;
                            $ideas_pay = [];
                            $project_pay = [];
                            $ideas_pay[] = $idea->id;
                            $project_pay[] = $idea->project_id;
                        } else {
                            if ($d != date('m-Y', time())) {
                                $count++;
                                $ideas_pay[] = $idea->id;
                                $project_pay[] = $idea->project_id;
                            }
                        }
                    } else {
                        $count++;
                        $ideas_pay[] = $idea->id;
                        $project_pay[] = $idea->project_id;
                    }
                    $date_m = $d;

            }

            if ($date_m != date('m-Y', time())) {
                $this->add_pay_stormer($date_m, $count, $ideas_pay, $project_pay);
            }

        }
        return [
            'access' => [
                'class' => AccessControl::className(),
//                'only' => ['create', 'update'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return array(
            'error' => array(
                'class' => 'yii\web\ErrorAction',
            ),
            'captcha' => array(
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ),
        );
    }

    public function actionIndex($username = '')
    {
        if (!Yii::$app->user->isGuest) {
            $userModel = User::find()->where(['id' => $username])->one();
            $file = new UploadCsv();
            $query = Projects::find()->where(['active'=>1,'owner_id'=>Yii::$app->user->id]);
            $count = $query->count();
            $page=true;
            if($count<=5){
                $page=false;
            }
            $active_project = $query->orderBy(['id'=>SORT_DESC])
                ->offset(0)
                ->limit(5)
                ->all();

            $request = Yii::$app->request;
            if ($_POST) {
                if (isset($_POST['skip']) == true) {
                    $userModel->scenario = 'change_status_registration';
                    $userModel->signup_step = '4';
                    $userModel->save();
                }


                if (isset($_POST['recommend_a_stormer']) == true) {
                    $userModel->scenario = 'change_status_registration';
                    $userModel->signup_step = '5';
                    $userModel->save();
                    if ($userModel->referal_key){
                        $referal_key = $userModel->referal_key;
                    }else{
                        $userModel->scenario='referal_key';
                        $userModel->referal_key=$userModel->referal();
                        $referal_key = $userModel->referal_key;
                        $userModel->save();
                    }

                    $file->file = UploadedFile::getInstance($file, 'file');
                    if ($file->file && $file->validate()) {
                        if($file->file->saveAs('tmp_uploads/csv/' . $file->file->baseName . Yii::$app->user->id .'.' . $file->file->extension)){
                            $handle = fopen('tmp_uploads/csv/' . $file->file->baseName .Yii::$app->user->id. '.' . $file->file->extension, 'r');
                            if ($handle) {
                                    while( ($line = fgetcsv($handle, 1000, ",")) != FALSE) {
                                       if ($line[0] && $line[1]){
                                           Yii::$app->mailer->compose('stormer-referal',[
                                               'recipient_name'=>$line[0],
                                               'referal_key'=>$referal_key,
                                           ]) // здесь устанавливается результат рендеринга вида в тело сообщения
                                           ->setFrom(Yii::$app->user->identity->email)
                                               ->setTo($line[1])
                                               ->setSubject('braincloud.solutions')
                                               ->send();
                                       }
                                    }
                                fclose($handle);
                            }
                        }
                    }

                    if ($_POST['email']){
                        Yii::$app->mailer->compose('stormer-referal',[
                            'recipient_name'=>'User',
                            'referal_key'=>$referal_key,
                        ]) // здесь устанавливается результат рендеринга вида в тело сообщения
                        ->setFrom(Yii::$app->user->identity->email)
                            ->setTo($_POST['email'])
                            ->setSubject('braincloud.solutions')
                            ->send();
                    }


                }


                if (isset($_POST['end_signup']) == true) {
                    $userModel->scenario = 'change_status_registration';
                    $userModel->signup_step = 'active';
                    if ($userModel->google_id or $userModel->facebook_id or $userModel->twitter_id){
                        $userModel->active = 1;
                    }
//                    $userModel->active = 1;
                    $userModel->save();



                }
                if (isset($_POST['nextChallenge']) == true) {
                    $stormerInfoModule = Stormerinfo::find()->where(['stormer_id' => $userModel->id])->one();
                    $stormerInfoModule->scenario = 'save_points';
                    $stormerInfoModule->points = $_POST['challengePoint'];
                    $stormerInfoModule->save();

                    $userModel->scenario = 'change_status_registration';
                    $userModel->signup_step = 'active';
                    $userModel->save();
                }
            }
            //last redistration
                if ($userModel->signup_step == '1') {
                    return $this->redirect(['/signup', 'signup_type' => 'stormer', 'step' => 'step_2', 'stormer_id' => $userModel->id]);
                } elseif ($userModel->signup_step == '2') {
                    return $this->redirect(['/signup', 'signup_type' => 'stormer', 'step' => 'step_3', 'stormer_id' => $userModel->id]);
                } elseif ($userModel->signup_step == '3') {
                    return $this->render('signup', [
                        'userModel' => $userModel,
                        'step' => '3',
                    ]);
                } elseif ($userModel->signup_step == '4') {

                    return $this->render('signup', [
                        'userModel' => $userModel,
                        'file'=>$file,
                        'step' => '4',
                    ]);
                } elseif ($userModel->signup_step == '5') {
                    return $this->render('signup', [
                        'userModel' => $userModel,
                        'sum_point' => Points::find()->where(['user_id'=>Yii::$app->user->id])->sum('point'),
                        'step' => '5',
                    ]);
                } elseif ($userModel->signup_step == 'active') {
                    if ($userModel->active == 1) {
                        $stormerInfoModule = Stormerinfo::find()->where(['stormer_id' => $userModel->id])->one();
                        $points = Points::find()->where(['user_id'=>Yii::$app->user->id])->sum('point');
                        return $this->render('index', [
                            'userModel' => $userModel,
                            'stormerInfoModule' => $stormerInfoModule,
                            'active_status' => 'active',
                            'active_project'=>$active_project,
                            'page'=>$page,
                            'points' => $points,
                        ]);
                    } else {
                        return $this->render('index', [
                            'userModel' => $userModel,
                            'active_status' => 'not_active',
                            'active_project'=>$active_project,
                            'page'=>$page,
                        ]);
                    }
                }
            // end last redistration


            return $this->render('signup', [
                'userModel' => $userModel,
            ]);
        } else {
            throw new HttpException(404);
        }
    }

    public function actionGetpuzzle()
    {
        $result = [];
        if (isset($_POST)) {
            if ($_POST['type'] == 'get') {
                $step = (int) $_POST['challengeStep'] + 1;
                $puzzleModel = Puzzle::find()->where(['level' => $step])->orderBy(new \yii\db\Expression('rand()'))->one();
                $puzzle = [];
                $puzzle['id'] = $puzzleModel->id;
                $puzzle['question'] = $puzzleModel->question;
                $puzzle['A'] = $puzzleModel->A;
                $puzzle['B'] = $puzzleModel->B;
                $puzzle['C'] = $puzzleModel->C;
                $puzzle['D'] = $puzzleModel->D;

                $result['puzzle'] = $puzzle;
                $result['status'] = 'success';
                $result['type'] = 'get';
            } elseif ($_POST['type'] == 'check') {
                $result['status'] = 'success';
                $result['type'] = 'check';
                $puzzleModel = Puzzle::find()->where(['id' => $_POST['puzzleId']])->one();
                if ($puzzleModel->correct_answer == $_POST['answer']) {
                    $result['status_answer'] = 'success';
                    $point = new Points();
                    $point->user_id= Yii::$app->user->id;
                    $point->point=5;
                    $point->date_week=$point->week();
                    $point->type='Puzzle_correct_answer';
                    $point->save();
                } else {
                    $result['status_answer'] = 'error';
                }
            }

            echo json_encode($result);
        } else {
            $result['status'] = 'error';
            echo json_encode($result);
        }
    }


    public function actionPoint_count(){
      return Points::find()->where(['user_id'=>Yii::$app->user->id])->sum('point');
    }

    public function actionProfile($username = '')
    {
        $userModel = User::find()->where(['id' => Yii::$app->user->id])->one();
        $stormerInfoModel = Stormerinfo::find()->where(['stormer_id' => Yii::$app->user->id])->one();
        $arrayCountry = ArrayHelper::map(Country::find()->orderBy('country_name ASC')->all(), 'id', 'country_name');
        $arrayCity = ArrayHelper::map(City::find()->where(['country_id'=>$stormerInfoModel->country])->orderBy('city_name ASC')->all(), 'id', 'city_name');


//        var_dump($stormerInfoModel);
//        exit();
        if ($_POST) {
            $request = Yii::$app->request;
            $stormerInfoModel->scenario = 'update';
            $stormerInfoModel->load($request->post());
            if (isset($_POST['city_head'])){
                $city = City::find()->where(['city_name'=>$_POST['city_head']])->one();
                if ($city){
                    $stormerInfoModel->city = $city->id;
                } else {
                    $stormerInfoModel->city = null;
                }
            }
            $userModel->email= $request->post('User')['email'];
            $userModel->scenario = 'update';
            if ($stormerInfoModel->save() && $userModel->save()) {
                Yii::$app->session->setFlash('profile_update');
            } else {
                Yii::$app->session->setFlash('profile_notupdate');
            }
        }

        $home_profile_src = '/stormer/'.$userModel->username;
        $city_name = City::find()->where(['id'=>$stormerInfoModel->city])->one();
        $stormerInfoModel->scenario = 'update';
        return $this->render('profile', [
            'userModel' => $userModel,
            'arrayCity' => $arrayCity,
            'city_name' => $city_name,
            'arrayCountry' => $arrayCountry,
            'stormerInfoModel' => $stormerInfoModel,
            'home_profile_src' => $home_profile_src,
        ]);
    }

    public function actionNews($id = '')
    {
        if ($id == '') {
            $query = News::find()->where(['category'=>'stormer'])->orderBy(['id'=>SORT_DESC]);
            $home_profile_src = '/stormer/'.Yii::$app->user->identity->username;
            $modelNews = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 3]]);

            return $this->render('news', [
                'home_profile_src' => $home_profile_src,
                'modelNews' => $modelNews->getModels(),
                'pagination' => $modelNews->pagination,
                'count' => $modelNews->pagination->totalCount,
                'status' => 'all',
            ]);
        } else {
            $newComment = new Comment();
            $modelOneNews = News::find()->where(['id' => $id])->one();

            if ($_POST) {
                $request = Yii::$app->request;

                if ($_POST['Comment']) {
                    $newComment->scenario = 'add_comment';
                    if ($newComment->load($request->post())) {
                        if ($newComment->save()) {
                            Yii::$app->session->setFlash('comment_added');
                            $newComment = new Comment();
                        } else {
                            Yii::$app->session->setFlash('comment_not_added');
                        }
                    }
                }

                if (isset($_POST['deleteComment'])) {
                    $comment_id = $_POST['comment_id'];
                    $modelCommentD = Comment::find()->where(['id' => $comment_id])->one();
                    if ($modelCommentD->delete()) {
                        Yii::$app->session->setFlash('comment_delete');
                    } else {
                        Yii::$app->session->setFlash('comment_not_delete');
                    }
                }
            }

            $home_news_link = '/stormer/news';
            $home_profile_src = '/stormer/'.Yii::$app->user->identity->username;
            $modelComment = Comment::find()->where(['news_id' => $modelOneNews->id])->all();

            return $this->render('news', [
                'modelNews' => $modelOneNews,
                'home_profile_src' => $home_profile_src,
                'home_news_link' => $home_news_link,
                'newComment' => $newComment,
                'modelComment' => $modelComment,
            ]);
        }
    }

    public function actionConfidentialityagreement($id)
    {
        /*$modelStormer = Stormerinfo::find()->where(['stormer_id' => \Yii::$app->user->id])->one();
        $modelStormer->scenario = 'confidentialityagreement';
        if($_POST){
            if(isset($_POST['Confirm'])){
                $modelStormer->confidentialityagreement = 1;
                if($modelStormer->save()){
                }
            }
        }
        if($modelStormer->confidentialityagreement == 0){
            return $this->render('confidentialityagreement', [
            ]);
        }else{
            return $this->redirect('projectintroduction');
        }*/
      $project = Projects::findOne($id);
      if ($project->project_type != 'customer')
        return $this->redirect('/stormer/divergent/'.$id);

        if($_POST){
            if(isset($_POST['Confirm'])){
                $modelProject = Projectstormer::find()->where(
                    ['project_id' => $id,
                        'stormer_id' => \Yii::$app->user->id])->one();
                    $modelProject->scenario = 'confirmation';
                    $modelProject->confirmation = 1;
                    if($modelProject->save()) {
                      return $this->redirect('projectintroduction_start?id='.$id);
                    }
            }
        }
        return $this->render('confidentialityagreement', [
        ]);
    }

    public function actionProjectintroduction_start($id)
    {
        $project = Projects::findOne($id);
        $projectData = ProjectData::find()->where(['project_id'=>$id])->all();
        return $this->render('projectintroduction_start', [
            'project' => $project,
            'projectData' => $projectData,
        ]);
    }

    public function actionProjectintroduction()
    {
        function addNewUserInProject($arrayUser){
            $modelRandomUser = User::find()
                ->where(['users_type' => 'stormer'])
                ->orderBy(new \yii\db\Expression('rand()'))->one();
            $status = '';
            foreach($arrayUser as $user){
                if($user['id'] == $modelRandomUser->id){
                    $status = 'error';
                }
            }
            if($status == 'error'){
                return addNewUserInProject($arrayUser);
            }else{
                return $modelRandomUser->id;
            }
        }
        
        if($_POST){
            if(isset($_POST['vidmov'])){
                $modelStormerProject = Projectstormer::find()->where(['project_id' => $_POST['project_id'],'stormer_id' => \Yii::$app->user->id])->one();
                $modelProject = Projects::find()->where(['id' => $_POST['project_id']])->one();
                if($modelStormerProject->delete()){
                    $countUserInProject = Projectstormer::find()->where(['project_id' => $_POST['project_id']])->count();
                    if($countUserInProject < $modelProject->size_team){
                        $arrayUserInProject = Projectstormer::find()->where(['project_id' => $_POST['project_id']])->asArray()->all();
                        $newUserId = addNewUserInProject($arrayUserInProject);
                        $newUserProjectstormer = new Projectstormer();
                        $newUserProjectstormer->scenario = 'add';
                        $newUserProjectstormer->project_id = $_POST['project_id'];
                        $newUserProjectstormer->stormer_id = $newUserId;
                        $newUserProjectstormer->save();
                    }
                }
            }
        }
        $query = new Query();
        $query->select(['*', 'id' => 'project_stormer.id'])
                        ->from('project_stormer')
                        ->join('LEFT JOIN',
                                        'projects',
                                        'projects.id = project_stormer.project_id'
                                )
            ->where([
                'project_stormer.stormer_id' => Yii::$app->user->identity->id,
                'projects.active'=>1,
                'project_stormer.confirmation'=>-1
            ])->orderBy('projects.create_at DESC');

        $command = $query->createCommand();
        $modelProjects = $command->queryAll();

        return $this->render('projectintroduction', [
            'modelProjects' => $modelProjects,
        ]);
    }

    public function actionProjectconfirmed()
    {
        $query = new Query();
        $query->select(['*', 'id' => 'project_stormer.id','status'=>'project_stormer.status'])
                        ->from('project_stormer')
                        ->join('LEFT JOIN',
                                        'projects',
                                        'projects.id = project_stormer.project_id'
                                )
            ->where(['stormer_id' => Yii::$app->user->identity->id, 'confirmation' => 1, 'project_stormer.status'=>0,'projects.status'=>1])
            ->orderBy('projects.create_at DESC');

        $command = $query->createCommand();
        $modelProjects = $command->queryAll();

        return $this->render('projectconfirmed', [
            'modelProjects' => $modelProjects,
        ]);
    }
    public function actionPast_project()
    {
        $query = new Query();
        $query->select(['*', 'id' => 'project_stormer.id','status'=>'project_stormer.status'])
            ->from('project_stormer')
            ->join('LEFT JOIN',
                'projects',
                'projects.id = project_stormer.project_id'
            )->where(['stormer_id' => Yii::$app->user->identity->id, 'confirmation' => 1, 'project_stormer.status'=>1])->orderBy('projects.create_at DESC');

        $command = $query->createCommand();
        $modelProjects = $command->queryAll();

        return $this->render('past_project', [
            'modelProjects' => $modelProjects,
        ]);
    }
    
    public function actionDivergent($project_id = '')
    {
        $modelProject = Projectstormer::find()->where(
            ['project_id' => $project_id,
             'stormer_id' => \Yii::$app->user->id])->one();
        if($modelProject->confirmation != 1){
            $modelProject->scenario = 'confirmation';
            $modelProject->confirmation = 1;
            if($modelProject->save()){
                Yii::$app->session->setFlash('confirmed');

                $query = new Query();
                $query->select(['count_ideas'=>'COUNT(*)'])
                    ->from('project_stormer')
                    ->join('LEFT JOIN',
                        'projects',
                        'projects.id = project_stormer.project_id'
                    )->where(['stormer_id' => Yii::$app->user->identity->id, 'confirmation' => 1,'project_type'=>'customer'])->count();

                $command = $query->createCommand();
//                var_dump($count->count_ideas);
                /*if($command->queryAll()>5){
                    if(!Points::find()->where(['type'=>'5_project_customer'])->one()){
                        $point = new Points();
                        $point->point = 25;
                        $point->user_id = Yii::$app->user->id;
                        $point->date_week=$point->week();
                        $point->type='5_project_customer';
                        $point->save();
                    }
                }
                if($command->queryAll()>10){
                    if(!Points::find()->where(['type'=>'10_project_customer'])->one()){
                        $point = new Points();
                        $point->point = 50;
                        $point->user_id = Yii::$app->user->id;
                        $point->date_week=$point->week();
                        $point->type='10_project_customer';
                        $point->save();
                    }
                }*/
/*

                $query = new Query();
                $query->select(['count_ideas'=>'COUNT(*)'])
                    ->from('project_stormer')
                    ->join('LEFT JOIN',
                        'projects',
                        'projects.id = project_stormer.project_id'
                    )->where(['stormer_id' => Yii::$app->user->identity->id, 'confirmation' => 1,'project_type'=>'Peer'])->count();

                $command = $query->createCommand();
//                var_dump($count->count_ideas);
                $count_ideas_ngo = Points::find()->where(['type'=>'10_point_peer_count'])->count();
                $count = $count_ideas_ngo*10 + rand(5,10);

                if($command->queryAll()>$count){
                    $point = new Points();
                    $point->point = 10;
                    $point->user_id = Yii::$app->user->id;
                    $point->date_week=$point->week();
                    $point->type='10_point_peer_count';
                    $point->save();
                }



                $query = new Query();
                $query->select(['count_ideas'=>'COUNT(*)'])
                    ->from('project_stormer')
                    ->join('LEFT JOIN',
                        'projects',
                        'projects.id = project_stormer.project_id'
                    )->where(['stormer_id' => Yii::$app->user->identity->id, 'confirmation' => 1,'project_type'=>'NGO'])->count();

                $command = $query->createCommand();
//                var_dump($count->count_ideas);
                $count_ideas_ngo = Points::find()->where(['type'=>'30_point_ngo_10'])->count();
                $count = $count_ideas_ngo*10 + 10;

                if($command->queryAll()>$count){
                    $point = new Points();
                    $point->point = 30;
                    $point->user_id = Yii::$app->user->id;
                    $point->date_week=$point->week();
                    $point->type='30_point_ngo_10';
                    $point->save();
                }
            */

            }else{
                Yii::$app->session->setFlash('not_confirmed');
            }
        }

        $projectMy = Projects::findOne($project_id);
        
        $ideas = Ideas::find()->where(['project_id' => $project_id, 'stormer_id' => Yii::$app->user->identity->id])->all();
        $mindM = (object) [
          'meta' => (object) [
            'name' => 'Divergent',
            'author' => Yii::$app->user->identity->username,
            'version' => '0.2',
          ],
          'format' => 'node_tree',
          'data' => (object) [
            'id' => 'root',
            'topic' => substr($projectMy->title, 0, 5).'...',
            'editable' => false,
            'children' => [],
          ],

        ];
        foreach ($ideas as $idea) {
            // var_dump($idea['ideas_description']);
          $mindM->data->children[] = (object) [
            'id' => $idea['id'],
            'topic' => $idea['ideas_description'],
            'e_update'=>true,
            'direction' => 'right',
            'editable' => true,
          ];
        }
        //  var_dump(  $mindM);
        // exit;
        return $this->render('divergent', [
            'count_idea'=> $projectMy->count_idea,
          'project_id' => $project_id,
            'project'=> $projectMy,
          'mindM' => json_encode($mindM),
        ]);
    }

    public function actionTerms_conditions(){
        $modelPages = Pages::find()->where(['page_url' => 'stormer/terms'])->one();
        return $this->render('terms_conditions', [
            'content' => $modelPages->content,
        ]);
    }

    public function actionSavedivergent()
    {
        $arr = [];
        if (isset($_POST['minds'])) {
            $project_id = $_POST['project_id'];
            $errors = array();
            $data = json_decode($_POST['minds']);
            if (isset($data->data->children)) {
                foreach ($data->data->children as $row) {
                    if (is_numeric($row->id)) {
                        $idea = Ideas::find()->where(['project_id' => $project_id, 'id' => $row->id])->one();
                        if (empty($idea)) {
                            $idea = new Ideas();
                        }
                    } else {
                        $idea = new Ideas();
                    }
                    $idea->scenario = 'divergent';
                    $idea->stormer_id = Yii::$app->user->identity->id;
                    $idea->project_id = $project_id;
                    $idea->ideas_description = $row->topic;
                    if ((Ideas::find()->where(['project_id' => $project_id])->count() !=100) or (Ideas::find()->where(['project_id' => $project_id])->count() <= 100))
                        if (!($idea->validate() && $idea->save())) {
                            $errors[] = [$row->id => $idea->errors];
                        } else {
                            $arr[] = $idea->id;
                        }
                }
                if (count($errors)) {
                    echo '<pre>';
                    print_r($errors);
                    echo 'sdsdf';
              // return HttpException(500);
                } else {
                    $arr_all = [];
                    $all = Ideas::find()->where(['project_id' => $project_id, 'stormer_id' => Yii::$app->user->identity->id])->all();
                    foreach ($all as $one) {
                        $arr_all[] = $one['id'];
                    }
              // print_r(array_diff($arr_all,$arr));
              $condition = array_diff($arr_all, $arr);
                    $my_project = Projects::findOne($project_id);
                    if ($my_project->project_type == 'Peer'){
                        $my_project->scenario = 'update_slider';
                        $my_project->count_idea = 100 - Ideas::find()->where(['project_id' => $project_id])->count();
                        $my_project->save();
                    }
                    Ideas::deleteAll(['id' => array_diff($arr_all, $arr)]);
              // return  Url::home();
              return  ($_POST['type'] == 'stage') ? Url::to(['stormer/wholepicture', 'project_id' => $project_id]) : Url::home();
                }
            }
        } else {
            throw new HttpException(404);
        }
    }
    public function actionExit_project($id)
    {
        $project = Projectstormer::find()->where(['project_id'=>$id, 'stormer_id'=>Yii::$app->user->id])->one();
        if($project){
            $project->status=1;
            $project->save();
        }
        $this->redirect('/');

    }

    public function actionSavewhole()
    {
        if (isset($_POST['minds'])) {

      // test_var = '{"meta":{"name":"Divergent","author":"DastivStepan","version":"0.2"},"format":"node_array","data":[{"id":"root","topic":"Ideas","expanded":true,"isroot":true,"editable":false},{"id":"28","topic":"DastivStepan","expanded":true,"parentid":"root","direction":"right","editable":false},{"id":"108","topic":"HUUUUUUUU","expanded":true,"parentid":"28","editable":true},{"id":"149","topic":"sdfgh2323","expanded":true,"parentid":"28","editable":true},{"id":"151","topic":"sdfgh23","expanded":true,"parentid":"28","editable":true},{"id":"152","topic":"Some test Idea","expanded":true,"parentid":"28","editable":true},{"id":"153","topic":"\\foo","expanded":true,"parentid":"28","editable":true},{"id":"40","topic":"DatsivSS","expanded":true,"parentid":"root","direction":"left","editable":false},{"id":"69","topic":"xvcv","expanded":true,"parentid":"40","editable":false},{"id":"3a5bab193115f769","topic":"New Node","expanded":true,"parentid":"69"},{"id":"70","topic":"sdfsdfsdf","expanded":true,"parentid":"40","editable":false},{"id":"71","topic":"xcvx","expanded":true,"parentid":"40","editable":false},{"id":"72","topic":"xcvx","expanded":true,"parentid":"40","editable":false},{"id":"75","topic":"xcvx","expanded":true,"parentid":"40","editable":false},{"id":"78","topic":"xcvx","expanded":true,"parentid":"40","editable":false},{"id":"81","topic":"xcvx","expanded":true,"parentid":"40","editable":false},{"id":"84","topic":"xcvx","expanded":true,"parentid":"40","editable":false},{"id":"87","topic":"xcvx","expanded":true,"parentid":"40","editable":false},{"id":"90","topic":"xcvx","expanded":true,"parentid":"40","editable":false},{"id":"93","topic":"xcvx","expanded":true,"parentid":"40","editable":false},{"id":"96","topic":"xcvx","expanded":true,"parentid":"40","editable":false},{"id":"99","topic":"xcvx","expanded":true,"parentid":"40","editable":false},{"id":"102","topic":"xcvx","expanded":true,"parentid":"40","editable":false},{"id":"37","topic":"sdfgh2323","expanded":true,"parentid":"root","direction":"right","editable":false},{"id":"66","topic":"xvcv","expanded":true,"parentid":"37","editable":false},{"id":"67","topic":"xcvx","expanded":true,"parentid":"37","editable":false},{"id":"68","topic":"sdfsdfsdf","expanded":true,"parentid":"37","editable":false},{"id":"73","topic":"xcvx","expanded":true,"parentid":"37","editable":false},{"id":"76","topic":"xcvx","expanded":true,"parentid":"37","editable":false},{"id":"79","topic":"xcvx","expanded":true,"parentid":"37","editable":false},{"id":"82","topic":"xcvx","expanded":true,"parentid":"37","editable":false},{"id":"85","topic":"xcvx","expanded":true,"parentid":"37","editable":false},{"id":"88","topic":"xcvx","expanded":true,"parentid":"37","editable":false},{"id":"91","topic":"xcvx","expanded":true,"parentid":"37","editable":false},{"id":"94","topic":"xcvx","expanded":true,"parentid":"37","editable":false},{"id":"97","topic":"xcvx","expanded":true,"parentid":"37","editable":false},{"id":"100","topic":"xcvx","expanded":true,"parentid":"37","editable":false},{"id":"103","topic":"xcvx","expanded":true,"parentid":"37","editable":false},{"id":"41","topic":"йцукен","expanded":true,"parentid":"root","direction":"left","editable":false},{"id":"36","topic":"sdfgh23","expanded":true,"parentid":"root","direction":"right","editable":false}]}';
      $project_id = $_POST['project_id'];
            $errors = array();
            $data = json_decode($_POST['minds']);
            foreach ($data->data as $oneIdea) {
                if ((isset($oneIdea->editable) && ($oneIdea->editable)) || (!isset($oneIdea->editable) && (!is_numeric($oneIdea->id)))) {
                    if (is_numeric($oneIdea->id)) {
                        $idea = Ideas::find()->where(['project_id' => $project_id, 'id' => $oneIdea->id])->one();
                        if (!empty($idea)) {
                          $idea->ideas_description = $oneIdea->topic;
                        }else {
                          $idea = new Ideas();
                          $idea->ideas_description = $oneIdea->topic;
                          $idea->parent_id = $oneIdea->parentid;
                          $idea->project_id = $project_id;
                          $idea->stormer_id = Yii::$app->user->identity->id;
                          # code...
                        }
                    } else {
                        $idea = new Ideas();
                        $idea->ideas_description = $oneIdea->topic;
                        $idea->project_id = $project_id;
                        $idea->stormer_id = Yii::$app->user->identity->id;
                        $idea->parent_id = $oneIdea->parentid;
                    }
                    $idea->scenario = 'wholepicture';
                    if (!($idea->validate() && $idea->save())) {
                        $errors[] = [$row->id => $idea->errors];
                    }
                }
            }
            if (count($errors)) {
                echo '<pre>';
                print_r($errors);
                echo 'sdsdf';
              // return HttpException(500);
            } else {
                return  ($_POST['type'] == 'stage') ? Url::to(['stormer/convergent', 'project_id' => $project_id]) : Url::home();
            }
        } else {
            throw new HttpException(404);
        }
    }
    public function actionSaveconvergent()
    {
        if (isset($_POST['minds'])) {
        //   echo $_POST['minds'];
        //   exit;
        // }elseif(true){
      //  $test_var = '{"meta":{"name":"Divergent","author":"DastivStepan","version":"0.2"},"format":"node_array","data":[{"id":"root","topic":"Ideas","expanded":true,"isroot":true,"editable":false},{"id":"108","topic":"Ale2","expanded":true,"parentid":"root","direction":"right","editable":true},{"id":"157","topic":"sdfgsdf","expanded":true,"parentid":"108","editable":true},{"id":"158","topic":"sdfgsdf","expanded":true,"parentid":"108","editable":true},{"id":"149","topic":"nmm.m.,m.,m.,m.m..,,m","expanded":true,"parentid":"root","direction":"right","editable":true},{"id":"151","topic":"hvhv","expanded":true,"parentid":"root","direction":"right","editable":true},{"id":"152","topic":"Barrrrr","expanded":true,"parentid":"root","direction":"right","editable":true},{"id":"155","topic":"New Node","expanded":true,"parentid":"152","editable":true},{"id":"159","topic":"Foo","expanded":true,"parentid":"152","editable":true},{"id":"160","topic":"sdfsdfsdf","expanded":true,"parentid":"152","editable":true},{"id":"171","topic":"Alexsdfandr","expanded":true,"parentid":"152","editable":false},{"id":"153","topic":"kjhkjh","expanded":true,"parentid":"root","direction":"right","editable":true},{"id":"163","topic":"New Node","expanded":true,"parentid":"root","direction":"right","editable":false}]}';
      $project_id = $_POST['project_id'];
            $errors = array();
            $data = json_decode($_POST['minds']);
            foreach ($data->data as $oneIdea) {
                if ((isset($oneIdea->editable) && ($oneIdea->editable)) || (!isset($oneIdea->editable) && (!is_numeric($oneIdea->id)))) {
                    if (is_numeric($oneIdea->id)) {
                        $idea = Ideas::find()->where(['project_id' => $project_id, 'id' => $oneIdea->id])->one();
                        if (!empty($idea)) {
                          $idea->ideas_description = $oneIdea->topic;
                        }else {
                          $idea = new Ideas();
                          $idea->ideas_description = $oneIdea->topic;
                          $idea->parent_id = $oneIdea->parentid;
                          $idea->project_id = $project_id;
                          $idea->stormer_id = Yii::$app->user->identity->id;
                          # code...
                        }
                    } else {
                        $idea = new Ideas();
                        $idea->ideas_description = $oneIdea->topic;
                        $idea->project_id = $project_id;
                        $idea->stormer_id = Yii::$app->user->identity->id;
                        $idea->parent_id = $oneIdea->parentid;
                    }
                    $idea->scenario = 'wholepicture';
                    if (!($idea->validate() && $idea->save())) {
                        $errors[] = [$row->id => $idea->errors];
                    }
                }
            }
            if (count($errors)) {
                echo '<pre>';
                print_r($errors);
                echo 'sdsdf';
              // return HttpException(500);
            } else {
              // var_dump('cool');
              // exit;
              // return  ($_POST['type'] == 'stage') ? Url::to(['stormer/complete', 'project_id' => $project_id]) : Url::home();
              return Url::home();
            }
        } else {
            throw new HttpException(404);
        }
    }
    public function actionWholepicture($project_id = '')
    {
        $projectMy = Projects::findOne($project_id);
        $ideas = Ideas::find()->where(['project_id' => $project_id])->all();
        $mindM = (object) [
          'meta' => (object) [
            'name' => 'Divergent',
            'author' => Yii::$app->user->identity->username,
            'version' => '0.2',
          ],
            'format' => 'node_array',
            'data' => [(object) [
            'id' => 'root',
            'isroot' => true,
            'topic' => substr($projectMy->title, 0, 5).'...',
            'editable' => false,
          ]],
      ];

        $stormers = Projectstormer::find()->where(['project_id' => $project_id,'confirmation'=>1])->all();
        $i = 0;
        /*foreach ($stormers as $stormer) {
            ++$i;
            $mindM->data[] = (object) [
          'id' => (string) $stormer['stormer_id'],
          'parentid' => 'root',
//          'topic' => User::findById($stormer['stormer_id'])['username'],
//          'topic' => User::findById($stormer['stormer_id'])['username'],
          'topic' => 'Stormer'.$i,
          'direction' => ($i % 2) ? 'right' : 'left',
          'editable' => false,
          // ''
        ];
        }*/
        foreach ($ideas as $idea) {
            // var_dump($idea['ideas_description']);
            $mindM->data[] = (object) [
              'id' => (string) $idea['id'],
//              'parentid' => (!empty($idea['parent_id'])) ? $idea['parent_id'] : (string) $idea['stormer_id'],
              'parentid' => 'root',
//              'topic' => (strlen($idea['ideas_description'])>150)?  substr($idea['ideas_description'], 0, 150).'...' : $idea['ideas_description'],
              'topic' => $idea['ideas_description'],
              'editable' => (Yii::$app->user->identity->id == $idea['stormer_id']) ? true : false,
                'value'=> (Yii::$app->user->identity->id == $idea['stormer_id']) ? true : false,
                'e_update'=> (Yii::$app->user->identity->id == $idea['stormer_id']) ? true : false,
              'background-color'=>(Yii::$app->user->identity->id == $idea['stormer_id']) ? '#D3D3D3' : '',
            ];
        }

        return $this->render('wholepicture', [
            'count_idea'=> $projectMy->count_idea,
            'count_idea_user'=> Ideas::find()->where(['project_id' => $project_id, 'stormer_id' => Yii::$app->user->identity->id])->count(),
            'project_id' => $project_id,
            'project'=>$projectMy,
            'mindM' => json_encode($mindM),
        ]);
    }
    public function actionConvergent($project_id = '')
    {
        // var_dump($project_id);
      // exit;
        $projectMy = Projects::findOne($project_id);
      $ideas = Ideas::find()->where(['project_id' => $project_id, 'stormer_id' => Yii::$app->user->identity->id])->all();
        $mindM = (object) [
          'meta' => (object) [
            'name' => 'Divergent',
            'author' => Yii::$app->user->identity->username,
            'version' => '0.2',
          ],
            'format' => 'node_array',
            'data' => [(object) [
            'id' => 'root',
              'isroot' => true,
            'topic' => substr($projectMy->title, 0, 5).'...',
            'editable' => false,
          ]],
      ];
          foreach ($ideas as $idea) {
              // var_dump($idea['ideas_description']);
            $mindM->data[] = (object) [
              'id' => (string) $idea['id'],
              'parentid' => (!empty($idea['parent_id'])) ? $idea['parent_id'] : "root",
              'topic' => $idea['ideas_description'],
              'editable' => (Yii::$app->user->identity->id == $idea['stormer_id']) ? false : false,
              'value' => (Yii::$app->user->identity->id == $idea['stormer_id']) ? true : false,
              'e_update'=> false,
            ];
          }

        $p = PriceIdea::findOne(1);


        return $this->render('convergent', [
            'count_idea'=> $projectMy->count_idea,
            'count_idea_user'=> Ideas::find()->where(['project_id' => $project_id, 'stormer_id' => Yii::$app->user->identity->id])->count(),
          'project_id' => $project_id,
          'mindM' => json_encode($mindM),
            'project'=>$projectMy,
            'price' => $p->price,
        ]);
    }
    
    public function actionStormerabout()
    {
        $modelPages = Pages::find()->where(['page_url' => 'stormer/about'])->one();
        return $this->render('stormer_about', [
            'content' => $modelPages->content,
        ]);
    }
    
    public function actionSupportpage()
    {
        $modelFaqs = Faqs::find()->where(['type' => 'stormer'])->all();
        return $this->render('support_page', [
            'modelFaqs' => $modelFaqs,
        ]);
    }
    
    public function actionMyprojects()
    {
        $query = Projects::find()->where(['owner_id' => \Yii::$app->user->id])->orderBy('create_at DESC');
        $modelProjects = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 3]]);
        
        return $this->render('myprojects', [
            'modelProjects' => $modelProjects->getModels(),
            'pagination' => $modelProjects->pagination,
        ]);
    }
    
    public function actionBehindthescenes($project_id = null)
    {

       /* $query = new Query;
        $query->select(['*', 'id' => 'ideas.id'])
            ->from('ideas')
            ->leftJoin('users', 'users.id = ideas.stormer_id')
            ->where(['project_id' => $project_id])
            ->orderBy(['ideas.organize' => SORT_ASC]);

        $command = $query->createCommand();
        $modelIdeas = $command->queryAll();

        //sort ideas by user /begin
        $arrayIdeas = [];
        foreach ($modelIdeas as $ideas) {
            $arrayIdeas[$ideas['stormer_id']]['idea'][] = $ideas;
        }
        //sort ideas by user /end

        //add rating to idea/begin
        //$arrayIdeas = [];
        foreach ($arrayIdeas as $key => $ideas) {
            $modelUserrating = Userrating::find()->where(['project_id' => $project_id, 'stormer_id' => $key])->one();
            $arrayIdeas[$key]['rating'] = $modelUserrating;
        }
        //add rating to idea /end
        //var_dump($arrayIdeas);exit;

        $projectModel = Projects::find()->where(['id' => $project_id])->one();*/
        $projectMy = Projects::findOne($project_id);
        $ideas = Ideas::find()->where(['project_id' => $project_id])->all();
        $mindM = (object) [
            'meta' => (object) [
                'name' => 'Divergent',
                'author' => Yii::$app->user->identity->username,
                'version' => '0.2',
            ],
            'format' => 'node_array',
            'data' => [(object) [
                'id' => 'root',
                'isroot' => true,
//                'topic' => '<img height="120px" src="/image/mozh.png"/>',
                'topic' => substr($projectMy->title, 0, 5).'...',
                'editable' => false,
            ]],
        ];

        $stormers = Projectstormer::find()->where(['project_id' => $project_id, 'confirmation'=>1])->all();
        $i = 0;
        foreach ($stormers as $stormer) {
            ++$i;
            $mindM->data[] = (object) [
                'id' => (string) $stormer['stormer_id'],
                'parentid' => 'root',
//          'topic' => User::findById($stormer['stormer_id'])['username'],
                'topic' => 'Stormer'.$i,
//                'topic' => '<img height="80px" src="/image/stormer.png"/>',
                'direction' => ($i % 2) ? 'right' : 'left',
                'editable' => false,
                'value'=> 'stormer',
              'e_update'=> false,
                // ''
            ];
        }
        foreach ($ideas as $idea) {
            // var_dump($idea['ideas_description']);
            $mindM->data[] = (object) [
                'id' => (string) $idea['id'],
                'parentid' => (!empty($idea['parent_id'])) ? $idea['parent_id'] : (string) $idea['stormer_id'],
                'topic' => $idea['ideas_description'],
                'editable' => false,
                'value'=> 'idea',
              'e_update'=> false,
            ];
        }

        return $this->render('behindthescenes', [
            'count_idea'=> $projectMy->count_idea,
            'project_id' => $project_id,
            'mindM' => json_encode($mindM),
            'ideas' => $ideas,
            'projectMy' => $projectMy,
        ]);
    }
    
    public function actionPostproject()
    {
        $modelNewProject = new Projects();
        $modelNewProject->scenario = 'project_stormer';
        if($_POST){
            if ($modelNewProject->load(Yii::$app->request->post())) {
                $modelNewProject->owner_id = \Yii::$app->user->id;
                $modelNewProject->type = '3';
                $modelNewProject->project_type='Peer';
                if($modelNewProject->save()){
                    $files = Yii::$app->request->post('Files');
                    if(isset($files)){
                        for ($i=0; $i < sizeof($files['explanation']); $i++) {
                            $fileModel = new ProjectData();
                            $fileModel->description = $files['explanation'][$i];
                            $fileModel->data = $files['path'][$i];
                            $fileModel->project_id = $modelNewProject->id;
                            $fileModel->type = '1';

                            if(!$fileModel->save()){
                                var_dump($fileModel);
                                var_dump($fileModel->getErrors());
                                exit;
                            }
                        }
                    }
                    $link = Yii::$app->request->post('Link');
                    if(isset($link)){
                        for ($i=0; $i < sizeof($link['brief_link']); $i++) {
                            $fileModel = new ProjectData();
                            $fileModel->description = $link['brief_link'][$i];
                            $fileModel->data = $link['link'][$i];
                            $fileModel->project_id = $modelNewProject->id;
                            $fileModel->type = '2';

                            if(!$fileModel->save()){
                              var_dump($fileModel);
                              var_dump($fileModel->getErrors());
                              exit;
                            }
                        }
                    }

                    return $this->redirect('/stormer/project-info/'.$modelNewProject->id);
                }
            }
        }
        
        return $this->render('postproject',[
            'modelNewProject' => $modelNewProject
        ]);
    }

    public function actionPostprojectedit($id)
    {
        $modelNewProject = Projects::findOne($id);
        $projectDate = ProjectData::find()->where(['project_id'=>$id])->all();
        $modelNewProject->scenario = 'project_stormer';
        if($_POST){
            if ($modelNewProject->load(Yii::$app->request->post())) {
                $modelNewProject->owner_id = \Yii::$app->user->id;
                $modelNewProject->type = '3';
                $modelNewProject->project_type='Peer';
                if($modelNewProject->save()){
                    ProjectData::deleteAll(['project_id'=>$id]);
                    $files = Yii::$app->request->post('Files');
                    if(isset($files)){
                        for ($i=0; $i < sizeof($files['explanation']); $i++) {
                            $fileModel = new ProjectData();
                            $fileModel->description = $files['explanation'][$i];
                            $fileModel->data = $files['path'][$i];
                            $fileModel->project_id = $modelNewProject->id;
                            $fileModel->type = '1';

                            if(!$fileModel->save()){
                                var_dump($fileModel);
                                var_dump($fileModel->getErrors());
                                exit;
                            }
                        }
                    }
                    $link = Yii::$app->request->post('Link');
                    if(isset($link)){
                        for ($i=0; $i < sizeof($link['brief_link']); $i++) {
                            $fileModel = new ProjectData();
                            $fileModel->description = $link['brief_link'][$i];
                            $fileModel->data = $link['link'][$i];
                            $fileModel->project_id = $modelNewProject->id;
                            $fileModel->type = '2';

                            if(!$fileModel->save()){
                                var_dump($fileModel);
                                var_dump($fileModel->getErrors());
                                exit;
                            }
                        }
                    }

                    return $this->redirect('/stormer/project-info/'.$modelNewProject->id);
                }
            }
        }

        return $this->render('postproject_edit',[
            'modelNewProject' => $modelNewProject,
            'projectDate'=>$projectDate,
        ]);
    }

    public function actionPostprojectinfo($id){
        $project = Projects::findOne($id);
        $projectData = ProjectData::find()->where(['project_id'=>$id])->all();
        return $this->render('postproject_info',[
            'project' => $project,
            'projectData' => $projectData
        ]);
    }
    public function actionCreate_peer($id){
        $project = Projects::findOne($id);
//        $projectData = ProjectData::find()->where(['project_id'=>$id])->all();
        $project->scenario = 'update_slider';
        $project->count_idea = 100;
        $project->size_team = User::find()->where(['users_type'=>'stormer', 'active'=>1])->andWhere(['<>','id',Yii::$app->user->id])->count();
        $project->save();
        $stormerIdArray = User::find()->where(['users_type'=>'stormer', 'active'=>1])->andWhere(['<>','id',Yii::$app->user->id])->all();
        foreach ($stormerIdArray as $stromerI) {
            $modelProjectstormer = new Projectstormer();
            $modelProjectstormer->scenario = 'add';
            $modelProjectstormer->stormer_id = $stromerI->id;
            $modelProjectstormer->project_id = $id;
            $modelProjectstormer->confirmation=-1;
            $modelProjectstormer->save();
        }

        return $this->redirect('/stormer/adminconfirmation');
    }

    //listat
    public function actionSelectstormer($project_id = '')
    {
        $discount=0;
        if (!Yii::$app->user->isGuest) {

            if ($_POST) {
                if (isset($_POST['confirmSliderSelect'])) {
                    $modelProject = Projects::find()->where(['id' => $project_id])->one();
                    $modelProject->scenario = 'update_slider';
                    $modelProject->count_idea = $_POST['slider_scope_project'];
                    $modelProject->size_team = $_POST['slider_team_size'];
                    $modelProject->save();
                }
                if (isset($_POST['saveSelectStormer'])) {
                    $stormerIdArray = json_decode($_POST['FormSelectStormer']);

                    foreach ($stormerIdArray as $stromerI) {
                        $modelProjectstormer = new Projectstormer();
                        $modelProjectstormer->scenario = 'add';
                        $modelProjectstormer->stormer_id = $stromerI;
                        $modelProjectstormer->project_id = $project_id;
                        $modelProjectstormer->confirmation=-1;
                        $modelProjectstormer->save();
                    }
//                    return $this->redirect(['customer/paymentproject', 'project_id' => $project_id]);
//                    return $this->redirect(['customer/payproject', 'project_id' => $project_id]);
                    return $this->redirect('/stormer/adminconfirmation');
                }
            }

            $modelProject = Projects::find()->where(['id' => $project_id])->one();
            $discount=$modelProject->discount;
            $query = new Query;
            $query->select(['*', 'id' => 'stormer_info.id', 'global_stars'=>'(SELECT SUM(sr.count_star) FROM stormer_rating sr WHERE sr.stormer_id = users.id )'])
                ->from('stormer_info')
                ->join('LEFT JOIN',
                    'users',
                    'users.id = stormer_info.stormer_id'
                )->where(['<>','stormer_id',Yii::$app->user->id])->limit('100')->orderBy(['global_stars'=>SORT_DESC]);
            $command = $query->createCommand();
            $modelStormers = $command->queryAll();
            $modelC = Country::find()->orderBy('country_name ASC')->all();
            $arrayCountry = ArrayHelper::map($modelC, 'id', 'country_name');

            return $this->render('select_stormers', [
                'modelProject' => $modelProject,
                'modelStormers' => $modelStormers,
                'arrayCountry' => $arrayCountry,
                'discount'=>$discount,
            ]);
        } else {
            throw new HttpException(404);
        }
    }
    //

    public function actionAdminconfirmation()
    {
        return $this->render('adminconfirmation',[
        ]);
    }
    
    public function actionJoinsalesteam()
    {
        //$modelPages = Pages::find()->where(['page_url' => 'customer/about'])->one();

        $user = Stormerinfo::find()->where(['stormer_id'=>Yii::$app->user->id])->one();
        $user->scenario='sales_team';

        if (Yii::$app->request->post()){
            $user->sales_team=true;
            $user->save();
        }


        if ($user->sales_team){
            $this->redirect('/stormer/joinsalesteam2');
        }
        return $this->render('joinsalesteam', [
          //  'content' => $modelPages->content,
        ]);
    }

    function referal_price($date){
        $sale_teams = SalesTeams::find()->where(['user_id'=>Yii::$app->user->id,'role'=>'customer'])->all();
        $total_price = 0;
        foreach ($sale_teams as $sale_team){
            if($sale_team->customer_id){
                $transactions = Transaction::find()->where(['user_id'=>$sale_team->customer_id,'complete'=>1])->all();
                $price = 0;
                foreach ($transactions as $transaction){
                    if($date == date('m-Y',$transaction->updated_at)){
                        $price+=$transaction->price;
                    }
                }
                $sale_team->price+=$price;
                $sale_team->save();
                $total_price+=$price;
            }
        }
        return $total_price*0.1;
    }

    function add_pay_stormer($date,$count,$ideas_pay,$project_pay){
        if (!PayStormer::find()->where(['user_id'=>Yii::$app->user->id,'date_for'=>$date])->one()){
            $idea_price = PriceIdea::findOne(1);
            $user = Stormerinfo::find()->where(['stormer_id'=>Yii::$app->user->id])->one();
            $pay = new PayStormer();
            $pay->user_id=Yii::$app->user->id;
            $pay->date_for=$date;
            $pay->referal_price=$this->referal_price($date);
            $pay->price=$count*$idea_price->price+$pay->referal_price;
            $pay->else=json_encode($ideas_pay);
            $pay->projects=json_encode($project_pay);
            $pay->price_idea=$idea_price->price;
            $pay->paypal=$user->paypal;
            if($pay->save()){

            }else{
//                echo '1sa';
//                var_dump($pay->getErrors());
//                exit;
            }

        }
    }
    public function actionEarnings()
    {
        //$modelPages = Pages::find()->where(['page_url' => 'customer/about'])->one();
        $user = Stormerinfo::find()->where(['stormer_id'=>Yii::$app->user->id])->one();
        $user->scenario='paypal';

        if(Yii::$app->request->post()){
            $user->load(Yii::$app->request->post());
            if ($user->save()){
                $pay_stormer = PayStormer::find()->where(['user_id'=>Yii::$app->user->id])->all();
                foreach($pay_stormer as $item){
                    if(!$item->paypal){
                        $item->paypal=$user->paypal;
                        $item->save();
                    }
                }
            }
        }

        $ideas = Ideas::find()
            ->where(['stormer_id'=>Yii::$app->user->id])
            ->leftJoin('projects',['ideas.project_id'=>'projects.id'])
            ->andWhere(['projects.project_type'=>'customer'])
            ->orderBy(['ideas.updated_at'=>SORT_ASC])
            ->all();

        $count = 0;
        foreach($ideas as $idea) {

                $d = date('m-Y', $idea->updated_at);
                if($d == date('m-Y', time())){
                    $count++;
                }

        }
        $p=PriceIdea::findOne(1);
        $price = $count*$p->price;

        $payments = PayStormer::find()->where(['user_id'=>Yii::$app->user->id])->all();

        return $this->render('earnings', [
            'payments'=>$payments,
            'price'=>$price,
            'user'=>$user,
          //  'content' => $modelPages->content,
        ]);
    }
    
    public function actionTelltheworld()
    {
        //$modelPages = Pages::find()->where(['page_url' => 'customer/about'])->one();
        return $this->render('telltheworld', [
          //  'content' => $modelPages->content,
        ]);
    }
    
    public function actionTerms(){
        $modelPages = Pages::find()->where(['page_url' => 'stormer/terms'])->one();
        return $this->render('terms', [
            'content' => $modelPages->content,
        ]);
    }
    //listat
    public function actionMessage($id){

        $project = Projects::findOne($id);
        $aler =false;

        if (Yii::$app->request->post()){
                $new_message=new Message();
                $new_message->message=Yii::$app->request->post('message');
                $new_message->stormer_id=Yii::$app->user->id;
                $new_message->project_id=$id;
                $new_message->type_user='stormer';
                $new_message->save();
                $aler=true;
        }

        $messages = Message::find()->where(['project_id'=>$id,'stormer_id'=>Yii::$app->user->id])->orderBy(['created_at'=>SORT_DESC]);

        $countQuery = clone $messages;
        $pages = new Pagination(['totalCount' => $countQuery->count(),'pageSize' => 10]);
        $models = $messages->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        return $this->render('message',[
            'aler'=>$aler,
            'project'=>$project,
            'messages'=>$models,
            'pages' => $pages,
        ]);
    }

    public function actionSendreferal()
    {
        $recipient_name = $_POST['recipient_name'];
        $recipient_email = $_POST['recipient_email'];
        $senders_email = $_POST['senders_email'];

        $user = User::findOne(Yii::$app->user->id);
        if ($user->referal_key){
            $referal_key = $user->referal_key;
        }else{
            $user->scenario='referal_key';
            $user->referal_key=$user->referal();
            $referal_key = $user->referal_key;
            $user->save();
        }

        Yii::$app->mailer->compose('stormer-referal',[
            'recipient_name'=>$recipient_name,
            'referal_key'=>$referal_key,
        ]) // здесь устанавливается результат рендеринга вида в тело сообщения
        ->setFrom($senders_email)
            ->setTo($recipient_email)
            ->setSubject('braincloud.solutions')
            ->send();
        if (!SalesTeams::find()->where(['email'=>$recipient_email])->one()){
            $sales_team = new SalesTeams();
            $sales_team->name=$recipient_name;
            $sales_team->email=$recipient_email;
            $sales_team->user_id=Yii::$app->user->id;
            $sales_team->role='stormer';
            $sales_team->status=0;
            $sales_team->save();
        }

        $result['status'] = 'send';
        echo json_encode($result);
    }

    public function actionReferalpage()
    {

        return $this->render('referalpage', [

        ]);
    }

    public function actionReferalpage_sales()
    {

        return $this->render('referalpage_sales', [

        ]);
    }

    public function actionJoinsalesteam2()
    {
        //$modelPages = Pages::find()->where(['page_url' => 'customer/about'])->one();

        if((time() > strtotime('25-'.date('m-Y'))) and (time() < strtotime('25-'.date('m-Y',strtotime('next month'))))){
            $sales_targe = SalesTarget::find()
                ->where([
                    'user_id'=>Yii::$app->user->id,
                    'date_for'=>'25-'.date('m-Y'),
                    'date_to'=>'25-'.date('m-Y',strtotime('next month')),
                ])
//                ->andWhere(['<','',''])
                ->one();
            if($sales_targe){

            }else{
                $sales_targe = new SalesTarget();
                $sales_targe->user_id=Yii::$app->user->id;
                $sales_targe->namber_customer=0;
                $sales_targe->sales_amount=0;
                $sales_targe->date_for='25-'.date('m-Y');
                $sales_targe->date_to='25-'.date('m-Y',strtotime('next month'));
                $sales_targe->save();
            }
        }else{
            $sales_targe = new SalesTarget();
            $sales_targe->user_id=Yii::$app->user->id;
            $sales_targe->namber_customer=0;
            $sales_targe->sales_amount=0;
            $sales_targe->date_for='25-'.date('m-Y');
            $sales_targe->date_to='25-'.date('m-Y',strtotime('next month'));
            $sales_targe->save();
        }


        if(Yii::$app->request->post()){
            $sales_targe->load(Yii::$app->request->post());
            $sales_targe->save();
        }

        $searchModel = new SalesTeamsSearch(['user_id'=>Yii::$app->user->id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $sailsTeams = SalesTeams::find()->where(['user_id'=>Yii::$app->user->id])->all();
        return $this->render('joinsalesteam2', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'sales_targe' => $sales_targe,
            'sailsTeams' => $sailsTeams,
        ]);
    }
    public function actionRankings()
    {
        $stormer = Stormerinfo::find()->where(['stormer_id'=>Yii::$app->user->id])->one();



        $date = explode('-',date('D-m-y',time()));
        $searchModel = new UserSearch(['users_type'=>'stormer']);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $user_count = User::find()
            ->select(['*','sum_point'=>'(SELECT SUM(point) FROM points WHERE users.id=user_id)'])
            ->where(['users_type'=>'stormer'])
            ->orderBy(['sum_point'=>SORT_DESC])
            ->all();
        $count_u =0;
        $count_us =0;
        foreach($user_count as $item){
            $count_us++;
            if (Yii::$app->user->id == $item->id)
                $count_u=$count_us;
        }
        if (Yii::$app->request->get('page')){

        } else {
            $dataProvider->pagination->page=floor($count_u/20);
        }





        $all_user = User::find()->select(['users.*','sum_point'=>'(SELECT SUM(point) FROM points WHERE user_id=users.id )'])->leftJoin('stormer_info','stormer_info.stormer_id = users.id')->where(['users.users_type'=>'stormer'])->orderBy(['sum_point'=>SORT_DESC])->asArray()->all();
        $location_user = User::find()->select(['users.*','sum_point'=>'(SELECT SUM(point) FROM points WHERE user_id=users.id )'])->leftJoin('stormer_info','stormer_info.stormer_id = users.id')->where(['users.users_type'=>'stormer','stormer_info.country'=>$stormer->country])->limit(10)->orderBy(['sum_point'=>SORT_DESC])->asArray()->all();
        $week_user = User::find()->select(['*','sum_point'=>'(SELECT SUM(point) FROM points WHERE user_id=users.id AND date_week>'.strtotime("last Sunday").' AND date_week<'.strtotime("Monday").')'])->where(['users_type'=>'stormer'])->limit(10)->orderBy(['sum_point'=>SORT_DESC])->asArray()->all();
        $day_user = User::find()->select(['*','sum_point'=>'(SELECT SUM(point) FROM points WHERE user_id=users.id AND created_at>'.strtotime($date[0]).')'])->where(['users_type'=>'stormer'])->limit(10)->orderBy(['sum_point'=>SORT_DESC])->asArray()->all();


        $frends_user = Projectstormer::find()->select(['t2.*','u.*','sum_point'=>'(SELECT SUM(point) FROM points WHERE user_id=t2.stormer_id )'])->innerJoin('project_stormer as t2','project_stormer.project_id = t2.project_id')->leftJoin('users as u','u.id=t2.stormer_id')->where(['project_stormer.stormer_id'=>Yii::$app->user->id,'t2.confirmation'=>1,'project_stormer.confirmation'=>1])->groupBy('t2.stormer_id')->orderBy(['sum_point'=>SORT_DESC])->asArray()->all();


        $position_frend = 1;
        $k=0;
        foreach($frends_user as $item){
            $k++;
            if($item['stormer_id'] == Yii::$app->user->id){
                $position_frend=$k;
            }
        }

        $position_location = 1;
        $k=0;
        foreach($location_user as $item){
            $k++;
            if($item['id'] == Yii::$app->user->id){
                $position_location=$k;
            }
        }

        $position_all = 1;
        $k=0;
        foreach($all_user as $item){
            $k++;
            if($item['id'] == Yii::$app->user->id){
                $position_all=$k;
            }
        }
        $fb = new Facebook([
            'app_id' => '665712863584192',
            'app_secret' => '0ef4f9ec43667575d86b0a4ca7c349ae',
            'default_graph_version' => 'v2.4',
            //'default_access_token' => '{access-token}', // optional
        ]);

        $helper = $fb->getRedirectLoginHelper();
        $loginUrl = $helper->getLoginUrl('http://braincloud.dev/api/facebook_frends', ['user_friends']);

        return $this->render('rankings', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'week_user' => $week_user,
            'day_user' => $day_user,
            'location_user' => $location_user,
            'frends_user' => $frends_user,
            'position_all' => $position_all,
            'position_location' => $position_location,
            'position_frend' => $position_frend,
            'loginUrl' => $loginUrl,
        ]);
    }

    public function actionStormer_benchmark(){
        $user = User::findOne(Yii::$app->user->id);
        return $this->render('stormer_benchmark', [
            'user'=>$user,
        ]);
    }
    public function actionProject_completed($id){

        $idea = Ideas::find()->where(['project_id'=>$id,'stormer_id'=>Yii::$app->user->id])->count();
        if (!Points::find()->where(['user_id'=>Yii::$app->user->id,'othe'=>$id])->one()) {
            $project_type = Projects::findOne($id);

            if ($project_type->project_type == 'Peer') {
                $point = new Points();
                $point->point = 5 * $idea;
                $point->user_id = Yii::$app->user->id;
                $point->date_week = $point->week();
                $point->type = 'idea_peer';
                $point->othe = $id;
                $point->save();
            }

            if ($project_type->project_type == 'customer') {
                $point = new Points();
                $point->point = 10 * $idea;
                $point->user_id = Yii::$app->user->id;
                $point->date_week = $point->week();
                $point->type = 'idea_customer';
                $point->othe = $id;
                $point->save();
            }
            if ($project_type->project_type == 'Open') {
                $point = new Points();
                $point->point = 4 * $idea;
                $point->user_id = Yii::$app->user->id;
                $point->date_week = $point->week();
                $point->type = 'idea_open';
                $point->othe = $id;
                $point->save();
            }

            if ($project_type->project_type == 'NGO') {
                $point = new Points();
                $point->point = 4 * $idea;
                $point->user_id = Yii::$app->user->id;
                $point->date_week = $point->week();
                $point->type = 'idea_ngo';
                $point->othe = $id;
                $point->save();
            }

        }

        $project = Projectstormer::find()->where(['project_id'=>$id,'stormer_id'=>Yii::$app->user->id])->one();
        $project->status = 1;
        $project->scenario='status';
        $project->save();


        $price_idea = PriceIdea::findOne(1);
        $price_idea = $price_idea->price;


        $projectModel = Projects::findOne($id);


        return $this->render('project_completed', [
            'idea'=>$idea,
            'price_idea'=>$price_idea,
            'projectModel'=>$projectModel,
        ]);
    }

    public function actionBeinspired($type)
    {

        $query = Projects::find()->where(['project_type' => $type,'be_inspired'=>1])->orderBy(['id' => SORT_DESC]);
        $modelProjectsOpen = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 10]]);

        foreach ($modelProjectsOpen->getModels() as $projectsO) {
            $query = new Query;
            $query->select(['users.username', 'id' => 'project_stormer.id'])
                ->from('project_stormer')
                ->join('LEFT JOIN',
                    'users',
                    'users.id = project_stormer.stormer_id'
                )
                ->where(['project_id' => $projectsO->id]);

            $command = $query->createCommand();
            $projectsO->team = $command->queryAll();
            $projectsO->countIdeaUser = Ideas::find()->where(['project_id' => $projectsO->id])->count();
        }
        return $this->render('beinspired', [
//            'newFeedbackModal' => $newFeedbackModal,
            'type'=>$type,
            'modelProjectsOpen' => $modelProjectsOpen->getModels(),
            'pagination' => $modelProjectsOpen->pagination,
            'count' => $modelProjectsOpen->pagination->totalCount,
        ]);
    }
}
