<?php

namespace app\controllers;

use app\models\DiscountProject;
use app\models\ProjectAudit;
use app\models\User;
use Yii;
use app\models\Projects;
use app\models\ProjectData;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\models\Projectquestion;
/**
 * ProjectsController implements the CRUD actions for Projects model.
 */
class ProjectsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Projects models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Projects::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionPostproject()
    {
      // var_dump(Yii::$app->user->identity->users_type);exit;
        $token=false;

      if(!Yii::$app->user->isGuest&&(Yii::$app->user->identity->users_type == 'customer')){
          if (Yii::$app->request->get('token')){
              $token=Yii::$app->request->get('token');
          }
        return $this->render('post_project',[
            'token'=>$token,
        ]);
      }else{
          throw new HttpException(404);
      }
    }

    /**
     * Displays a single Projects model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Projects model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($type=null)
    {
        if($type == null){
          throw new Exception("Error Processing Request", 1);
        }

        if(!Yii::$app->user->isGuest&&(Yii::$app->user->identity->users_type == 'customer')){

            $model = new Projects();
            if (Yii::$app->request->get('token')){
                $audit = ProjectAudit::find()->where(['token'=>Yii::$app->request->get('token')])->one();
                if ($audit){
                    $model->title=$audit->title;
                    $model->size_team=$audit->size_team;
                    $model->count_idea=$audit->count_idea;


                    $count = ProjectAudit::find()->where(['project_id'=>$audit->project_id, 'status'=>1])->count()+1;
                    if (($count<=3)){
                        $discount_bd = DiscountProject::find()->where(['count'=>$count])->one();
                        if($discount_bd){
                            $model->discount=$discount_bd->discount;
                        }
                    }else{
                        $discount_bd = DiscountProject::find()->where(['count'=>3])->one();
                        if($discount_bd){
                            $discount=$discount_bd->discount;
                        }
                    }

                }

            }

            $user = User::findOne(Yii::$app->user->id);
            if (($user->users_type == 'customer') and $user->level){
                if ($user->level == 1){
                    $model->discount = $model->discount+5;
                }
                if ($user->level == 2){
                    $model->discount = $model->discount+7;
                }
                if ($user->level == 3){
                    $model->discount = $model->discount+10;
                }
                if ($user->level == 4){
                    $model->discount = $model->discount+15;
                }
                if ($user->level == 5){
                    $model->discount = $model->discount+50;
                }
            }


            $model->owner_id = Yii::$app->user->id;
            $model->type = ($type == 'create')?1:2;
            $model->project_type = 'customer';
            if ($type == 'create') {
              $model->category = "";
              $model->question_convergent = "";
              $model->question_divergent = "";
              $model->question_custom = "";
              $model->perspectives = "";
              $model->scenario = 'create';
            }else{
              $model->scenario = 'target';
              $categories =  array(
                'Employee engagement' => 'Employee engagement',
                'Customer experience' => 'Customer experience',
                'Customer Reach' => 'Customer Reach',
                'Research and development' => 'Research and development',
                'Cost containment' => 'Cost containment',
                'Sales planning' => 'Sales planning',
                'Value chain management' => 'Value chain management',
                'Human resource management' => 'Human resource management',
                'Business model innovation' => 'Business model innovation',
              );
              
              // var_dump($question_divergent);
            }


            if ($model->load(Yii::$app->request->post())) {

                if ($model->ngo){
                    $model->project_type='NGO';
                }
                    
                if(($model->tmpCategory != null) || ($model->tmpCategory != '')){
                    $model->category = $model->tmpCategory ;
                }

                if (Yii::$app->request->get('token')){
                    $audit = ProjectAudit::find()->where(['token'=>Yii::$app->request->get('token')])->one();
                    $audit->status=1;
                    $audit->save();

                }


                if( $model->save()){
                    return $this->redirect(['customer/post-project/'.$type.'2/'.$model->id]);
                }
            }else{
                if ($type == 'create') {
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }else{
                  // var_dump($question_divergent);exit;
                  return $this->render('target', [
                      'model' => $model,
                      'categories' => $categories,
                  ]);
                }
            }
        }else{
            throw new HttpException(404);
        }
    }
    
    public function actionCreate2($type,$project_id=null)
    {
        $modelProject = Projects::find()->where(['id' => $project_id])->one();
        if($type == 'target2'){
            
            $question_divergent =  ArrayHelper::map(Projectquestion::find()->where(['type' => 'divergent'])->all(), 'id', 'question_title');;
            $question_convergent =  ArrayHelper::map(Projectquestion::find()->where(['type' => 'convergent'])->all(), 'id', 'question_title');;
            $modelProject->scenario = 'target2';
        }
        
        if ($_POST) {
            if ($modelProject->load(Yii::$app->request->post())) {
                $modelProject->save();
            }
                $files = Yii::$app->request->post('Files');
                if(isset($files)){
                  for ($i=0; $i < sizeof($files['explanation']); $i++) {
                    $fileModel = new ProjectData();
                    $fileModel->description = $files['explanation'][$i];
                    $fileModel->data = $files['path'][$i];
                    $fileModel->project_id = $modelProject->id;
                    $fileModel->type = '1';
                    if(!$fileModel->save()){
                      var_dump($fileModel);
                      var_dump($fileModel->getErrors());
                      exit;
                    }

                  }
                }
                $link = Yii::$app->request->post('Link');
                if(isset($link)){
                    for ($i=0; $i < sizeof($link['brief_link']); $i++) {
                        $fileModel = new ProjectData();
                        $fileModel->description = $link['brief_link'][$i];
                        $fileModel->data = $link['link'][$i];
                        $fileModel->project_id = $modelProject->id;
                        $fileModel->type = '2';

                        if(!$fileModel->save()){
                          var_dump($fileModel);
                          var_dump($fileModel->getErrors());
                          exit;
                        }
                    }
                }

                return $this->redirect(['customer/selectstormer/', 'project_id' => $modelProject->id]);
            
        }
        if($type == 'target2'){
            return $this->render('create2', [
                'question_convergent' => $question_convergent,
                'question_divergent' => $question_divergent,
                'model' => $modelProject,
                'type' => $type,
            ]);
        }else{
            return $this->render('create2', [
                'model' => $modelProject,
                'type' => $type,
            ]);
        }
        
    }

    /**
     * Updates an existing Projects model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Projects model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Projects model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Projects the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Projects::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
