<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;

class UserController extends Controller
{
  public function actions()
    {
        return array(
            'error' => array(
                'class' => 'yii\web\ErrorAction',
            ),
            'captcha' => array(
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ),
        );
    }

    public function actionIndex()
    {
        if(Yii::$app->user->isGuest){
            throw new HttpException(404);
        }else{
            $this->redirect('user/'.Yii::$app->user->identity->getUsername(Yii::$app->user->id));
        }
    }
}
