<?php

namespace app\controllers;

use app\commands\Paypal;
use app\commands\PaypalController;
use app\models\ContactUs;
use app\models\DiscountProject;
use app\models\IdeasSearch;
use app\models\Message;
use app\models\Points;
use app\models\PointsMessage;
use app\models\ProjectAudit;
use app\models\SalesTeams;
use app\models\TransactionSearch;
use app\models\UserBan;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Exception\PayPalConnectionException;
use yii\base\Exception;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;
use yii\web\HttpException;
use yii\base\UserException;
use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Transactions;
use app\models\User;
use app\models\Faqs;
use app\models\Pages;
use app\models\Country;
use app\models\City;
use app\models\News;
use app\models\Stormerinfo;
use app\models\Comment;
use app\models\Customerinfo;
use app\models\Projects;
use app\models\Ideas;
use app\models\Organizeidea;
use app\models\Feedback;
use app\models\Flaggingideas;
use app\models\Userrating;
use app\models\ProjectData;
use app\models\Projectstormer;
use app\models\Projectquestion;
use yii\db\Query;

class CustomerController extends Controller
{
    public function actions()
    {
        return array(
            'error' => array(
                'class' => 'yii\web\ErrorAction',
            ),
            'captcha' => array(
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ),
        );
    }

    public function beforeAction($action)
    {
        // ...set `$this->enableCsrfValidation` here based on some conditions...
        // call parent method that will check CSRF if such property is true.
        if (($action->id === 'paymentproject') || ($action->id === 'getstormer')) {
            # code...
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        if ((!Yii::$app->user->isGuest) and (Yii::$app->user->identity->users_type == 'customer')){
            //sesion avarat
            $session = Yii::$app->session;
            if ($session->isActive) {
                $u_i = Customerinfo::find()->where(['customer_id'=>Yii::$app->user->id])->one();
                $session['avatar']=$u_i->image_name;
            }else{
                $session->open();
                $u_i = Customerinfo::find()->where(['customer_id'=>Yii::$app->user->id])->one();
                $session['avatar']=$u_i->image_name;
            }
            if (!Yii::$app->user->isGuest) {
                $ban = UserBan::find()->where(['stormer_id' => Yii::$app->user->id])->one();
                if ($ban) {
                    if (strtotime(date('d-M-Y')) <= strtotime($ban->date)) {
                        if ($ban->type == 1) {
                            $this->redirect('/lock');
                        } else {
                            if (Yii::$app->requestedAction->id == 'index') {
                                echo "<script>
                                        alert('Friendly warning');

                                       // swal({
                                       //     title: \"Friendly warning\",
                                       //     text: \"\",
                                       //     type: \"warningPs\",
                                       //     showCancelButton: false,
                                       //     confirmButtonColor: \"#4594de\",
                                       //     confirmButtonText: \"OK\",
                                       //     closeOnConfirm: true,
                                       // })

                                    </script>";
                            }

                        }
                    } else {
                        $ban->delete();
                    }

                }
            }
        }
        return [
            'access' => [
                'class' => AccessControl::className(),
//                'only' => ['create', 'update'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['getcity','userratingajax'],
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }
    public function actionIndex($username = '')
    {
        if (!Yii::$app->user->isGuest) {
            $userModel = User::find()->where(['id' => Yii::$app->user->id])->one();
            $customerInfo = Customerinfo::find()->where(['customer_id' => Yii::$app->user->id])->one();

            $profile_src = '/customer/profile/' . $userModel->username;

            $query = Projects::find()->where(['active'=>1,'owner_id'=>Yii::$app->user->id]);
            $count = $query->count();
            $page=true;
            if($count<=5){
                $page=false;
            }
            $active_project = $query->orderBy(['id'=>SORT_DESC])
                ->offset(0)
                ->limit(5)
                ->all();
            return $this->render('index', [
                'userModel' => $userModel,
                'customerInfo' => $customerInfo,
                'profile_src' => $profile_src,
                'active_project'=>$active_project,
                'page'=>$page,
            ]);
        } else {
            throw new HttpException(404);
        }
    }

    public function actionProfile($username = '')
    {
        if (!Yii::$app->user->isGuest) {
        
            $userModel = User::find()->where(['id' => Yii::$app->user->id])->one();

            $customerInfo = Customerinfo::find()->where(['customer_id' => Yii::$app->user->id])->one();

            if ($customerInfo == null) {
                $customerInfo = new Customerinfo();
                $customerInfo->customer_id = Yii::$app->user->id;
                $customerInfo->scenario = 'customer_update';
                $customerInfo->save();
            }

            if ($_POST) {
//                $customerInfo = Customerinfo::find()->where(['customer_id' => Yii::$app->user->id])->one();
                $customerInfo->scenario = 'customer_update';
                $request = Yii::$app->request;
                $userModel->email= $request->post('User')['email'];
                $userModel->scenario = 'update';
                if ($customerInfo->load($request->post())) {
                    if (isset($_POST['city_head'])){
                        $city = City::find()->where(['city_name'=>$_POST['city_head']])->one();
                        if ($city){
                            $customerInfo->city = $city->id;
                        } else {
                            $customerInfo->city = null;
                        }
                    }
                    if ($customerInfo->save() && $userModel->save()) {
                        Yii::$app->session->setFlash('profile_update');
                    } else {
                        Yii::$app->session->setFlash('profile_notupdate');
                    }
                };
            }

            $city_name = City::find()->where(['id'=>$customerInfo->city])->one();

            $arrayCountry = ArrayHelper::map(Country::find()->orderBy('country_name ASC')->all(), 'id', 'country_name');
            $arrayCity = ArrayHelper::map(City::find()->where(['country_id'=>$customerInfo->country])->orderBy('city_name ASC')->all(), 'id', 'city_name');

            $home_profile_src = '/customer/' . $userModel->username;
            return $this->render('profile', [
                'arrayCity' => $arrayCity,
                'arrayCountry' => $arrayCountry,
                'userModel' => $userModel,
                'city_name' => $city_name,
                'customerInfo' => $customerInfo,
                'home_profile_src' => $home_profile_src
            ]);
        } else {
            throw new HttpException(404);
        }
    }

    public function actionNews($id = '')
    {
        if (!Yii::$app->user->isGuest) {
            if ($id == '') {
                $query = News::find()->where(['category'=>'customer'])->orderBy(['id'=>SORT_DESC]);
                $home_profile_src = '/customer/' . Yii::$app->user->identity->username;
                $modelNews = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 3]]);

                return $this->render('news', [
                    'home_profile_src' => $home_profile_src,
                    'modelNews' => $modelNews->getModels(),
                    'pagination' => $modelNews->pagination,
                    'count' => $modelNews->pagination->totalCount,
                    'status' => 'all'
                ]);
            } else {

                $newComment = new Comment;
                $modelOneNews = News::find()->where(['id' => $id])->one();

                if ($_POST) {
                    $request = Yii::$app->request;

                    if ($_POST['Comment']) {
                        $newComment->scenario = 'add_comment';
                        if ($newComment->load($request->post())) {
                            if ($newComment->save()) {
                                Yii::$app->session->setFlash('comment_added');
                                $newComment = new Comment;
                            } else {
                                Yii::$app->session->setFlash('comment_not_added');
                            }
                        }
                    }

                    if (isset($_POST['deleteComment'])) {
                        $comment_id = $_POST['comment_id'];
                        $modelCommentD = Comment::find()->where(['id' => $comment_id])->one();
                        if ($modelCommentD->delete()) {
                            Yii::$app->session->setFlash('comment_delete');
                        } else {
                            Yii::$app->session->setFlash('comment_not_delete');
                        }
                    }
                }
                $home_news_link = '/customer/news';
                $home_profile_src = '/customer/' . Yii::$app->user->identity->username;

                $modelComment = Comment::find()->where(['news_id' => $modelOneNews->id])->all();
                return $this->render('news', [
                    'modelNews' => $modelOneNews,
                    'home_profile_src' => $home_profile_src,
                    'home_news_link' => $home_news_link,
                    'newComment' => $newComment,
                    'modelComment' => $modelComment,
                    'status' => 'one'
                ]);

            }
        } else {
            throw new HttpException(404);
        }
    }

    public function actionSelectstormer($project_id = '')
    {
        $discount=0;
        if (!Yii::$app->user->isGuest) {

            if ($_POST) {
                if (isset($_POST['confirmSliderSelect'])) {
                    $modelProject = Projects::find()->where(['id' => $project_id])->one();
                    $modelProject->scenario = 'update_slider';
                    $modelProject->count_idea = $_POST['slider_scope_project'];
                    $modelProject->size_team = $_POST['slider_team_size'];
//                    $modelProject->discount = $_POST['slider_discount'];
                    $modelProject->save();
                }
                if (isset($_POST['saveSelectStormer'])) {
                    $stormerIdArray = json_decode($_POST['FormSelectStormer']);

                    foreach ($stormerIdArray as $stromerI) {
                        $modelProjectstormer = new Projectstormer();
                        $modelProjectstormer->scenario = 'add';
                        $modelProjectstormer->stormer_id = $stromerI;
                        $modelProjectstormer->project_id = $project_id;
                        $modelProjectstormer->confirmation=-1;
                        $modelProjectstormer->save();
                    }
                    $project = Projects::findOne($project_id);
                    $project->scenario = 'update_slider';
                    if (isset($_POST['slider_discount']))
                        $project->discount = $_POST['slider_discount'];
                    $project->save();
//                    echo  $_POST['slider_discount'];
//                    return $this->redirect(['customer/paymentproject', 'project_id' => $project_id]);
                    $pr = Projects::find()->where(['id' => $project_id])->one();
                    if($pr->ngo){
                        return $this->redirect(['customer/adminconfirmation']);
                    }else{
                        return $this->redirect(['customer/payproject', 'project_id' => $project_id]);
                    }

                }
            }

            $modelProject = Projects::find()->where(['id' => $project_id])->one();
            $discount=$modelProject->discount;

            $query = new Query;
            if ($modelProject->project_type == 'customer'){
                $query->select([
                    '*',
                    'id' => 'stormer_info.id',
                    'global_stars'=>'(SELECT SUM(sr.count_star) FROM stormer_rating sr WHERE sr.stormer_id = users.id )',
                    'points_sum'=>'(SELECT SUM(pt.point) FROM points pt WHERE pt.user_id = users.id )',
                    'country_name'=>'(SELECT country_name FROM countries sr WHERE sr.id = stormer_info.country )',
                    'sity_name'=>'(SELECT city_name FROM cities sr WHERE sr.id = stormer_info.city )',
                ])
                    ->from('stormer_info')
                    ->join('LEFT JOIN',
                        'users',
                        'users.id = stormer_info.stormer_id')
                    ->having(['>', 'points_sum', 350])
                    ->limit('100')->orderBy(['global_stars'=>SORT_DESC]);
            } else {
                $query->select([
                    '*',
                    'id' => 'stormer_info.id',
                    'global_stars'=>'(SELECT SUM(sr.count_star) FROM stormer_rating sr WHERE sr.stormer_id = users.id )',
                    'points'=>'(SELECT SUM(pt.point) FROM points pt WHERE pt.user_id = users.id )',
                    'country_name'=>'(SELECT country_name FROM countries sr WHERE sr.id = stormer_info.country )',
                    'sity_name'=>'(SELECT city_name FROM cities sr WHERE sr.id = stormer_info.city )',
                ])
                    ->from('stormer_info')
                    ->join('LEFT JOIN',
                        'users',
                        'users.id = stormer_info.stormer_id')
//                    ->where(['>','points', '350'])
                    ->limit('100')->orderBy(['global_stars'=>SORT_DESC]);
            }
            $command = $query->createCommand();
            $modelStormers = $command->queryAll();

            $modelC = Country::find()->orderBy('country_name ASC')->all();
            $arrayCountry = ArrayHelper::map($modelC, 'id', 'country_name');
            
            return $this->render('select_stormers', [
                'modelProject' => $modelProject,
                'modelStormers' => $modelStormers,
                'arrayCountry' => $arrayCountry,
                'discount'=>$discount,
            ]);
        } else {
            throw new HttpException(404);
        }
    }

    public function actionGetstormer()
    {
        if ($_POST['type'] == 'filter') {
            $filterArray = $_POST['filterParameter'];
            $result = [];
            $filterValue = [];
            $search = [];
            $filterValue['category'] = [];
            foreach ($filterArray as $value) {
                if ($value['name'] == 'changeViewCount') {
                    $filterValue['limit'] = $value['value'];
                }

                if ($value['name'] == 'filterStormerCountry') {
                    if ($value['value'] != 0) {
                        $search['country'] = $value['value'];
                    }
                }

                if ($value['name'] == 'filterStormerCity') {
                    if ($value['value'] != 0) {
                        $search['city'] = $value['value'];
                    }
                }

                if ($value['name'] == 'filterStormerUsername') {
                    if ($value['value']) {
                        $filterValue['username'] = $value['value'];
                    } else {
                        $filterValue['username'] = '';
                    }
                }

                if ($value['name'] == 'multtiSelect[]') {
                    if ($value['value']) {
                        $filterValue['category'][] = $value['value'];
                    } else {
                        $filterValue['category'] = '';
                    }
                }
            }

            $query = new Query;
            if ($_POST['project_id']){
                $project = Projects::findOne($_POST['project_id']);
                if ($project->project_type == 'customer'){
                    $query->select(['*', 'id' => 'stormer_info.id',
                        'global_stars' => '(SELECT SUM(sr.count_star) FROM stormer_rating sr WHERE sr.stormer_id = users.id )',
                        'country_name' => '(SELECT country_name FROM countries sr WHERE sr.id = stormer_info.country )',
                        'sity_name' => '(SELECT city_name FROM cities sr WHERE sr.id = stormer_info.city )',
                        'points_sum' => '(SELECT SUM(pt.point) FROM points pt WHERE pt.user_id = users.id )',
                    ])
                        ->from('stormer_info')
                        ->join('LEFT JOIN',
                            'users',
                            'users.id = stormer_info.stormer_id'
                        )
                        ->where($search)
                        ->andWhere(['LIKE', 'username', $filterValue['username']])
                        ->andWhere(['<>', 'stormer_id', Yii::$app->user->id])
//                        ->andWhere(['>', 'points', '350'])
                        ->having(['>', 'points_sum', 350])
                        ->limit($filterValue['limit'])
                        ->orderBy(['global_stars' => SORT_DESC]);
                } else {
                    $query->select(['*', 'id' => 'stormer_info.id',
                        'global_stars' => '(SELECT SUM(sr.count_star) FROM stormer_rating sr WHERE sr.stormer_id = users.id )',
                        'country_name' => '(SELECT country_name FROM countries sr WHERE sr.id = stormer_info.country )',
                        'sity_name' => '(SELECT city_name FROM cities sr WHERE sr.id = stormer_info.city )',
                        'points' => '(SELECT SUM(pt.point) FROM points pt WHERE pt.user_id = users.id )',
                    ])
                        ->from('stormer_info')
                        ->join('LEFT JOIN',
                            'users',
                            'users.id = stormer_info.stormer_id'
                        )
                        ->where($search)
                        ->andWhere(['LIKE', 'username', $filterValue['username']])
                        ->andWhere(['<>', 'stormer_id', Yii::$app->user->id])
//                        ->andWhere(['>', 'points', '350'])
                        ->limit($filterValue['limit'])
                        ->orderBy(['global_stars' => SORT_DESC]);
                }
            } else {
                $query->select(['*', 'id' => 'stormer_info.id',
                    'global_stars' => '(SELECT SUM(sr.count_star) FROM stormer_rating sr WHERE sr.stormer_id = users.id )',
                    'country_name' => '(SELECT country_name FROM countries sr WHERE sr.id = stormer_info.country )',
                    'sity_name' => '(SELECT city_name FROM cities sr WHERE sr.id = stormer_info.city )',
                    'points_sum' => '(SELECT SUM(pt.point) FROM points pt WHERE pt.user_id = users.id )',
                ])
                    ->from('stormer_info')
                    ->join('LEFT JOIN',
                        'users',
                        'users.id = stormer_info.stormer_id'
                    )
                    ->where($search)
                    ->andWhere(['LIKE', 'username', $filterValue['username']])
                    ->andWhere(['<>', 'stormer_id', Yii::$app->user->id])
//                    ->andWhere(['>', 'points', '350'])
                    ->limit($filterValue['limit'])
                    ->orderBy(['global_stars' => SORT_DESC]);
            }

                if($filterValue['category'] != ''){
                    if ($filterValue['category'] > 1){
                        $masOr = ['or'];
                        foreach ($filterValue['category'] as $key => $categoryN) {
                            $masOr[]=['LIKE', 'category', $categoryN];
                        }
                        $query->andWhere($masOr);
                    } else
                        $query->andWhere(['LIKE', 'category', $filterValue['category'][0]]);
                }

            $command = $query->createCommand();
            $stormersModel = $command->queryAll();

            echo json_encode($stormersModel);

        } elseif ($_POST['type'] == 'getStormers') {
            if($_POST['countGet'] == 4){
                $limit = $_POST['size_team'];
            }else{
                $limit = $_POST['countGet'];
            }
            if ($_POST['project_id']){
                $project = Projects::findOne($_POST['project_id']);
                if ($project->project_type == 'customer') {
                    $modelStor = Stormerinfo::find()
                        ->select(['*', 'points_sum' => '(SELECT SUM(pt.point) FROM points pt WHERE pt.user_id = stormer_info.stormer_id )',])
//                        ->where(['>', 'points', '350'])
                        ->having(['>', 'points_sum', 350])
                        ->orderBy(new \yii\db\Expression('rand()'))->limit($limit)->all();
                } else {
                    $modelStor = Stormerinfo::find()
                        ->select(['*', 'points_sum' => '(SELECT SUM(pt.point) FROM points pt WHERE pt.user_id = stormer_info.stormer_id )',])
//                        ->where(['>', 'points', '350'])
                        ->orderBy(new \yii\db\Expression('rand()'))
                        ->limit($limit)->all();
                }
            } else {
                $modelStor = Stormerinfo::find()
                    ->select(['*', 'points' => '(SELECT SUM(pt.point) FROM points pt WHERE pt.user_id = stormer_info.stormer_id )',])
//                        ->where(['>', 'points', '350'])
                    ->orderBy(new \yii\db\Expression('rand()'))
                    ->limit($limit)->all();
            }

//            $query = new Query;
//            $query->select(['*', 'id' => 'stormer_info.id',
//                'global_stars'=>'(SELECT SUM(sr.count_star) FROM stormer_rating sr WHERE sr.stormer_id = users.id )',
//                'country_name'=>'(SELECT country_name FROM countries sr WHERE sr.id = stormer_info.country )',
//                'sity_name'=>'(SELECT city_name FROM cities sr WHERE sr.id = stormer_info.city )',
//                'points'=>'(SELECT SUM(pt.point) FROM points pt WHERE pt.user_id = users.id )',
//            ])
//                ->from('stormer_info')
//                ->join('LEFT JOIN',
//                    'users',
//                    'users.id = stormer_info.stormer_id')
//                ->where(['>','points', '350'])
//                ->orderBy(new \yii\db\Expression('rand()'))
//                ->limit($limit);
//            $command = $query->createCommand();
//            $modelStor = $command->queryAll();
            $stormer_id = [];
            foreach($modelStor as $stormer){
                $stormer_id[$stormer->stormer_id] = $stormer->stormer_id;
            }


            if (count($stormer_id) > $_POST['size_team'])
                $stormerId = array_rand($stormer_id, $_POST['size_team']);
            else
                $stormerId = $stormer_id;
            echo json_encode($stormerId);

        } elseif ($_POST['type'] == 'getOneStormers') {
            $query = new Query;
            $query->select(['*', 'id' => 'stormer_info.id','global_stars'=>'(SELECT SUM(sr.count_star) FROM stormer_rating sr WHERE sr.stormer_id = users.id )',
                'country_name'=>'(SELECT country_name FROM countries sr WHERE sr.id = stormer_info.country )',
                'sity_name'=>'(SELECT city_name FROM cities sr WHERE sr.id = stormer_info.city )',
            ])
                ->from('stormer_info')
                ->join('LEFT JOIN',
                    'users',
                    'users.id = stormer_info.stormer_id'
                )
                ->where(['stormer_id' => $_POST['stormerId']])
                ->andWhere(['<>','stormer_id',Yii::$app->user->id])
                ->orderBy(['global_stars'=>SORT_DESC]);;

            $command = $query->createCommand();
            $stormersModel = $command->queryOne();
            echo json_encode($stormersModel);
        } else {
            throw new HttpException(404);
        }
    }

    public function actionPayproject($project_id){
        if (!Yii::$app->user->isGuest) {
            $mProject = Projects::find()->where(['id' => $project_id])->one();
            $dPrice = $mProject->size_team * 6 + $mProject->count_idea * 5 * $mProject->size_team;

            if ($mProject->discount){
                $one_procent=$dPrice/100;
                $dPrice=round(($dPrice-($one_procent*$mProject->discount)),2);
            }

            $pay = new Paypal();

            $payer = new Payer();
            $payer->setPaymentMethod("paypal");

            $item2 = new Item();
            $item2->setName('Project '.$mProject->title)
                ->setCurrency('USD')
                ->setQuantity(1)
                ->setSku($mProject->id+'-N-'+time()) // Similar to `item_number` in Classic API
                ->setPrice($dPrice);
            $itemList = new ItemList();
            $itemList->setItems(array($item2));


            $details = new Details();
            $details->setShipping(0.00)
                ->setTax(0.00)
                ->setSubtotal($dPrice);


            $amount = new Amount();
            $amount->setCurrency("USD")
                ->setTotal($dPrice+0.00)
                ->setDetails($details);



            $transaction = new Transaction();
            $transaction->setAmount($amount)
                ->setItemList($itemList)
                ->setDescription('Project '.$mProject->title)
                ->setInvoiceNumber(uniqid());

            $redirectUrls = new RedirectUrls();
            $redirectUrls->setReturnUrl(Url::home(true).'customer/pay?approved=true&id_project='.$project_id)
                ->setCancelUrl(Url::home(true).'customer/pay?approved=false&id_project='.$project_id);


            $payment = new Payment();
            $payment->setIntent("sale")
                ->setPayer($payer)
                ->setRedirectUrls($redirectUrls)
                ->setTransactions(array($transaction));

            try{
                $payment->create($pay->api);
            }catch (PayPalConnectionException $e){

                throw new UserException($e->getMessage());

            }
            $trans= new \app\models\Transaction();
            $trans->user_id= Yii::$app->user->id;
            $trans->price=$dPrice;
            $trans->payment_id=$payment->getId();
            $trans->hash=md5($payment->getId());
            $trans->othe='Create project "'.$mProject->title.'"';
            $trans->project_id=$mProject->id;
            $trans->save();
            var_dump( $payment->getApprovalLink());
                    Yii::$app->getResponse()->redirect($payment->getApprovalLink());
        } else {
            throw new HttpException(403);
        }
    }
    public function actionPay(){


        $pay = new Paypal();

        if (isset($_GET['approved']) && $_GET['approved'] == 'true') {

            $mProject = Projects::find()->where(['id' => $_GET['id_project']])->one();
            $dPrice = $mProject->size_team * 6 + $mProject->count_idea * 5 * $mProject->size_team;

            if($mProject){
                $paymentId = $_GET['paymentId'];

                $payment = Payment::get($paymentId, $pay->api);

                $execution = new PaymentExecution();
                $execution->setPayerId($_GET['PayerID']);
//                $payment->execute($execution, $pay->api);


                try {
                    $result = $payment->execute($execution, $pay->api);
                } catch (PayPalConnectionException $e) {
                    throw new UserException($e->getMessage());
                }

                $mProject->active=1;
                $mProject->scenario='update_status';
                $mProject->save();

//                Projectstormer::updateAll(['confirmation'=>0],['project_id'=>$mProject->id]);

                $transaction = \app\models\Transaction::find()->where(['user_id'=>Yii::$app->user->id,'hash'=>md5($paymentId)])->one();
                $transaction->complete=1;
                $transaction->save();


                $point = new Points();
                $point->point=50;
                $point->user_id=Yii::$app->user->id;
                $point->type='customer_50';
                $point->date_week=$point->week();
                $point->save();

                return $this->render('complete',
                    [
                        'message'=>true,
                        'message_send'=>''
                    ]);

//                $transaction = new Transaction();
//                $amount = new Amount();
//                $details = new Details();
//                $details->setShipping(0.00)
//                    ->setTax(0.00)
//                    ->setSubtotal($dPrice);
//                $amount->setCurrency('USD');
//                $amount->setTotal($dPrice+0.00);
//                $amount->setDetails($details);
//                $transaction->setAmount($amount);
//
//                $execution->addTransaction($transaction);
//                try {
//
//                    $result = $payment->execute($execution, $pay->api);
//
//                    try {
//                        $payment = Payment::get($paymentId, $pay->api);
//                    } catch (PayPalConnectionException $e) {
//                        throw new UserException($e->getMessage());
//                    }
//                } catch (PayPalConnectionException $e) {
//                    throw new UserException($e->getMessage());
//                }


            } else {
                throw new UserException('Not found project');
            }


        } else {
            return $this->render('complete',
                [
                    'message'=>false,
                    'message_send'=>'You refused to pay for the project!'
                ]);
        }
    }
    public function actionPaymentproject($project_id, $status = null)
    {
        if (!Yii::$app->user->isGuest) {
            $mProject = Projects::find()->where(['id' => $project_id])->one();
            /*
                $mProject->scenario = "update_status";
                $mProject->save();
            */

            $dPrice = $mProject->size_team * 6 + $mProject->count_idea * 5 * $mProject->size_team;
            $aUrls = array(
              'cancel_url' => Url::to(['paymentproject','project_id' => $project_id, 'status' => "cancel"],true),
              'notify_url' => Url::to(['paymentproject','project_id' => $project_id, 'status' => "notify"],true),
              'return_url' => Url::to(['paymentproject','project_id' => $project_id, 'status' => "pdt"],true),
              'sended' => Url::to(['paymentproject','project_id' => $project_id, 'status' => "sended"],true),
            );

            switch ($status) {
                case 'pdt':
                    $request = curl_init();

                    $tx_token = (isset($_GET['tx']))?$_GET['tx']:'';
                    $auth_token = \Yii::$app->params['paypal_access_token'];
                    // Set request options
                    curl_setopt_array($request, array(
                        CURLOPT_URL => \Yii::$app->params['paypal'],
                        CURLOPT_POST => true,
                        CURLOPT_POSTFIELDS => http_build_query(array(
                            'cmd' => '_notify-synch',
                            'tx' => $tx_token,
                            'at' => $auth_token,
                        )),
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_HEADER => false,
                        CURLOPT_SSL_VERIFYPEER => false,
                    ));

                    // Execute request and get response and status code
                    $res = curl_exec($request);
                    $status = curl_getinfo($request, CURLINFO_HTTP_CODE);
                    if (!$res) {

                        echo 'Ошибка curl: ' . curl_error($request);
                    } else {
                        curl_close($request);
                        // parse the data
                        $lines = explode("\n", $res);
                        error_reporting(E_ALL & ~E_NOTICE);

                        $keyarray = array();
                        if (strcmp($lines[0], 'SUCCESS') == 0) {
                            for ($i = 1; $i < count($lines); ++$i) {
                                list($key, $value) = explode('=', $lines[$i]);
                                $keyarray[urldecode($key)] = urldecode($value);
                            }
                            if($keyarray['payment_status'] != "Completed"){
                              throw new UserException("Payment Status ".$keyarray['payment_status']);
                            }
                            if($keyarray['item_number'] != $project_id){
                              throw new UserException("Wrong project");
                            }
                            if(Transactions::find()->where(['txn_id' => $keyarray['txn_id']])->exists()){
                              throw new UserException("This purchase already Completed");
                            }
                            if(\Yii::$app->params['paypalEmail'] != $keyarray['business']){
                              throw new UserException("Purchase to another Account");
                            }
                            if(($dPrice != $keyarray['payment_gross'])||($keyarray['mc_currency']!='USD')){
                              throw new UserException("Error price");
                            }
                            // process payment
                            $mTransaction = new Transactions();
                            $mTransaction->project_id = $keyarray['item_number'];
                            $mTransaction->payer_email = $keyarray['payer_email'];
                            $mTransaction->txn_id = $keyarray['txn_id'];
                            $mTransaction->first_name = $keyarray['first_name'];
                            $mTransaction->payer_id = $keyarray['payer_id'];
                            $mTransaction->payment_gross = $keyarray['payment_gross'];
                            $mTransaction->last_name = $keyarray['last_name'];
                            $mTransaction->residence_country = $keyarray['residence_country'];

                            if($mTransaction->validate()&&$mTransaction->save()){
                                  $mProject->scenario = "update_status";
                                  $mProject->active = 1;
                                  $mProject->save();

                              return $this->render('payment', [
                                  'price'=>$dPrice,
                                  'aUrls'=>$aUrls,
                                  'mProject'=>$mProject,
                              ]);
                            }else{
                              return $this->render('cancel_payment', [
                                'errors' => $mTransaction->getErrors()
                              ]);
                            }
                            // ['project_id', 'payer_email', 'txn_id', 'first_name', 'payer_id', 'payment_gross', 'last_name', 'residence_country']
                        } elseif (strcmp($lines[0], 'FAIL') == 0) {
                            throw new UserException("Error Processing Request");
                        }
                    }
                    break;
                case "cancel":
                  return $this->render('cancel_payment', [
                    'errors' => [],
                    'dPrice'=>$dPrice,
                    'aUrls'=>$aUrls,
                    'mProject'=>$mProject,
                  ]);
                    break;
                case "sended":
                  $mProject->scenario = "update_status";
                  $mProject->active = 2;
                  if($mProject->validate()&&$mProject->save()){
                    return 1;
                  }else {
                    throw new HttpException(500);
                  }
                  break;
                case "notify":
                  // CONFIG: Enable debug mode. This means we'll log requests into 'ipn.log' in the same directory.
                  // Especially useful if you encounter network errors or other intermittent problems with IPN (validation).
                  // Set this to 0 once you go live or don't require logging.
                  define("DEBUG", 1);
                  // Set to 0 once you're ready to go live
                  define("USE_SANDBOX", 1);
                  define("LOG_FILE", "./ipn.log");
                  // Read POST data
                  // reading posted data directly from $_POST causes serialization
                  // issues with array data in POST. Reading raw POST data from input stream instead.
                  $raw_post_data = file_get_contents('php://input');
                  $raw_post_array = explode('&', $raw_post_data);
                  $myPost = array();
                  foreach ($raw_post_array as $keyval) {
                  	$keyval = explode ('=', $keyval);
                  	if (count($keyval) == 2)
                  		$myPost[$keyval[0]] = urldecode($keyval[1]);
                  }
                  // read the post from PayPal system and add 'cmd'
                  $req = 'cmd=_notify-validate';
                  if(function_exists('get_magic_quotes_gpc')) {
                  	$get_magic_quotes_exists = true;
                  }
                  foreach ($myPost as $key => $value) {
                  	if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                  		$value = urlencode(stripslashes($value));
                  	} else {
                  		$value = urlencode($value);
                  	}
                  	$req .= "&$key=$value";
                  }
                  // Post IPN data back to PayPal to validate the IPN data is genuine
                  // Without this step anyone can fake IPN data
                  if(USE_SANDBOX == true) {
                  	$paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
                  } else {
                  	$paypal_url = "https://www.paypal.com/cgi-bin/webscr";
                  }
                  $ch = curl_init($paypal_url);
                  if ($ch == FALSE) {
                  	return FALSE;
                  }
                  curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
                  curl_setopt($ch, CURLOPT_POST, 1);
                  curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
                  curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
                  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
                  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                  curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
                  if(DEBUG == true) {
                  	curl_setopt($ch, CURLOPT_HEADER, 1);
                  	curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
                  }
                  // CONFIG: Optional proxy configuration
                  //curl_setopt($ch, CURLOPT_PROXY, $proxy);
                  //curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
                  // Set TCP timeout to 30 seconds
                  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
                  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
                  // CONFIG: Please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path
                  // of the certificate as shown below. Ensure the file is readable by the webserver.
                  // This is mandatory for some environments.
                  //$cert = __DIR__ . "./cacert.pem";
                  //curl_setopt($ch, CURLOPT_CAINFO, $cert);
                  $res = curl_exec($ch);
                  if (curl_errno($ch) != 0) // cURL error
                  	{
                  	if(DEBUG == true) {
                  		error_log(date('[Y-m-d H:i e] '). "Can't connect to PayPal to validate IPN message: " . curl_error($ch) . PHP_EOL, 3, LOG_FILE);
                  	}
                  	curl_close($ch);
                  	exit;
                  } else {
                  		// Log the entire HTTP response if debug is switched on.
                  		if(DEBUG == true) {
                  			error_log(date('[Y-m-d H:i e] '). "HTTP request of validation request:". curl_getinfo($ch, CURLINFO_HEADER_OUT) ." for IPN payload: $req" . PHP_EOL, 3, LOG_FILE);
                  			error_log(date('[Y-m-d H:i e] '). "HTTP response of validation request: $res" . PHP_EOL, 3, LOG_FILE);
                  		}
                  		curl_close($ch);
                  }
                  // Inspect IPN validation result and act accordingly
                  // Split response headers and payload, a better way for strcmp
                  $tokens = explode("\r\n\r\n", trim($res));
                  $res = trim(end($tokens));
                  if (strcmp ($res, "VERIFIED") == 0) {
                  	// check whether the payment_status is Completed
                  	// check that txn_id has not been previously processed
                  	// check that receiver_email is your PayPal email
                  	// check that payment_amount/payment_currency are correct
                  	// process payment and mark item as paid.
                  	// assign posted variables to local variables
                  	//$item_name = $_POST['item_name'];
                  	//$item_number = $_POST['item_number'];
                  	//$payment_status = $_POST['payment_status'];
                  	//$payment_amount = $_POST['mc_gross'];
                  	//$payment_currency = $_POST['mc_currency'];
                  	//$txn_id = $_POST['txn_id'];
                  	//$receiver_email = $_POST['receiver_email'];
                  	//$payer_email = $_POST['payer_email'];

                  	if(DEBUG == true) {
                  		error_log(date('[Y-m-d H:i e] '). "Verified IPN: $req ". PHP_EOL, 3, LOG_FILE);
                  	}
                  } else if (strcmp ($res, "INVALID") == 0) {
                  	// log for manual investigation
                  	// Add business logic here which deals with invalid IPN messages
                  	if(DEBUG == true) {
                  		error_log(date('[Y-m-d H:i e] '). "Invalid IPN: $req" . PHP_EOL, 3, LOG_FILE);
                  	}
                  }
                  break;
                default:
                    return $this->render('payment', [
                        'price'=>$dPrice,
                        'aUrls'=>$aUrls,
                        'mProject'=>$mProject,
                    ]);
                    break;
            }
        } else {
            throw new HttpException(403);
        }
    }

    public function actionMyprojects($type=null)
    {
        $newFeedbackModal = new Feedback();
        $newFeedbackModal->scenario = 'add_feedback';
        
        if ($_POST) {
            if (isset($_POST['Feedback'])) {
                $request = Yii::$app->request;
                if ($newFeedbackModal->load($request->post())) {
                    $newFeedbackModal->user_id=Yii::$app->user->id;
                    if ($newFeedbackModal->save()) {
                        $project = Projects::find()->where(['id' => $newFeedbackModal->project_id])->one();
                        $project->scenario = 'update_status';
                        $project->status = 0;
                        if ($project->save()) {
                            return $this->redirect('/customer/organizeidea/' . $newFeedbackModal->project_id);
                        }
                    }
                }
            } elseif (isset($_POST['buttonCloseProject'])) {
                $project = Projects::find()->where(['id' => $_POST['project_id']])->one();
                $project->scenario = 'update_status';
                $project->status = 0;
                if ($project->save()) {
                    return $this->redirect('/customer/organizeidea/' . $_POST['project_id']);
                }
            }
        }
        $newFeedbackModal->question = '2';
        
        if($type == 'current'){
            $query = Projects::find()->where(['owner_id' => Yii::$app->user->id])->andWhere(['status' => '1'])->orderBy(['id' => SORT_DESC]);
            $modelProjectsOpen = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 10]]);
            
            foreach ($modelProjectsOpen->getModels() as $projectsO) {
                $query = new Query;
                $query->select(['users.username', 'id' => 'project_stormer.id'])
                    ->from('project_stormer')
                    ->join('LEFT JOIN',
                        'users',
                        'users.id = project_stormer.stormer_id'
                    )
                    ->where(['project_id' => $projectsO->id]);

                $command = $query->createCommand();
                $projectsO->team = $command->queryAll();
                $projectsO->countIdeaUser = Ideas::find()->where(['project_id' => $projectsO->id])->count();
            }
            return $this->render('mycurrentprojects', [
                'newFeedbackModal' => $newFeedbackModal,
                'modelProjectsOpen' => $modelProjectsOpen->getModels(),
                'pagination' => $modelProjectsOpen->pagination,
                'count' => $modelProjectsOpen->pagination->totalCount,
            ]);
            
        }elseif($type== 'past'){
            $query = Projects::find()->where(['owner_id' => Yii::$app->user->id])->andWhere(['status' => '0'])->orderBy(['id' => SORT_DESC]);
            $modelProjectsClose = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 10]]);
        
            foreach ($modelProjectsClose->getModels() as $projectsC) {
                $query = new Query;
                $query->select(['users.username', 'id' => 'project_stormer.id'])
                    ->from('project_stormer')
                    ->join('LEFT JOIN',
                        'users',
                        'users.id = project_stormer.stormer_id'
                    )
                    ->where(['project_id' => $projectsC->id]);

                $command = $query->createCommand();
                $projectsC->team = $command->queryAll();
            }
            
            return $this->render('mypastprojects', [
                'newFeedbackModal' => $newFeedbackModal,
                'modelProjectsClose' => $modelProjectsClose->getModels(),
                'pagination' => $modelProjectsClose->pagination,
                'count' => $modelProjectsClose->pagination->totalCount,
            ]);
            
        }else{
            throw new HttpException(404);
        }
    }

    function OrganizeRec($id, $data){
        $searIdeaChild = Ideas::find()->where(['parent_id'=>$id])->all();
        if($searIdeaChild) {
            foreach ($searIdeaChild as $item) {
                $item->scenario = 'organize';
                $item->organize = 1;
                $item->save();
                if(!Organizeidea::find()->where(['idea_id'=>$item->id])->one()) {
                    $orId = new Organizeidea();
                    $orId->scenario = 'add_organize';
                    $orId->load($data);
                    if($orId->question_1 <= 3 ){
                        $orId->category = 'what you know works';
                    } else {
                        $orId->category = 'out of box thinking';
                    }

                    if(($orId->question_2 >= 3) or ($orId->question_4 >= 3)){
                        $orId->cat_child = 'can be implemented with little expense to resources';
                    }

                    if(($orId->question_2 < 3) or ($orId->question_4 < 3) or ($orId->question_3 >= 3) or ($orId->question_5 >= 3)){
                        $orId->cat_child = "needs developing - develop further (<a href ='/customer/post-project/create?title=".$item->ideas_description."'>create</a>)";
                    }

                    if(($orId->question_2 < 3) or ($orId->question_4 < 3) or ($orId->question_3 < 3) or ($orId->question_5 < 3)){
                        $orId->cat_child = 'not usable at this time';
                    }
                    $orId->idea_id = $item->id;
                    $orId->save();
                }
                $this->OrganizeRec($item->id,$data);
            }
        }
    }

    public function actionOrganize($project_id = null)
    {
        $projectMy = Projects::findOne($project_id);
        $newOrganizeIdea = new Organizeidea();
        $newOrganizeIdea->scenario = 'add_organize';
        if (Yii::$app->request->post()) {
            $data = Yii::$app->request->post();
            if ($newOrganizeIdea->load(Yii::$app->request->post())) {
                $modelIdea = Ideas::findOne($newOrganizeIdea->idea_id);
                $modelIdea->scenario = 'organize';
                $modelIdea->organize = 1;

                if($newOrganizeIdea->question_1 <= 3 ){
                    $newOrganizeIdea->category = 'what you know works';
                } else {
                    $newOrganizeIdea->category = 'out of box thinking';
                }

                if(($newOrganizeIdea->question_2 >= 3) or ($newOrganizeIdea->question_4 >= 3)){
                    $newOrganizeIdea->cat_child = 'can be implemented with little expense to resources';
                }

                if(($newOrganizeIdea->question_2 < 3) or ($newOrganizeIdea->question_4 < 3) or ($newOrganizeIdea->question_3 >= 3) or ($newOrganizeIdea->question_5 >= 3)){
                    $newOrganizeIdea->cat_child = "needs developing - develop further (<a href ='/customer/post-project/create?title=".$modelIdea->ideas_description."'>create</a>)";
                }

                if(($newOrganizeIdea->question_2 < 3) or ($newOrganizeIdea->question_4 < 3) or ($newOrganizeIdea->question_3 < 3) or ($newOrganizeIdea->question_5 < 3)){
                    $newOrganizeIdea->cat_child = 'not usable at this time';
                }



                if ($modelIdea->save() && $newOrganizeIdea->save()) {
                    $searIdeaChild = Ideas::find()->where(['parent_id'=>$modelIdea->id])->all();
                    if($searIdeaChild){
                        foreach($searIdeaChild as $item){
                            $item->scenario = 'organize';
                            $item->organize = 1;
                            $item->save();
                            if(!Organizeidea::find()->where(['idea_id'=>$item->id])->one()) {
                                $orId = new Organizeidea();
                                $orId->scenario = 'add_organize';
                                $orId->load($data);
                                if($orId->question_1 <= 3 ){
                                    $orId->category = 'what you know works';
                                } else {
                                    $orId->category = 'out of box thinking';
                                }

                                if(($orId->question_2 >= 3) or ($orId->question_4 >= 3)){
                                    $orId->cat_child = 'can be implemented with little expense to resources';
                                }

                                if(($orId->question_2 < 3) or ($orId->question_4 < 3) or ($orId->question_3 >= 3) or ($orId->question_5 >= 3)){
                                    $orId->cat_child = "needs developing - develop further (<a href ='/customer/post-project/create?title=".$item->ideas_description."'>create</a>)";
                                }

                                if(($orId->question_2 < 3) or ($orId->question_4 < 3) or ($orId->question_3 < 3) or ($orId->question_5 < 3)){
                                    $orId->cat_child = 'not usable at this time';
                                }
                                $orId->idea_id = $item->id;
                                $orId->save();
                            }
                            $this->OrganizeRec($item->id,$data);
                        }
                    }


                    $this->redirect('/customer/organizeidea/'.$project_id);
                }

            }
            //var_dump($_POST);exit;
        }

        $ideas = Ideas::find()->where(['project_id' => $project_id,'organize' => 0])->all();
        $mindM = (object) [
            'meta' => (object) [
                'name' => 'Divergent',
                'author' => 'Ideas',
                'version' => '0.2',
            ],
            'format' => 'node_array',
            'data' => [(object) [
                'id' => 'root',
                'isroot' => true,
                'topic' => substr($projectMy->title, 0, 5).'...',
                'editable' => false,
            ]],
        ];
        foreach ($ideas as $idea) {
            // var_dump($idea['ideas_description']);
            $mindM->data[] = (object) [
                'id' => (string) $idea['id'],
                'parentid' => (!empty($idea['parent_id'])) ? $idea['parent_id'] : "root",
                'topic' => $idea['ideas_description'],
                'editable' => false,
            ];
        }


        return $this->render('organize',[
            'count_idea'=> $projectMy->count_idea,
            'count_idea_user'=> Ideas::find()->where(['project_id' => $project_id, 'stormer_id' => Yii::$app->user->identity->id])->count(),
            'project_id' => $project_id,
            'mindM' => json_encode($mindM),
            'project'=>$projectMy,
            'newOrganizeIdea' => $newOrganizeIdea,
        ]);
    }


    public function actionOrganizeidea($project_id = null)
    {

        $newOrganizeIdea = new Organizeidea();
        $newOrganizeIdea->scenario = 'add_organize';
        if ($_POST) {
            $request = Yii::$app->request;
            if ($newOrganizeIdea->load($request->post())) {
                $modelIdea = Ideas::find()->where(['id' => $newOrganizeIdea->idea_id])->one();
                $modelIdea->scenario = 'organize';
                $modelIdea->organize = 1;

                if ($modelIdea->save() && $newOrganizeIdea->save()) {
                    //var_dump($modelIdea);exit;
                }

            }
            //var_dump($_POST);exit;
        }


//        $query = new Query;
//        $query->select(['*', 'id' => 'ideas.id'])
//            ->from('ideas')
//            ->leftJoin('users', 'users.id = ideas.stormer_id')
//            ->where(['project_id' => $project_id,'organize' => 1])
//            ->orderBy(['ideas.organize' => SORT_ASC]);
//
//        $command = $query->createCommand();
//        $modelIdeas = $command->queryAll();

        $countIdeaNotOrganize = Ideas::find()->where(['project_id' => $project_id, 'organize' => 0])->count();

        //$modelIdea = Ideas::find()->where(['project_id' => $post_id])->all();

        $searchModel = new IdeasSearch(['project_id' => $project_id, 'organize' => 1]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $message = false;
        $message_procent = false;

        $project = Projects::findOne($project_id);

        $stormers = Projectstormer::find()->where(['project_id'=>$project_id, 'confirmation'=>1])->orderBy(['updated_at'=>SORT_ASC])->all();
        $mas_stormer=null;
        if($stormers){
            $k=1;
            foreach($stormers as $stormer){
                $mas_stormer[$stormer->id]=$k;
                $k++;
            }
        }


        if (($project->show_message != 1)){
            if($project->show_message != 2)
                $message = true;
        }elseif($project->show_message != 2){
            $message_procent = true;
        }

        $one_procent=100/($project->count_idea*$project->size_team);
        if( ($one_procent*\app\models\Ideas::find()->where(['project_id'=>$project_id])->count()) > 100){
            $procent =100;
        }else{
            $procent = $one_procent*\app\models\Ideas::find()->where(['project_id'=>$project_id])->count();
        }

        return $this->render('organize_idea', [
//            'modelIdea' => $modelIdeas,
            'project_id' => $project_id,
            'newOrganizeIdea' => $newOrganizeIdea,
            'countIdeaNotOrganize' => $countIdeaNotOrganize,
            'message'=>$message,
            'message_procent'=>$message_procent,
            'procent'=>$procent,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'mas_stormer' => $mas_stormer,
        ]);
    }

    public function actionDevelopfurther($project_id = null)
    {
        return $this->render('developfurther', [

        ]);
    }

    public function actionBehindthescenes($project_id = null)
    {
        /*
        $query = new Query;
        $query->select(['*', 'id' => 'ideas.id'])
            ->from('ideas')
            ->leftJoin('users', 'users.id = ideas.stormer_id')
            ->where(['project_id' => $project_id])
            ->orderBy(['ideas.organize' => SORT_ASC]);

        $command = $query->createCommand();
        $modelIdeas = $command->queryAll();

        //sort ideas by user /begin
        $arrayIdeas = [];
        foreach ($modelIdeas as $ideas) {
            $arrayIdeas[$ideas['stormer_id']]['idea'][] = $ideas;
        }
        //sort ideas by user /end

        //add rating to idea/begin
        //$arrayIdeas = [];
        foreach ($arrayIdeas as $key => $ideas) {
            $modelUserrating = Userrating::find()->where(['project_id' => $project_id, 'stormer_id' => $key])->one();
            $arrayIdeas[$key]['rating'] = $modelUserrating;
        }
        //add rating to idea /end
        //var_dump($arrayIdeas);exit;

        $projectModel = Projects::find()->where(['id' => $project_id])->one();

*/

        $projectMy = Projects::findOne($project_id);
        $ideas = Ideas::find()->where(['project_id' => $project_id])->all();
        $mindM = (object) [
            'meta' => (object) [
                'name' => 'Divergent',
                'author' => Yii::$app->user->identity->username,
                'version' => '0.2',
            ],
            'format' => 'node_array',
            'data' => [(object) [
                'id' => 'root',
                'isroot' => true,
                'topic' => substr($projectMy->title, 0, 5).'...',
                'editable' => false,
            ]],
        ];

        $stormers = Projectstormer::find()->where(['project_id' => $project_id, 'confirmation'=>1])->all();
        $i = 0;
        foreach ($stormers as $stormer) {
            ++$i;

            $star = Userrating::find()->where(['stormer_id'=>$stormer['stormer_id'],'project_id'=>$projectMy->id])->one();
            $mindM->data[] = (object) [
                'id' => (string) $stormer['stormer_id'],
                'parentid' => 'root',
//          'topic' => User::findById($stormer['stormer_id'])['username'],
                'topic' => $star ? '<span aria-hidden="true" class="glyphicon glyphicon-star"></span> Stormer'.$i : 'Stormer'.$i,
                'direction' => ($i % 2) ? 'right' : 'left',
                'editable' => false,
                'value'=> 'stormer',
              'e_update'=> false,
                // ''
            ];
        }
        foreach ($ideas as $idea) {
            // var_dump($idea['ideas_description']);
            $star = Userrating::find()->where(['idea_id'=>$idea['id']])->one();
            $mindM->data[] = (object) [
                'id' => (string) $idea['id'],
                'parentid' => (!empty($idea['parent_id'])) ? $idea['parent_id'] : (string) $idea['stormer_id'],
                'topic' => $star? '<span aria-hidden="true" class="glyphicon glyphicon-star"></span>'.$idea['ideas_description']:$idea['ideas_description'],
                'editable' => false,
              'e_update'=> false,
                'value'=> 'idea',
            ];
        }


        return $this->render('behindthescenes1', [
            'count_idea'=> $projectMy->count_idea,
            'project_id' => $project_id,
            'mindM' => json_encode($mindM),
            'ideas' => $ideas,
            'projectMy' => $projectMy,
        ]);
    }

    public function actionUserratingajax()
    {
        $idea_id = $_POST['idea_id'];

        $idea = Ideas::findOne($idea_id);
        if($idea){
            $star = Userrating::find()->where(['customer_id'=>Yii::$app->user->id,'idea_id'=>$idea_id])->one();
            if ($star){

            }else{
                $modelUserrating = new Userrating;
                $modelUserrating->stormer_id = $idea->stormer_id;
                $modelUserrating->project_id = $idea->project_id;
                $modelUserrating->customer_id = Yii::$app->user->id;
                $modelUserrating->idea_id=$idea_id;
                $modelUserrating->count_star = 1;
                $modelUserrating->save();

                $points = new Points();
                $points->user_id =$idea->stormer_id;
                $points->point = 2;
                $points->othe = $idea->id;
                $points->type='stars';
                $points->date_week=$points->week();
                $points->save();
            }
            return true;
        }else {
            return false;
        }
    }

    public function actionFlaggingideasajax()
    {
        $idea_id = $_POST['idea_id'];
        $why_flagging = $_POST['why_flagging'];

        $modelFlaggingideas = new Flaggingideas;
        $modelFlaggingideas->idea_id = $idea_id;
        $modelFlaggingideas->customer_id = \Yii::$app->user->id;
        $modelFlaggingideas->why_flagging = $why_flagging;
        $result['status'] = [];
        if ($modelFlaggingideas->save()) {
            $modelIdea = Ideas::find()->where(['id' => $idea_id])->one();
            $modelIdea->scenario = 'flagging';
            $modelIdea->flagging = 1;
            if ($modelIdea->save()) {
                $result['status'] = 'success';
            } else {
                $result['status'] = 'error dsfsdf';
            }
        } else {
            $result['status'] = 'error';
            $result['error'] = $modelFlaggingideas->errors;
        }
        echo json_encode($result);
    }
    
    public function actionTelltheworld()
    {
        return $this->render('telltheworld', [
            
        ]);
    }
    
    public function actionReferalpage()
    {
        $user = User::findOne(Yii::$app->user->id);
        if ($user->referal_key){
            $referal_key = $user->referal_key;
        }else{
            $user->scenario='referal_key';
            $user->referal_key=$user->referal();
            $referal_key = $user->referal_key;
            $user->save();
        }
        return $this->render('referalpage', [
            'user' => $user
        ]);
    }
    
    public function actionSendreferal()
    {
        $recipient_name = $_POST['recipient_name'];
        $recipient_email = $_POST['recipient_email'];
        $senders_email = $_POST['senders_email'];

        $user = User::findOne(Yii::$app->user->id);
        if ($user->referal_key){
            $referal_key = $user->referal_key;
        }else{
            $user->scenario='referal_key';
            $user->referal_key=$user->referal();
            $referal_key = $user->referal_key;
            $user->save();
        }

        if (isset($_POST['message'])){
            Yii::$app->mailer->compose('send_mail',[
                'recipient_name'=>$recipient_name,
                'recipient_email'=>$recipient_email,
                'referal_key'=>$referal_key,
                'message'=>$_POST['message'],
            ]) // здесь устанавливается результат рендеринга вида в тело сообщения
            ->setFrom($senders_email)
                ->setTo($recipient_email)
                ->setSubject('braincloud.solutions')
                ->send();
        } else {
            Yii::$app->mailer->compose('customer-referal',[
                'recipient_name'=>$recipient_name,
                'recipient_email'=>$recipient_email,
                'referal_key'=>$referal_key,
            ]) // здесь устанавливается результат рендеринга вида в тело сообщения
            ->setFrom($senders_email)
                ->setTo($recipient_email)
                ->setSubject('braincloud.solutions')
                ->send();
        }


        if (!SalesTeams::find()->where(['email'=>$recipient_email])->one()){
            $sales_team = new SalesTeams();
            $sales_team->name=$recipient_name;
            $sales_team->email=$recipient_email;
            $sales_team->user_id=Yii::$app->user->id;
            $sales_team->role='customer';
            $sales_team->status=0;
            $sales_team->save();
        }
        $result['status'] = 'send';
        $result['post'] = $_POST;
        echo json_encode($result);
    }
    
    public function actionSupportpage()
    {
        $modelFaqs = Faqs::find()->where(['type' => 'customer'])->all();
        return $this->render('support_page', [
            'modelFaqs' => $modelFaqs,
        ]);
    }
    
    public function actionCustomerabout()
    {
        $modelPages = Pages::find()->where(['page_url' => 'customer/about'])->one();
        return $this->render('customer_about', [
            'content' => $modelPages->content,
        ]);
    }
    
    public function actionGetcity()
    {
        $result = [];
        $modelCity = City::find()->where(['country_id' => $_POST['country_id']])->orderBy('city_name ASC')->all();
        $arrayCountry = ArrayHelper::map($modelCity, 'id', 'city_name');
        ArrayHelper::multisort($arrayCountry, ['city_name'], [SORT_ASC]);
        $result['city'] = $arrayCountry;
        if($arrayCountry != null){
            $result['status'] = 'good';
        }else{
            $result['status'] = 'null';
        }
        echo json_encode($result);
    }
    
    public function actionTerms(){
        $modelPages = Pages::find()->where(['page_url' => 'customer/terms'])->one();
        return $this->render('terms', [
            'content' => $modelPages->content,
        ]);
    }

    public function actionTerms_conditions(){
        $modelPages = Pages::find()->where(['page_url' => 'customer/terms'])->one();
        return $this->render('terms_conditions', [
            'content' => $modelPages->content,
        ]);
    }
    
    public function actionGetperspectivequestion(){
        $question = Projectquestion::find()->where(['type' => 'perspectives'])->orderBy(new \yii\db\Expression('rand()'))->one();
        if ($question){
            echo json_encode($question->question_text);
        }
    }

    public function actionNot_corect_timeline(){
        $message = new ContactUs();
        $message->username=Yii::$app->user->identity->username;
        $message->descriptions = 'Not_corect_timeline';
        $message->timeline_inquire=1;
        $message->save();
        echo json_encode(true);
    }
    
    public function actionGetquestiontext(){
//        use app\models\Projectdivergentquestion;
//        use app\models\Projectconvergentquestion;
        $question = '';
        if($_POST['question_id'] != ''){
            $question = Projectquestion::find()->where(['id' => $_POST['question_id']])->one();
        }
        if($question != null){
            echo json_encode($question->question_text);
        }else{
            echo json_encode('');
        }
    }


    //listat_an
    public function actionDevelopyour($title, $id){
        $valid=true;
        $displayArrow = false;
        $discount = 0;

        $count = ProjectAudit::find()->where(['project_id'=>$id, 'status'=>1])->count()+1;
        if (($count<=3)){
            $discount_bd = DiscountProject::find()->where(['count'=>$count])->one();
            if($discount_bd){
                $discount=$discount_bd->discount;
            }
        }else{
            $discount_bd = DiscountProject::find()->where(['count'=>3])->one();
            if($discount_bd){
                $discount=$discount_bd->discount;
            }
        }


        if (!ProjectAudit::find()->where(['title'=>$title,'project_id'=>$id])->one()) {
            if (Yii::$app->request->post()) {

                $audit = new ProjectAudit();
                $audit->title = $title;
                $audit->project_id = $id;
                $audit->size_team = Yii::$app->request->post('slider_team_size');
                $audit->count_idea = Yii::$app->request->post('slider_scope_project');
                $audit->token = Yii::$app->security->generateRandomString() . time();
                $audit->save();
                $this->redirect('/customer/post-project?token='.$audit->token);
            }
        }else {

            $audit = ProjectAudit::find()->where(['title'=>$title,'project_id'=>$id])->one();
            if (ProjectAudit::find()->where(['title'=>$title,'project_id'=>$id, 'status'=>1])->one()){
                $valid = false;
                $displayArrow = true;
            }else {
                if (Yii::$app->request->post()) {

                    $audit->size_team = Yii::$app->request->post('slider_team_size');
                    $audit->count_idea = Yii::$app->request->post('slider_scope_project');
                    $audit->token = Yii::$app->security->generateRandomString() . time();
                    $audit->save();
                    $this->redirect('/customer/post-project?token=' . $audit->token);
                }
            }

        }

        return $this->render('developyour', [
            'valid'=>$valid,
            'displayArrow'=>$displayArrow,
            'discount'=>$discount,
        ]);
    }

    public function actionMessage($id, $user_id=''){

        $project = Projects::findOne($id);
        $project_stormer = Projectstormer::find()->where(['project_id'=>$id])->all();
        $aler = false;
        if($user_id){
            if (Yii::$app->request->post()){
                $new_message=new Message();
                $new_message->message=Yii::$app->request->post('message');
                $new_message->stormer_id=$user_id;
                $new_message->project_id=$id;
                $new_message->type_user='customer';
                $new_message->save();
                $aler=true;
            }

            $messages = Message::find()->where(['project_id'=>$id,'stormer_id'=>$user_id])->orderBy(['created_at'=>SORT_DESC]);

            $countQuery = clone $messages;
            $pages = new Pagination(['totalCount' => $countQuery->count(),'pageSize' => 10]);
            $models = $messages->offset($pages->offset)
                ->limit($pages->limit)
                ->all();

            return $this->render('message_stormer',[
                'aler'=>$aler,
                'project'=>$project,
                'messages'=>$models,
                'pages' => $pages,
            ]);
        }
        return $this->render('message',[
            'project'=>$project,
            'project_stormer'=>$project_stormer,
        ]);
    }
    public function actionInvoices(){

        $searchModel = new TransactionSearch(['user_id'=>Yii::$app->user->id,'complete'=>1]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('invoices',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionAdminconfirmation()
    {
        return $this->render('adminconfirmation',[
        ]);
    }
    public function actionBeinspired($type)
    {

        $query = Projects::find()->where(['project_type' => $type,'be_inspired'=>1])->orderBy(['id' => SORT_DESC]);
        $modelProjectsOpen = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 10]]);

        foreach ($modelProjectsOpen->getModels() as $projectsO) {
            $query = new Query;
            $query->select(['users.username', 'id' => 'project_stormer.id'])
                ->from('project_stormer')
                ->join('LEFT JOIN',
                    'users',
                    'users.id = project_stormer.stormer_id'
                )
                ->where(['project_id' => $projectsO->id]);

            $command = $query->createCommand();
            $projectsO->team = $command->queryAll();
            $projectsO->countIdeaUser = Ideas::find()->where(['project_id' => $projectsO->id])->count();
        }
        return $this->render('beinspired', [
//            'newFeedbackModal' => $newFeedbackModal,
            'type'=>$type,
            'modelProjectsOpen' => $modelProjectsOpen->getModels(),
            'pagination' => $modelProjectsOpen->pagination,
            'count' => $modelProjectsOpen->pagination->totalCount,
        ]);
    }

}
