<?php

namespace app\controllers;
use app\models\ContactUs;
use app\models\ContactUsSearch;
use app\models\Country;
use app\models\DiscountProject;
use app\models\DiscountProjectSearch;
use app\models\Feedback;
use app\models\FlaggingideasSearch;
use app\models\PayStormerSearch;
use app\models\Points;
use app\models\PriceIdea;
use app\models\ProjectAudit;
use app\models\ProjectData;
use app\models\ProjectSearch;
use app\models\Projectstormer;
use app\models\UserBan;
use app\models\UserSearch;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\db\Query;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Faqs;
use app\models\Pages;
use app\models\User;
use app\models\News;
use app\models\Projects;
use app\models\Flaggingideas;
use app\models\Ideas;
use app\models\Comment;
use app\models\Projectquestion;

class AdminController extends Controller
{
  public function actions()
    {
        return array(
            'error' => array(
                'class' => 'yii\web\ErrorAction',
            ),
            'captcha' => array(
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ),
        );
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
//                'only' => ['create', 'update'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    public function actionIndex()
    {
        if (!\Yii::$app->user->isGuest) {
            if(\Yii::$app->user->identity->username != 'admin'){
                return $this->goHome();
            }else{
                return $this->redirect('admin/news');
            }
        }

        $newModelLogin = new LoginForm();
        if ($newModelLogin->load(Yii::$app->request->post())) {
            if($newModelLogin->username == 'admin'){
                if($newModelLogin->login()){
                    return $this->redirect('admin/news');
                }
            }else{
                return $this->goBack();
            }
        }
        
        return $this->render('login', [
            'model' => $newModelLogin,
        ]);
    }
    
    public function actionNews()
    {
        $newModelNews = new News();
        $newModelNews->scenario = 'add_news';
        $request = Yii::$app->request;
        $query = News::find()->orderBy(['id'=>SORT_DESC]);
        $modelNews = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
            
            if($_POST){
                if($newModelNews->load($request->post())){
                    if($newModelNews->save()){
                        Yii::$app->session->setFlash('news_added');
                        $newModelNews = new News();
                    }else{
                       Yii::$app->session->setFlash('news_not_added');
                    }
                }
            }
        return $this->render('news', [
            'newModelNews' => $newModelNews,
            'modelNews' => $modelNews->getModels(),
            'pagination' => $modelNews->pagination,
            'count' => $modelNews->pagination->totalCount,
        ]);
    }
     
    public function actionOnenews($id=''){
        $modelNews = News::find()->where(['id' => $id])->one();
        //var_dump($modelNews);exit;
        $newComment = new Comment;
        
        if($_POST){
            $request = Yii::$app->request;
            if(isset($_POST['News'])){
                $modelNews->scenario = 'update_news';
                if($modelNews->load($request->post())){
                    if($modelNews->save()){
                        Yii::$app->session->setFlash('news_update');
                    }else{
                        Yii::$app->session->setFlash('news_not_update');
                    }
                }
            }
            
            if(isset($_POST['Comment'])){
                $newComment->scenario = 'add_comment';
                if($newComment->load($request->post())){
                    if($newComment->save()){
                        Yii::$app->session->setFlash('comment_added');
                        $newComment = new Comment;
                    }else{
                        Yii::$app->session->setFlash('comment_not_added');
                    }
                }
            }
            if(isset($_POST['deleteComment'])){
                $comment_id = $_POST['comment_id'];
                $modelCommentD = Comment::find()->where(['id' => $comment_id])->one();
                if($modelCommentD->delete()){
                    Yii::$app->session->setFlash('comment_delete');
                }else{
                    Yii::$app->session->setFlash('comment_not_delete');
                }
            }
        }
        $user_id = \Yii::$app->user->identity->id;
        $modelComment = Comment::find()->where(['news_id' => $modelNews->id])->all();
        
        return $this->render('onenews', [
            'modelNews' => $modelNews,
            'user_id' => $user_id,
            'newComment' => $newComment,
            'modelComment' => $modelComment
        ]);
    }
    
    public function actionFlaggingideas(){
        
        $query = new Query;
        $query	->select(['*','id'=>'ideas.id'])
                        ->from('ideas')
                        ->join('LEFT JOIN',
                                        'flagging_ideas',
                                        'flagging_ideas.idea_id = ideas.id'
                                )->where(['flagging' => '1']);
        
        $command = $query->createCommand();
        $modelIdeas = $command->queryAll();
        
        return $this->render('flaggingideas',[
            'modelIdeas' => $modelIdeas,
        ]);
    }
    
    public function actionProject($id=null){
        $modelProject = Projects::find()->where(['id' => $id])->one();
        $modelIdeas = Ideas::find()->where(['project_id' => $id])->all();
        
        $arrayIdeas = [];
            foreach($modelIdeas as $ideas){
                $arrayIdeas[$ideas['stormer_id']][] = $ideas;
            }
            
        return $this->render('project',[
            'arrayIdeas' => $arrayIdeas,
            'modelProject' => $modelProject,
        ]);
    }
    
    public function actionDeleteflag(){
        $result = [];
        $action = $_POST['action'];
        $idea_id = $_POST['idea_id'];
        $modelIdea = Ideas::find()->where(['id' => $idea_id])->one();
            if($action == 'ok'){
                $modelIdea->scenario = 'flagging';
                $modelIdea->flagging = '0';
                $modelIdea->save();
            }else{
                $modelIdea->delete();
            }
        
        $modelFlaggingideas = Flaggingideas::find()->where(['idea_id' => $idea_id])->one();
        $modelFlaggingideas->delete();
        
        $result['status'] = 'success';
        echo json_encode($result);
    }
    
    public function actionFaqsdelete($faqs_id)
    {
        $modelFaqs = Faqs::find()->where(['id' => $faqs_id])->one();
        if($modelFaqs->delete()){
            Yii::$app->session->setFlash('faqs_delete');
        }else{
           Yii::$app->session->setFlash('faqs_not_delete');
        }
        return $this->redirect('/admin/faqs');
    }
    
    public function actionFaqs()
    {
        $newModelFaqs = new Faqs();
        $newModelFaqs->scenario = 'add_faqs';
        $request = Yii::$app->request;
        $query = Faqs::find();
        $modelFaqs = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
            
            if($_POST){
                if(isset($_POST['saveFaq'])){
                    if($newModelFaqs->load($request->post())){
                        if($newModelFaqs->save()){
                            Yii::$app->session->setFlash('faqs_added');
                            $newModelFaqs = new Faqs();
                        }else{
                           Yii::$app->session->setFlash('faqs_not_added');
                        }
                    }
                }else{
                    $modelUpdateFasq = Faqs::find()->where(['id' => $_POST['Faqs']['id']])->one();
                    $modelUpdateFasq->scenario = 'add_faqs';
                    $modelUpdateFasq->question = $_POST['Faqs']['question'];
                    $modelUpdateFasq->answer = $_POST['Faqs']['answer'];
                    $modelUpdateFasq->type = $_POST['Faqs']['type'];
                    if($modelUpdateFasq->save()){
                        Yii::$app->session->setFlash('faqs_update');
                    }else{
                       Yii::$app->session->setFlash('faqs_not_update');
                    }
                }
//                }
            }
        return $this->render('faqs', [
            'newModelFaqs' => $newModelFaqs,
            'modelFaqs' => $modelFaqs->getModels(),
            'pagination' => $modelFaqs->pagination,
            'count' => $modelFaqs->pagination->totalCount,
        ]);
    }
    
    public function actionPages()
    {
        $newModelPages = new Pages();
        $newModelPages->scenario = 'add_page';
        $request = Yii::$app->request;
        $query = Pages::find();
        $modelPages = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
            
            if($_POST){
                if(isset($_POST['savePage'])){
                    if($newModelPages->load($request->post())){
                        if($newModelPages->save()){
                            Yii::$app->session->setFlash('pages_added');
                            $newModelPages = new Pages();
                        }else{
                           Yii::$app->session->setFlash('pages_not_added');
                        }
                    }
                }else{
                    $newModelPages = Pages::find()->where(['id' => $_POST['Pages']['id']])->one();
                    $newModelPages->scenario = 'update_faqs';
                    $newModelPages->content = $_POST['Pages']['content'];
                    $newModelPages->date_update = date("Y-m-d H:i:s");
                    if($newModelPages->save()){
                        Yii::$app->session->setFlash('pages_update');
                    }else{
                       Yii::$app->session->setFlash('pages_not_update');
                    }
                }
            }
        return $this->render('pages', [
            'newModelPages' => $newModelPages,
            'modelPages' => $modelPages->getModels(),
            'pagination' => $modelPages->pagination,
            'count' => $modelPages->pagination->totalCount,
        ]);
    }
    
    public function actionProjectquestion()
    {
        $newModelProjectQuestion = new Projectquestion();
        if($_POST){
            if(isset($_POST['updateQuestion'])){
                $modelProjectQ = Projectquestion::find()->where(['id' => $_POST['Projectquestion']['id']])->one();
                $modelProjectQ->scenario = 'add_question';
                $modelProjectQ->question_title = $_POST['Projectquestion']['question_title'];
                $modelProjectQ->question_text = $_POST['Projectquestion']['question_text'];
                $modelProjectQ->type = $_POST['Projectquestion']['type'];
                if($modelProjectQ->save()){
                    Yii::$app->session->setFlash('question_update');
                }else{
                    Yii::$app->session->setFlash('question_not_update');
                }
            }else{
                $request = Yii::$app->request;
                $newModelProjectQuestion->scenario = 'add_question';
                if($newModelProjectQuestion->load($request->post())){
                    if($newModelProjectQuestion->save()){
                        Yii::$app->session->setFlash('question_added');
                    }else{
                        Yii::$app->session->setFlash('question_not_added');
                    }
                }
            }
        }
        $queryProjectQuestion = Projectquestion::find();
        $modelProjectQuestion = new ActiveDataProvider(['query' => $queryProjectQuestion, 'pagination' => ['pageSize' => 20]]);
        return $this->render('projectquestion', [      
            'modelProjectQuestion' => $modelProjectQuestion->getModels(),
            'pagination' => $modelProjectQuestion->pagination,
            'newModelProjectQuestion' => $newModelProjectQuestion,
        ]);
    }

//listat_an
    public function actionProjects()
    {
        $message=false;
        $message_send = '';

        if(Yii::$app->request->post() && Yii::$app->request->post('up_project_id')){
            $message = true;

            $upProject = Projects::findOne(Yii::$app->request->post('up_project_id'));
            $upProject->active = Yii::$app->request->post('up_project_active');
            $upProject->scenario = 'update_status';
            $upProject->save();
            $message_send = 'Update active ptoject "'.$upProject->title.'"';
        }
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('projects', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'message' => $message,
            'message_send' => $message_send,
        ]);
    }

    public function actionProjectsstormer()
    {
        $message=false;
        $message_send = '';
        $message_error=false;

        if(Yii::$app->request->post()){
            if (Yii::$app->request->post('active')){
                $message = true;

                $upProject = Projects::findOne(Yii::$app->request->post('up_project_id'));
                $upProject->active = Yii::$app->request->post('up_project_active');
                $upProject->scenario = 'update_status';
                $upProject->save();

                $point = new Points();
                $point->user_id = $upProject->owner_id;
                $point->type='project';
                $point->point=5;
                $point->othe=$upProject->id;
                $point->save();

                $message_send = 'Update active ptoject "'.$upProject->title.'"';
            }

            if (Yii::$app->request->post('inspired')){
                if (Yii::$app->request->post('up_project_inspired')){
                    if(Projects::find()->where(['be_inspired'=>1,'type'=>3])->count()<5){
                        var_dump(Projects::find()->where(['be_inspired'=>1,'type'=>3])->count()<=5);
                        $message = true;
                        $upProject = Projects::findOne(Yii::$app->request->post('up_project_inspired_id'));
                        $upProject->be_inspired = Yii::$app->request->post('up_project_inspired');
                        $upProject->scenario = 'update_be_inspired';
                        $upProject->save();
                        $message_send = 'Add be inspired ptoject "'.$upProject->title.'"';
                    }else{
                        $message_error=true;
                        $message_send = 'Number of projects on the page "Be Inspired" should not exceed 5. ';
                    }
                }else{
                    if(Projects::find()->where(['be_inspired'=>1,'type'=>3])->count()<=5){
                        var_dump(Projects::find()->where(['be_inspired'=>1,'type'=>3])->count()<=5);
                        $message = true;
                        $upProject = Projects::findOne(Yii::$app->request->post('up_project_inspired_id'));
                        $upProject->be_inspired = Yii::$app->request->post('up_project_inspired');
                        $upProject->scenario = 'update_be_inspired';
                        $upProject->save();
                        $message_send = 'Add be inspired ptoject "'.$upProject->title.'"';
                    }else{
                        $message_error=true;
                        $message_send = 'Number of projects on the page "Be Inspired" should not exceed 5. ';
                    }
                }


            }
        }
        $searchModel = new ProjectSearch(['type'=>3]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('projects_stormer', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'message' => $message,
            'message_error' => $message_error,
            'message_send' => $message_send,
        ]);
    }

    public function actionProjectsopen()
    {
        $message=false;
        $message_send = '';
        $message_error=false;

        if(Yii::$app->request->post()){
            if (Yii::$app->request->post('active')){
                $message = true;

                $upProject = Projects::findOne(Yii::$app->request->post('up_project_id'));
                $upProject->active = Yii::$app->request->post('up_project_active');
                $upProject->scenario = 'update_status';
                $upProject->save();
                $message_send = 'Update active ptoject "'.$upProject->title.'"';
            }

            if (Yii::$app->request->post('inspired')){
                if (Yii::$app->request->post('up_project_inspired')){
                    if(Projects::find()->where(['be_inspired'=>1,'project_type'=>'Open'])->count()<5){
                        var_dump(Projects::find()->where(['be_inspired'=>1,'project_type'=>'Open'])->count()<=5);
                        $message = true;
                        $upProject = Projects::findOne(Yii::$app->request->post('up_project_inspired_id'));
                        $upProject->be_inspired = Yii::$app->request->post('up_project_inspired');
                        $upProject->scenario = 'update_be_inspired';
                        $upProject->save();
                        $message_send = 'Add be inspired ptoject "'.$upProject->title.'"';
                    }else{
                        $message_error=true;
                        $message_send = 'Number of projects on the page "Be Inspired" should not exceed 5. ';
                    }
                }else{
                    if(Projects::find()->where(['be_inspired'=>1,'project_type'=>'Open'])->count()<=5){
                        var_dump(Projects::find()->where(['be_inspired'=>1,'project_type'=>'Open'])->count()<=5);
                        $message = true;
                        $upProject = Projects::findOne(Yii::$app->request->post('up_project_inspired_id'));
                        $upProject->be_inspired = Yii::$app->request->post('up_project_inspired');
                        $upProject->scenario = 'update_be_inspired';
                        $upProject->save();
                        $message_send = 'Add be inspired ptoject "'.$upProject->title.'"';
                    }else{
                        $message_error=true;
                        $message_send = 'Number of projects on the page "Be Inspired" should not exceed 5. ';
                    }
                }


            }
        }
        $searchModel = new ProjectSearch(['project_type'=>'Open']);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('projects_open', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'message' => $message,
            'message_error' => $message_error,
            'message_send' => $message_send,
        ]);
    }

    public function actionProjects_customer()
    {
        $message=false;
        $message_send = '';
        $message_send_error = '';
        $message_error=false;

        if(Yii::$app->request->post()){
            if (Yii::$app->request->post('active')){
                $message = true;

                $upProject = Projects::findOne(Yii::$app->request->post('up_project_id'));
                $upProject->active = Yii::$app->request->post('up_project_active');
                $upProject->scenario = 'update_status';
                $upProject->save();
                $message_send = 'Update active ptoject "'.$upProject->title.'"';
            }

            if (Yii::$app->request->post('inspired')){

                if (Yii::$app->request->post('up_project_inspired')){
                    if(Projects::find()->where(['be_inspired'=>1,'project_type'=>'NGO'])->count()<5){
                        $message = true;
                        $upProject = Projects::findOne(Yii::$app->request->post('up_project_inspired_id'));
                        $upProject->be_inspired = Yii::$app->request->post('up_project_inspired');
                        $upProject->scenario = 'update_be_inspired';
                        $upProject->save();
                        $message_send = 'Add be inspired ptoject "'.$upProject->title.'"';
                    }else{
                        $message_error=true;
                        $message_send = 'Number of projects on the page "Be Inspired" should not exceed 5. ';
                    }
                }else {
                    if(Projects::find()->where(['be_inspired'=>1,'project_type'=>'NGO'])->count()<=5){
                        $message = true;
                        $upProject = Projects::findOne(Yii::$app->request->post('up_project_inspired_id'));
                        $upProject->be_inspired = Yii::$app->request->post('up_project_inspired');
                        $upProject->scenario = 'update_be_inspired';
                        $upProject->save();
                        $message_send = 'Add be inspired ptoject "'.$upProject->title.'"';
                    }else{
                        $message_error=true;
                        $message_send = 'Number of projects on the page "Be Inspired" should not exceed 5. ';
                    }
                }


            }
        }
        $searchModel = new ProjectSearch(['project_type'=>'customer']);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('projects_customer', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'message' => $message,
            'message_error' => $message_error,
            'message_send' => $message_send,
        ]);
    }

    public function actionProjectsngo()
    {
        $message=false;
        $message_send = '';
        $message_send_error = '';
        $message_error=false;

        if(Yii::$app->request->post()){
            if (Yii::$app->request->post('active')){
                $message = true;

                $upProject = Projects::findOne(Yii::$app->request->post('up_project_id'));
                $upProject->active = Yii::$app->request->post('up_project_active');
                $upProject->scenario = 'update_status';
                $upProject->save();
                $message_send = 'Update active ptoject "'.$upProject->title.'"';

                $point = new Points();
                $point->user_id = $upProject->owner_id;
                $point->type = 'NGO';
                $point->point = 50;
                $point->save();
            }

            if (Yii::$app->request->post('inspired')){

                if (Yii::$app->request->post('up_project_inspired')){
                    if(Projects::find()->where(['be_inspired'=>1,'project_type'=>'NGO'])->count()<5){
                        $message = true;
                        $upProject = Projects::findOne(Yii::$app->request->post('up_project_inspired_id'));
                        $upProject->be_inspired = Yii::$app->request->post('up_project_inspired');
                        $upProject->scenario = 'update_be_inspired';
                        $upProject->save();
                        $message_send = 'Add be inspired ptoject "'.$upProject->title.'"';
                    }else{
                        $message_error=true;
                        $message_send = 'Number of projects on the page "Be Inspired" should not exceed 5. ';
                    }
                }else {
                    if(Projects::find()->where(['be_inspired'=>1,'project_type'=>'NGO'])->count()<=5){
                        $message = true;
                        $upProject = Projects::findOne(Yii::$app->request->post('up_project_inspired_id'));
                        $upProject->be_inspired = Yii::$app->request->post('up_project_inspired');
                        $upProject->scenario = 'update_be_inspired';
                        $upProject->save();
                        $message_send = 'Add be inspired ptoject "'.$upProject->title.'"';
                    }else{
                        $message_error=true;
                        $message_send = 'Number of projects on the page "Be Inspired" should not exceed 5. ';
                    }
                }


            }
        }
        $searchModel = new ProjectSearch(['project_type'=>'NGO']);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('projects_ngo', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'message' => $message,
            'message_error' => $message_error,
            'message_send' => $message_send,
        ]);
    }

    public function actionPay_stormer()
    {
        $message=false;
        $message_send = '';
        $message_send_error = '';
        $message_error=false;
        $idea = PriceIdea::findOne(1);
        if(Yii::$app->request->post()){
            if (Yii::$app->request->post('price')){
                $message = true;
                $idea->load(Yii::$app->request->post());
                $idea->save();
                $message_send = 'Update price idea';
            }

            if (Yii::$app->request->post('pay')){

            }
        }
        $searchModel = new PayStormerSearch(['status'=>0]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);



        return $this->render('pay_stormer', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'message' => $message,
            'message_error' => $message_error,
            'message_send' => $message_send,
            'idea'=>$idea
        ]);
    }

    public function actionPyamend_stormer()
    {
        $message=false;
        $message_send = '';
        $message_send_error = '';
        $message_error=false;
        $idea = PriceIdea::findOne(1);
        if(Yii::$app->request->post()){
            if (Yii::$app->request->post('price')){
                $message = true;
                $idea->load(Yii::$app->request->post());
                $idea->save();
                $message_send = 'Update price idea';
            }

            if (Yii::$app->request->post('pay')){

            }
        }
        $searchModel = new PayStormerSearch(['status'=>1]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);



        return $this->render('paymend_stormer', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'message' => $message,
            'message_error' => $message_error,
            'message_send' => $message_send,
            'idea'=>$idea
        ]);
    }



    public function actionContact()
    {
        $message=false;
        $message_send = '';
        $message_send_error = '';
        $message_error=false;

        if(Yii::$app->request->post()){
            if (Yii::$app->request->post('send')){
                $contact = ContactUs::findOne(Yii::$app->request->post('up_project_id'));
                if ($contact){
                    Yii::$app->mailer->compose()
                        ->setTo($contact->email)
                        ->setFrom([Yii::$app->params['adminEmail'] => 'braincloud'])
                        ->setSubject($contact->subject_mail)
                        ->setHtmlBody('Your meil <br><p>  '.$contact->descriptions.'</p><br> Admin: <p>'.Yii::$app->request->post('message').'</p>')
                        ->send();
                    $message = true;
                    $message_send='Mail send';
                }

            }
            if (Yii::$app->request->post('point')){
                $contact = ContactUs::findOne(Yii::$app->request->post('up_project_inspired_id'));
                if ($contact){
                    if($contact->parent_id){
                        if (Yii::$app->request->post('up_project_inspired')) {
                            if (!Points::find()->where(['user_id' => $contact->parent_id, 'type' => '10_points_glitch'])->one()) {
                                $point = new Points();
                                $point->user_id = $contact->parent_id;
                                $point->date_week = $point->week();
                                $point->point = 10;
                                $point->type = '10_points_glitch';
                                $point->save();
                            } else {
                                $point = new Points();
                                $point->user_id = $contact->parent_id;
                                $point->date_week = $point->week();
                                $point->point = 2;
                                $point->type = '10_points_glitch';
                                $point->save();
                            }
                            $contact->point = true;
                            $contact->save();
                            $message = true;
                            $message_send = 'Point count';
                        }
                    }


                }
            }

        }
        $searchModel = new ContactUsSearch(['timeline_inquire'=>0]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('contact_us', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'message' => $message,
            'message_error' => $message_error,
            'message_send' => $message_send,
        ]);
    }

    public function actionFlag()
    {
        $message=false;
        $message_send = '';

        if(Yii::$app->request->post() && Yii::$app->request->post('stormer_id')){
            $message = true;
            $ban_item =UserBan::find()->where(['stormer_id'=>Yii::$app->request->post('stormer_id')])->one();
            if ($ban_item){
                $ban_item->date=Yii::$app->request->post('date');
                $ban_item->type = Yii::$app->request->post('type');
                $ban_item->save();
            }else{
                $ban = new UserBan();
                $ban->stormer_id=Yii::$app->request->post('stormer_id');
                $ban->date=Yii::$app->request->post('date');
                $ban->type = Yii::$app->request->post('type');
                $ban->save();
            }
            if(Yii::$app->request->post('type') == 1){
                $message_send = 'Lock stormer "'.Yii::$app->request->post('stormer_name').'" date: '.Yii::$app->request->post('date');
            }else {
                $message_send = 'Message stormer "'.Yii::$app->request->post('stormer_name').'"';
            }

        }
        $searchModel = new FlaggingideasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('stormer_flag', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'message' => $message,
            'message_send' => $message_send,
        ]);
    }

    public function actionFlag_users()
    {
        $message=false;
        $message_send = '';

        if(Yii::$app->request->post() && Yii::$app->request->post('stormer_id')){
            $message = true;
            $ban_item =UserBan::find()->where(['stormer_id'=>Yii::$app->request->post('stormer_id')])->one();
            if ($ban_item){
                $ban_item->date=Yii::$app->request->post('date');
                $ban_item->type = Yii::$app->request->post('type');
                $ban_item->save();
            }else{
                $ban = new UserBan();
                $ban->stormer_id=Yii::$app->request->post('stormer_id');
                $ban->date=Yii::$app->request->post('date');
                $ban->type = Yii::$app->request->post('type');
                $ban->save();
            }
            if(Yii::$app->request->post('type') == 1){
                $message_send = 'Lock stormer "'.Yii::$app->request->post('stormer_name').'" date: '.Yii::$app->request->post('date');
            }else {
                $message_send = 'Message stormer "'.Yii::$app->request->post('stormer_name').'"';
            }

        }
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('stormer_flag_user', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'message' => $message,
            'message_send' => $message_send,
        ]);
    }

    public function actionFeedback()
    {
        $message=false;
        $message_send = '';

        if(Yii::$app->request->post() && Yii::$app->request->post('up_project_id')){
            $message = true;

            $upProject = Feedback::findOne(Yii::$app->request->post('up_project_id'));
            $upProject->show_status = Yii::$app->request->post('up_project_active');
            $upProject->scenario = 'up_status';
            $upProject->save();
            $message_send = 'FEEDBACK';
        }
        $model = Feedback::find()->orderBy(['created_at'=>SORT_DESC]);
        $pages = new Pagination(['totalCount' => $model->count(), 'pageSize' => 10]);
        // приводим параметры в ссылке к ЧПУ
        $pages->pageSizeParam = false;
        $models = $model->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('feedback', [
            'model' => $models,
            'pages' => $pages,
            'message' => $message,
            'message_send' => $message_send,
        ]);
    }


    public function actionDiscountproject(){
        $searchModel = new DiscountProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('discountproject',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPostproject()
    {
        // var_dump(Yii::$app->user->identity->users_type);exit;
        $token=false;

        if(!Yii::$app->user->isGuest){
            if (Yii::$app->request->get('token')){
                $token=Yii::$app->request->get('token');
            }
            return $this->render('post_project',[
                'token'=>$token,
            ]);
        }else{
            throw new HttpException(404);
        }
    }

    public function actionCreate($type=null)
    {
        if($type == null){
            throw new Exception("Error Processing Request", 1);
        }

        if(!Yii::$app->user->isGuest){

            $model = new Projects();
            if (Yii::$app->request->get('token')){
                $audit = ProjectAudit::find()->where(['token'=>Yii::$app->request->get('token')])->one();
                if ($audit){
                    $model->title=$audit->title;
                    $model->size_team=$audit->size_team;
                    $model->count_idea=$audit->count_idea;


                    $count = ProjectAudit::find()->where(['project_id'=>$audit->project_id, 'status'=>1])->count()+1;
                    if (($count<=3)){
                        $discount_bd = DiscountProject::find()->where(['count'=>$count])->one();
                        if($discount_bd){
                            $model->discount=$discount_bd->discount;
                        }
                    }else{
                        $discount_bd = DiscountProject::find()->where(['count'=>3])->one();
                        if($discount_bd){
                            $discount=$discount_bd->discount;
                        }
                    }

                }

            }
            $model->owner_id = Yii::$app->user->id;
            $model->type = ($type == 'create')?1:2;
            if ($type == 'create') {
                $model->category = "";
                $model->question_convergent = "";
                $model->question_divergent = "";
                $model->question_custom = "";
                $model->perspectives = "";
                $model->scenario = 'create';
            }else{
                $model->scenario = 'target';
                $categories =  array(
                    'Employee engagement' => 'Employee engagement',
                    'Customer experience' => 'Customer experience',
                    'Customer Reach' => 'Customer Reach',
                    'Research and development' => 'Research and development',
                    'Cost containment' => 'Cost containment',
                    'Sales planning' => 'Sales planning',
                    'Value chain management' => 'Value chain management',
                    'Human resource management' => 'Human resource management',
                    'Business model innovation' => 'Business model innovation',
                );

                // var_dump($question_divergent);
            }


            if ($model->load(Yii::$app->request->post())) {


                $model->project_type='Open';


                if(($model->tmpCategory != null) || ($model->tmpCategory != '')){
                    $model->category = $model->tmpCategory ;
                }

                if (Yii::$app->request->get('token')){
                    $audit = ProjectAudit::find()->where(['token'=>Yii::$app->request->get('token')])->one();
                    $audit->status=1;
                    $audit->save();

                }


                if( $model->save()){
                    return $this->redirect(['admin/post-project/'.$type.'2/'.$model->id]);
                }
            }else{
                if ($type == 'create') {
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }else{
                    // var_dump($question_divergent);exit;
                    return $this->render('target', [
                        'model' => $model,
                        'categories' => $categories,
                    ]);
                }
            }
        }else{
            throw new HttpException(404);
        }
    }

    public function actionCreate2($type,$project_id=null)
    {
        $modelProject = Projects::find()->where(['id' => $project_id])->one();
        if($type == 'target2'){

            $question_divergent =  ArrayHelper::map(Projectquestion::find()->where(['type' => 'divergent'])->all(), 'id', 'question_title');;
            $question_convergent =  ArrayHelper::map(Projectquestion::find()->where(['type' => 'convergent'])->all(), 'id', 'question_title');;
            $modelProject->scenario = 'target2';
        }

        if ($_POST) {
            if ($modelProject->load(Yii::$app->request->post())) {
                $modelProject->save();
            }
            $files = Yii::$app->request->post('Files');
            if(isset($files)){
                for ($i=0; $i < sizeof($files['explanation']); $i++) {
                    $fileModel = new ProjectData();
                    $fileModel->description = $files['explanation'][$i];
                    $fileModel->data = $files['path'][$i];
                    $fileModel->project_id = $modelProject->id;
                    $fileModel->type = '1';
                    if(!$fileModel->save()){
                        var_dump($fileModel);
                        var_dump($fileModel->getErrors());
                        exit;
                    }

                }
            }
            $link = Yii::$app->request->post('Link');
            if(isset($link)){
                for ($i=0; $i < sizeof($link['brief_link']); $i++) {
                    $fileModel = new ProjectData();
                    $fileModel->description = $link['brief_link'][$i];
                    $fileModel->data = $link['link'][$i];
                    $fileModel->project_id = $modelProject->id;
                    $fileModel->type = '2';

                    if(!$fileModel->save()){
                        var_dump($fileModel);
                        var_dump($fileModel->getErrors());
                        exit;
                    }
                }
            }

            return $this->redirect(['admin/selectstormer/', 'project_id' => $modelProject->id]);

        }
        if($type == 'target2'){
            return $this->render('create2', [
                'question_convergent' => $question_convergent,
                'question_divergent' => $question_divergent,
                'model' => $modelProject,
                'type' => $type,
            ]);
        }else{
            return $this->render('create2', [
                'model' => $modelProject,
                'type' => $type,
            ]);
        }

    }


    public function actionSelectstormer($project_id = '')
    {
        $discount=0;
        if (!Yii::$app->user->isGuest) {

            if ($_POST) {
                if (isset($_POST['confirmSliderSelect'])) {
                    $modelProject = Projects::find()->where(['id' => $project_id])->one();
                    $modelProject->scenario = 'update_slider';
                    $modelProject->count_idea = $_POST['slider_scope_project'];
                    $modelProject->size_team = $_POST['slider_team_size'];
                    $modelProject->save();
                }
                if (isset($_POST['saveSelectStormer'])) {
                    $stormerIdArray = json_decode($_POST['FormSelectStormer']);

                    foreach ($stormerIdArray as $stromerI) {
                        $modelProjectstormer = new Projectstormer();
                        $modelProjectstormer->scenario = 'add';
                        $modelProjectstormer->stormer_id = $stromerI;
                        $modelProjectstormer->project_id = $project_id;
                        $modelProjectstormer->confirmation=-1;
                        $modelProjectstormer->save();
                    }
//                    return $this->redirect(['customer/paymentproject', 'project_id' => $project_id]);
                    $pr = Projects::find()->where(['id' => $project_id])->one();

                        return $this->redirect(['/admin/projectsopen']);


                }
            }

            $modelProject = Projects::find()->where(['id' => $project_id])->one();
            $discount=$modelProject->discount;
            $query = new Query;
            $query->select(['*', 'id' => 'stormer_info.id', 'global_stars'=>'(SELECT SUM(sr.count_star) FROM stormer_rating sr WHERE sr.stormer_id = users.id )'])
                ->from('stormer_info')
                ->join('LEFT JOIN',
                    'users',
                    'users.id = stormer_info.stormer_id'
                )->limit('100')->orderBy(['global_stars'=>SORT_DESC]);
            $command = $query->createCommand();
            $modelStormers = $command->queryAll();
            $modelC = Country::find()->orderBy('country_name ASC')->all();
            $arrayCountry = ArrayHelper::map($modelC, 'id', 'country_name');

            return $this->render('../customer/select_stormers', [
                'modelProject' => $modelProject,
                'modelStormers' => $modelStormers,
                'arrayCountry' => $arrayCountry,
                'discount'=>$discount,
            ]);
        } else {
            throw new HttpException(404);
        }
    }

    public function actionDelete_flag($id)
    {
        $flag = Flaggingideas::find()->where(['id'=>$id])->one();


        $modelIdea = Ideas::find()->where(['id' => $flag->idea_id])->one();
        $modelIdea->scenario = 'flagging';
        $modelIdea->flagging = 0;
        $modelIdea->save();
        $flag->delete();
        return $this->redirect('/admin/flag');
    }

    public function actionDelete_new($id)
    {
        $new = News::find()->where(['id'=>$id])->one();
        $new->delete();
        return $this->redirect('/admin/news');
    }

    public function actionDelete_question($id)
    {
        $new = Projectquestion::find()->where(['id'=>$id])->one();
        $new->delete();
        return $this->redirect('/admin/projectquestion');
    }

    public function actionMyprojects($type=null)
    {
        $newFeedbackModal = new Feedback();
        $newFeedbackModal->scenario = 'add_feedback';

        if ($_POST) {
            if (isset($_POST['Feedback'])) {
                $request = Yii::$app->request;
                if ($newFeedbackModal->load($request->post())) {
                    $newFeedbackModal->user_id=Yii::$app->user->id;
                    if ($newFeedbackModal->save()) {
                        $project = Projects::find()->where(['id' => $newFeedbackModal->project_id])->one();
                        $project->scenario = 'update_status';
                        $project->status = 0;
                        if ($project->save()) {
                            return $this->redirect('/admin/organizeidea/' . $newFeedbackModal->project_id);
                        }
                    }
                }
            } elseif (isset($_POST['buttonCloseProject'])) {
                $project = Projects::find()->where(['id' => $_POST['project_id']])->one();
                $project->scenario = 'update_status';
                $project->status = 0;
                if ($project->save()) {
                    return $this->redirect('/admin/organizeidea/' . $_POST['project_id']);
                }
            }
        }
        $newFeedbackModal->question = '1';

        if($type == 'current'){
            $query = Projects::find()->where(['owner_id' => Yii::$app->user->id])->andWhere(['status' => '1'])->orderBy(['id' => SORT_DESC]);
            $modelProjectsOpen = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 10]]);

            foreach ($modelProjectsOpen->getModels() as $projectsO) {
                $query = new Query;
                $query->select(['users.username', 'id' => 'project_stormer.id'])
                    ->from('project_stormer')
                    ->join('LEFT JOIN',
                        'users',
                        'users.id = project_stormer.stormer_id'
                    )
                    ->where(['project_id' => $projectsO->id]);

                $command = $query->createCommand();
                $projectsO->team = $command->queryAll();
                $projectsO->countIdeaUser = Ideas::find()->where(['project_id' => $projectsO->id])->count();
            }
            return $this->render('mycurrentprojects', [
                'newFeedbackModal' => $newFeedbackModal,
                'modelProjectsOpen' => $modelProjectsOpen->getModels(),
                'pagination' => $modelProjectsOpen->pagination,
                'count' => $modelProjectsOpen->pagination->totalCount,
            ]);

        }elseif($type== 'past'){
            $query = Projects::find()->where(['owner_id' => Yii::$app->user->id])->andWhere(['status' => '0'])->orderBy(['id' => SORT_DESC]);
            $modelProjectsClose = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 10]]);

            foreach ($modelProjectsClose->getModels() as $projectsC) {
                $query = new Query;
                $query->select(['users.username', 'id' => 'project_stormer.id'])
                    ->from('project_stormer')
                    ->join('LEFT JOIN',
                        'users',
                        'users.id = project_stormer.stormer_id'
                    )
                    ->where(['project_id' => $projectsC->id]);

                $command = $query->createCommand();
                $projectsC->team = $command->queryAll();
            }

            return $this->render('mypastprojects', [
                'newFeedbackModal' => $newFeedbackModal,
                'modelProjectsClose' => $modelProjectsClose->getModels(),
                'pagination' => $modelProjectsClose->pagination,
                'count' => $modelProjectsClose->pagination->totalCount,
            ]);

        }else{
            throw new HttpException(404);
        }
    }

    public function actionClear_ban($id){
        $ban = UserBan::find()->where(['stormer_id'=>$id])->one();
        if ($ban) {
            $ban->delete();
        }
        $this->redirect('/admin/flag_users');
    }


    public function actionTimeline_inquire(){
        $searchModel = new ContactUsSearch(['timeline_inquire'=>1]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('timeline_inquire',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }



}