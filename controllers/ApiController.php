<?php
/**
 * Created by PhpStorm.
 * User: Listat
 * Date: 27.07.2016
 * Time: 12:23
 */

namespace app\controllers;


use app\components\Paypal;
use app\models\City;
use app\models\InnovationQuestions;
use app\models\Inspire;
use app\models\PayStormer;
use app\models\Points;
use app\models\PointsMessage;
use app\models\Projects;
use app\models\Projectstormer;
use app\models\Transaction;
use app\models\User;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Facebook\FacebookRequest;
use Facebook\PersistentData\FacebookMemoryPersistentDataHandler;
use Facebook\PersistentData\PersistentDataFactory;
use kartik\mpdf\Pdf;
use nodge\eauth\ErrorException;
use PayPal\Api\Currency;
use PayPal\Api\Payout;
use PayPal\Api\PayoutItem;
use PayPal\Api\PayoutSenderBatchHeader;
use yii\data\Pagination;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Response;
use Yii;

use Facebook\PersistentData\PersistentDataInterface;

class PersistentDataHandlerYii implements PersistentDataInterface
{
    /**
     * @var string Prefix to use for session variables.
     */
    protected $sessionPrefix = 'FBRLH_';

    public function get($key)
    {
        return Yii::$app->session->get($this->sessionPrefix . $key);
    }

    public function set($key, $value)
    {
        Yii::$app->session->set($this->sessionPrefix . $key, $value);
    }
}

class ApiController extends \yii\web\Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionCloseproject(){
        if(\Yii::$app->request->post()){
            $project_id = \Yii::$app->request->post('id');
            $model = Projects::findOne($project_id);
            $model->scenario = 'update_status';
            $model->status = 0;
            $model->save();
          $project_stormers = Projectstormer::findAll(['project_id'=>$project_id,'confirmation'=>1]);
          foreach ($project_stormers as $project_stormer){
            $project_stormer->scenario = 'status';
            $project_stormer->status = 1;
            $project_stormer->save();
            $message = new PointsMessage();
            $message->user_id = $project_stormer->stormer_id;
            $message->message = 'Closed project "'.$model->title.'"';
            $message->save();
          }
        }
        return true;
    }
    public function actionTest(){
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $page=\Yii::$app->request->post('page');
        $query = Projects::find()->where(['active'=>1,'owner_id'=>\Yii::$app->user->id]);

        $count = $query->count();
        $mas=[];
        if ($count>0){
            if (($count-$page*5)>5){
                $json['page']=true;
            }else{
                $json['page']=false;
            }
            if ($page*5<$count){
                $countries = $query->orderBy(['id'=>SORT_DESC])
                    ->offset($page*5)
                    ->limit(5)
                    ->all();
                foreach($countries as $country){
                    $mas[]=['id'=>$country->id,'title'=>$country->title];
                }
                $json['status']=true;
            }else{
                $json['status']=false;
            }

        }else {
            $mas=[];
            $json['status']=false;
            $json['page']=false;
        }

        $json['data']=$mas;

         return $json;
    }
    public function actionPhpinfo(){
        phpinfo();
    }
    public function actionDemo(){
        print("<pre>");
        $xportlist = stream_get_transports();
        print_r($xportlist);
    }

    public function actionSendmailadmin(){

        $user = User::findOne(\Yii::$app->user->id);
        if($user){
            $admins = User::find()->where(['users_type'=>''])->all();
            foreach ($admins as $admin) {
                if($admin->email){
                    \Yii::$app->mailer->compose()
                        ->setTo($admin->email)
                        ->setFrom([\Yii::$app->params['adminEmail'] => 'braind'])
                        ->setHtmlBody('Problem create project ')
                        ->send();
                }else{
                    return false;
                }

            }
            return true;
        }else{
            return false;
        }

    }
    public function actionAdmin(){
        var_dump(User::find()->where(['users_type'=>''])->asArray()->all());
    }
    public function actionAddpoints(){
        $type= \Yii::$app->request->post('type');
        $point= \Yii::$app->request->post('point');
        if(Points::find()->where(['user_id'=>\Yii::$app->user->id, 'type'=>$type])->count() < 4){
            $point_new = new Points();
            $point_new->user_id=\Yii::$app->user->id;
            $point_new->type=$type;
            $point_new->point=$point;
            $point_new->save();
        }
       return true;
    }

    public function actionWeek(){
        echo time().'<br>';
        echo strtotime('last Sunday');
        echo Points::find()->where(['user_id'=>\Yii::$app->user->id])->andWhere(['>','date_week',time()])->sum('point');
    }

    public function actionSocial(){

        $servise = Yii::$app->getRequest()->getQueryParam('servise');
        $type = Yii::$app->getRequest()->getQueryParam('type');
        $user = Yii::$app->getRequest()->getQueryParam('user');

        if ($type == 'register'){
            if ($user == 'stormer'){
                $eauth = \Yii::$app->get('eauth')->getIdentity($servise);
                $eauth->setRedirectUrl(Yii::$app->getUser()->getReturnUrl());
                $eauth->setCancelUrl(Yii::$app->getUrlManager()->createAbsoluteUrl('signup/stormer/step_1'));
                try {
                    if ($eauth->authenticate()) {
//                        var_dump($eauth->getIsAuthenticated(), $eauth->getAttributes());
//                        exit;
                        $atribute = $eauth->getAttributes();
                        if(User::find()->where([$servise.'_id'=>$atribute['id']])->one()){
                            $this->redirect('/api/social?servise='.$servise.'&type=login');
                        }else{
                            $user = new User();
                            $user->scenario='social_reg';
                            $user->name=$atribute['name'];
                            switch ($servise) {
                                case 'google':
                                    $user->google_id=$atribute['id'];
                                    break;
                                case 'twitter':
                                    $user->twitter_id=$atribute['id'];
                                    break;
                                case 'facebook':
                                    $user->facebook_id=$atribute['id'];
                                    break;
                            }
                            $user->password=Yii::$app->security->generateRandomString(6);
                            $user->password_repeat=$user->password;
                            $user->password_hash = \Yii::$app->security->generatePasswordHash($user->password);
                            $user->auth_key = 'key';
                            $user->signup_step = '1';
                            $user->active = '0';
                            $user->users_type='stormer';
//                            $user->email= $atribute['id'].'@demo.test';

//                            var_dump($user->save());
//                            var_dump($user->getErrors());
//
//                            exit();
                            if ($user->save()){
                                return $this->redirect(['/signup', 'signup_type' => 'stormer', 'step' => 'step_2','stormer_id' => $user->id]);
                            }else{
                                return $this->redirect('/signup/stormer/step_1');
                            }
                        }
                    }
                    else {
//                        echo 'cancel';
                        return $this->redirect('/signup/stormer/step_1');
                    }
                }
                catch (\nodge\eauth\ErrorException $e) {
                    throw new ErrorException($e->getMessage());

                }
            }
        }elseif($type =='login'){
                $eauth = \Yii::$app->get('eauth')->getIdentity($servise);
                $eauth->setRedirectUrl(Yii::$app->getUser()->getReturnUrl());
                $eauth->setCancelUrl(Yii::$app->getUrlManager()->createAbsoluteUrl('site/login'));
                try {
                    if ($eauth->authenticate()) {
//                        var_dump($eauth->makeSignedRequest('https://www.googleapis.com/plus/v1/people/109402027182301816606'));
//                        var_dump($eauth->getAttributes());
//                        exit;
                        if (!$eauth->getIsAuthenticated()) {
                            throw new ErrorException('EAuth user should be authenticated before creating identity.');
                        }

                        $atribute = $eauth->getAttributes();

                        $user = User::find()->where([$servise.'_id'=>$atribute['id']])->one();
                        if ($user){
                            if(Yii::$app->user->login($user)){
                                $this->redirect('/');
                            }else{
                                $this->redirect('/site/login?ms=1');
                            }
                        }else{
                            $this->redirect('/site/login?ms=1');
                        }

                    }
                    else {
//                        echo 'cancel';
                        $this->redirect('/site/login?ms=1');
                    }
                }
                catch (\nodge\eauth\ErrorException $e) {
                    throw new \yii\base\ErrorException($e->getMessage());

                }

        }


    }

    public function actionInvoice($id) {
        // get your HTML raw content without any layouts or scripts
        $tr = Transaction::findOne($id);
        $date = Yii::$app->formatter->asDatetime($tr->created_at);
        $content = "
        <h2>Project \" ".$tr->project->title."\"</h2>
        <h4>Description \"".$tr->project->description."\"</h4>
        <p><strong>Size Team :</strong>".$tr->project->size_team." </p>
        <p><strong>Count Idea :</strong>".$tr->project->count_idea." </p>
        <p><strong>Discount :</strong>".$tr->project->discount." </p>
        <p><strong>Price :</strong>".$tr->price." </p>
        <p><strong>Date :</strong> $date </p>
        ";


        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            'filename'=>'Invoice.pdf',
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_DOWNLOAD,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            //'cssFile' => '@web/frontend/css/main.css',
            // any css to be embedded if required
            'cssInline' => '.sum_param{color: #a9a9a9;
                                        float: left;
                                        font-size: 15px;
                                        line-height: 15px;
                                        margin: 0 2% 20px 0;
                                        width: 30%;}',
            // set mPDF properties on the fly
            //'options' => ['title' => 'Krajee Report Title'],
            // call mPDF methods on the fly
//            'methods' => [
//                'SetHeader'=>['Krajee Report Header'],
//                'SetFooter'=>['{PAGENO}'],
//            ]
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    public function actionEarning($id) {
        // get your HTML raw content without any layouts or scripts
        $tr = PayStormer::findOne($id);
        $date = Yii::$app->formatter->asDatetime($tr->created_at);
        $content = "";

        $content .= '<table border="1" class="table">';
        $content .= '<tr><td>Project name</td><td>Count ideas</td><td>Price ($)</td></tr>';
        $projects = \app\models\Projects::find()->where(['id'=>array_unique( json_decode($tr->projects))])->all();
//                            var_dump($projects);
        foreach($projects as $project){
            $content .= '<tr><td>'.$project->title.'</td><td>'.\app\models\Ideas::find()->where(['project_id'=>$project->id,'stormer_id'=>Yii::$app->user->id])->count().'</td><td>'.\app\models\Ideas::find()->where(['project_id'=>$project->id,'stormer_id'=>Yii::$app->user->id])->count()*$tr->price_idea.'</td></tr>';
        }
        $content .= '</table>';
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            'filename'=>$tr->date_for.'.pdf',
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_DOWNLOAD,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@vendor/bower/bootstrap/dist/css/bootstrap.min.css',
//            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            //'cssFile' => '@web/frontend/css/main.css',
            // any css to be embedded if required
            'cssInline' => '.sum_param{color: #a9a9a9;
                                        float: left;
                                        font-size: 15px;
                                        line-height: 15px;
                                        margin: 0 2% 20px 0;
                                        width: 30%;}',
            // set mPDF properties on the fly
            //'options' => ['title' => 'Krajee Report Title'],
            // call mPDF methods on the fly
//            'methods' => [
//                'SetHeader'=>['Krajee Report Header'],
//                'SetFooter'=>['{PAGENO}'],
//            ]
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    function actionInstruction(){
        $json = InnovationQuestions::find()
            ->where(['type'=>Yii::$app->request->post('type')])
            ->orderBy(new Expression('rand()'))
            ->asArray()
            ->one();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $json;
    }
    function actionInspire(){
        $json = Inspire::find()
            ->orderBy(new Expression('rand()'))
            ->asArray()
            ->one();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $json;
    }

    public function actionStormer_benchmark() {
        // get your HTML raw content without any layouts or scripts
        $user = User::findOne(Yii::$app->user->id);

        $content = "This certifies that $user->name has reached level $user->level in our innovation system.<br>";
        if ($user->level <20)
            $content .= 'Congratulations! You are now an Ns (Nimbostratus) Stormer!<br>';
        elseif ($user->level <30)
            $content .= 'Congratulations! You are now a St (Stratus) Stormer!<br>';
        elseif ($user->level <40)
            $content .= 'Congratulations! You are now a Cu (Cumulus) Stormer!<br>';
        elseif ($user->level <50)
            $content .= 'Congratulations! You are now a Sc (Stratocumulus) Stormer!<br>';
        elseif ($user->level <60)
            $content .= 'Congratulations! You are now an As (Altostratus) Stormer!<br>';
        elseif ($user->level <70)
            $content .= 'Congratulations! You are now an Ac (Altocumulus) Stormer!<br>';
        elseif ($user->level <80)
            $content .= 'Congratulations! You are now a Ci (Cirrus) Stormer!<br>';
        elseif ($user->level <90)
            $content .= 'Congratulations! You are now a Cc (Cirrocumulus) Stormer!<br>';
        elseif ($user->level >=10)
            $content .= 'Congratulations! You are now a Cs (Cirrostratus) Stormer!<br>';
        elseif ($user->level <100)
            $content .= 'Congratulations! You are now a Cb (Cumulonimbus) Stormer!<br>';
        $content .='This certifies that that the bearer has handled a wide range of difficult and challenging innovation problems. Although there is always room to grow, this is a declaration of the bearer’s amazing achievements so far.<br>
            If the contents of the certificate need to be independently verified, feel free to contact us at support@braincloud.solutions.<br>
            Chris Lawrence signature<br>
            CEO BrainCloud Solutions<br>
           ';
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            'filename'=>'stormer_benchmark.pdf',
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_DOWNLOAD,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@vendor/bower/bootstrap/dist/css/bootstrap.min.css',
//            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            //'cssFile' => '@web/frontend/css/main.css',
            // any css to be embedded if required
            'cssInline' => '.sum_param{color: #a9a9a9;
                                        float: left;
                                        font-size: 15px;
                                        line-height: 15px;
                                        margin: 0 2% 20px 0;
                                        width: 30%;}',
            // set mPDF properties on the fly
            //'options' => ['title' => 'Krajee Report Title'],
            // call mPDF methods on the fly
//            'methods' => [
//                'SetHeader'=>['Krajee Report Header'],
//                'SetFooter'=>['{PAGENO}'],
//            ]
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    public function actionPayouts(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $mas_req = [];
        if (Yii::$app->request->post('pay_id')){

            $pay_stormer = PayStormer::findOne(Yii::$app->request->post('pay_id'));
            $pay = new \app\commands\Paypal();
            $payouts = new Payout();
            $senderBatchHeader = new PayoutSenderBatchHeader();
            $senderBatchHeader->setSenderBatchId(uniqid())
                ->setEmailSubject("You have a payment");

            $senderItem1 = new PayoutItem();
            $senderItem1->setRecipientType('Email')
                ->setNote('Thanks you.')
                ->setReceiver($pay_stormer->paypal)
                ->setSenderItemId("item_1" . uniqid())
                ->setAmount(new Currency('{
                        "value":"'.$pay_stormer->price.'",
                        "currency":"USD"
                    }'));

            $payouts->setSenderBatchHeader($senderBatchHeader)
                ->addItem($senderItem1);

            $request = clone $payouts;

            try {
                $output = $payouts->create(null, $pay->api);
            } catch (Exception $ex) {
                $mas_req['status']=false;
                $mas_req['data']=$ex;
                return $mas_req;
            }

            $payoutBatch = $output;

            $payoutBatchId = $payoutBatch->getBatchHeader()->getPayoutBatchId();

            try {
                $output = Payout::get($payoutBatchId, $pay->api);
            } catch (Exception $ex) {
                $mas_req['status']=false;
                $mas_req['data']=$ex;
                return $mas_req;
            }

            $pay_stormer->status=1;
            $pay_stormer->save();
            $mas_req['status']=true;
            $mas_req['data']=json_decode($output);
            return $mas_req;
        }
        return $mas_req['status']=false;
    }

    public function actionGetcity()
    {
        $result = [];
        $modelCity = City::find()->where(['country_id' => $_POST['country_id']])->orderBy('city_name ASC')->all();
        $arrayCountry = ArrayHelper::map($modelCity, 'id', 'city_name');
        ArrayHelper::multisort($arrayCountry, ['city_name'], [SORT_ASC]);
        $result['city'] = $arrayCountry;
        if($arrayCountry != null){
            $result['status'] = 'good';
        }else{
            $result['status'] = 'null';
        }
        echo json_encode($result);
    }

    public function actionForgot(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user=User::find()->where(['email'=>Yii::$app->request->post('email')])->one();
        if ($user){
            Yii::$app->mailer->compose()
                ->setTo($user->email)
                ->setFrom(Yii::$app->params['supportEmail'])
                ->setSubject('Login on the site braincloud.')
                ->setHtmlBody("Your login to access the site <strong>$user->username</strong>, password <strong>$user->password</strong>")
                ->send();
            $rezult['status'] = true;
            $rezult['msg'] = 'Login and password send to email!';
        }else{
            $rezult['status'] = false;
            $rezult['msg'] = 'Email not found';
        }
        return $rezult;
    }

    public function actionFacebook_frends(){
        $fb = new Facebook([
            'app_id' => '665712863584192',
            'app_secret' => '0ef4f9ec43667575d86b0a4ca7c349ae',
            'default_graph_version' => 'v2.8',
            'persistent_data_handler' => new PersistentDataHandlerYii(),
            //'default_access_token' => '{access-token}', // optional
        ]);

        $helper = $fb->getRedirectLoginHelper();
        Yii::$app->session['FBRLH_state']=$_GET['state'];
        try {
            $accessToken = $helper->getAccessToken();
        } catch(FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        if (isset($accessToken)) {
            // Logged in!
//            $_SESSION['facebook_access_token'] = (string) $accessToken;
//            echo $accessToken;
            try {
                $requestFriends = $fb->get('/me/taggable_friends?fields=id,name,email&limit=100',$accessToken);
                $friends = $requestFriends->getGraphEdge();
//                $response = $fb->get('/me?fields=taggable_friends{id}',$accessToken);
//                $users = $response->getGraphEdge();
            } catch(FacebookResponseException $e) {
                // When Graph returns an error
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch(FacebookSDKException $e) {
                // When validation fails or other local issues
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }

            if ($fb->next($friends)) {
                $allFriends = array();
                $friendsArray = $friends->asArray();
                $allFriends = array_merge($friendsArray, $allFriends);
                while ($friends = $fb->next($friends)) {
                    $friendsArray = $friends->asArray();
                    $allFriends = array_merge($friendsArray, $allFriends);
                }
                var_dump($allFriends);
                /*foreach ($allFriends as $key) {
                    echo $key['name'] . "<br>";
                }*/
//                echo count($allfriends);
            } else {
                $allFriends = $friends->asArray();
                var_dump($allFriends);
                $totalFriends = count($allFriends);
                /*foreach ($allFriends as $key) {
                    echo $key['id'] . "<br>";
                }*/
            }

//            var_dump($response->getDecodedBody()['taggable_friends']['data']);
//            var_dump($graphObject);
//            $all = $users->asArray();
//            var_dump($all);
            // Now you can redirect to another page and use the
            // access token from $_SESSION['facebook_access_token']
        } else {
            echo 'error';
        }
    }

    public function actionNotification(){
        PointsMessage::updateAll(['show' => 1], 'user_id = '.Yii::$app->user->id );
        return true;
    }

    public function actionWebhook(){
//        \Yii::$app->response->format = Response::FORMAT_JSON;
        if (file_get_contents('php://input')) {
            $webhook = new Transaction();
            $webhook->othe = file_get_contents('php://input');
            $webhook->save();
            return 'ok';
        } else {
            return 'false';
        }

    }

    public function actionSend_code(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->post('email')) {


            $user = User::findById(Yii::$app->user->id);
            $user->scenario = 'update';
            $user->email = Yii::$app->request->post('email');
            $user->save();

            Yii::$app->mailer->compose('activation-html', [
                'user' => $user,
            ])// здесь устанавливается результат рендеринга вида в тело сообщения
            ->setFrom(Yii::$app->params['adminEmail'])
                ->setTo($user->email)
                ->setSubject('braincloud activation code')
                ->send();
            return true;
        } else {
            return false;
        }
    }
}
