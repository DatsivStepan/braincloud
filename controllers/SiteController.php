<?php

namespace app\controllers;
use app\models\City;
use app\models\EmailQuoneForm;
use app\models\Points;
use app\models\PointsMessageSearch;
use app\models\Projectstormer;
use app\models\SalesTeams;
use app\models\UserBan;
use yii\helpers\Json;
use yii\web\HttpException;
use yii\data\ActiveDataProvider;
use Yii;

use yii\db\Query;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Pages;
use app\models\Country;
use app\models\Faqs;
use app\models\News;
use app\models\Comment;
use app\models\ActivationUser;
use app\models\ActivateConfirm;
use app\models\Customerinfo;
use app\models\Stormerinfo;
use app\lib\reCaptcha;
use app\lib\uploadFoto;
use app\lib\localization;
use yii\web\NotFoundHttpException;

class SiteController extends Controller
{
  public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            'eauth' => array(
                // required to disable csrf validation on OpenID requests
                'class' => \nodge\eauth\openid\ControllerBehavior::className(),
                'only' => array('register_social'),
            ),
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $newModelLogin = new LoginForm();

        if (!Yii::$app->user->isGuest){
            if (Yii::$app->user->identity->users_type =='stormer'){
                return $this->redirect('/stormer/'.Yii::$app->user->id);
            }
            if (Yii::$app->user->identity->users_type =='customer'){
                return $this->redirect('/customer/'.Yii::$app->user->id);
            }
            if (Yii::$app->user->identity->users_type =='admin'){
                return $this->redirect('/admin/news');
            }
        }
        return $this->render('index',[
            'newModelLogin' => $newModelLogin
        ]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $newModelLogin = new LoginForm();
        if ($newModelLogin->load(Yii::$app->request->post()) && $newModelLogin->login()) {

            if (Yii::$app->user->identity->users_type =='stormer'){
                return $this->redirect('/stormer/'.Yii::$app->user->id);
            }
            if (Yii::$app->user->identity->users_type =='customer'){
                return $this->redirect('/customer/'.Yii::$app->user->id);
            }
            return $this->goBack();
        }else{
            return $this->render('login', [
                'model' => $newModelLogin,
            ]);
        }

        return $this->render('index', [
            'newModelLogin' => $newModelLogin,
        ]);

    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionSignup($signup_type,$step=0,$stormer_id=0){



        if (!Yii::$app->user->isGuest){
            Yii::$app->user->logout();
        }
        $arrayCountry = ArrayHelper::map(Country::find()->orderBy('country_name ASC')->all(), 'id', 'country_name');
        $modelUser = new User();
        $arraySity = [];
        $email_input=false;
        $terms = Pages::find()->where(['page_url' => 'site/terms'])->one();
        $modal_terms_stormer = Pages::find()->where(['page_url' => 'stormer/modal_terms'])->one();
        $modal_terms_customer = Pages::find()->where(['page_url' => 'customer/modal_terms'])->one();
            if($signup_type == 'stormer'){
                $modelStormer = new Stormerinfo();
                $modelUser->scenario = 'registration_s';
                $request = Yii::$app->request;
                /*if (Yii::$app->request->get('ref')){
                    $user = User::find()->where(['referal_key'=>Yii::$app->request->get('ref')])->one();
                    if ($user){
                        $modelUser->parent_id=$user->id;
                        if (Yii::$app->request->get('email')){
                            $modelUser->email=Yii::$app->request->get('email');
                            $sale_teams=SalesTeams::find()->where(['user_id'=>$user->id,'email'=>Yii::$app->request->get('email')])->one();
                            if ($sale_teams){
                                $sale_teams->status=1;
                                $sale_teams->save();
                            }
                        }
                    }

                }*/

                if($_POST){
                    if($step == 'step_1'){
                        if ($_POST['ref_code']){
                            $user = User::find()->where(['referal_key'=>$_POST['ref_code']])->one();
                            if ($user){
                                $modelUser->parent_id=$user->id;
                                if (Yii::$app->request->get('email')){
                                    $modelUser->email=Yii::$app->request->get('email');
                                    $sale_teams=SalesTeams::find()->where(['user_id'=>$user->id,'email'=>Yii::$app->request->get('email')])->one();
                                    if ($sale_teams){
                                        $sale_teams->status=1;
                                        $sale_teams->save();
                                    }
                                }
                            }

                        }
                            if($modelUser->load($request->post())){
                                $modelUser->password_hash = \Yii::$app->security->generatePasswordHash($modelUser->password);
                                $modelUser->auth_key = 'key';
                                $modelUser->signup_step = '1';
                                $modelUser->active = '0';

                                if ($modelUser->save()){
                                    return $this->redirect(['signup', 'signup_type' => 'stormer', 'step' => 'step_2','stormer_id' => $modelUser->id]);
                                }
                            }

                    }elseif($step == 'step_2'){
                        $modelStormer->scenario = 'customer_save_step_2';
                        $modelStormer->tags = json_encode(explode(",", substr($_POST['value_post'], 1)));
                            $yourCategorySelect = [];
                            if(isset($_POST['your_category1'])){
                                $yourCategorySelect = [];
//                                $yourCategorySelect[] = $_POST['your_category1'];
                                for ($i = 1; $i < $_POST['count_your_catecory']; $i++){
                                    $category = $_POST['your_category'.$i];
                                    if($category != null){
                                        $yourCategorySelect[] = $_POST['your_category'.$i];
                                    }else{
                                        break;
                                    }
                                }
                            }


                        $arrayCategorys = array_merge($yourCategorySelect, $_POST['multtiSelect']);
                        $modelStormer->category = json_encode($arrayCategorys);
                        $modelStormer->stormer_id = $stormer_id;

                        $stormerI = User::find()->where(['id' =>  $stormer_id])->one();
                        $stormerI->signup_step = '2';
                        $stormerI->scenario = 'change_status_registration';

                        if (($modelStormer->save()) && ($stormerI->save())){
                            return $this->redirect(['signup', 'signup_type' => 'stormer', 'step' => 'step_3','stormer_id' => $stormer_id]);
                        }

                    }elseif($step == 'step_3'){
                        $modelStormerU = Stormerinfo::find()->where(['stormer_id' => $stormer_id])->one();
                        $modelStormerU->scenario = 'customer_save_step_3';
                        if($modelStormerU->load($request->post())){
                            if (isset($_POST['city_head'])){
                                $city = City::find()->where(['city_name'=>$_POST['city_head']])->one();
                                if ($city){
                                    $modelStormerU->city = $city->id;
                                } else {
                                    $modelStormerU->city = null;
                                }
                            }
                          if (isset($_POST['country_head'])){
                            $city = Country::find()->where(['country_name'=>$_POST['country_head']])->one();
                            if ($city){
                              $modelStormerU->country = $city->id;
                            } else {
                              $modelStormerU->country = null;
                            }
                          }
                            $stormerU = User::find()->where(['id' =>  $stormer_id])->one();
                            $stormerU->signup_step = '3';
                            $stormerU->scenario = 'change_status_registration';
                            if (!$stormerU->email){
                                $stormerU->scenario = 'change_status_registration_email';
                                $stormerU->email=$_POST['User']['email'];
                            }

//                            var_dump($_POST);
//                            exit;
                            if (($modelStormerU->save()) && ($stormerU->save())){
                              $model = new ActivationUser();
                              $model['email'] = $stormerU->email;

                              if ($model->sendEmail()){
                                  Yii::$app->session->setFlash('EMAIL_sended');
                              } else {
                                  Yii::$app->session->setFlash('Email_error');
                              }
                                if (Yii::$app->user->isGuest){
                                    if(Yii::$app->getUser()->login($stormerU)){
                                        return $this->redirect('stormer/'.$stormerU->id);
                                    }
                                }else{
                                        return $this->redirect('stormer/'.$stormerU->id);
                                }
                            }else{
                                if($stormerU->getErrors()){
                                    $email_input=true;
                                }
                            }
                        }
                    }
                };

                if((string)$step == 'step_1'){
                    if (Yii::$app->user->isGuest){
                        return $this->render('signup',[
                           'user_type' => 'stormer',
                           'newModel' => $modelUser,
                           'role' => $signup_type,
                           'step' => $step,
                            'arrayCountry' => $arrayCountry,
                            'terms'=>$terms,
                            'modal_terms_stormer'=>$modal_terms_stormer,
                            'modal_terms_customer'=>$modal_terms_customer,
                        ]);
                    }else{
                        throw new HttpException(404);
                    }
                }elseif((string)$step == 'step_2'){
                    $userFindById = User::findById($stormer_id);
                    if(($stormer_id != 0) && ($userFindById != null)){
                        if($userFindById->active == '1'){
                            return $this->redirect('index');
                        }
                        if($userFindById->signup_step == '1'){

                            return $this->render('signup',[
                               'user_type' => 'stormer',
                               'newModel' => $modelUser,
                               'role' => $signup_type,
                               'step' => $step,
                               'stormer_id' => $stormer_id,
                                'arrayCountry' => $arrayCountry,
                                'terms'=>$terms,
                                'modal_terms_stormer'=>$modal_terms_stormer,
                                'modal_terms_customer'=>$modal_terms_customer,
                            ]);
                        }else{
                            return $this->redirect(['signup', 'signup_type' => 'stormer', 'step' => 'step_'.(int)$userFindById->signup_step + 1,'stormer_id' => $stormer_id]);
                        }
                    }else{
                        throw new HttpException(404);
                    }
                }elseif((string)$step == 'step_3'){

                    if (!isset($stormerU)){
                        $stormerU=User::findOne($stormer_id);
                    }

                    $userFindById = User::findById($stormer_id);
                    if(($stormer_id != 0) && ($userFindById != null)){
                        if($userFindById->active == '1'){
                            return $this->redirect('index');
                        }
                        if($userFindById->signup_step == '2'){
                            $modelStormer->scenario = 'customer_save_step_3';
                            return $this->render('signup',[
                               'user_type' => 'stormer',
                               'newModel' => $modelUser,
                               'role' => $signup_type,
                                'user' => $stormerU,
                               'step' => $step,
                               'stormer_id' => $stormer_id,
                               'modelStormerInfo' => $modelStormer,
                                'arrayCountry' => $arrayCountry,
                                'terms'=>$terms,
                                'email_input'=>$email_input,
                                'modal_terms_stormer'=>$modal_terms_stormer,
                            'modal_terms_customer'=>$modal_terms_customer,
                            ]);
                        }else{
                            //
                            //доробити відпрвку на інший крок
                            return $this->redirect(['signup', 'signup_type' => 'stormer', 'step' => 'step_'.(int)$userFindById->signup_step + 1,'stormer_id' => $stormer_id]);
                        }
                    }else{
                        throw new HttpException(404);
                    }

                }else{
                    return $this->render('stormerinfo');
                }


                //customer registration
            }elseif($signup_type == 'customer'){

                $modelUser->scenario = 'registration';
                $modelCustomer = new Customerinfo();

                $modelCustomer->scenario = 'customer_signup';
                $request = Yii::$app->request;
                if (Yii::$app->request->get('email')){
                    $modelUser->email=Yii::$app->request->get('email');
                }

                    if ((isset($_POST['User'])) && (isset($_POST['Customerinfo']))) {
                        $modelUser->load($request->post());
                        $modelCustomer->load($request->post());
                        if (isset($_POST['city_head'])){
                            $city = City::find()->where(['city_name'=>$_POST['city_head']])->one();
                            if ($city){
                                $modelCustomer->city = $city->id;
                            } else {
                                $modelCustomer->city = null;
                            }
                        }
                      if (isset($_POST['country_head'])){
                        $city = Country::find()->where(['country_name'=>$_POST['country_head']])->one();
                        if ($city){
                          $modelCustomer->country = $city->id;
                        } else {
                          $modelCustomer->country = null;
                        }
                      }
                        $modelCustomer->customer_id=-1;
                        $modelUser->password_hash = \Yii::$app->security->generatePasswordHash($modelUser->password);
                        $modelUser->auth_key = 'key';
                        if (isset($_POST['post_project_basic']) or isset($_POST['take_look_basic'])) {
                            if ($modelUser->save() && $modelUser->validate()) {
                                Yii::$app->mailer->compose()
                                    ->setTo($modelUser->email)
                                    ->setFrom(Yii::$app->params['supportEmail'])
                                    ->setSubject('Registration on the site braincloud.')
                                    ->setHtmlBody("Congratulations! You have registered on the site. Your login to access the site <strong>$modelUser->username</strong>, password <strong>$modelUser->password</strong>")
                                    ->send();
                                if ($_POST['ref_code']){
                                    $user = User::find()->where(['referal_key' => $_POST['ref_code']])->one();
                                    if ($user) {
                                        $modelUser->parent_id = $user->id;
                                        $modelUser->save();
                                        $point_c1 = new Points();
                                        $point_c1->point = 50;
                                        $point_c1->type = 'referal_customer_50';
                                        $point_c1->user_id = $user->id;
                                        $point_c1->date_week = $point_c1->week();
                                        $point_c1->save();

                                        $point_c1 = new Points();
                                        $point_c1->point = 25;
                                        $point_c1->type = 'referal_customer_25';
                                        $point_c1->user_id = $modelUser->id;
                                        $point_c1->date_week = $point_c1->week();
                                        $point_c1->save();

                                        if (Yii::$app->request->get('email')){
                                            $sale_teams=SalesTeams::find()->where(['user_id'=>$user->id,'email'=>Yii::$app->request->get('email')])->one();
                                            if ($sale_teams){
                                                $sale_teams->status=1;
                                                $sale_teams->customer_id= $modelUser->id;
                                                $sale_teams->save();
                                            }
                                        }
                                    }
                                }
                                /*if (Yii::$app->request->get('ref')) {
                                    $user = User::find()->where(['referal_key' => Yii::$app->request->get('ref')])->one();
                                    if ($user) {
                                        $modelUser->parent_id = $user->id;
                                        $modelUser->save();
                                        $point_c1 = new Points();
                                        $point_c1->point = 50;
                                        $point_c1->type = 'referal_customer_50';
                                        $point_c1->user_id = $user->id;
                                        $point_c1->date_week = $point_c1->week();
                                        $point_c1->save();

                                        $point_c1 = new Points();
                                        $point_c1->point = 25;
                                        $point_c1->type = 'referal_customer_25';
                                        $point_c1->user_id = $modelUser->id;
                                        $point_c1->date_week = $point_c1->week();
                                        $point_c1->save();

                                        if (Yii::$app->request->get('email')){
                                            $sale_teams=SalesTeams::find()->where(['user_id'=>$user->id,'email'=>Yii::$app->request->get('email')])->one();
                                            if ($sale_teams){
                                                $sale_teams->status=1;
                                                $sale_teams->customer_id= $modelUser->id;
                                                $sale_teams->save();
                                            }
                                        }
                                    }
                                }*/
                                $modelCustomer->customer_id = $modelUser->id;
                                $modelCustomer->scenario = 'customer_signup_basic';
                                $modelCustomer->save();
                                //
                                //begin Busy? Do this later -> Post a project now
                                if (isset($_POST['post_project_basic'])) {
                                    if (Yii::$app->getUser()->login($modelUser)) {
                                        return $this->redirect('../../projects/postproject');
                                    }
                                }
                                //end Busy? Do this later -> Post a project now
                                //

                                //
                                //begin Busy? Do this later -> Take a look around firs
                                if (isset($_POST['take_look_basic'])) {
                                    if (Yii::$app->getUser()->login($modelUser)) {
                                        return $this->redirect('../../customer');
                                    }
                                }
                                //end Busy? Do this later -> Take a look around firs
                                //


                                //
                                //begin Continue -> Post a project now
                                if (isset($_POST['post_project_all'])) {
                                    if ($modelCustomer->save()) {
                                        if (Yii::$app->getUser()->login($modelUser)) {
                                            return $this->redirect('../../projects/postproject');
                                        }
                                    }
                                }
                                //end Continue -> Post a project now
                                //

                                //
                                //begin Continue -> Take a look around firs
                                if (isset($_POST['take_look_all'])) {
                                    if ($modelCustomer->save()) {
                                        if (Yii::$app->getUser()->login($modelUser)) {
                                            return $this->redirect('../../customer');
                                        }
                                    }
                                }
                                //end Continue -> Take a look around firs
                                //

                            } else {
                                if ($modelCustomer->country) {
                                    $arraySity = ArrayHelper::map(City::find()->where(['country_id' => $modelCustomer->country])->orderBy('city_name ASC')->all(), 'id', 'city_name');
                                }
                                return $this->render('signup', [
                                    'user_type' => 'customer',
                                    'newModel' => $modelUser,
                                    'modelCustomer' => $modelCustomer,
                                    'role' => $signup_type,
                                    'arrayCountry' => $arrayCountry,
                                    'terms' => $terms,
                                    'arraySity' => $arraySity,
                                    'modal_terms_stormer'=>$modal_terms_stormer,
                                    'modal_terms_customer'=>$modal_terms_customer,
                                ]);
                            }
                        }else{
                            if ($modelCustomer->validate() && $modelUser->validate() ){
                                if ($modelUser->save()) {
                                    Yii::$app->mailer->compose()
                                        ->setTo($modelUser->email)
                                        ->setFrom(Yii::$app->params['supportEmail'])
                                        ->setSubject('Registration on the site braincloud.')
                                        ->setHtmlBody("Congratulations! You have registered on the site. Your login to access the site <strong>$modelUser->username</strong>, password <strong>$modelUser->password</strong>")
                                        ->send();
                                    if (Yii::$app->request->get('ref')) {
                                        $user = User::find()->where(['referal_key' => Yii::$app->request->get('ref')])->one();
                                        if ($user) {
                                            $modelUser->parent_id = $user->id;
                                            $modelUser->save();
                                            $point_c1 = new Points();
                                            $point_c1->point = 50;
                                            $point_c1->type = 'referal_customer_50';
                                            $point_c1->user_id = $user->id;
                                            $point_c1->date_week = $point_c1->week();
                                            $point_c1->save();

                                            $point_c1 = new Points();
                                            $point_c1->point = 25;
                                            $point_c1->type = 'referal_customer_25';
                                            $point_c1->user_id = $modelUser->id;
                                            $point_c1->date_week = $point_c1->week();
                                            $point_c1->save();

                                            if (Yii::$app->request->get('email')){
                                                $sale_teams=SalesTeams::find()->where(['user_id'=>$user->id,'email'=>Yii::$app->request->get('email')])->one();
                                                if ($sale_teams){
                                                    $sale_teams->status=1;
                                                    $sale_teams->customer_id= $modelUser->id;
                                                    $sale_teams->save();
                                                }
                                            }
                                        }
                                    }
                                    $modelCustomer->customer_id = $modelUser->id;
                                    //
                                    //begin Busy? Do this later -> Post a project now
                                    if (isset($_POST['post_project_basic'])) {
                                        if (Yii::$app->getUser()->login($modelUser)) {
                                            return $this->redirect('../../projects/postproject');
                                        }
                                    }
                                    //end Busy? Do this later -> Post a project now
                                    //

                                    //
                                    //begin Busy? Do this later -> Take a look around firs
                                    if (isset($_POST['take_look_basic'])) {
                                        if (Yii::$app->getUser()->login($modelUser)) {
                                            return $this->redirect('../../customer');
                                        }
                                    }
                                    //end Busy? Do this later -> Take a look around firs
                                    //


                                    //
                                    //begin Continue -> Post a project now
                                    if (isset($_POST['post_project_all'])) {
                                        if ($modelCustomer->save()) {
                                            if (Yii::$app->getUser()->login($modelUser)) {
                                                return $this->redirect('../../projects/postproject');
                                            }
                                        }
                                    }
                                    //end Continue -> Post a project now
                                    //

                                    //
                                    //begin Continue -> Take a look around firs
                                    if (isset($_POST['take_look_all'])) {
                                        if ($modelCustomer->save()) {
                                            if (Yii::$app->getUser()->login($modelUser)) {
                                                return $this->redirect('../../customer');
                                            }
                                        }
                                    }
                                    //end Continue -> Take a look around firs
                                    //

                                } else {
                                    if ($modelCustomer->country) {
                                        $arraySity = ArrayHelper::map(City::find()->where(['country_id' => $modelCustomer->country])->orderBy('city_name ASC')->all(), 'id', 'city_name');
                                    }
                                    return $this->render('signup', [
                                        'user_type' => 'customer',
                                        'newModel' => $modelUser,
                                        'modelCustomer' => $modelCustomer,
                                        'role' => $signup_type,
                                        'arrayCountry' => $arrayCountry,
                                        'terms' => $terms,
                                        'arraySity' => $arraySity,
                                        'modal_terms_stormer'=>$modal_terms_stormer,
                                        'modal_terms_customer'=>$modal_terms_customer,
                                    ]);
                                }
                            }else{
                                if ($modelCustomer->country) {
                                    $arraySity = ArrayHelper::map(City::find()->where(['country_id' => $modelCustomer->country])->orderBy('city_name ASC')->all(), 'id', 'city_name');
                                }
                                return $this->render('signup', [
                                    'user_type' => 'customer',
                                    'newModel' => $modelUser,
                                    'modelCustomer' => $modelCustomer,
                                    'role' => $signup_type,
                                    'arrayCountry' => $arrayCountry,
                                    'terms' => $terms,
                                    'arraySity' => $arraySity,
                                    'modal_terms_stormer'=>$modal_terms_stormer,
                                    'modal_terms_customer'=>$modal_terms_customer,
                                ]);
                            }
                        }
                    }




                    if((string)$step == 'step_2'){

                        if($modelCustomer->country){
                            $arraySity= ArrayHelper::map(City::find()->where(['country_id'=>$modelCustomer->country])->orderBy('city_name ASC')->all(), 'id', 'city_name');
                        }
                        return $this->render('signup',[
                            'user_type' => 'customer',
                            'newModel' => $modelUser,
                            'modelCustomer' => $modelCustomer,
                            'role' => $signup_type,
                            'arrayCountry' => $arrayCountry,
                            'terms'=>$terms,
                            'arraySity'=>$arraySity,
                            'modal_terms_stormer'=>$modal_terms_stormer,
                            'modal_terms_customer'=>$modal_terms_customer,
                        ]);
                    }else{
                        return $this->render('customerinfo');
                    }

            }else{
                throw new HttpException(404);
            }
    }

    
    public function actionEmailquote()
    {
        //var_dump($_GET);exit;
        $slider_scope_project = $_GET['scope_project'];
        $slider_team_size = $_GET['team_size'];
        $price_slider = $_GET['count_price'];

        $model = new EmailQuoneForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact()){
            Yii::$app->session->setFlash('contactFormSubmitted');
        }

        return $this->render('email_quote',[
            'model'=>$model,
        ]);
    }
    
    public $enableCsrfValidation = false;
    
    public function actionAcademics(){
        $modelPages = Pages::find()->where(['page_url' => 'site/academics'])->one();
        return $this->render('for_academic', [
            'content' => $modelPages->content,
        ]);
    }
    
    public function actionAbout()
    {
        $modelPages = Pages::find()->where(['page_url' => 'site/about'])->one();
        return $this->render('about', [
            'content' => $modelPages->content,
        ]);
    }

    public function actionNgosprofits(){

        $modelPages = Pages::find()->where(['page_url' => 'site/ngosprofits'])->one();
        return $this->render('for_ngosprofit', [
            'content' => $modelPages->content,
        ]);
    }

    public function actionPrivacy(){
        $modelPages = Pages::find()->where(['page_url' => 'site/privacy'])->one();
        return $this->render('privacy', [
            'content' => $modelPages->content,
        ]);
    }

    public function actionTerms(){
        $modelPages = Pages::find()->where(['page_url' => 'site/terms'])->one();
        return $this->render('terms', [
            'content' => $modelPages->content,
        ]);
    }
    public function actionFaq(){
        $modelFaqs = Faqs::find()->where(['type' => 'home'])->all();
        return $this->render('faq', [
            'modelFaqs' => $modelFaqs,
        ]);
    }

    public function actionOurmethodology(){

        return $this->render('ourmethodology',[]);
    }

    public function actionLock(){

        $ban = UserBan::find()->where(['stormer_id'=>Yii::$app->user->id])->one();
        return $this->render('lock',[
            'ban'=>$ban,
        ]);
    }

    public function actionImgsavefile($src){
        $uploadf = new Uploadfoto();

        if($src == 'img_save_to_file'){
            echo json_encode($uploadf->img_save_to_file());
        }elseif($src == 'img_crop_to_file'){
            echo json_encode($uploadf->img_crop_to_file());
        }else{
            throw new HttpException(404);
        }
    }
    
    public function actionSavedropedfile(){
      $ds          = DIRECTORY_SEPARATOR;  //1
      $storeFolder = 'tmp_uploads';   //2
      // mkdir($storeFolder);
      if (!empty($_FILES)) {
        if(!is_dir($storeFolder.$ds.Yii::$app->user->id)){
          mkdir($storeFolder.$ds.Yii::$app->user->id);
        }
        
        $tempFile = $_FILES['file']['tmp_name'];

        $targetPath =  $storeFolder . $ds.Yii::$app->user->id.$ds;  //4

        $targetFile =  $targetPath. $_FILES['file']['name'];  //5

        move_uploaded_file($tempFile,$targetFile); //6
        return $targetFile;
      }else{
          echo"2";
          var_dump($_FILES);exit;
      }
      
    }
    
    public function actionActivation($token)
    {
        // var_dump($token);exit;
      if (!Yii::$app->user->isGuest){
        Yii::$app->user->logout();
      }
        $model = User::findByActivationToken($token);
        if (!$model) {
            throw new NotFoundHttpException('User not found');
        }
        $model->active = 1;
        $model->removeActivationToken();
        $model->scenario='active_token';
        if ($model->save()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');
//            var_dump($model);

            $user = User::findOne(Yii::$app->user->id);
            if($user->parent_id){

                $point_c1= new Points();
                $point_c1->point = 20;
                $point_c1->type = 'referal_stormer_20';
                $point_c1->user_id=$user->parent_id;
                $point_c1->date_week=$point_c1->week();
                $point_c1->save();
            }
            Yii::$app->mailer->compose()
                ->setTo($user->email)
                ->setFrom(Yii::$app->params['supportEmail'])
                ->setSubject('Registration on the site braincloud.')
                ->setHtmlBody("Congratulations! You have registered on the site. Your login to access the site <strong>$user->username</strong>, password <strong>$user->password</strong>")
                ->send();
            return $this->goHome();
        } else {
            return $this->redirect('/');
        }

    }
    
    public function actionSavedprofilephoto(){
        $ds          = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot').'/image/users_images/';   //2
        // mkdir($storeFolder);
        if (!empty($_FILES)) {
            if(!is_dir($storeFolder.$ds.Yii::$app->user->id)){
              mkdir($storeFolder.$ds.Yii::$app->user->id, 0777, true);
            }
              $tempFile = $_FILES['file']['tmp_name'];          //3

              $targetPath =  $storeFolder . $ds.Yii::$app->user->id.$ds;  //4

              $for_name = time();

              $targetFile =  $targetPath.$for_name.'.jpg';  //5

              move_uploaded_file($tempFile,$targetFile); //6
              return \Yii::$app->user->id.'/'.$for_name.'.jpg';
        }
    }
    
    public function actionDeleteprofilephoto(){
        $storeFolder = \Yii::getAlias('@webroot').'/image/users_images/';
        $path = $storeFolder.$_POST['file_name'];
        if(unlink($path)){
            echo 'delete';
        }else{
            echo 'problem';
        }
    }
    
    public function actionBlogs(){
        $request = Yii::$app->request;
        $query = News::find()->where(['category'=>'home'])->orderBy(['id'=>SORT_DESC]);
        $modelNews = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
        
        return $this->render('blogs', [
            'modelNews' => $modelNews->getModels(),
            'pagination' => $modelNews->pagination,
            'count' => $modelNews->pagination->totalCount,
        ]);
    }

    public function actionBlog($blog_id){
        $modelNews = News::find()->where(['id' => $blog_id])->one();
        //var_dump($modelNews);exit;
        $newComment = new Comment;
        
        if($_POST){
            $request = Yii::$app->request;
            if(isset($_POST['News'])){
                $modelNews->scenario = 'update_news';
                if($modelNews->load($request->post())){
                    if($modelNews->save()){
                        Yii::$app->session->setFlash('news_update');
                    }else{
                        Yii::$app->session->setFlash('news_not_update');
                    }
                }
            }
            
            if(isset($_POST['Comment'])){
                $newComment->scenario = 'add_comment';
                if($newComment->load($request->post())){
                    if($newComment->save()){
                        Yii::$app->session->setFlash('comment_added');
                        $newComment = new Comment;
                    }else{
                        Yii::$app->session->setFlash('comment_not_added');
                    }
                }
            }
            if(isset($_POST['deleteComment'])){
                $comment_id = $_POST['comment_id'];
                $modelCommentD = Comment::find()->where(['id' => $comment_id])->one();
                if($modelCommentD->delete()){
                    Yii::$app->session->setFlash('comment_delete');
                }else{
                    Yii::$app->session->setFlash('comment_not_delete');
                }
            }
        }
        $user_id = \Yii::$app->user->identity->id;
        $modelComment = Comment::find()->where(['news_id' => $modelNews->id])->all();
        $query = new Query;
        $query->select(['*', 'id' => 'comment.id'])
            ->from('comment')
            ->join('LEFT JOIN',
                'users',
                'users.id = comment.user_id'
            )->where(['comment.news_id' => $modelNews->id]);
        $command = $query->createCommand();
        $modelComment = $command->queryAll();
        
        return $this->render('blog', [
            'modelNews' => $modelNews,
            'user_id' => $user_id,
            'newComment' => $newComment,
            'modelComment' => $modelComment
        ]);
    }

    public function actionCountryList($q = null) {
        $query = new Query;

        $query->select('city_name')
            ->from('cities')
            ->where('city_name LIKE "%' . $q .'%"')
            ->orderBy('city_name');
        $command = $query->createCommand();
        $data = $command->queryAll();
        $out = [];
        foreach ($data as $d) {
            $out[] = $d['city_name'];
        }
        echo Json::encode($out);
    }

  public function actionKrainyList($q = null) {
    $query = new Query;

    $query->select('country_name')
      ->from('countries')
      ->where('country_name LIKE "%' . $q .'%"')
      ->orderBy('country_name');
    $command = $query->createCommand();
    $data = $command->queryAll();
    $out = [];
    foreach ($data as $d) {
      $out[] = $d['country_name'];
    }
    echo Json::encode($out);
  }

    public function actionNotification()
    {
        $searchModel = new PointsMessageSearch(['user_id'=>Yii::$app->user->id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('notification',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
