<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'language' => 'en-EN',
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
        	'baseUrl' => '',
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'KIPpz30dWlEb9Km_jokPce0-IU4i6-YE',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
          'mailer' => [
              'class' => 'yii\swiftmailer\Mailer',
              // send all mails to a file by default. You have to set
              // 'useFileTransport' to false and configure a transport
              // for the mailer to send real emails.

              'useFileTransport' => false,
              'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'listat.an@gmail.com',
//                'username' => 'padlasmail@gmail.com',
//                'password' => 'alexandr9317',
                'password' => 'Listat_Greed',
//                  'port' => '587',
//                  'encryption' => 'tls',
                'port' => '465',
                'encryption' => 'ssl',
              ],
          ],
        'log' => [
            'traceLevel' => 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'eauth' => [
            'class' => 'nodge\eauth\EAuth',
            'popup' => true, // Use the popup window instead of redirecting.
            'cache' => false, // Cache component name or false to disable cache. Defaults to 'cache' on production environments.
            'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
            'httpClient' => [
                // uncomment this to use streams in safe_mode
                //'useStreamsFallback' => true,
            ],
            'services' => [ // You can change the providers and their classes.
                'google' => [
                    // register your app here: https://code.google.com/apis/console/
                    'class' => 'nodge\eauth\services\GoogleOAuth2Service',
                    'clientId' => '4052400487-pg6c85ne4bicgir4mvmvvvpuv7b7uf2t.apps.googleusercontent.com',
                    'clientSecret' => 'HcUxBf9TO_-8C2sHeErFMe7f',
                    'title' => 'Google',
                ],
                'twitter' => [
                    // register your app here: https://dev.twitter.com/apps/new
                    'class' => 'nodge\eauth\services\TwitterOAuth1Service',
                    'key' => 'PuSOOsus1efuMDe0EEH3u1QTf',
                    'secret' => 'vczK66wIqBVHrsSLKBR2Pe5rYOwyp5YchEPcKkVqdaz3edvseG',
                ],
                'yandex' => [
                    // register your app here: https://oauth.yandex.ru/client/my
                    'class' => 'nodge\eauth\services\YandexOAuth2Service',
                    'clientId' => '...',
                    'clientSecret' => '...',
                    'title' => 'Yandex',
                ],
                'facebook' => [
                    // register your app here: https://developers.facebook.com/apps/
                    'class' => 'nodge\eauth\services\FacebookOAuth2Service',
                    'clientId' => '665712863584192',
                    'clientSecret' => '0ef4f9ec43667575d86b0a4ca7c349ae',
                    'scope' =>['email']
                ],
                'yahoo' => [
                    'class' => 'nodge\eauth\services\YahooOpenIDService',
                    //'realm' => '*.example.org', // your domain, can be with wildcard to authenticate on subdomains.
                ],
                'linkedin' => [
                    // register your app here: https://www.linkedin.com/secure/developer
                    'class' => 'nodge\eauth\services\LinkedinOAuth1Service',
                    'key' => '...',
                    'secret' => '...',
                    'title' => 'LinkedIn (OAuth1)',
                ],
                'linkedin_oauth2' => [
                    // register your app here: https://www.linkedin.com/secure/developer
                    'class' => 'nodge\eauth\services\LinkedinOAuth2Service',
                    'clientId' => '...',
                    'clientSecret' => '...',
                    'title' => 'LinkedIn (OAuth2)',
                ],
                'github' => [
                    // register your app here: https://github.com/settings/applications
                    'class' => 'nodge\eauth\services\GitHubOAuth2Service',
                    'clientId' => '...',
                    'clientSecret' => '...',
                ],
                'live' => [
                    // register your app here: https://account.live.com/developers/applications/index
                    'class' => 'nodge\eauth\services\LiveOAuth2Service',
                    'clientId' => '...',
                    'clientSecret' => '...',
                ],
                'steam' => [
                    'class' => 'nodge\eauth\services\SteamOpenIDService',
                    //'realm' => '*.example.org', // your domain, can be with wildcard to authenticate on subdomains.
                    'apiKey' => '...', // Optional. You can get it here: https://steamcommunity.com/dev/apikey
                ],
                'instagram' => [
                    // register your app here: https://instagram.com/developer/register/
                    'class' => 'nodge\eauth\services\InstagramOAuth2Service',
                    'clientId' => '...',
                    'clientSecret' => '...',
                ],
                'vkontakte' => [
                    // register your app here: https://vk.com/editapp?act=create&site=1
                    'class' => 'nodge\eauth\services\VKontakteOAuth2Service',
                    'clientId' => '...',
                    'clientSecret' => '...',
                ],
                'mailru' => [
                    // register your app here: http://api.mail.ru/sites/my/add
                    'class' => 'nodge\eauth\services\MailruOAuth2Service',
                    'clientId' => '...',
                    'clientSecret' => '...',
                ],
                'odnoklassniki' => [
                    // register your app here: http://dev.odnoklassniki.ru/wiki/pages/viewpage.action?pageId=13992188
                    // ... or here: http://www.odnoklassniki.ru/dk?st.cmd=appsInfoMyDevList&st._aid=Apps_Info_MyDev
                    'class' => 'nodge\eauth\services\OdnoklassnikiOAuth2Service',
                    'clientId' => '...',
                    'clientSecret' => '...',
                    'clientPublic' => '...',
                    'title' => 'Odnoklas.',
                ],
            ],
        ],
        'i18n' => [
            'translations' => [
                'eauth' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@eauth/messages',
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'site/index',
                'savedropedfile' => 'site/savedropedfile',
                'customer/post-project/<type:(create|target)>'=>'projects/create',
                'customer/post-project/<type:(create2|target2)>/<project_id:\w+>'=>'projects/create2',
                'customer/post-project'=>'projects/postproject',
                'activation/<token>'=>'site/activation',
                'stormer/terms' => 'stormer/terms',
                'customer/terms' => 'customer/terms',
                'stormer/beinspired/<type:\w+>' => 'stormer/beinspired',
                'signup'=>'site/signup',
                'signup/<signup_type:\w+>'=>'site/signup',
                'signup/<signup_type:\w+>/<step:\w+>'=>'site/signup',
                'signup/<signup_type:\w+>/<step:\w+>/<stormer_id:\w+>'=>'site/signup',


                //listst_ls
                'customer/payproject/<project_id:\w+>' => 'customer/payproject',
                'customer/message/<id:\w+>' => 'customer/message',
                'stormer/message/<id:\w+>' => 'stormer/message',
                'stormer/project-info/<id:\w+>' => 'stormer/postprojectinfo',
                'stormer/project-edit/<id:\w+>' => 'stormer/postprojectedit',
                'customer/message/<id:\w+>/<user_id:\w+>' => 'customer/message',
                'customer/organize/<project_id:\w+>' => 'customer/organize',
                'admin/organize/<project_id:\w+>' => 'customer/organize',
                'customer/developyour' => 'customer/developyour',
                'customer/beinspired/<type:\w+>' => 'customer/beinspired',
                'customer/adminconfirmation' => 'customer/adminconfirmation',
                'customer/pay' => 'customer/pay',
                'lock' => 'site/lock',
                'customer/invoices' => 'customer/invoices',
                'admin/post-project/<type:(create|target)>'=>'admin/create',
                'admin/post-project/<type:(create2|target2)>/<project_id:\w+>'=>'admin/create2',
                'admin/selectstormer/<project_id:\w+>'=>'admin/selectstormer',
                'admin/myprojects/<type:\w+>' => 'admin/myprojects',
                'admin/organizeidea/<project_id:\d+>'=>'customer/organizeidea',
                'stormer/referalpage'=>'stormer/referalpage',
                'stormer/sendreferal'=>'stormer/sendreferal',
                'stormer/exit_project/<id:\d+>'=>'stormer/exit_project',
                'stormer/rankings'=>'stormer/rankings',
                'stormer/referalpage_sales'=>'stormer/referalpage_sales',
                'stormer/stormer_benchmark'=>'stormer/stormer_benchmark',
                'stormer/terms_conditions'=>'stormer/terms_conditions',
                'customer/terms_conditions'=>'customer/terms_conditions',
                'stormer/past_project'=>'stormer/past_project',
                'stormer/project_completed/<id:\d+>'=>'stormer/project_completed',
                'stormer/create-peer/<id:\d+>'=>'stormer/create_peer',
                'notification' => 'site/notification',
                //

                'stormer/about'=>'stormer/stormerabout',
                'stormer/wholepicture/save'=>'stormer/savewhole',
                'stormer/wholepicture/<project_id:\w+>'=>'stormer/wholepicture',

                'stormer/divergent/save'=>'stormer/savedivergent',
                'stormer/divergent/<project_id:\w+>'=>'stormer/divergent',

                'stormer/convergent/save'=>'stormer/saveconvergent',
                'stormer/point_count'=>'stormer/point_count',
                'stormer/convergent/<project_id:\w+>'=>'stormer/convergent',

                'stormer/confidentialityagreement'=>'stormer/confidentialityagreement',
                'stormer/projectintroduction'=>'stormer/projectintroduction',
                'stormer/projectintroduction_start'=>'stormer/projectintroduction_start',
                'stormer/projectconfirmed'=>'stormer/projectconfirmed',
                'stormer/getpuzzle'=>'stormer/getpuzzle',
                'customer/not_corect_timeline'=>'customer/not_corect_timeline',
                'customer/flaggingideasajax'=>'customer/flaggingideasajax',
                'customer/userratingajax'=>'customer/userratingajax',
                'customer/organizeidea/<project_id:\d+>'=>'customer/organizeidea',
                'customer/developfurther/<project_id:\d+>'=>'customer/developfurther',
                'customer/behindthescenes/<project_id:\d+>'=>'customer/behindthescenes',
                'customer/news' => 'customer/news',
                'customer/myprojects/<type:\w+>' => 'customer/myprojects',

                'customer/paymentproject/<project_id:\w+>/<status>' => 'customer/paymentproject',
                'customer/paymentproject/<project_id:\w+>' => 'customer/paymentproject',

                'customer/getquestiontext'=>'customer/getquestiontext',
                'customer/getperspectivequestion'=>'customer/getperspectivequestion',
                'customer/news/<id:\w+>' => 'customer/news',
                'customer/getstormer'=>'customer/getstormer',
                'customer/getcity'=>'customer/getcity',
                'customer/about' => 'customer/customerabout',
                'customer/sendreferal'=>'customer/sendreferal',
                'customer/supportpage'=>'customer/supportpage',
                'customer/telltheworld'=>'customer/telltheworld',
                'stormer/telltheworld'=>'stormer/telltheworld',
                'customer/referalpage'=>'customer/referalpage',
                'customer/selectstormer/<project_id:\w+>'=>'customer/selectstormer',
                'customer/<username:\w+>'=>'customer/index',
                'customer/profile/<username:\w+>'=>'customer/profile',
                'stormer/behindthescenes/<project_id:\d+>'=>'stormer/behindthescenes',
                'stormer/supportpage'=>'stormer/supportpage',
                'stormer/joinsalesteam'=>'stormer/joinsalesteam',
                'stormer/joinsalesteam2'=>'stormer/joinsalesteam2',
                'stormer/earnings'=>'stormer/earnings',
                'stormer/profile/<username:\w+>'=>'stormer/profile',
                'stormer/news' => 'stormer/news',
                'stormer/myprojects' => 'stormer/myprojects',
                'stormer/adminconfirmation' => 'stormer/adminconfirmation',
                'stormer/postproject' => 'stormer/postproject',
                'stormer/selectstormer/<project_id:\w+>' => 'stormer/selectstormer',
                'stormer/news/<id:\w+>' => 'stormer/news',
                'stormer/<username:\w+>'=>'stormer/index',
                'blog/<blog_id:\w+>'=>'site/blog',
                'blogs'=>'site/blogs',
                'site/savedprofilephoto' => 'site/savedprofilephoto',
                'site/deleteprofilephoto' => 'site/deleteprofilephoto',
                'forAcademics' => 'site/academics',
                'OurMethodology' => 'site/ourmethodology',
                'forNgosProfits' => 'site/ngosprofits',
                'imgsavefile/<src:\w+>'=>'site/imgsavefile',
                'about' => 'site/about',
                'privacy' => 'site/privacy',
                'terms' => 'site/terms',
                'faq' => 'site/faq',
                'contact' => 'site/contact',
                'email_quote' => 'site/emailquote',
                'login'=>'site/login',
                'admin' => 'admin/index',
                'admin/faqs' => 'admin/faqs',
                'admin/pages' => 'admin/pages',
                'admin/flaggingideas' => 'admin/flaggingideas',
                'admin/project/<id:\w+>' => 'admin/project',
                'admin/faqsdelete/<faqs_id:\w+>'=>'admin/faqsdelete',
                'admin/onenews/<id:\w+>'=>'admin/onenews',
                'admin/deleteflag'=>'admin/deleteflag',
            ],
        ],

    ],
    'modules' => [
        'gridview' =>  [
            'class' => '\kartik\grid\Module',
            // enter optional module parameters below - only if you need to
            // use your own export download action or custom translation
            // message source
            'downloadAction' => 'gridview/export/download',
            'i18n' => [
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => '@kvgrid/messages',
                'forceTranslation' => true
            ]
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
