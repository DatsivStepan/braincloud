
function initJSmind(mind) {
    var options = {
        container: 'jsm_container',
        theme: 'greensea',
        editable: true,
        mode: 'full',
        clickable: true,
        layout: {
            hspace: 150,

        },
    }
    return jsM = jsMind.show(options, mind);

}
$(document).ready(function(){


    console.log(mindM);
    console.log((mindM));
    var jsM = initJSmind(mindM)
    jsM._event_bind = function() {

    }


    $(document).on('click', '#flag_idea', function() {
        console.log(jsM.get_selected_node());
        var items = jsM.get_selected_node();
        if(items){
            if (items.data.value == 'idea'){
                $('#idea'+items.id).modal('show');
            }else {
                swal("", "Select idea!", "warning");
            }
        }else{
            swal("", "Select idea!", "warning");
        }


    });

    $(document).on('click', '#star_stormer', function() {
        console.log(jsM.get_selected_node());
        var items = jsM.get_selected_node();
        if(items){
            if (items.data.value == 'idea'){
                $.ajax({
                    type: 'POST',
                    url: '../../customer/userratingajax',
                    data: {idea_id:items.id},
                    dataType: "json",
                    success: function(response){
                        console.log(response);
                        if (response){
                            swal({
                                title:'',
                                text:'We have received your alert. An admin will examine the situation at the earliest possible time. You will receive a message or a notification informing you of the action taken. Thank you, and have a nice day!',
                                type:'success'
                            });
                            location.reload()
                        }else {
                            swal({
                                title:'You can not give this idea a star!',
                                type:'warning'
                            });
                        }



                    }
                });
            }else {
                swal("", "Select idea!", "warning");
            }
        }else{
            swal("", "Select idea!", "warning");
        }


    });

    $(document).on('click', 'jmnode', function() {
        console.log(jsM.get_selected_node().id);
        var items = jsM.get_selected_node();

        //alert(items.data.value)
        if (items.id == 'root'){
            $('#root').modal('show');
        }
        //if (items.data.value == 'stormer'){
        //    $('#stormer'+items.id).modal('show');
        //}

    });


    if (one_show_ms){
        if($('#scenarios_user_type').data('type') == 'stormer'){
            swal({
                title:'',
                html:true,
                text: '<b style="text-align:left;">Here you can monitor the current status of your project.</b>'+'<br />'
                +'The flag system: The flag system is designed to prevent abuse of our system. Flag any idea you feel is gratuitous or offensive. Please bear in mind, some of the innovation instructions challenge stormers to be radical during the divergent stage, so it may be worth waiting until convergent stage before flagging, as a radical idea may be explained during later stages.'+
                '<br />'+'<br />'+'When you flag an idea, a BrainCloud admin will explore the scenario and take an appropriate course of action to help.'+
                '<br />'+'<br />'+'Warning: flagging an idea takes man power and time to sort out. Abusing the system could get your account suspended.',
                confirmButtonText: "Understood!",
            });
        }else{
            swal({
                title:'',
                html:true,
                text: '<b style="text-align:left;">Here you can monitor the current status of your project.</b>'+'<br />'
                +'The flag system: The flag system is designed to prevent abuse of our system. Flag any idea you feel is gratuitous or offensive. Please bear in mind, some of the innovation instructions challenge stormers to be radical during the divergent stage, so it may be worth waiting until convergent stage before flagging, as a radical idea may be explained during later stages.'+
                '<br />'+'<br />'+'When you flag an idea, a BrainCloud admin will explore the scenario and take an appropriate course of action to help.'+
                '<br />'+'<br />'+'Warning: Here, you can also give up to 3 stars for every ten stormers if you really like their ideas! It’s not compulsory, but is very much appreciated, as it helps us evaluate their performance!',
                confirmButtonText: "Understood!",
            });
        }
    }

    
    $('[class^=rating_stormer]').rating({ min: 0, max: 5, step: 1, size: 'xs', showClear: true });
    $('[class^=rating_stormer]').on('rating.change', function(event, value, caption) {
        var count_star = $(this).val();
        var project_id = $(this).data('project_id');
        var customer_id = $(this).data('customer_id');
        var stormer_id = $(this).data('stormer_id');
        
        $.ajax({
            type: 'POST',
            url: '../../customer/userratingajax',
            data: {count_star:count_star,project_id:project_id,customer_id:customer_id,stormer_id:stormer_id},
            dataType: "json",
            success: function(response){
                console.log(response);
                swal({   
                    title: '',
                    text:'We have received your alert. An admin will examine the situation at the earliest possible time. You will receive a message or a notification informing you of the action taken. Thank you, and have a nice day!',
                    type:'success'
                });
            }
        });
    });
    
    $('[class^=rating_stormer]').on('rating.clear', function(event) {
        alert($(this).val());
    })
    
    $(document).on('click','.ideaFlag',function(){
        //$('#ideaFlag').data('idea_id');
        $('#blockIdeaFlag'+$(this).data('idea_id')).modal('show');
        
//      $(this).attr("disabled", true);
//        $('#blockIdeaFlag'+$(this).data('idea_id')).modal('hide');
    });
    
    $(document).on('click','.flagIcon',function(){
        $('.flagBlock'+$(this).data('idea_id')).toggle();
    });
    
    $(document).on('click','.SendFlag',function(){
        var thisE = $(this);
        var idea_id = $(this).data('idea_id')
        var checkedInput = $('.flagBlock'+idea_id).find("input[name=reason_for_flagging]:checked" );
        if(checkedInput.length > 0){
            var why_flagging = '';
            if(checkedInput.val() == '3'){
                why_flagging = $('.flagBlock'+idea_id).find('input[name=reason_for_flagging_other]').val();
            }else{
                why_flagging = checkedInput.val();
            }
            
            if(why_flagging != ''){
                $.ajax({
                    type: 'POST',
                    url: '../../customer/flaggingideasajax',
                    data: {idea_id:idea_id,why_flagging:why_flagging},
                    dataType: "json",
                    success: function(response){
                        $('.flagBlock'+idea_id).html('');
                        $('.flagBlock'+idea_id).html('Ideas flagging');
                        swal({   
                            title: '',
                            text:'We have received your alert. An admin will examine the situation at the earliest possible time. You will receive a message or a notification informing you of the action taken. Thank you, and have a nice day!',
                            type:'success',
                            html: true
                        });
                    }
                });
                swal({   
                    title:'',
                    text:'We have received your alert. An admin will examine the situation at the earliest possible time. You will receive a message or a notification informing you of the action taken. Thank you, and have a nice day!',
                    type:'success',
                    html: true
                });
            }else{
                swal({   
                    title:'Enter data!',
                    type:'error'
                });
            }
            
        }else{
            swal({   
                title:'Pleace checked!',
                type:'error'
            });
        }
    });



});