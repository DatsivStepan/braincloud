
$(document).ready(function(){
    var project_id = null;
    if ($('#select_st_project_id')){
        project_id=$('#select_st_project_id').val();
    }
    
    $(document).on('change','#filterStormerCountry',function(){
        var country_id = $(this).val();
        
        if(country_id != 0){
            $.ajax({
                type: 'POST',
                url: '../../customer/getcity',
                data: {country_id:country_id},
                dataType: "json",
                success: function(response){
                    if(response.status == 'good'){
                        $('#filterStormerCity').html('');
                        $('#filterStormerCity').append('<option value="0">Select City</option>');
                        for(var key in response.city){
                            $('#filterStormerCity').append('<option value='+key+'>'+response.city[key]+'</option>');
                        }
                    }
                }
            });
        }else{
            $('#filterStormerCity').html('');
            $('#filterStormerCity').append('<option value="0">First select a country</option>');
        }
        
    });
    
    var yourStormerSelectId = [];
        $('#changeViewCount, #filterStormerCountry, #filterStormerCity, #filterStormerUsername, #filterStormerCategory').change(function(e){
            filterStormerSelect();
        });
        $('#filterStormer').on('submit', function(e){
            e.preventDefault();
        });

        $(document).on('change','input[name=random_my_team]',function(){
            var countGet = $(this).val();
            var size_team = $('input[name=size_team_c]').val();
            $.ajax({
                type: 'POST',
                url: '../../customer/getstormer',
                data: {type:'getStormers',countGet:countGet,size_team:size_team,project_id:project_id},
                dataType: "json",
                success: function(response){
                    console.log(response);
                    $('#boxWithYourStormers').html('');
                    $('input[name=stormerCheckboxSelect]').prop('checked',false);
                    yourStormerSelectId = [];
                    for(var key in response){
                        addToYouHave(response[key]);
                        yourStormerSelectId.push(response[key]);
                        $('.stormerCheckbox'+response[key]).prop('checked',true);
                        //console.log(response[key]);
                    }
                    $('input[name=FormSelectStormer]').val(JSON.stringify(yourStormerSelectId));
                    $('#youHaveStormers').text(selecte_stormers-yourStormerSelectId.length);
                }
            });
        });

        function filterStormerSelect(){
            var filterParameter = $('#filterStormer').serializeArray();

                $.ajax({
                    type: 'POST',
                    url: '../../customer/getstormer',
                    data: {filterParameter:filterParameter,type:'filter',project_id:project_id},
                    dataType: "json",
                    success: function(response){
                        $('#boxWithAllStormers').html('');
                        for(var key in response){
                            var stormerO = response[key];

                            var categoryA = JSON.parse(stormerO.category);
                            var categoryS = categoryA.toString();

                            var tagsA = JSON.parse(stormerO.tags);
                            var tagsS = tagsA.toString();

                            var ChackedChackbox = '';
                            if(jQuery.inArray(parseInt(stormerO.stormer_id), yourStormerSelectId) == -1){
                                ChackedChackbox = ' ';
                                console.log('no');
                            }else{
                                ChackedChackbox = ' checked ';
                                console.log('yes');
                            }

                            var facebook ='';
                            if(stormerO.facebook_link != ''){
                                facebook = '<a href="'+stormerO.facebook_link+'" class="pull-right social_icon_style">\n\
                                    <div class="social_face_icon" style="height:20px;width: 20px"></div>\n\
                                </a>';
                            }

                            var twitter ='';
                            if(stormerO.twitter_link != ''){
                                twitter = '<a href="'+stormerO.twitter_link+'" class="pull-right social_icon_style">\n\
                                    <div class="social_twit_icon" style="height:20px;width: 20px"></div>\n\
                                </a>';
                            }

                            var linkedin ='';
                            if(stormerO.linkedin_link != ''){
                                linkedin = '<a href="'+stormerO.linkedin_link+'" class="pull-right social_icon_style">\n\
                                    <div class="social_in_icon" style="height:20px;width: 20px"></div>\n\
                                </a>';
                            }
                            var templateStormer = '<div class="stormer'+stormerO.stormer_id+' stormer-item"  id="allStormer'+stormerO.stormer_id+'">\n\
                                                        <div class="col-sm-2">\n\
                                                            <img class="stormer-avatar img-responsive" src="'+stormerO.image_src+'">\n\
                                                        </div>\n\
                                                        <div class="col-sm-10">\n\
                                                            <div class="row">\n\
                                                                <div class="col-xs-10">\n\
                                                                    <h2 class="stormer-name">'+stormerO.username+'</h2>\n\
                                                                </div>\n\
                                                                <div class="col-xs-2">\n\
                                                                    <div class="row">\n\
                                                                        <input type="checkbox" name="stormerCheckboxSelect" data-id="'+stormerO.stormer_id+'" '+ChackedChackbox+' class="form-control stormerCheckbox'+stormerO.stormer_id+' pull-right">\n\
                                                                    </div>\n\
                                                                </div>\n\
                                                                <div class="col-sm-12 stormerUserInfo" style="display:none;">\n\
                                                                    <table class="table">\n\
                                                                        <tr>\n\
                                                                            <td>Email</td>\n\
                                                                            <td>'+stormerO.email+'</td>\n\
                                                                        </tr>\n\
                                                                        <tr>\n\
                                                                            <td>City</td>\n\
                                                                            <td>'+stormerO.sity_name+'</td>\n\
                                                                            <td>Gender</td>\n\
                                                                            <td>'+stormerO.country_name+'</td>\n\
                                                                        </tr>\n\
                                                                        <tr>\n\
                                                                            <td>Category</td>\n\
                                                                            <td>'+categoryS+'</td>\n\
                                                                            <td>Tags</td>\n\
                                                                            <td>'+tagsS+'</td>\n\
                                                                        </tr>\n\
                                                                        <tr>\n\
                                                                            <td>Introduction</td>\n\
                                                                            <td colspan="3">'+stormerO.introduction+'</td>\n\
                                                                        </tr>\n\
                                                                        <tr>\n\
                                                                            <td>Star</td>\n\
                                                                            <td colspan="3">'+stormerO.global_stars+'</td>\n\
                                                                        </tr>\n\
                                                                    </table>\n\
                                                                    <div class="row">\n\
                                                                        <div class="col-sm-12 text-right">\n\
                                                                            <div class="row">\n\
                                                                                '+facebook+'\n\
                                                                                '+twitter+'\n\
                                                                                '+linkedin+'\n\
                                                                            </div>\n\
                                                                        </div>\n\
                                                                    </div>\n\
                                                                </div>\n\
                                                                <div class="col-sm-12">\n\
                                                                    <a class="btn btn-show-info stormerUsername pull-right">Show more info</a>\n\
                                                                </div>\n\
                                                            </div>\n\
                                                        </div>\n\
                                                        <div class="clearfix"></div>\n\
                                                    </div>';
                            $('#boxWithAllStormers').append(templateStormer);
                        }
                    }
                });
        };

        $(document).on('change','input[name=yourStormerCheckboxSelect]',function(){
            if($(this).prop('checked') == false){
                $('#yourStormerBlock'+$(this).data('id')).remove();
                $('#allStormer'+$(this).data('id')).find('input[name=stormerCheckboxSelect]').prop('checked',false);
                var thisDataId = $(this).data('id');
                yourStormerSelectId = jQuery.grep(yourStormerSelectId, function(value) {
                    return value != thisDataId;
                });
                $('#youHaveStormers').text(selecte_stormers-yourStormerSelectId.length);
                $('input[name=FormSelectStormer]').val(JSON.stringify(yourStormerSelectId));
            }
        });

        $(document).on('change','input[name=stormerCheckboxSelect]',function(){
            var thisChe = $(this);
            if($(this).prop('checked') == true){
                if(yourStormerSelectId.length >= parseInt($('#slider_team_sizeV').text())){
                    $('#allStormer'+$(this).data('id')).find('input[name=stormerCheckboxSelect]').prop('checked',false);
                    swal("Limit is exceeded!");
                    swal({
                        title: "Limit is exceeded!",
                        type: "warning",
                    },
                    function(){
                        $(document).scrollTop(0);
                    });
                }else{
                    addToYouHave($(this).data('id'));
                    yourStormerSelectId.push($(this).data('id'));
                    $('#youHaveStormers').text(selecte_stormers-yourStormerSelectId.length);
                    $('input[name=FormSelectStormer]').val(JSON.stringify(yourStormerSelectId));
                }
            }else{
                deleteYouHave($(this).data('id'));
                var thisDataId = $(this).data('id');
                yourStormerSelectId = jQuery.grep(yourStormerSelectId, function(value) {
                    return value != thisDataId;
                });
                $('#youHaveStormers').text(selecte_stormers-yourStormerSelectId.length);
                $('input[name=FormSelectStormer]').val(JSON.stringify(yourStormerSelectId));
            }
        });

        function addToYouHave(stormerId){
            //alert(stormerId);
            $.ajax({
                type: 'POST',
                url: '../../customer/getstormer',
                data: {type:'getOneStormers',stormerId:stormerId,project_id:project_id},
                dataType: "json",
                success: function(response){
                        //console.log(response);
                        var stormerO = response;
                        var categoryA = JSON.parse(stormerO.category);
                        var categoryS = categoryA.toString();

                        var tagsA = JSON.parse(stormerO.tags);
                        var tagsS = tagsA.toString();

                        var facebook ='';
                        if(stormerO.facebook_link != ''){
                            facebook = '<a href="'+stormerO.facebook_link+'" class="pull-right social_icon_style">\n\
                                <div class="social_face_icon" style="height:35px;width: 35px"></div>\n\
                            </a>';
                        }

                        var twitter ='';
                        if(stormerO.twitter_link != ''){
                            twitter = '<a href="'+stormerO.twitter_link+'" class="pull-right social_icon_style">\n\
                                <div class="social_twit_icon" style="height:35px;width: 35px"></div>\n\
                            </a>';
                        }

                        var linkedin ='';
                        if(stormerO.linkedin_link != ''){
                            linkedin = '<a href="'+stormerO.linkedin_link+'" class="pull-right social_icon_style">\n\
                                <div class="social_in_icon" style="height:35px;width: 35px"></div>\n\
                            </a>';
                        }

                        var templateStormer   = '<div class="yourStormerBlock" id="yourStormerBlock'+stormerO.stormer_id+'">\n\
                                                    <div class="row">\n\
                                                        <div class="col-sm-12">\n\
                                                            <div class="col-sm-10 no-padding">\n\
                                                                <h2 class="stormerUsernameClick">'+stormerO.username+'</h2>\n\
                                                            </div>\n\
                                                            <div class="col-sm-2">\n\
                                                                <input type="checkbox" checked class="form-control pull-right" name="yourStormerCheckboxSelect" data-id="'+stormerO.stormer_id+'">\n\
                                                            </div>\n\
                                                        </div>\n\
                                                        <div class="col-sm-12 displayStormerInfo"  style="display:none;">\n\
                                                            <table class="table">\n\
                                                                <tr>\n\
                                                                    <td>Email</td>\n\
                                                                    <td>'+stormerO.email+'</td>\n\
                                                                <tr>\n\
                                                                </tr>\n\
                                                                    <td>Country</td>\n\
                                                                    <td>'+stormerO.gender+'</td>\n\
                                                                </tr>\n\
                                                                <tr>\n\
                                                                    <td>City</td>\n\
                                                                    <td>'+stormerO.city+'</td>\n\
                                                                <tr>\n\
                                                                </tr>\n\
                                                                    <td>Gender</td>\n\
                                                                    <td>'+stormerO.country+'</td>\n\
                                                                </tr>\n\
                                                                <tr>\n\
                                                                    <td>Category</td>\n\
                                                                    <td>'+categoryS+'</td>\n\
                                                                <tr>\n\
                                                                </tr>\n\
                                                                    <td>Tags</td>\n\
                                                                    <td>'+tagsS+'</td>\n\
                                                                </tr>\n\
                                                                <tr>\n\
                                                                    <td>Introduction</td>\n\
                                                                    <td colspan="3">'+stormerO.introduction+'</td>\n\
                                                                </tr>\n\
                                                                <tr>\n\
                                                                    <td>Statr</td>\n\
                                                                    <td colspan="3">'+stormerO.global_stars+'</td>\n\
                                                                </tr>\n\
                                                            </table>\n\
                                                            <div class="col-sm-12">\n\
                                                                '+facebook+'\n\
                                                                '+twitter+'\n\
                                                                '+linkedin+'\n\
                                                            </div>\n\
                                                        </div>\n\
                                                    </div>\n\
                                                    <hr>\n\
                                                </div>';
                    $('#boxWithYourStormers').append(templateStormer);
                }
            });
        }

        function deleteYouHave(stormerId){
            $('#yourStormerBlock'+stormerId).remove();
        }

        $(document).on('click','.stormerUsernameClick',function(){
            $(this).parent().parent().next().toggle();
        });

    function PriceSelectStormerpage(){
        if(parseInt($( "#slider_scope_project_selectStormerpage .ui-slider-handle" ).text()) != 0){
            var count_scope_project = parseInt($( "#slider_team_size_selectStormerpage .ui-slider-handle" ).text()) * parseInt($( "#slider_scope_project_selectStormerpage .ui-slider-handle" ).text()) * 5;
        }else{
            var count_scope_project = 0;
        }

        if(parseInt($( "#slider_team_size_selectStormerpage .ui-slider-handle" ).text()) != 0){
            var count_team_size = parseInt($( "#slider_team_size_selectStormerpage .ui-slider-handle" ).text()) * 6;
        }else{
            var count_team_size = 0;
        }

        if (typeof discount != 'undefined'){


            var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
            var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount*one_procent);
            $('.totalPrice').text( price.toFixed(2));

            if (typeof level != 'undefined'){

                if (level == 1){
                   var begin_discount = discount-5;
                    if ((parseInt(count_scope_project) + parseInt(count_team_size))<2000){
                        var discount_l=begin_discount+5;
                        var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
                        var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
                        $('.totalPrice').text( price.toFixed(2));
                        $('#my_discount').text('Discount ('+discount_l+'%)');
                        $('#input_discount').val(discount_l);
                    }
                    if (((parseInt(count_scope_project) + parseInt(count_team_size))>2000) && ((parseInt(count_scope_project) + parseInt(count_team_size))<5000) ){
                        var discount_l=begin_discount+10;
                        var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
                        var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
                        $('.totalPrice').text( price.toFixed(2));
                        $('#my_discount').text('Discount ('+discount_l+'%)');
                        $('#input_discount').val(discount_l);
                    }
                    if ((parseInt(count_scope_project) + parseInt(count_team_size))>5000){
                        var discount_l=begin_discount+15;
                        var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
                        var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
                        $('.totalPrice').text( price.toFixed(2));
                        $('#my_discount').text('Discount ('+discount_l+'%)');
                        $('#input_discount').val(discount_l);
                    }
                }
                if (level == 2){
                    var begin_discount = discount-7;
                    if ((parseInt(count_scope_project) + parseInt(count_team_size))<2000){
                        var discount_l=begin_discount+7;
                        var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
                        var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
                        $('.totalPrice').text( price.toFixed(2));
                        $('#my_discount').text('Discount ('+discount_l+'%)');
                        $('#input_discount').val(discount_l);
                    }
                    if (((parseInt(count_scope_project) + parseInt(count_team_size))>2000) && ((parseInt(count_scope_project) + parseInt(count_team_size))<5000) ){
                        var discount_l=begin_discount+15;
                        var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
                        var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
                        $('.totalPrice').text( price.toFixed(2));
                        $('#my_discount').text('Discount ('+discount_l+'%)');
                        $('#input_discount').val(discount_l);
                    }
                    if ((parseInt(count_scope_project) + parseInt(count_team_size))>5000){
                        var discount_l=begin_discount+20;
                        var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
                        var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
                        $('.totalPrice').text( price.toFixed(2));
                        $('#my_discount').text('Discount ('+discount_l+'%)');
                        $('#input_discount').val(discount_l);
                    }
                }
                if (level == 3){
                    var begin_discount = discount-10;
                    if ((parseInt(count_scope_project) + parseInt(count_team_size))<2000){
                        var discount_l=begin_discount+10;
                        var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
                        var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
                        $('.totalPrice').text( price.toFixed(2));
                        $('#my_discount').text('Discount ('+discount_l+'%)');
                        $('#input_discount').val(discount_l);
                    }
                    if (((parseInt(count_scope_project) + parseInt(count_team_size))>2000) && ((parseInt(count_scope_project) + parseInt(count_team_size))<5000) ){
                        var discount_l=begin_discount+20;
                        var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
                        var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
                        $('.totalPrice').text( price.toFixed(2));
                        $('#my_discount').text('Discount ('+discount_l+'%)');
                        $('#input_discount').val(discount_l);
                    }
                    if ((parseInt(count_scope_project) + parseInt(count_team_size))>5000){
                        var discount_l=begin_discount+25;
                        var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
                        var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
                        $('.totalPrice').text( price.toFixed(2));
                        $('#my_discount').text('Discount ('+discount_l+'%)');
                        $('#input_discount').val(discount_l);
                    }
                }

                if (level == 4){
                    var begin_discount = discount-15;
                    if ((parseInt(count_scope_project) + parseInt(count_team_size))<2000){
                        var discount_l=begin_discount+15;
                        var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
                        var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
                        $('.totalPrice').text( price.toFixed(2));
                        $('#my_discount').text('Discount ('+discount_l+'%)');
                        $('#input_discount').val(discount_l);
                    }
                    if (((parseInt(count_scope_project) + parseInt(count_team_size))>2000) && ((parseInt(count_scope_project) + parseInt(count_team_size))<5000) ){
                        var discount_l=begin_discount+25;
                        var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
                        var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
                        $('.totalPrice').text( price.toFixed(2));
                        $('#my_discount').text('Discount ('+discount_l+'%)');
                        $('#input_discount').val(discount_l);
                    }
                    if ((parseInt(count_scope_project) + parseInt(count_team_size))>5000){
                        var discount_l=begin_discount+30;
                        var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
                        var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
                        $('.totalPrice').text( price.toFixed(2));
                        $('#my_discount').text('Discount ('+discount_l+'%)');
                        $('#input_discount').val(discount_l);
                    }
                }

            }
        }

        $('.sliderPrice').text(parseInt(count_scope_project) + parseInt(count_team_size));

         $('input[name=slider_scope_project]').val($( "#slider_scope_project_selectStormerpage .ui-slider-handle" ).text());
         $('input[name=slider_team_size]').val($( "#slider_team_size_selectStormerpage .ui-slider-handle" ).text());
    }

    $( "#slider_scope_project_selectStormerpage" ).slider({
        min: 5,
        max: 30,
        step: 5,
        value: 5,
        animate:true,
        slide: function(event, ui) {
            $( "#slider_scope_project_selectStormerpage .ui-slider-handle" ).text(ui.value);
            PriceSelectStormerpage();
        }
    });


    $( "#slider_team_size_selectStormerpage" ).slider({
        min: 5,
        max: 50,
        step: 5,
        value: 5,
        animate:true,
        slide: function(event, ui) {
            $( "#slider_team_size_selectStormerpage .ui-slider-handle" ).text(ui.value);
            PriceSelectStormerpage();
        }
    });



    $( ".ui-slider-handle" ).text('5');

    setTimeout(function(){
        $( "#slider_scope_project_selectStormerpage" ).slider( "value", parseInt($('#slider_scope_projectV').text()));
        $( "#slider_scope_project_selectStormerpage .ui-slider-handle" ).text( parseInt($('#slider_scope_projectV').text()));

        $( "#slider_team_size_selectStormerpage" ).slider( "value",  parseInt($('#slider_team_sizeV').text()));
        $( "#slider_team_size_selectStormerpage .ui-slider-handle" ).text( parseInt($('#slider_team_sizeV').text()));
        PriceSelectStormerpage();
    }, 0);


    $('[name=saveSelectStormer]').click(function(e){
        if(($('input[name=FormSelectStormer]').val() == '') || ( $('input[name=FormSelectStormer]').val() == '[]')){
            e.preventDefault();
            swal({
                title: "You did not select all stormerssssssss!",
                type: "warning",
            });
        }
        if(yourStormerSelectId.length < parseInt($('#slider_team_sizeV').text())){
            e.preventDefault();
            swal({
                title: "You did not select all stormers!",
                type: "warning",
            });
        }
    });

    $('[name=confirmSliderSelect]').click(function(e){
        if(($('input[name=slider_scope_project]').val() == '0') || ( $('input[name=slider_team_size]').val() == '0')){
            e.preventDefault();
        }
    });

    $('.showScole').click(function(){
        $(this).parent().parent().find('.panel-body').toggle();
        if($(this).hasClass("fa-arrow-down")){
            $(this).removeClass('fa-arrow-down');
            $(this).addClass('fa-arrow-up');
        }else{
            $(this).removeClass('fa-arrow-up');
            $(this).addClass('fa-arrow-down');
        }
    });
    setTimeout(function(){
        $('.multiselectClass').find('.multiselect').attr('title','');
        $('.multiselectClass').find('.btn-group').css('width','100%');
        $('.multiselectClass').find('.multiselect').css('width','100%');
    }, 1000);
    
    $(document).on('click','.stormerUsername',function(){
        if($(this).text() == 'Show more info'){
            $(this).text('Hide more info');
            $(this).parent().parent().find('.stormerUserInfo').show();
        }else{
            $(this).text('Show more info');
            $(this).parent().parent().find('.stormerUserInfo').hide();
        }
    });
});
