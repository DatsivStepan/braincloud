function imageUpload(){

var croppicContaineroutputOptions = {
	uploadUrl:'../../../../imgsavefile/img_save_to_file',
	cropUrl:'../../../../imgsavefile/img_crop_to_file',
	//cropUrl:'../../../../../js/img_crop_to_file.php',
	outputUrlId:'customerinfo-image_name',
	modal:false,
	loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',
	onBeforeImgUpload: function(){ console.log('onBeforeImgUpload') },
	onAfterImgUpload: function(){ console.log('onAfterImgUpload') },
	onImgDrag: function(){ console.log('onImgDrag') },
	onImgZoom: function(){ console.log('onImgZoom') },
	onBeforeImgCrop: function(){ console.log('onBeforeImgCrop') },
	onAfterImgCrop:function(){ console.log('onAfterImgCrop') },
	onReset:function(){ console.log('onReset') },
	onError:function(errormessage){ console.log('onError:'+errormessage) }
}

var cropContaineroutput = new Croppic('cropContaineroutput', croppicContaineroutputOptions);

}

function imageUploadFotoProfile(){

var croppicContaineroutputOptions = {
	uploadUrl:'../../../../imgsavefile/img_save_to_file',
	cropUrl:'../../../../imgsavefile/img_crop_to_file',
	//cropUrl:'../../../../../js/img_crop_to_file.php',
	outputUrlId:'stormerinfo-image_src',
	modal:false,
	loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> ',
	onBeforeImgUpload: function(){ console.log('onBeforeImgUpload') },
	onAfterImgUpload: function(){ console.log('onAfterImgUpload') },
	onImgDrag: function(){ console.log('onImgDrag') },
	onImgZoom: function(){ console.log('onImgZoom') },
	onBeforeImgCrop: function(){ console.log('onBeforeImgCrop') },
	onAfterImgCrop:function(){ console.log('onAfterImgCrop') },
	onReset:function(){ console.log('onReset') },
	onError:function(errormessage){ console.log('onError:'+errormessage) }
}

var cropContaineroutput = new Croppic('cropContaineroutputFoto', croppicContaineroutputOptions);

}
$(document).ready(function(){
	
	$(document).on('click','.panelHead',function(){
		$(this).parent().find('.panelBody').toggle();
	});
	
	$('.ratingThree').rating({
		  min: 0,
		  max: 10,
		  step: 1,
		  size: 'sm',
		  stars: 10,
		  showClear: false
	   });
	$('.ratingOne').rating({
		  min: 0,
		  max: 5,
		  step: 1,
		  size: 'sm',
		  showClear: false
	   });
	$('.ratingTwo').rating({
		  min: 0,
		  max: 5,
		  step: 1,
		  size: 'sm',
		  showClear: false
	   });

	$('.ratingOne').on('rating.change', function() {
		$(this).parent().parent().find('#feedback-ranting_1').val($(this).val())
	});
	
	$('.ratingTwo').on('rating.change', function() {
		$(this).parent().parent().find('#feedback-ranting_2').val($(this).val())
	});
	
	$('.ratingThree').on('rating.change', function() {
		$(this).parent().parent().find('#feedback-ranting_3').val($(this).val())
	});
	
	var clicked = false;
	 $('.formFeedback').on('submit', function(e){
		var ranting_1 = $(this).find('#feedback-ranting_1').val();
		var ranting_2 = $(this).find('#feedback-ranting_2').val();
		var ranting_3 = $(this).find('#feedback-ranting_3').val();
		var my_form = $(this);
		var about_experience = $(this).find('#feedback-about_experience').val();
		if((ranting_1 == '') || (ranting_2 == '') ||(ranting_3 == '')){
			e.preventDefault();
			swal("Select Rating", "", "error");
		}else{
			if(!clicked){
				e.preventDefault();
				swal({   
					title:'Thank you!',
				   text: "Your feedback will be taken seriously, we appreciate the time to let us know what you think of the experience. ",
				   type: "success",
				   showCancelButton: false,
				   confirmButtonColor: "#4594de",
				   confirmButtonText: "Ok",
				}, function(isConfirm){
				   if (isConfirm) {
					   clicked = true;
					   my_form.trigger('submit');
				   } 
				});
			}
		}
	 });
	
	imageUpload();
	imageUploadFotoProfile();
	function Price(){
			if(parseInt($( "#slider_team_size .ui-slider-handle" ).text()) != 0){
			  var count_team_size = parseInt($( "#slider_team_size .ui-slider-handle" ).text()) * 6;
			}else{
			  var count_team_size = 0;
			}

			if(parseInt($( "#slider_scope_project .ui-slider-handle" ).text()) != 0){
				var count_scope_project = parseInt($( "#slider_scope_project .ui-slider-handle" ).text()) * 5 * parseInt($( "#slider_team_size .ui-slider-handle" ).text());
			}else{
				var count_scope_project = 0;
			}
		if (typeof discount != 'undefined'){


			var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
			var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount*one_procent);
			$('.totalPrice').text( price.toFixed(2));

			if (typeof level != 'undefined'){

				if (level == 1){
					var begin_discount = discount-5;
					if ((parseInt(count_scope_project) + parseInt(count_team_size))<2000){
						var discount_l=begin_discount+5;
						var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
						var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
						$('.totalPrice').text( price.toFixed(2));
						$('#my_discount').text('Discount ('+discount_l+'%)');
						$('#input_discount').val(discount_l);
					}
					if (((parseInt(count_scope_project) + parseInt(count_team_size))>2000) && ((parseInt(count_scope_project) + parseInt(count_team_size))<5000) ){
						var discount_l=begin_discount+10;
						var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
						var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
						$('.totalPrice').text( price.toFixed(2));
						$('#my_discount').text('Discount ('+discount_l+'%)');
						$('#input_discount').val(discount_l);
					}
					if ((parseInt(count_scope_project) + parseInt(count_team_size))>5000){
						var discount_l=begin_discount+15;
						var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
						var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
						$('.totalPrice').text( price.toFixed(2));
						$('#my_discount').text('Discount ('+discount_l+'%)');
						$('#input_discount').val(discount_l);
					}
				}
				if (level == 2){
					var begin_discount = discount-7;
					if ((parseInt(count_scope_project) + parseInt(count_team_size))<2000){
						var discount_l=begin_discount+7;
						var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
						var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
						$('.totalPrice').text( price.toFixed(2));
						$('#my_discount').text('Discount ('+discount_l+'%)');
						$('#input_discount').val(discount_l);
					}
					if (((parseInt(count_scope_project) + parseInt(count_team_size))>2000) && ((parseInt(count_scope_project) + parseInt(count_team_size))<5000) ){
						var discount_l=begin_discount+15;
						var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
						var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
						$('.totalPrice').text( price.toFixed(2));
						$('#my_discount').text('Discount ('+discount_l+'%)');
						$('#input_discount').val(discount_l);
					}
					if ((parseInt(count_scope_project) + parseInt(count_team_size))>5000){
						var discount_l=begin_discount+20;
						var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
						var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
						$('.totalPrice').text( price.toFixed(2));
						$('#my_discount').text('Discount ('+discount_l+'%)');
						$('#input_discount').val(discount_l);
					}
				}
				if (level == 3){
					var begin_discount = discount-10;
					if ((parseInt(count_scope_project) + parseInt(count_team_size))<2000){
						var discount_l=begin_discount+10;
						var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
						var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
						$('.totalPrice').text( price.toFixed(2));
						$('#my_discount').text('Discount ('+discount_l+'%)');
						$('#input_discount').val(discount_l);
					}
					if (((parseInt(count_scope_project) + parseInt(count_team_size))>2000) && ((parseInt(count_scope_project) + parseInt(count_team_size))<5000) ){
						var discount_l=begin_discount+20;
						var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
						var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
						$('.totalPrice').text( price.toFixed(2));
						$('#my_discount').text('Discount ('+discount_l+'%)');
						$('#input_discount').val(discount_l);
					}
					if ((parseInt(count_scope_project) + parseInt(count_team_size))>5000){
						var discount_l=begin_discount+25;
						var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
						var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
						$('.totalPrice').text( price.toFixed(2));
						$('#my_discount').text('Discount ('+discount_l+'%)');
						$('#input_discount').val(discount_l);
					}
				}

				if (level == 4){
					var begin_discount = discount-15;
					if ((parseInt(count_scope_project) + parseInt(count_team_size))<2000){
						var discount_l=begin_discount+15;
						var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
						var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
						$('.totalPrice').text( price.toFixed(2));
						$('#my_discount').text('Discount ('+discount_l+'%)');
						$('#input_discount').val(discount_l);
					}
					if (((parseInt(count_scope_project) + parseInt(count_team_size))>2000) && ((parseInt(count_scope_project) + parseInt(count_team_size))<5000) ){
						var discount_l=begin_discount+25;
						var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
						var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
						$('.totalPrice').text( price.toFixed(2));
						$('#my_discount').text('Discount ('+discount_l+'%)');
						$('#input_discount').val(discount_l);
					}
					if ((parseInt(count_scope_project) + parseInt(count_team_size))>5000){
						var discount_l=begin_discount+30;
						var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
						var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
						$('.totalPrice').text( price.toFixed(2));
						$('#my_discount').text('Discount ('+discount_l+'%)');
						$('#input_discount').val(discount_l);
					}
				}

			}
		}
			$('.sliderPrice').text(parseInt(count_scope_project) + parseInt(count_team_size));

			 $('input[name=scope_project]').val($( "#slider_scope_project .ui-slider-handle" ).text());
			 $('input[name=team_size]').val($( "#slider_team_size .ui-slider-handle" ).text());
			 $('input[name=count_price]').val(parseInt(count_scope_project) + parseInt(count_team_size));

	}
	function PriceNew(){
		if(parseInt($( "#slider_team_size_selectStormerpage .ui-slider-handle" ).text()) != 0){
			var count_team_size = parseInt($( "#slider_team_size_selectStormerpage .ui-slider-handle" ).text()) * 6;
		}else{
			var count_team_size = 0;
		}

		if(parseInt($( "#slider_scope_project_selectStormerpage .ui-slider-handle" ).text()) != 0){
			var count_scope_project = parseInt($( "#slider_scope_project_selectStormerpage .ui-slider-handle" ).text()) * 5 * parseInt($( "#slider_team_size_selectStormerpage .ui-slider-handle" ).text());
		}else{
			var count_scope_project = 0;
		}
		if (typeof discount != 'undefined'){


			var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
			var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount*one_procent);
			$('.totalPrice').text( price.toFixed(2));
			if (typeof level != 'undefined'){

				if (level == 1){
					var begin_discount = discount-5;
					if ((parseInt(count_scope_project) + parseInt(count_team_size))<2000){
						var discount_l=begin_discount+5;
						var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
						var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
						$('.totalPrice').text( price.toFixed(2));
						$('#my_discount').text('Discount ('+discount_l+'%)');
						$('#input_discount').val(discount_l);
					}
					if (((parseInt(count_scope_project) + parseInt(count_team_size))>2000) && ((parseInt(count_scope_project) + parseInt(count_team_size))<5000) ){
						var discount_l=begin_discount+10;
						var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
						var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
						$('.totalPrice').text( price.toFixed(2));
						$('#my_discount').text('Discount ('+discount_l+'%)');
						$('#input_discount').val(discount_l);
					}
					if ((parseInt(count_scope_project) + parseInt(count_team_size))>5000){
						var discount_l=begin_discount+15;
						var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
						var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
						$('.totalPrice').text( price.toFixed(2));
						$('#my_discount').text('Discount ('+discount_l+'%)');
						$('#input_discount').val(discount_l);
					}
				}
				if (level == 2){
					var begin_discount = discount-7;
					if ((parseInt(count_scope_project) + parseInt(count_team_size))<2000){
						var discount_l=begin_discount+7;
						var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
						var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
						$('.totalPrice').text( price.toFixed(2));
						$('#my_discount').text('Discount ('+discount_l+'%)');
						$('#input_discount').val(discount_l);
					}
					if (((parseInt(count_scope_project) + parseInt(count_team_size))>2000) && ((parseInt(count_scope_project) + parseInt(count_team_size))<5000) ){
						var discount_l=begin_discount+15;
						var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
						var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
						$('.totalPrice').text( price.toFixed(2));
						$('#my_discount').text('Discount ('+discount_l+'%)');
						$('#input_discount').val(discount_l);
					}
					if ((parseInt(count_scope_project) + parseInt(count_team_size))>5000){
						var discount_l=begin_discount+20;
						var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
						var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
						$('.totalPrice').text( price.toFixed(2));
						$('#my_discount').text('Discount ('+discount_l+'%)');
						$('#input_discount').val(discount_l);
					}
				}
				if (level == 3){
					var begin_discount = discount-10;
					if ((parseInt(count_scope_project) + parseInt(count_team_size))<2000){
						var discount_l=begin_discount+10;
						var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
						var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
						$('.totalPrice').text( price.toFixed(2));
						$('#my_discount').text('Discount ('+discount_l+'%)');
						$('#input_discount').val(discount_l);
					}
					if (((parseInt(count_scope_project) + parseInt(count_team_size))>2000) && ((parseInt(count_scope_project) + parseInt(count_team_size))<5000) ){
						var discount_l=begin_discount+20;
						var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
						var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
						$('.totalPrice').text( price.toFixed(2));
						$('#my_discount').text('Discount ('+discount_l+'%)');
						$('#input_discount').val(discount_l);
					}
					if ((parseInt(count_scope_project) + parseInt(count_team_size))>5000){
						var discount_l=begin_discount+25;
						var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
						var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
						$('.totalPrice').text( price.toFixed(2));
						$('#my_discount').text('Discount ('+discount_l+'%)');
						$('#input_discount').val(discount_l);
					}
				}

				if (level == 4){
					var begin_discount = discount-15;
					if ((parseInt(count_scope_project) + parseInt(count_team_size))<2000){
						var discount_l=begin_discount+15;
						var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
						var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
						$('.totalPrice').text( price.toFixed(2));
						$('#my_discount').text('Discount ('+discount_l+'%)');
						$('#input_discount').val(discount_l);
					}
					if (((parseInt(count_scope_project) + parseInt(count_team_size))>2000) && ((parseInt(count_scope_project) + parseInt(count_team_size))<5000) ){
						var discount_l=begin_discount+25;
						var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
						var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
						$('.totalPrice').text( price.toFixed(2));
						$('#my_discount').text('Discount ('+discount_l+'%)');
						$('#input_discount').val(discount_l);
					}
					if ((parseInt(count_scope_project) + parseInt(count_team_size))>5000){
						var discount_l=begin_discount+30;
						var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
						var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount_l*one_procent);
						$('.totalPrice').text( price.toFixed(2));
						$('#my_discount').text('Discount ('+discount_l+'%)');
						$('#input_discount').val(discount_l);
					}
				}

			}
		}

		$('.sliderPrice').text(parseInt(count_scope_project) + parseInt(count_team_size));

		$('input[name=slider_scope_project]').val($( "#slider_scope_project_selectStormerpage .ui-slider-handle" ).text());
		$('input[name=slider_team_size]').val($( "#slider_team_size_selectStormerpage .ui-slider-handle" ).text());
		$('input[name=slider_count_price]').val(parseInt(count_scope_project) + parseInt(count_team_size));

	}

	//$( "#slider_scope_project" ).slider({
	//    min: 5,
	//    max: 30,
	//    step: 5,
	//    value: 5,
	//    animate:true,
	//    slide: function(event, ui) {
	//        $( "#slider_scope_project .ui-slider-handle" ).text(ui.value);
	//        Price();
	//    }
	//});
	//
	//$( "#slider_team_size" ).slider({
	//    min: 5,
	//    max: 50,
	//    step: 5,
	//    value: 5,
	//    animate:true,
	//    slide: function(event, ui) {
	//        $( "#slider_team_size .ui-slider-handle" ).text(ui.value);
	//        Price();
	//    }
	//});
	//
	//$( ".ui-slider-handle" ).text('5');
	//Price()

	$('.our_recomendation').click(function(){
		$( "#slider_scope_project_selectStormerpage" ).slider( "value", 15);
		$( "#slider_scope_project_selectStormerpage .ui-slider-handle" ).text(15);
		$( "#slider_scope_project" ).slider( "value", 15);
		$( "#slider_scope_project .ui-slider-handle" ).text(15);
		//
		$( "#slider_team_size_selectStormerpage" ).slider( "value", 25);
		$( "#slider_team_size_selectStormerpage .ui-slider-handle" ).text(25);
		$( "#slider_team_size" ).slider( "value", 25);
		$( "#slider_team_size .ui-slider-handle" ).text(25);
		Price();
		PriceNew();

	});

	$('body').tooltip({
		selector: '.createdDiv'
	});
	
	$('[data-toggle="tooltip"]').tooltip({html:true});

	$('.buttom_bottom').click(function(){
		$('body, html').animate({
			scrollTop: 50000
		}, 4000);
	});

	$('.busy_button').click(function(){
		$(this).find('.busy_block').toggle();
		$(this).toggleClass('active');
	});


	var arrayRandomInfo = [
		['Mars Property Project','Real Estate', 'We sell property on mars for terraforming and future colonization'],
		['S&M Dental Clinic','Dental Care', 'Smiths and Matthews Dental clinic, since 1976'],
		['In Hot Water','Relationship Counselling', 'When it’s not a hot bath you’re in, you probably want out. '],
		['ACME Nets And Traps','Environmental protection', 'Providing high quality pest control for over 60 years.'],
		['Fit and Fat','Healthcare', 'Professional weight gaining for wrestlers and entertainers'],
		['Tasty and Clean','Dental Health', 'Fluoride-free edible toothpastes for survival situations'],
		['Bigger the Better','Apparel ', 'Plus size lingerie for women with more to show'],
		['Deathly Hallows Rock And Death Metal','Music Producers', 'Elevator music specialists'],
		['Sleep A Night Incorporated','Childcare Service', 'Overnight childcare for stage IV tiredness.'],
		['Give’adog Broom','Auto Manufacturer', 'Giving driving dogs all the tools they need.'],
		['Phew! Mints','Food and Beverage', 'Breath freshening productsfor pets and farm animals'],
		['PrintaPrinta','Technology', '3D printer printers, for when you need more than one.'],
		['KookieCola','Food and Beverage', 'Kookie high-sugar snacks and drinks, we know you like it!'],
		['As Clean As A Fish','Animals and Pets', 'Bringing goldfishtheir gold, with a range of home fish cleaning products'],
		['Breakibuns','Food and Beverage', 'Start your day like a king! Breakfast cakes and cookies'],
		['Cook and Wash','Consumer electronics', 'Ultra high-heat washing machines; when you need something in a smaller size'],
		['Drop A Brick','Healthcare', 'Weight loss tablets; couch loversdeserve better bodies!'],
		['Flash Italiano','Food and Beverage', 'Frozen, pre-cooked pasta, never boil water again!'],
		['Titan Kits','Automotive Manufacturer', 'Amphibious vehicleconversion kits; you don’t need a yacht when you have a van']
	];

	$('.anonymise_me').click(function(){
		var number = Math.floor(Math.random() * 19);
		var data = arrayRandomInfo[number];
		$('input[name="Customerinfo[business_name]"]').val(data['0']);
		$('input[name="Customerinfo[business_type]"]').val(data['1']);
		$('textarea[name="Customerinfo[business_services]"]').val(data['2']);
	});

//
//
//
//20.
//•	Millionaire Project
//•	Finance and business consulting
//•	Personal finance advice, you may already be rich!
//21.
//•	C++ Toddle Bear
//•	Software and games
//•	We make games for developing C++ and JAVAskills in toddlers and pre-schoolers
//22.
//•	Breathe Easy
//•	Home electronics
//•	Air purification –Making inside your home as fresh as nature,but better
//23.
//•	6a’Peel
//•	Consumer cosmetics
//•	New seasonal ranges of low-cost,luxury fragrances;be young and trusting
//24.
//•	P.M.S Finance
//•	Banking
//•	Peter-Myes-Smith; the bank you can trust
//25.
//•	Ping Pong Ping
//•	Personal care
//•	Body removal creams and shavers, be as sexy as a ping pong ball!
//26.
//•	Betta ‘n 3
//•	Consumer goods
//•	Alcohol, cigarette and knife gift packages; some things are better in threes!
//27.
//•	Petners
//•	Dating Service
//•	Dog and cat dating –Help your honey find their own honey
//28.
//•	Jack’all Whiskey
//•	Food and Beverage
//•	Alcohol-free whiskey for drivers and recovering alcoholics
//29.
//•	Goblin Cheese
//•	Food and Beverage
//•	Foot-flavoured, and other exotic-flavour creamed cheeses
//30.
//•	Hard As Stone
//•	Personal care
//•	Skin toughening creams and body lotions; Kung Fu master in only 30 days
//31.
//•	Pro Tone Cereal
//•	Food and Beverage
//•	Protein-based breakfast cereals, get more muscle for thehustle
//32.
//•	Heel Me
//•	Apparel
//•	High heel and stiletto stabilisers for those that want to, but can’t
//33.
//•	Fresh Fire
//•	Personal care
//•	Chilli-infused aftershaves; wake up with a bang!
//34.
//•	Super-Market
//•	Retail
//•	Personal items, food and beverage for super heroes. Even Spiderman needs underpants!
//35.
//•	Desert Air
//•	Personal Care
//•	Shower gel scented with the natural aroma of deserts. Take the desert to your shower!
//36.
//•	G&B
//•	Apparel
//•	Clothing line, carefully selectedby George Bush Sr.
//37.
//•	SpitFire Airlines
//•	Transportation
//•	Travel to your next international destination in a fire-spitting hot air balloon, around the world travel in eighty-days or less. Guaranteed.
//38.
//•	Smiling Monk
//•	Personal Care
//•	Protect your scalp from hot days; foranyone suffering from Alopecia.
//39.
//•	Cake In A Flash
//•	Bakery
//•	Freshly baked cakes sent anywhere in the world by rocket-propelled drones. For when you really need a cake!
//40.
//•	Box Wheels
//•	Auto Manufacturing
//•	Square wheels for cars and bicycles, you can’t reinvent the what?
//41.
//•	Flat Springs Beer
//•	Food and beverage
//•	Carbonated-free beer; reducing the world’s carbon footprint, one bottle at a time!
//42.
//•	Pillow-Board
//•	Home Electronics
//•	The keyboard that doubles as a pillow. For tired IT professionals.
//43.
//•	Kanga-Sneeks
//•	Apparel
//•	Running shoes, with embedded springs. Do the real moon walk!
//44.
//•	Sun Shine
//•	Food and beverage
//•	High quality drinking alcohol, high ABV and low calorie! Have the time of your life!
//45.
//•	So-Vive Travel Services
//•	Travel service
//•	Practice your survival skills be challenged! 7 day no-item drop-offs in any remote region of the world.
//46.
//•	Budget Funeral Services LTD
//•	Service
//•	We sell low-cost, two-for-one funeral packages; gets the nail in without breaking the bank
//47.
//•	A-Hole Construction
//•	Construction
//•	A-Hole offers instantquotations and a deal you’ll never forget.
//48.
//•	Ballsy
//•	Manufacturing
//•	Balls of all kind made for any sport! Don’t get something that will let you down, get ballsy balls!
//49.
//•	Bat Beans
//•	Food and beverage
//•	Coffee beans extracted from the droppings of African fruit bats. The best brew you’ll have!
//50.
//•	Inka Smile
//•	Tattoo service
//•	Specialising in tribal and biomechanical facial tattoos, if we can’t make you smile forever, no one can!

	$('.showUpload_photo').click(function(){
		$(this).hide();
		$('.containerUpload').show();
	});
	$('input').on('beforeItemAdd', function(event) {
		$('#value_post').val($('#value_post').val()+','+event.item);
	});
	$('input').on('itemRemoved', function(event) {
		var values_i = '';
		$(".bootstrap-tagsinput span").each(function(index) {
			values_i = values_i + ',' + $(this).text();
		});
		$('#value_post').val(values_i);
	});

	$('.add_category_button').click(function(){
		//console.log($(".your_category").length);

		if($('.add_category_input').val().length > 0){
			var count =parseInt($('#count_your_catecory').val());
			var liLength = $("input[name*='your_category']").length;
			if($(".your_category").length > 0){
				$('.multiselect-container').append('<li class="your_category_li" id="your_category_li"><a tabindex="0"><label class="checkbox"><input onchange="look_around_first()" type="checkbox" checked  name="your_category'+count+'" value="'+$('.add_category_input').val()+'">'+$('.add_category_input').val()+'</label></a></li>')
				$('#your_category').append('<option value="'+$('.add_category_input').val()+'">'+$('.add_category_input').val()+'</option>');
			}else{
				$('.multiselect-container').append('<li class="multiselect-item multiselect-group"><label class="your_category">Your category</label></li>');
				$('.multiselect-container').append('<li class="your_category_li" id="your_category_li" ><a tabindex="0"><label class="checkbox"><input onchange="look_around_first()" type="checkbox" name="your_category'+count+'" checked value="'+$('.add_category_input').val()+'">'+$('.add_category_input').val()+'</label></a></li>')
				$('#multiXX').append('<optgroup label="Your category" id="your_category"></optgroup>');
				$('#your_category').append('<option value="'+$('.add_category_input').val()+'">'+$('.add_category_input').val()+'</option>');

			}

			$('#count_your_catecory').val(count+1);
			$('.add_category_input').val('');
			look_around_first();
			swal("Category added!", " ", "success");

		}else{
			swal("Category cannot be blank!", " ", "error")
		}
	});


		function successTags(){
			$('.field_customer_tags').removeClass('has-error').addClass('has-success');
			$('.bootstrap-tagsinput').css('border','1px solid #3c763d');
			$('.field_customer_tags > .error_text').hide();
		}

		function errorTags(){
			$('.field_customer_tags').removeClass('has-success').addClass('has-error');
			$('.bootstrap-tagsinput').css('border','1px solid #a94442');
			$('.field_customer_tags > .error_text').show();
		}


		function successCategory(){
			//$('.field_customer_category').removeClass('has-error').addClass('has-success');
			$('.field_customer_category > label').css('color','#3c763d');
			$('.field_customer_category > .category_input_block > .btn-group button').css('border','1px solid #3c763d');
			$('.field_customer_category > .category_input_block > .btn-group button').css('color','#3c763d');
			$('.field_customer_category > .error_text').hide();
		}

		function errorCategory(){
			//$('.field_customer_category').removeClass('has-success').addClass('has-error');
			$('.field_customer_category > label').css('color','#a94442');
			$('.field_customer_category > .category_input_block > .btn-group button').css('border','1px solid #a94442');
			$('.field_customer_category > .category_input_block > .btn-group button').css('color','#a94442');
			$('.field_customer_category > .error_text').css('color','#a94442');
			$('.field_customer_category > .error_text').show();
		}

		$('input[name=step_2_next]').click(function(event){
			if($('.bootstrap-tagsinput span').length == false){
				event.preventDefault();
				errorTags();
			}
			if(($('.field_customer_category > .category_input_block > .btn-group >.multiselect-container li.active').length <= 0) && ($('.your_category_li>a>label>input:checked').length <= 0)){

				event.preventDefault();
				errorCategory();
			}
		});

		$('input').on('itemAdded', function(event) {
			if($('.bootstrap-tagsinput span').length > 0){
				successTags();
			}else{
				errorTags();
			}
		});

		$('input').on('itemRemoved', function(event){
			if($('.bootstrap-tagsinput span').length > 0 ){
				successTags();
			}else{
				errorTags();
			}
		});


		$(document).on('change','.multiselect-container li', function(){
			if(($('.field_customer_category > .category_input_block > .btn-group >.multiselect-container li.active').length <= 0) && ($('.your_category_li>a>label>input:checked').length <= 0)){
				errorCategory();
			}else{
				successCategory();
			}
		});

		if($('#stormerinfo-where_here_about_us').val() == '4'){
			$('.which_website').show();
		}

		$(document).on('change','#stormerinfo-where_here_about_us', function(){
			if($(this).val() == '4'){
				$('.which_website').show();
			}else{
				$('.which_website').hide();
				$('#stormerinfo-which_website').val('');
			}
		});

		function getpuzzle(type,answer,timeBetween){
			var challengeStep = $('.challengeStep').text();
			var puzzleId = $('#puzzleId').data('id');
			$.ajax({
				type: 'POST',
				url: '../stormer/getpuzzle',
				data: {puzzleId:puzzleId,challengeStep:challengeStep,type:type,answer:answer},
				dataType: "json",
				success: function(response){
					if(response.type == 'get'){
						var d = new Date();
						$('.challenge').text(' ');
						$('.challenge').append('<p>'+response.puzzle.question+'</p>\n\
							<span id="puzzleId" data-id="'+response.puzzle.id+'"></span>\n\
							<span id="puzzleTime" data-time="'+d+'"></span>\n\
							<input type="checkbox" name="A"  value="A"><label style="margin-left:10px;">'+response.puzzle.A+'</label></br>\n\
							<input type="checkbox" name="B" value="B"><label style="margin-left:10px;">'+response.puzzle.B+'</label></br>\n\
							<input type="checkbox" name="C" value="C"><label style="margin-left:10px;">'+response.puzzle.C+'</label></br>\n\
							<input type="checkbox" name="D" value="D"><label style="margin-left:10px;">'+response.puzzle.D+'</label></br>\n\
						');
					}else{
						if(response.status_answer != 'error'){
							var result = 25 + (30 - timeBetween);
							$('.result'+parseInt($('.challengeStep').text())).text(result);
						}else{
							$('.result'+parseInt($('.challengeStep').text())).text('5');
						}

					}
				}
			});
		}

		$('.ChallengeButton').click(function(){
			$('.ChallengeBlok').show();
			$(this).attr('disabled',true);
			getpuzzle('get','','');
		});

		$(document).on('change','.challenge > input',function(){

			//if($('.challengeStep').text() < '3'){
				var dateEnd = new Date();
				var dateStart = new Date($('#puzzleTime').data('time'));
				var timeBetween = parseInt((dateEnd.getTime() - dateStart.getTime()) /1000);
					if(timeBetween < 30){
						var result_answer = getpuzzle('check',$(this).val(),timeBetween);
					}else{
						$('.result'+parseInt($('.challengeStep').text())).text('5');
					}
					if(parseInt($('.challengeStep').text()) >= 2){
						$('.challengeStep').text(parseInt($('.challengeStep').text()) + 1);
						$('.challenge').hide();
						$('.ChallengeBlok').hide();
						setTimeout(function(){
							$('.ChallengeBlok').show();
							$('.ChallengeFinish').show();
							$('.challengePoint').text(parseInt($('.result1').text()) + parseInt($('.result2').text()) + parseInt($('.result3').text()));
							$('input[name="challengePoint"]').val(parseInt($('.result1').text()) + parseInt($('.result2').text()) + parseInt($('.result3').text()));
              $.get("/stormer/point_count", function(data){
                $('#view_point').text(data);
              });
						}, 1000);
					}else{
						$('.challengeStep').text(parseInt($('.challengeStep').text()) + 1);
						getpuzzle('get','','');
					}
			//}
		});

		$('#datetimepicker1').datetimepicker({
			language: 'pt-BR',
			 startDate: new Date(new Date().getTime()+(5*24*60*60*1000)),
			 defaultDate: new Date(new Date().getTime()+(5*24*60*60*1000))
		  });

		$('.boxClick').click(function(){
			$('.boxShow').toggle();
		});
		
		
		$(document).on('mouseenter','.infoButtonShow',function(){
			$(this).parent().find('.infoBlockShow').show();
		});
		$(document).on('mouseleave','.infoButtonShow',function(){
				var classH = $(this).parent().find('.infoBlockShow');
				
				setTimeout(function () {
					if (classH.is(':hover')) {
					}else {
						classH.hide()
					}
				}, 400);
		});
		
		$(document).on('mouseleave','.infoBlockShow',function(){
			var classH = $(this).parent().find('.infoBlockShow');
			setTimeout(function () {
				if (classH.is(':hover')) {
				}else {
					classH.hide()
				}
			}, 400);
		});

		// Placeholder for input in customer/post-project/target2/
		var placeholder = 'Eg. How would a business consider this problem?\nEg. How would this look from a customer perspective?\nEg. How might suppliers react to this?';
		$('#projects-perspectives').attr('placeholder', placeholder);

		$('#projects-perspectives').focus(function(){
			if($(this).val() === placeholder){
				$(this).attr('placeholder', '');
			}
		});

		$('#projects-perspectives').blur(function(){
			if($(this).val() ===''){
				$(this).attr('placeholder', placeholder);
			}
		});
});

///////listat_an
function lamp(){
	$(".titles-wrap .btn").on('mouseenter', function(){
		$('#large-header2').addClass("lamp-show");
	})
	$(".titles-wrap .btn").on('mouseleave', function(){
		$('#large-header2').removeClass("lamp-show");
	})
}
lamp();



function send_mail_admin(){
	$.post("/api/sendmailadmin", function(result){
		if(result){
			swal("", "Messages sent to the administrator!", "success")
		}else {
			swal("error", "Message not sent", "error")
		}

	});
}

function message(){
	swal('','Please note that completed projects that have not been marked as closed by the customer will not have been paid in to your account.If you feel there is an issue or problem with your payment, contact. Payment is made monthly, and is calculated from the first day to the last day of the month. The transfer is then made automatically before the fifth working day of the next month.','');
}


$(document).ready(function(){

	function showMessage(){
		$('#myModal1').modal('show');
	}

	// HOME PANEl

	$('#large-header').closest('.wrap').find('.guest').css({
		'background-color': 'transparent',
		'border-bottom': 0
	})

	//FAQ ACCORDEON

	var el = $('.question-list-wrap').find('.answer');
	
	for(var i=0; i < el.length; i++){

		var accordeon = $(el[i]);

		if(accordeon.innerHeight() > 110){
			accordeon.addClass('turn');
		}

	}

	$('.question-list-wrap').find('.arrow').on('click', function(){
		
		$(this).parent().toggleClass('show');
	})

	//ADMIN MENU

	$('#cssmenu .menu-burger').on('click', function(){
		$('#cssmenu .list-menu').toggleClass('show');
	})

	// Notification right panel

	$("#btn-notification").on('click', function(){
		var notificationPanel = $("#notification-panel");
		if (notificationPanel.hasClass('active')){
			notificationPanel.removeClass('active');
		}else{
			notificationPanel.addClass('active');
		};
	});

	$(document).mouseup(function (e){
		var panel = $("#notification-panel");

		if (!panel.is(e.target)
			&& panel.has(e.target).length === 0
			&& $("#btn-notification").has(e.target).length === 0) {
			panel.removeClass('active');
		}else{
			if (e.target.id === 'notification_url'){
        location.href = '/stormer/projectintroduction';
      }
		}
	});
})


$(window).on('scroll load', function() {

	scrollX = $(this).scrollTop();

	if(scrollX >=5){
		$('.guest').addClass('bg');
	}else{
		$('.guest').removeClass('bg');
	}

})