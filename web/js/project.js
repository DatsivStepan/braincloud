$(document).ready(function(){
    $(document).on('click','.not_corect_timeline',function(){
        $.get("/customer/not_corect_timeline", function(data){
            swal('','Thank you, your opinion has been recorded and will be reviewed','success');
        });

    });
    changeProjectCategory($('#projects-category').val());
    
    $(document).on('change','#projects-category',function(){
        changeProjectCategory($(this).val())
    });
    function changeProjectCategory(value){
        switch (value) {
            case 'Employee engagement':
                $('.category_icon').attr('src','../../../image/project/target/employee_engagement.png');
                break;
            case 'Customer experience':
                $('.category_icon').attr('src','../../../image/project/target/customer_experience.png');
                break;
            case 'Customer Reach':
                $('.category_icon').attr('src','../../../image/project/target/customer_reach.png');
                break;
            case 'Research and development':
                $('.category_icon').attr('src','../../../image/project/target/research_and_development.png');
                break;
            case 'Cost containment':
                $('.category_icon').attr('src','../../../image/project/target/cost_containment.png');
                break;
            case 'Sales planning':
                $('.category_icon').attr('src','../../../image/project/target/sales_planning.png');
                break;
            case 'Value chain management':
                $('.category_icon').attr('src','../../../image/project/target/value_chain_management.png');
                break;
            case 'Human resource management':
                $('.category_icon').attr('src','../../../image/project/target/human_resource_management.png');
                break;
            case 'Business model innovation':
                $('.category_icon').attr('src','../../../image/project/target/business_model_innovation.png');
                break;
        }
    }
    
});