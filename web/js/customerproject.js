$(document).ready(function(){
    $(document).on('click','.closeProject',function(){
        var project_id = $(this).data('project_id');
        var ideaUserCount = $('input[name="ideaUserCount'+project_id+'"]').val();
        var ideaCount = $('input[name="ideaCount'+project_id+'"]').val();
        var sizeTeam = $('input[name="sizeTeam'+project_id+'"]').val();
        var endDate = $('input[name="endDate'+project_id+'"]').val();
        var stayIdea = (sizeTeam * ideaCount) - ideaUserCount;
        
        swal({   
            title: "",
            text: 'Please confirm you have checked Behind the Scenes and now satisfied to close the project.<br /><br /><span class="danger">*Warning</span>: Once the project is closed, it cannot be reopened, nor can any modification requests be made.',
            type: "warning",   
            html:true,
            showCancelButton: true,   
            confirmButtonColor: "#fa8a13",
            confirmButtonText: "Close the Project!",
            closeOnConfirm: true 
        }, function(){
            $.post("/api/closeproject", {id: project_id}, function (data) {
                if (data) {
                    $('#closeProject'+project_id).modal('show');
                }
            });

        });
    });
});