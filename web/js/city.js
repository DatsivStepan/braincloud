$(document).ready(function(){
    
    $(document).on('change','.selectCountry',function(){
        var country_id = $(this).val();
        
        if(country_id != 0){
            $.ajax({
                type: 'POST',
                url: '../../customer/getcity',
                data: {country_id:country_id},
                dataType: "json",
                success: function(response){
                    console.log(response);
                    if(response.status == 'good'){
                        $('.selectCity').html('');
                        $('.selectCity').append('<option value="0">Select City</option>');
                        for(var key in response.city){
                            $('.selectCity').append('<option value='+key+'>'+response.city[key]+'</option>');
                        }
                    }
                }
            });
        }else{
            $('.selectCity').html('');
            $('.selectCity').append('<option value="0">First select a country</option>');
        }
        
    });
    
    $(document).on('click','.showHint',function(e){
        e.preventDefault();
        $(this).parent().find('p').toggle();
    });
    
    function deleteFile(file_name){
        $.ajax({
            type: 'POST',
            url: '../../../site/deleteprofilephoto',
            data: {file_name:file_name},
            success: function(response){
            }
        });
    }
    if($('.clicable').length){
        var previewNode = document.querySelector("#template");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        var myDropzone = new Dropzone($('.clicable')[0], { // Make the whole body a dropzone
            maxFiles:1,
            uploadMultiple:false,
            acceptedFiles:'image/*',
            url: "site/savedprofilephoto", // Set the url
            previewTemplate: previewTemplate,
            //autoQueue: false, // Make sure the files aren't queued until manually added
            previewsContainer: "#previewsS", // Define the container to display the previews
            clickable: ".clicable" // Define the element that should be used as click trigger to select files.
        });

        myDropzone.on("maxfilesexceeded", function(file) {
                myDropzone.removeAllFiles();
                myDropzone.addFile(file);
        });

        myDropzone.on("complete", function(response) {
            if (response.status == 'success') {
                $('#stormerinfo-image_src').val(response.xhr.response);
            }
        });

        myDropzone.on("removedfile", function(response) {
            if(response.xhr != null){
               deleteFile(response.xhr.response);
            }
        });
    }

});