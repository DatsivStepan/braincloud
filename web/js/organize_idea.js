function initJSmind(mind) {
    var options = {
        container: 'jsm_container',
        theme: 'greensea',
        editable: true,
        mode: 'full',
        clickable: true,
        layout: {
            hspace: 150,

        },
    }
    return jsM = jsMind.show(options, mind);

}

function save(type) {
    var mind_data = jsM.get_data('node_array');
    var mind_string = jsMind.util.json.json2string(mind_data);
    $.ajax({
        type: 'POST',
        url: 'save',
        data: {
            minds: mind_string,
            project_id: project_id,
            type: type
        },
        success: function(data) {
            swal({
                title: 'aaand we’re done!\nrWe have received x ideas, and earned $x. You’ve met your quota! Well done!',
                text: 'We are now awaiting confirmation from the customer to review and close the project. It is possible that we may have to request modifications, so please keep a close eye on your emails and notifications.',
                type: "success",
                confirmButtonText: "Take Me Home"
            }, function(isConfirm) {
                if (isConfirm) {
                    window.location.replace(window.location.protocol + "//" + window.location.host + data)
                    // save('stage')
                }
            })
        },
        error: function(data) {
            swal({
                title: 'Oooops!',
                text: data,
                type: "error"
            })
        }
    });
}
$(document).ready(function() {
    console.log(mindM);
    console.log((mindM));
    var jsM = initJSmind(mindM)
    jsM._event_bind = function() {

    }


    $('.add-node').click(function(e) {
        e.preventDefault()
        var mas = jsM.mind.nodes;
        var count_idea_create = 0;



        for (var key in mas) {
            console.log(key);
            if (key !='root'){
                count_idea_create++;

            }
        };
        if(count_idea > count_idea_create) {
            var selected_node = jsM.get_selected_node(); // as parent of new nod/
            // selected_node = jsM.get_root()
            var nodeid = jsMind.util.uuid.newid();
            var topic = $('.idea-sescription').val();
            if (selected_node) {
                if (!topic) {
                    swal({
                        title: 'Write your idea first!'
                    })
                } else {
                    if (topic.length > 50) {
                        var topic = topic.slice(0, 25) + "<a class='Readmore'> Readmore</a><span class='hiddenText'>" + topic.slice(25) + '</span>';
                    }
                    topic = topic.replace(/(?:\r\n|\r|\n)/g, '<br />');
                    var node = jsM.add_node(selected_node, nodeid, topic);
                    $('.idea-sescription').val('');
                }
            } else {
                swal({
                    title: 'Select node first!'
                })
            }
        }else {
            swal({
                title: 'Not create idea!',
                text: "For this project, you can create only "+count_idea+" ideas."
            })
        }
    })

    $(document).on('click', '.Readmore', function(e) {
        e.preventDefault()
        var nodeid = $(this).parent().attr('nodeid')
        $(this).parent().find('.hiddenText').addClass('showedText').removeClass('hiddenText')
        var some = $(this).parent()
        $(this).parent().append('<a class="Readless"> Readless</a>')
        $(this).remove()
        jsM.update_node(nodeid, some.html())

    })

    $(document).on('click', '.Readless', function(e) {
        e.preventDefault()
        var nodeid = $(this).parent().attr('nodeid')
        $(this).parent().find('.showedText').addClass('hiddenText').removeClass('showedText').before('<a class="Readmore"> Readmore</a>')
        some = $(this).parent();
        $(this).remove()
        jsM.update_node(nodeid, some.html())
        console.log(jsM);
    })

    $(document).on('click', '.saveD', function(e) {
        e.preventDefault()
        var id = $(this).data('id')
        if (id == 'stage') {
            // swal({
            //   title: "Are you sure?",
            //   text: "Please confirm that you have checked all the other ideas to ensure you’re not making any obvious repeats",
            //   type: "warning",
            //   showCancelButton: true,
            //   confirmButtonText: "Confirm",
            //   cancelButtonText: "Let me double check",
            //   closeOnConfirm: false,
            //   closeOnCancel: true
            // }, function(isConfirm) {
            //   if (isConfirm) {
            // alert(window.location.host)
            save('stage')
            // }
            // });
        } else {
            save('exit')
        }
    })

    $(document).on('click', 'jmnode', function() {


        item = jsM.get_selected_node();
        if(item.id != 'root'){
            $('#idea_id').val(item.id);
            $('#organizeIdea').modal('show');
        }

    });
});
