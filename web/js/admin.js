/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    function actionIdea(action,idea_id){
        
         $.ajax({
            type: 'POST',
            url: '../../admin/deleteflag',
            data: {action:action,idea_id:idea_id},
            dataType: "json",
            success: function(response){
                if(response.status == 'success'){
                    
                    $('.blockIdea'+idea_id).html('Idea not Flagging');
                    swal({   
                        title:'Idea',
                        type:'success'
                    });
                }
            }
        });
    }
    
    $(document).on('click','.ideaOk',function(){
        actionIdea('ok',$(this).data('idea_id'));
    });
    
    $(document).on('click','.ideaDelete',function(){
        actionIdea('delete',$(this).data('idea_id'));
    });
    
});