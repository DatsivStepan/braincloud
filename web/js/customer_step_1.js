/**
 * Created by Listat on 09.08.2016.
 */

$(document).ready(function() {
    function Price(){
        if(parseInt($( "#slider_team_size .ui-slider-handle" ).text()) != 0){
            var count_team_size = parseInt($( "#slider_team_size .ui-slider-handle" ).text()) * 6;
        }else{
            var count_team_size = 0;
        }

        if(parseInt($( "#slider_scope_project .ui-slider-handle" ).text()) != 0){
            var count_scope_project = parseInt($( "#slider_scope_project .ui-slider-handle" ).text()) * 5 * parseInt($( "#slider_team_size .ui-slider-handle" ).text());
        }else{
            var count_scope_project = 0;
        }
        if (typeof discount != 'undefined'){


            var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
            var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount*one_procent);
            console.log(price);
            console.log(one_procent);
            alert('sd');
            $('.totalPrice').text( price.toFixed(2));
        }
        $('.sliderPrice').text(parseInt(count_scope_project) + parseInt(count_team_size));

        $('input[name=scope_project]').val($( "#slider_scope_project .ui-slider-handle" ).text());
        $('input[name=team_size]').val($( "#slider_team_size .ui-slider-handle" ).text());
        $('input[name=count_price]').val(parseInt(count_scope_project) + parseInt(count_team_size));

    }
    function PriceNew(){
        if(parseInt($( "#slider_team_size_selectStormerpage .ui-slider-handle" ).text()) != 0){
            var count_team_size = parseInt($( "#slider_team_size_selectStormerpage .ui-slider-handle" ).text()) * 6;
        }else{
            var count_team_size = 0;
        }

        if(parseInt($( "#slider_scope_project_selectStormerpage .ui-slider-handle" ).text()) != 0){
            var count_scope_project = parseInt($( "#slider_scope_project_selectStormerpage .ui-slider-handle" ).text()) * 5 * parseInt($( "#slider_team_size_selectStormerpage .ui-slider-handle" ).text());
        }else{
            var count_scope_project = 0;
        }
        if (typeof discount != 'undefined'){


            var one_procent= (parseInt(count_scope_project) + parseInt(count_team_size))/100;
            var price = (parseInt(count_scope_project) + parseInt(count_team_size))-(discount*one_procent);
            $('.totalPrice').text( price.toFixed(2));
        }

        $('.sliderPrice').text(parseInt(count_scope_project) + parseInt(count_team_size));

        $('input[name=slider_scope_project]').val($( "#slider_scope_project_selectStormerpage .ui-slider-handle" ).text());
        $('input[name=slider_team_size]').val($( "#slider_team_size_selectStormerpage .ui-slider-handle" ).text());
        $('input[name=slider_count_price]').val(parseInt(count_scope_project) + parseInt(count_team_size));

    }
    $("#slider_scope_project").slider({
        min: 5,
        max: 30,
        step: 5,
        value: 5,
        animate: true,
        slide: function (event, ui) {
            $("#slider_scope_project .ui-slider-handle").text(ui.value);
            Price();
        }
    });

    $("#slider_team_size").slider({
        min: 5,
        max: 50,
        step: 5,
        value: 5,
        animate: true,
        slide: function (event, ui) {
            $("#slider_team_size .ui-slider-handle").text(ui.value);
            Price();
        }
    });

    $(".ui-slider-handle").text('5');
    Price()

    $('.our_recomendation_customer').click(function(){
        $( "#slider_scope_project_selectStormerpage" ).slider( "value", 15);
        $( "#slider_scope_project_selectStormerpage .ui-slider-handle" ).text(15);
        $( "#slider_scope_project" ).slider( "value", 15);
        $( "#slider_scope_project .ui-slider-handle" ).text(15);
        //
        $( "#slider_team_size_selectStormerpage" ).slider( "value", 25);
        $( "#slider_team_size_selectStormerpage .ui-slider-handle" ).text(25);
        $( "#slider_team_size" ).slider( "value", 25);
        $( "#slider_team_size .ui-slider-handle" ).text(25);
        Price();

    });

});