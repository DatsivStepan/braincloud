/**
 * Created by Listat on 11.08.2016.
 */

function look_around_first(){
    if ($('.multiselect-container input:checked').length == 0){
        $('.multiselect-selected-text').text('None selected');
    } else {
        $('.multiselect-selected-text').text($('.multiselect-container input:checked').length + ' selected');
    }

    //alert($('.multiselect-container input:checked').length);
}

$(document).ready(function(){
    $( "#multiXX" ).change(function() {
        //look_around_first();
        setTimeout(look_around_first, 500);
    });
});