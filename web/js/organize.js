$(document).ready(function(){
    //swal({
    //    title:'',
    //    text: "You have x ideas and potential solutions. If you were to look at each one and consider the value, then it could take you (x times 30m= hrs/minutes) to complete. We highly recommend using our quick and easy idea organization system. You can still export all your ideas, they’ll just much better organized, and much more guaranteed to end in usable solutions. Simply click on an idea to begin. Organizing all your ideas will take between 30m to an hour, and is time very well invested! You can always leave at any time and come back to complete it later. ",
    //    confirmButtonText: "Close",
    //});
    
    $(document).on('click','[id^=blockIdea]',function(){
        //alert($(this).data('idea_id'));
    });
    
    $('.question_1').rating({ min: 0, max: 5, step: 1, size: 'sm', showClear: false });
    $('.question_1').on('rating.change', function() {
        $(this).parent().parent().find('#organizeidea-question_1').val($(this).val())
    });
    
    $('.question_2').rating({ min: 0, max: 5, step: 1, size: 'sm', showClear: false });
    $('.question_2').on('rating.change', function() {
        $(this).parent().parent().find('#organizeidea-question_2').val($(this).val());
    });
    
    $('.question_3').rating({ min: 0, max: 5, step: 1, size: 'sm', showClear: false });
    $('.question_3').on('rating.change', function() {
        $(this).parent().parent().find('#organizeidea-question_3').val($(this).val());
    });
    
    $('.question_4').rating({ min: 0, max: 5, step: 1, size: 'sm', showClear: false });
    $('.question_4').on('rating.change', function() {
        $(this).parent().parent().find('#organizeidea-question_4').val($(this).val());
    });
    
    $('.question_5').rating({ min: 0, max: 5, step: 1, size: 'sm', showClear: false });
    $('.question_5').on('rating.change', function() {
        $(this).parent().parent().find('#organizeidea-question_5').val($(this).val());
    });
    
    var clicked = false;
     $('.formOrganize_idea').on('submit', function(e){
        var ranting_1 = $(this).find('#organizeidea-question_1').val();
        var ranting_2 = $(this).find('#organizeidea-question_2').val();
        var ranting_3 = $(this).find('#organizeidea-question_3').val();
        var ranting_4 = $(this).find('#organizeidea-question_4').val();
        var ranting_5 = $(this).find('#organizeidea-question_5').val();
        var my_form = $(this);
        
        if((ranting_1 == '') || (ranting_2 == '') || (ranting_3 == '') || (ranting_4 == '') || (ranting_5 == '')){
            e.preventDefault();
            swal("Answer question", "", "error");
        }else{
            if(!clicked){
                e.preventDefault();
                swal({   
                    title:'',
                    text: "The idea of organized",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonText: "Ok",
                }, function(isConfirm){
                   if (isConfirm) {
                       clicked = true;
                       my_form.trigger('submit');
                   } 
                });
            }
        }
     });




//    $(document).on('click','.implement_ideas',function(){
//                swal({
//                    title:'',
//                    text: "We not only encourage stormers to think outside the box and create radical ideas, it’s also important that you do too. We’ve created some great tips to help inspire you to use the ideas in different ways. Would you like to see them? For best results, we recommend that all ideas are first organized.",
//                    //type: "success",
//                    showCancelButton: true,
//                    confirmButtonText: "Let’s Get Started!",
//                    cancelButtonText: "I’m Fine Without, Thanks.",
//                }, function(isConfirm){
//                    if (isConfirm) {
////                        window.open('/content/Organization_instructions_for_customer.doc', '_blank');
//                        $('#organizeInstruction').modal('show');
//                    }
//                });
//    });
    
    
    var doc = new jsPDF();
    var specialElementHandlers = {
        '#editor': function (element, renderer) {
            return true;
        }
    };

    $('.exportButton').click(function () {
        doc.fromHTML($('.for_pdf').html(), 15, 15, {
            'width': 170,
                'elementHandlers': specialElementHandlers
        });
        doc.save('project_idea.pdf');
    });
    
});

function implement(id){
    swal({
        title:'',
        text: "We not only encourage stormers to think outside the box and create radical ideas, it’s also important that you do too. We’ve created some great tips to help inspire you to use the ideas in different ways. Would you like to see them? For best results, we recommend that all ideas are first organized.",
        //type: "success",
        showCancelButton: true,
        confirmButtonText: "Let’s Get Started!",
        confirmButtonColor: "#fa8a13",
        cancelButtonText: "I’m Fine Without, Thanks.",
    }, function(isConfirm){
        if (isConfirm) {
//                        window.open('/content/Organization_instructions_for_customer.doc', '_blank');
//            $('#organizeInstruction').modal('show');
            location.href = '/customer/organize/'+id;
        }
    });
}