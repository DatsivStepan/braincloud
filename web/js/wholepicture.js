function initJSmind(mind) {
  var options = {
    container: 'jsm_container',
    theme: 'greensea',
    editable: true,
    mode: 'full',
    clickable: false,
    layout: {       
      hspace: 150,
         
    },
  }
  return jsM = jsMind.show(options, mind);

}

function save(type) {
  var mind_data = jsM.get_data('node_array');
  var mind_string = jsMind.util.json.json2string(mind_data);
  $.ajax({
    type: 'POST',
    url: 'save',
    data: {
      minds: mind_string,
      project_id: project_id,
      type: type
    },
    success: function(data) {
      swal({
        title: 'OK!',
        text: 'Your data stored successfully',
        type: "success"
      }, function(isConfirm) {
        if (isConfirm) {
          window.location.replace(window.location.protocol + "//" + window.location.host + data)
            // save('stage')
        }
      })
    },
    error: function(data) {
      swal({
        title: 'Oooops!',
        text: data,
        type: "error"
      })
    }
  });
}

$(document).on('click', 'jmnode', function() {
  console.log(jsM.get_selected_node().id);
  var items = jsM.get_selected_node();

  //alert(items.data.value)
  if (items.id == 'root'){
    $('#root').modal('show');
  }
  //if (items.data.value == 'stormer'){
  //    $('#stormer'+items.id).modal('show');
  //}

});

$(document).on('click', 'jmup', function() {
  console.log(this.getAttribute("nodeid"))
  var items = jsM.get_node(this.getAttribute("nodeid"));
  console.log('node',items.topic);
  $('#idea_input_update').text(items.topic);
  $('#idea_input_update').val(items.topic);
  $('#nodeid').val(items.id);
  $('#idea_up').modal('show');
})

$(document).on('click', '#jsm_container', function() {

  var items = jsM.get_selected_node();
  console.log(jsM.get_selected_node());
  console.log(items.data.value);
  //alert(items.data.value)
  if(items){
    /*if (items.data.value == true){
      console.log(items.topic)
      $('#update').show();
      $('.idea-sescription').val(items.topic);
      $('#show_idea').hide();
    }else{
      //$('#update').show();
      //$('#update').hide();
      //$('.idea-sescription').val('');
      if(items.data.value == false){
        $('#show_idea').show();
        $('#idea_text').text(items.topic);
        $('#update').hide();
        $('.idea-sescription').val('');
      } else {
        $('#update').hide();
        $('.idea-sescription').val('');
        $('#show_idea').hide();
      }
    }*/
    $('#description_idea').text(items.topic);
    $('#idea_view').modal('show');
  }else{
    // $('#update').hide();
    $('.idea-sescription').val('');
    $('#show_idea').hide();
  }

});

$(document).ready(function() {

  innovation_next(2);
  console.log(mindM);
  console.log((mindM));
  var jsM = initJSmind(mindM)
  jsM._event_bind = function() {

  }

  var defoult_count_idea = 0;

  for (var key1 in jsM.mind.nodes) {
    defoult_count_idea++;
  };
  defoult_count_idea = defoult_count_idea - count_idea_user;

  $('.add-node').click(function(e) {
    e.preventDefault()
    var mas = jsM.mind.nodes;
    var count_idea_create = 0;

    for (var key in mas) {
        count_idea_create++;
    };
    if(count_idea > (count_idea_create-defoult_count_idea)) {
      var selected_node = jsM.get_node(user_id); // as parent of new nod/
      // selected_node = jsM.get_root()
      console.log(jsM.get_root());
      var nodeid = jsMind.util.uuid.newid();
      var topic = $('.idea-sescription').val();
      if (selected_node) {
        if (!topic) {
          swal({
            title: 'Write your idea first!'
          })
        } else {
          /*if (topic.length > 50) {
            var topic = topic.slice(0, 25) + "<a class='Readmore'> Readmore</a><span class='hiddenText'>" + topic.slice(25) + '</span>';
          }*/
          topic = topic.replace(/(?:\r\n|\r|\n)/g, '<br />');
          var node = jsM.add_node(selected_node, nodeid, topic);
          $('.idea-sescription').val('');
          instruction++;
          if(instruction == 2){
            instruction_show(2);
            inspire_next();
            instruction=0;
          }
        }
      } else {
        swal({
          title: 'Select node first!'
        })
      }
    }else{
      swal({
        title: 'Not create idea!',
        text: "For this project, you can create only "+count_idea+" ideas."
      })
    }
  })


  $('#update').click(function() {

    // var items = jsM.get_selected_node();
    var items = jsM.get_node($('#nodeid').val());
    //alert(items.data.value)
    if(items) {
      var topic = $('#idea_input_update').val();
      if (!topic) {
        swal({
          title: 'Write your idea first!'
        })
      } else {
        if (topic.length > 50) {
          var topic = topic.slice(0, 25) + "<a class='Readmore'> Readmore</a><span class='hiddenText'>" + topic.slice(25) + '</span>';
        }
        topic = topic.replace(/(?:\r\n|\r|\n)/g, '<br />');
        var node = jsM.update_node( items.id, topic);
        //$('.idea-sescription').val('');
        instruction++;
        if(instruction == 2){
          innovation_next(2);
          inspire_next();
          instruction=0;
        }
      }
      $('#idea_up').modal('hide');
    }
  });
  $(document).on('click', '.Readmore', function(e) {
    e.preventDefault()
    var nodeid = $(this).parent().attr('nodeid')
    $(this).parent().find('.hiddenText').addClass('showedText').removeClass('hiddenText')
    var some = $(this).parent()
    $(this).parent().append('<a class="Readless"> Readless</a>')
    $(this).remove()
    jsM.update_node(nodeid, some.html())

  })

  $(document).on('click', '.Readless', function(e) {
    e.preventDefault()
    var nodeid = $(this).parent().attr('nodeid')
    $(this).parent().find('.showedText').addClass('hiddenText').removeClass('showedText').before('<a class="Readmore"> Readmore</a>')
    some = $(this).parent();
    $(this).remove()
    jsM.update_node(nodeid, some.html())
    console.log(jsM);
  })

  $(document).on('click', '.saveD', function(e) {
    e.preventDefault()
    var mas = jsM.mind.nodes;
    var count_idea_create = 0;

    for (var key in mas) {
      if (key !='root'){
        count_idea_create++;
      }
    };
    var id = $(this).data('id')

    if (id == 'stage') {
      if(count_idea > (count_idea_create-defoult_count_idea)) {
        swal({
          title: "",
          text: "Please confirm that you have checked all the other ideas to ensure you’re not making any obvious repeats.",
          type: "warning",
          showCancelButton: true,
          confirmButtonText: "Confirm",
          cancelButtonText: "Let me double check",
          closeOnConfirm: false,
          closeOnCancel: true
        }, function (isConfirm) {
          if (isConfirm) {
            // alert(window.location.host)
            save('stage')
          }
        });
      }else {
        save('stage')
        //alert(count_idea_create+'|'+count_idea_p);
      }
    } else {
      save('exit')
    }
  })
});
