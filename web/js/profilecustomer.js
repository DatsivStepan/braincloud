$(document).ready(function(){
    
    $(document).on('change','.selectCountry',function(){
        var country_id = $(this).val();
        
        if(country_id != 0){
            $.ajax({
                type: 'POST',
                url: '../../customer/getcity',
                data: {country_id:country_id},
                dataType: "json",
                success: function(response){
                    //console.log(response);
                    if(response.status == 'good'){
                        $('.selectCity').html('');
                        $('.selectCity').append('<option>Select City</option>');
                        for(var key in response.city){
                            $('.selectCity').append('<option value='+key+'>'+response.city[key]+'</option>');
                        }
                    }
                }
            });
        }else{
            $('.selectCity').html('');
            $('.selectCity').append('<option>First select a country</option>');
        }
        
    });
    
    $(document).on('click','.showHint',function(e){
        e.preventDefault();
        $(this).parent().find('p').toggle();
    });
    
    if($('.clicable').length){

        var myDropzone = new Dropzone($('.clicable')[0], { // Make the whole body a dropzone
            maxFiles:1,
            uploadMultiple:false,
            acceptedFiles:'image/*',
            url: "../../site/savedprofilephoto", // Set the url
            previewsContainer: "#previewsS", // Define the container to display the previews
        });

        myDropzone.on("maxfilesexceeded", function(file) {
                myDropzone.removeAllFiles();
                myDropzone.addFile(file);
        });

        myDropzone.on("complete", function(response) {
            if (response.status == 'success') {
                $('#customerinfo-image_name').val(response.xhr.response);
                $('.profilePhoto').attr('src','../../../image/users_images/'+response.xhr.response);
            } else {
                swal("Not download avatar!", "The file is too large.", "warning")
            }
        });

        myDropzone.on("removedfile", function(response) {
            if(response.xhr != null){
               deleteFile(response.xhr.response);
            }
        });
    }
});