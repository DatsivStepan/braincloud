$(document).ready(function(){
    
    $(document).on('keyup','input[name=recipient_name]',function(){
        if($(this).val().length >= 1){
            $('.RecipientName').text($(this).val());
        }else{
            $('.RecipientName').text('Recipient name');
        }
    });
    
    $(document).on('click','input[name=referalButton]',function(e){
        e.preventDefault();
        var recipient_name = $('input[name=recipient_name]');
        var recipient_email = $('input[name=recipient_email]');
        var senders_email = $('input[name=senders_email]');
        
        if((recipient_name.val().length >= 3) && (recipient_email.val().length >= 3) && (senders_email.val().length = 3)){
            $.ajax({
                url:'../../../customer/sendreferal',
                method:'post',
                data:{masseage:$('#messageTamplate').text(),recipient_name:recipient_name.val(),recipient_email:recipient_email.val(),senders_email:senders_email.val()},
                dataType:'json',
            }).done(function(responce){
                if(responce.status == 'send'){
                   swal("Good job!", "Thank you! Your message has been sent!", "success")
                }else{
                    
                }
            });
        }else{
            if(recipient_name.val().length <= 3){
                recipient_name.parent().addClass('has-error');
                recipient_name.parent().removeClass('has-success');
            }else{
                recipient_name.parent().addClass('has-success');
                recipient_name.parent().removeClass('has-error');
            }
            if(recipient_email.val().length <= 3){
                recipient_email.parent().addClass('has-error');
                recipient_email.parent().removeClass('has-success');
            }else{
                recipient_email.parent().addClass('has-success');
                recipient_email.parent().removeClass('has-error');
            }
            if(senders_email.val().length <= 3){
                senders_email.parent().addClass('has-error');
                senders_email.parent().removeClass('has-success');
            }else{
                senders_email.parent().addClass('has-success');
                senders_email.parent().removeClass('has-error');
            }
        }        
    });

    $(document).on('click','input[name=referalstormerButton]',function(e){
        e.preventDefault();
        var recipient_name = $('input[name=recipient_name]');
        var recipient_email = $('input[name=recipient_email]');
        var senders_email = $('input[name=senders_email]');

        if((recipient_name.val().length >= 3) && (recipient_email.val().length >= 3) && (senders_email.val().length = 3)){
            $.ajax({
                url:'../../../stormer/sendreferal',
                method:'post',
                data:{masseage:$('#messageTamplate').text(),recipient_name:recipient_name.val(),recipient_email:recipient_email.val(),senders_email:senders_email.val()},
                dataType:'json',
            }).done(function(responce){
                if(responce.status == 'send'){
                    swal("Good job!", "Thank you! Your message has been sent!", "success")
                }else{

                }
            });
        }else{
            if(recipient_name.val().length <= 3){
                recipient_name.parent().addClass('has-error');
                recipient_name.parent().removeClass('has-success');
            }else{
                recipient_name.parent().addClass('has-success');
                recipient_name.parent().removeClass('has-error');
            }
            if(recipient_email.val().length <= 3){
                recipient_email.parent().addClass('has-error');
                recipient_email.parent().removeClass('has-success');
            }else{
                recipient_email.parent().addClass('has-success');
                recipient_email.parent().removeClass('has-error');
            }
            if(senders_email.val().length <= 3){
                senders_email.parent().addClass('has-error');
                senders_email.parent().removeClass('has-success');
            }else{
                senders_email.parent().addClass('has-success');
                senders_email.parent().removeClass('has-error');
            }
        }
    });

    $(document).on('click','input[name=referalstormersalesButton]',function(e){
        e.preventDefault();
        var recipient_name = $('input[name=recipient_name]');
        var recipient_email = $('input[name=recipient_email]');
        var senders_email = $('input[name=senders_email]');

        if((recipient_name.val().length >= 3) && (recipient_email.val().length >= 3) && (senders_email.val().length = 3)){

            $.ajax({
                url:'../../../customer/sendreferal',
                method:'post',
                data:{ message:$('#messageTamplate').text(),recipient_name:recipient_name.val(),recipient_email:recipient_email.val(),senders_email:senders_email.val()},
                dataType:'json',
            }).done(function(responce){
                if(responce.status == 'send'){
                    console.log(responce);
                    swal("Good job!", "Thank you! Your message has been sent!", "success");
                    //window.location = "/stormer/joinsalesteam2";
                }else{

                }
            });
        }else{
            if(recipient_name.val().length <= 3){
                recipient_name.parent().addClass('has-error');
                recipient_name.parent().removeClass('has-success');
            }else{
                recipient_name.parent().addClass('has-success');
                recipient_name.parent().removeClass('has-error');
            }
            if(recipient_email.val().length <= 3){
                recipient_email.parent().addClass('has-error');
                recipient_email.parent().removeClass('has-success');
            }else{
                recipient_email.parent().addClass('has-success');
                recipient_email.parent().removeClass('has-error');
            }
            if(senders_email.val().length <= 3){
                senders_email.parent().addClass('has-error');
                senders_email.parent().removeClass('has-success');
            }else{
                senders_email.parent().addClass('has-success');
                senders_email.parent().removeClass('has-error');
            }
        }
    });

});