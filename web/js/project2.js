$(document).ready(function(){
        var previewNode = document.querySelector("#template");
            console.log(previewNode);
    if(previewNode) {


        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);
        var myDropzone = new Dropzone($('.dropzonefile')[0], { // Make the whole body a dropzone
            url: "../../../savedropedfile", // Set the url
            maxFilesize: 50000,
            autoQueue: true, // Make sure the files aren't queued until manually added
            previewTemplate: previewTemplate,
            previewsContainer: "#previews", // Define the container to display the previews
            clickable: ".fileinput-button" // Define the element that should be used as click trigger to select files.
        });
        myDropzone.on("addedfile", function (file) {
            // Hookup the start button
            //file.previewElement.querySelector(".start").onclick = function() { myDropzone.enqueueFile(file); };
        });
        // Update the total progress bar
        myDropzone.on("totaluploadprogress", function (progress) {
            document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
        });
        myDropzone.on("complete", function (response) {
            if (response.status == 'success') {
                $(response.previewElement).find('.path').val(response.xhr.response)
            }
        });
        myDropzone.on("removedfile", function (response) {
            console.log(response)
        });
        myDropzone.on("sending", function (file) {
            // Show the total progress bar when upload starts
            document.querySelector("#total-progress").style.opacity = "1";
            // And disable the start button
            // file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
        });
        // Hide the total progress bar when nothing's uploading anymore
        myDropzone.on("queuecomplete", function (progress) {
            document.querySelector("#total-progress").style.opacity = "0";
        });
        // Setup the buttons for all transfers
        // The "add files" button doesn't need to be setup because the config
        // `clickable` has already been specified.
        // document.querySelector("#actions .start").onclick = function(e) {
        //   e.preventDefault()
        //   myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED));
        // };
        document.querySelector("#actions .cancel").onclick = function () {
            myDropzone.removeAllFiles(true);
        };

    }
            
            
            $(document).on('click','#add_link',function(){
                
                if($('input[name=link_name]').val() != ''){
                    var href = $('input[name=link_name]').val();
                    $('input[name=link_name]').val('');
                    $('.linkContainer').append('\
                        <div class="row">'+
                            '<div class="col-sm-12">'+
                                '<input type="hidden" name="Link[link][]" class="form-control" value="'+href+'">'+
                                '<h4 class="link">'+href+'</h4>'+
                            '</div>'+
                            '<div class="col-sm-6">'+
                                '<label>Brief description of link '+
                                    '<a class="infoButtonShow">Support..'+
                                    '</a>'+
                                    '<div class="infoBlockShow">'+
                                        'Add links to support your project. Don’t forget to mention why you’re including these links.'+
                                    '</div>'+
                                '</label>'+
                                '<input type="text" name="Link[brief_link][]" class="form-control" placeholder="Brief description" required="">'+
                            '</div>'+
                            '<div class="col-sm-6">'+
                                '<a data-dz-remove class="btn deleteLink" onclick="delete_link(this)"> '+
                                    '<i class="glyphicon glyphicon-trash"></i>'+
                                    '<span>Delete</span>'+
                                '</a>'+
                            '</div>'+
                        '</div>');
                }
                
            });
            
            $(document).on('click','.avtomaticalPerspectiveQuestion',function(){
              alert('ld')
                 $.ajax({
                    type: 'POST',
                    url: '../../../customer/getperspectivequestion',
                    data: {},
                    dataType: "json",
                    success: function(response){
                      console.log(response);
                        $('#projects-perspectives').text(response);
                    }
                });
            });

            function delete_link(elment){
                $( elment ).parent().parent().remove();
            }
            function convergentQuestion(question_id){
              console.log(question_id);
                $.ajax({
                    type: 'POST',
                    url: '../../../customer/getquestiontext',
                    data: {question_type:'convergent',question_id:question_id},
                    dataType: "json",
                    success: function(response){
                      console.log(response);
                        $('.example_convergent').val(response);
                    }
                });
            }
            $(document).on('change','#projects-question_convergent',function(){
                convergentQuestion($(this).val());
            });
            
            $(document).on('click','.avtomaticalConvergent',function(){
                var $options = $('#projects-question_convergent').find('option'),
                random = ~~(Math.random() * $options.length);
              console.log($options);
                if(random == ''){
                    random = ~~(Math.random() * $options.length);
                }
                if(random == ''){
                    random = ~~(Math.random() * $options.length);
                }
              console.log($options[random].value);
                convergentQuestion($options[random].value);
                $options.eq(random).prop('selected', true);
            });
            
            
            
            function divergentQuestion(question_id){
                $.ajax({
                    type: 'POST',
                    url: '../../../customer/getquestiontext',
                    data: {question_type:'divergent',question_id:question_id},
                    dataType: "json",
                    success: function(response){
                        $('.example_divergent').val(response);
                    }
                });
            }
            $(document).on('change','#projects-question_divergent',function(){
                divergentQuestion($(this).val());
            });
            $(document).on('click','.avtomaticalDivergent',function(){
                var $options = $('#projects-question_divergent').find('option'),
                random = ~~(Math.random() * $options.length);
                if(random == ''){
                    random = ~~(Math.random() * $options.length);
                }
                if(random == ''){
                    random = ~~(Math.random() * $options.length);
                }
                divergentQuestion($options[random].value);
                $options.eq(random).prop('selected', true);
            });
});