function initJSmind(mind) {
  var options = {
    container: 'jsm_container',
    theme: 'greensea',
    editable: true,
    mode: 'side',
    clickable: true,
    layout: {
      hspace: 100,

    },
  }
  return jsM = jsMind.show(options, mind);

}

function save(type) {
  var mind_data = jsM.get_data('node_tree');
  var mind_string = jsMind.util.json.json2string(mind_data);
  $.ajax({
    type: 'POST',
    url: 'save',
    data: {
      minds: mind_string,
      project_id: project_id,
      type: type
    },
    success: function(data) {
      swal({
        title: 'OK!',
        text: 'Your data stored successfully',
        type: "success"
      }, function(isConfirm) {
        if (isConfirm) {
          window.location.replace(window.location.protocol + "//" + window.location.host + data)
            // save('stage')
        }
      })
    },
    error: function(data) {
      swal({
        title: 'Oooops!',
        text: data,
        type: "error"
      })
    }
  });
}

$(document).on('click', 'jmup', function() {
  console.log(this.getAttribute("nodeid"))
  var items = jsM.get_node(this.getAttribute("nodeid"));
  console.log('node',items.topic);
  $('#idea_input_update').text(items.topic);
  $('#idea_input_update').val(items.topic);
  $('#nodeid').val(items.id);
  $('#idea_up').modal('show');
})

$(document).on('click', '#jsm_container', function() {

  var items = jsM.get_selected_node();
  console.log('select element',items);
  //alert(items.data.value)
  if(items){
    if (items.id != 'root'){
      console.log(items.topic)
      // $('#update').show();
      // $('.idea-sescription').val(items.topic);
      $('#description_idea').text(items.topic);
      $('#idea_view').modal('show');
    }else{
    // $('#update').hide();
    $('.idea-sescription').val('');
  }
  }else{
    // $('#update').hide();
    $('.idea-sescription').val('');
  }

});

$(document).ready(function() {

  innovation_next(1);

  $('jmnode').focus(function(){

  });
  console.log(mindM);
  console.log((mindM));
  var jsM = initJSmind(mindM)
  jsM._event_bind = function() {

    }

  $(document).on('click', 'jmnode', function() {
    console.log(jsM.get_selected_node().id);
    var items = jsM.get_selected_node();

    //alert(items.data.value)
    if (items.id == 'root'){
      $('#root').modal('show');
    }
    //if (items.data.value == 'stormer'){
    //    $('#stormer'+items.id).modal('show');
    //}

  });
    // $('jmnode').readmore()
    // $('#myModal').modal('show');
if(one_show_ms)
  if (project=='customer'){
    swal({
      title: '',
      type: 'info',
      text: "Welcome! In order to get the finest experience, please follow the instructions as best you can. If you get stuck, click the ‘Tips and Help’ button. (You can leave and save your work anytime, and come back to it later, although there is a time bonus for completing it in the first 48 hours!) if customer projects. Your quota is "+count_idea+" ideas. Please don’t forget that one idea includes all three stages of the ideation. Your ideas will not be counted if all three stages are not completed. Good luck!",
      confirmButtonText: "Start Now!",
    });
  }else{
    swal({
      title: '',
      type: 'info',
      text: "Your quota is "+count_idea+" ideas. Please don’t forget that one idea includes all three stages of the ideation. Your ideas will not be counted if all three stages are not completed. Good luck!",
      confirmButtonText: "Start Now!",
    });
  }

  $('#update').click(function(e) {
    e.preventDefault();
    var items = jsM.get_node($('#nodeid').val());

    //alert(items.data.value)
    if(items) {
      var topic = $('#idea_input_update').val();
      if (!topic) {
        swal({
          title: 'Write your idea first!'
        })
      } else {
       /* if (topic.length > 50) {
          var topic = topic.slice(0, 25) + "<a class='Readmore'> Readmore</a><span class='hiddenText'>" + topic.slice(25) + '</span>';
        }*/
        topic = topic.replace(/(?:\r\n|\r|\n)/g, '<br />');
        var node = jsM.update_node( items.id, topic);
        //$('.idea-sescription').val('');
        instruction++;
        if(instruction == 2){
          innovation_next(1);
          inspire_next();
          instruction=0;
        }
      }
      $('#idea_up').modal('hide');
    }
  });


  $('.add-node').click(function(e) {
    e.preventDefault()

var mas = jsM.mind.nodes;
var count_idea_create = 0;



    for (var key in mas) {
        console.log(key);
       if (key !='root'){
         count_idea_create++;
       }
    };
    if(count_idea > count_idea_create){

      // var selected_node = jsM.get_selected_node(); // as parent of new nod/
      selected_node = jsM.get_root();
      var nodeid = jsMind.util.uuid.newid();
      var topic = $('.idea-sescription').val();
      if (!topic) {
        swal({
          title: 'Write your idea first!'
        })
      } else {
        /*if (topic.length > 50) {
          var topic = topic.slice(0, 25) + "<a class='Readmore'> Readmore</a><span class='hiddenText'>" + topic.slice(25) + '</span>';
        }*/
        topic = topic.replace(/(?:\r\n|\r|\n)/g, '<br />');
        var node = jsM.add_node(selected_node, nodeid, topic);
        instruction++;
        if(instruction == 2){
          instruction_show(1);
          inspire_next();
          instruction=0;
        }
        $('.idea-sescription').val('');
      }
    }else{
      swal({
        title: 'Not create idea!',
        text: "For this project, you can create only "+count_idea+" ideas."
      })
    }

  })
  $('.remove-node').click(function(e) {
    e.preventDefault()
    var selected_node = jsM.get_selected_node();
    if (!selected_node) {
      swal({
        'title': 'select idea first'
      })
    } else {
      jsM.remove_node(selected_node);
    } // as parent of new nod/

  })
  $(document).on('click', '.Readmore', function(e) {
    e.preventDefault()
    var nodeid = $(this).parent().attr('nodeid')
    $(this).parent().find('.hiddenText').addClass('showedText').removeClass('hiddenText')
    var some = $(this).parent()
    $(this).parent().append('<a class="Readless"> Readless</a>')
    $(this).remove()
    jsM.update_node(nodeid, some.html())

  })
  $(document).on('click', '.Readless', function(e) {
    e.preventDefault()
    var nodeid = $(this).parent().attr('nodeid')
    $(this).parent().find('.showedText').addClass('hiddenText').removeClass('showedText').before('<a class="Readmore"> Readmore</a>')
    some = $(this).parent();
    // $(this).parent().append('<a class="Readless"> Readless</a>')
    $(this).remove()
    jsM.update_node(nodeid, some.html())
    console.log(jsM);
  })
  $(document).on('click', '.saveD', function(e) {
    e.preventDefault();
    var mas = jsM.mind.nodes;
    var count_idea_create = 0;

    for (var key in mas) {
      if (key !='root'){
        count_idea_create++;
      }
    };

    var id = $(this).data('id')
    if (id == 'stage') {
      if (peer){
        save('stage');
      } else {
        if (count_idea == count_idea_create) {
          /*swal({
           title: "",
           text: "You have not posted all ideas. Please double check before moving to next stage.",
           type: "warning",
           showCancelButton: true,
           confirmButtonText: "Confirm",
           cancelButtonText: "Let me double check",
           closeOnConfirm: false,
           closeOnCancel: true
           }, function (isConfirm) {
           if (isConfirm) {
           save('stage')
           }
           });*/
          save('stage');
        } else {
          swal({
            title: 'Not next stage!',
            text: "You have not posted " + count_idea + " ideas. Please double check before moving to next stage.",
            type: "warning",
          })
        }
      }
    } else {
      if(count_idea < count_idea_create) {
        swal({
          title: "",
          text: "You have not posted all ideas. Please double check before moving to next stage.",
          type: "warning",
          showCancelButton: true,
          confirmButtonText: "Confirm",
          cancelButtonText: "Let me double check",
          closeOnConfirm: false,
          closeOnCancel: true
        }, function (isConfirm) {
          if (isConfirm) {
            save('exit')
          }
        });
      }else{
        save('exit')
      }

    }
  })
});
