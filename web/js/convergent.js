function initJSmind(mind) {
  var options = {
    container: 'jsm_container',
    theme: 'greensea',
    editable: true,
    mode: 'full',
    clickable: true,
    layout: {       
      hspace: 150,
         
    },
  }
  return jsM = jsMind.show(options, mind);

}

function save(type) {
  var mind_data = jsM.get_data('node_array');
  var mind_string = jsMind.util.json.json2string(mind_data);
  $.ajax({
    type: 'POST',
    url: 'save',
    data: {
      minds: mind_string,
      project_id: project_id,
      type: type
    },
    success: function(data) {
      if (type == 'exit'){
        window.location = '/stormer/myprojects';
      }else{
        window.location = '/stormer/project_completed/'+project_id;
      }

    },
    error: function(data) {
      swal({
        title: 'Oooops!',
        text: data,
        type: "error"
      })
    }
  });

  /*var mas = jsM.mind.nodes;
  var count_idea_create = 0;
  for (var key in mas) {
    if (key !='root'){
      count_idea_create++;
    }
  };
  swal({
    title: 'aaand we’re done!\nrWe have received '+parseInt(count_idea_create)+' ideas, and earned $'+parseInt(count_idea_create)*parseFloat(price)+'. You’ve met your quota! Well done!',
    text: 'We are now awaiting confirmation from the customer to review and close the project. It is possible that we may have to request modifications, so please keep a close eye on your emails and notifications.',
    type: "success",
    confirmButtonText: "Take Me Home"
  }, function(isConfirm) {
    if (isConfirm) {
      window.location = '/'
      // save('stage')
    }
  })*/
}

$(document).on('click', 'jmup', function() {
  console.log(this.getAttribute("nodeid"))
  var items = jsM.get_node(this.getAttribute("nodeid"));
  console.log('node',items.topic);
  $('#idea_input_update').text(items.topic);
  $('#idea_input_update').val(items.topic);
  $('#nodeid').val(items.id);
  $('#idea_up').modal('show');
})

$(document).on('click', '#jsm_container', function() {

  var items = jsM.get_selected_node();
  console.log('select', items);
  if(items){
    if(items.id != 'root'){
      $('#description_idea').text(items.topic);
      $('#idea_view').modal('show');
      if ((items.data.value == true)){
        $('#add').show();
      } else{
        $('#add').hide();
      }
    }
      /*if ((items.data.value != true)){
        console.log(items.topic)
        $('#update').show();
        $('#add').hide();
        $('.idea-sescription').val(items.topic);
      }else{
        $('#desc_idea').text(items.topic);
        $('#idea').modal('show');

        $('#update').hide();
        $('#add').show();
        $('.idea-sescription').val('');
      }*/

  }else{
    $('#update').hide();
    $('#add').hide();
    $('.idea-sescription').val('');
  }

});

$(document).on('click', 'jmnode', function() {
  console.log(jsM.get_selected_node().id);
  var items = jsM.get_selected_node();

  //alert(items.data.value)
  if (items.id == 'root'){
    $('#root').modal('show');
  }
  //if (items.data.value == 'stormer'){
  //    $('#stormer'+items.id).modal('show');
  //}

});
$(document).ready(function() {


  innovation_next(3);
  console.log(mindM);
  console.log((mindM));
  var jsM = initJSmind(mindM)
  jsM._event_bind = function() {

  }
  $('#update').click(function(e) {
    e.preventDefault();
    // var items = jsM.get_selected_node();
    var items = jsM.get_node($('#nodeid').val());
    //alert(items.data.value)
    if(items) {
      var topic = $('.idea-sescription').val();
      if (!topic) {
        swal({
          title: 'Write your idea first!'
        })
      } else {
        /*if (topic.length > 50) {
          var topic = topic.slice(0, 25) + "<a class='Readmore'> Readmore</a><span class='hiddenText'>" + topic.slice(25) + '</span>';
        }*/
        topic = topic.replace(/(?:\r\n|\r|\n)/g, '<br />');
        var node = jsM.update_node( items.id, topic);
        instruction++;
        if(instruction == 2){
          innovation_next(3);
          inspire_next();
          instruction=0;
        }
      }
      $('#idea_up').modal('hide');
    }
  });

  $('.add-node').click(function(e) {
    e.preventDefault()
    var selected_node = jsM.get_selected_node(); // as parent of new nod/

    if((selected_node.children.length < 1) && (selected_node.data.value == true)) {

      // selected_node = jsM.get_root()
      var nodeid = jsMind.util.uuid.newid();
      var topic = $('.idea-sescription').val();
      if (selected_node) {
        if (!topic) {
          swal({
            title: 'Write your idea first!'
          })
        } else {
          /*if (topic.length > 50) {
            var topic = topic.slice(0, 25) + "<a class='Readmore'> Readmore</a><span class='hiddenText'>" + topic.slice(25) + '</span>';
          }*/
          topic = topic.replace(/(?:\r\n|\r|\n)/g, '<br />');
          var node = jsM.add_node(selected_node, nodeid, topic, {value: false});
          $('.idea-sescription').val('');
          instruction++;
          if(instruction == 2){
            instruction_show(3);
            inspire_next();
            instruction=0;
          }
        }
      } else {
        swal({
          title: 'Select node first!'
        })
      }
    }else {
      swal({
        title: 'Not create idea!',
        text: "For this idea, you can create only 1 idea."
      })
    }
  })

  $(document).on('click', '.Readmore', function(e) {
    e.preventDefault()
    var nodeid = $(this).parent().attr('nodeid')
    $(this).parent().find('.hiddenText').addClass('showedText').removeClass('hiddenText')
    var some = $(this).parent()
    $(this).parent().append('<a class="Readless"> Readless</a>')
    $(this).remove()
    jsM.update_node(nodeid, some.html())

  })

  $(document).on('click', '.Readless', function(e) {
    e.preventDefault()
    var nodeid = $(this).parent().attr('nodeid')
    $(this).parent().find('.showedText').addClass('hiddenText').removeClass('showedText').before('<a class="Readmore"> Readmore</a>')
    some = $(this).parent();
    $(this).remove()
    jsM.update_node(nodeid, some.html())
    console.log(jsM);
  })

  $(document).on('click', '.saveD', function(e) {
    e.preventDefault();
    var mas = jsM.mind.nodes;
    var count_idea_create = 0;

    for (var key in mas) {
      if (key !='root'){
        count_idea_create++;
      }
    };
    console.log('count create idea',count_idea_create);
    console.log('count create',count_idea);
    var id = $(this).data('id')
    if (id == 'stage') {
      if((count_idea*2) == count_idea_create) {
        save('stage')
      }else{
        //save('stage')
        swal({
          title: "",
          text: "You have not posted all ideas. Please double check before moving to next stage.",
          type: "warning",
        });
      }
    } else {
      save('exit')
    }
  })
});
