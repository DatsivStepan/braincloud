<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap-datetimepicker.min.css',
        'css/sweetalert.css',
        'css/site.css',
        //'css/bootstrap-multiselect.css',
        'css/jquery-ui.css',
        'css/croppic.css',
        'css/main.css',
        "https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css",
        // 'https://rawgit.com/enyo/dropzone/master/dist/dropzone.css',
        'css/star-rating.css',
        'css/animate.min.css',
        'css/style.css',
        'css/style_sass.css',
        'css/jquery.toastmessage.css'
    ];
    public $js = [
        'js/bootstrap-datetimepicker.min.js',
        //'js/jquery-ui.js',
        'js/sweetalert.min.js',
        //'js/bootstrap-multiselect.js',
        "https://www.google.com/recaptcha/api.js",
        'js/croppic.min.js',
        'js/dropzone.js',
        'js/star-rating.js',
        'js/main.js',
        'js/wow.min.js',
        'js/custom.js',
        'js/jquery.toastmessage.js'

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
