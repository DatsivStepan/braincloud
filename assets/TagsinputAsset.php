<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class TagsinputAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/tagsinput/bootstrap-tagsinput.css',
        'css/tagsinput/bootstrap-theme.min.css',
    ];
    public $js = [
        'js/tagsinput/typeahead.bundle.min.js',
        'js/tagsinput/bootstrap-tagsinput.min.js',
        'js/city.js',
        'js/dropzone.js',
        'js/customer_step2.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
