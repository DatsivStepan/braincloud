<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Flaggingideas;

/**
 * FlaggingideasSearch represents the model behind the search form of `app\models\Flaggingideas`.
 */
class FlaggingideasSearch extends Flaggingideas
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'idea_id', 'customer_id', 'created_at', 'updated_at'], 'integer'],
            [['why_flagging'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Flaggingideas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'idea_id' => $this->idea_id,
            'customer_id' => $this->customer_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);
        $query->orderBy(['created_at'=>SORT_DESC]);

        $query->andFilterWhere(['like', 'why_flagging', $this->why_flagging]);

        return $dataProvider;
    }
}
