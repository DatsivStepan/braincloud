<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Projects;

/**
 * ProjectSearch represents the model behind the search form about `app\models\Projects`.
 */
class ProjectSearch extends Projects
{
    /**
     * @inheritdoc
     */
    public $array;
    public function rules()
    {
        return [
            [['id', 'owner_id', 'type', 'size_team', 'count_idea', 'status','ngo','be_inspired'], 'integer'],
            [['array','active','title', 'description', 'goals', 'evaluation_benchmark', 'end_date', 'create_at', 'perspectives', 'category', 'question_convergent', 'question_divergent', 'question_custom','project_type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Projects::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $query->orderBy(['id'=>SORT_DESC]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->active == 'Yes' || $this->active == 'yes' || $this->active == 'y' || $this->active == 'Y'){
            $this->active = 1;
        }
        if ($this->active == 'No' || $this->active == 'no' || $this->active == 'n' || $this->active == 'N'){
            $this->active = 0;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'owner_id' => $this->owner_id,
            'active' => $this->active,
            'type' => $this->type,
            'end_date' => $this->end_date,
            'create_at' => $this->create_at,
            'be_inspired' => $this->be_inspired,
            'size_team' => $this->size_team,
            'count_idea' => $this->count_idea,
            'status' => $this->status,
            'ngo' => $this->ngo,
            'id' => $this->array,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'goals', $this->goals])
            ->andFilterWhere(['like', 'evaluation_benchmark', $this->evaluation_benchmark])
            ->andFilterWhere(['like', 'perspectives', $this->perspectives])
            ->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'project_type', $this->project_type])
            ->andFilterWhere(['like', 'question_convergent', $this->question_convergent])
            ->andFilterWhere(['like', 'question_divergent', $this->question_divergent])
            ->andFilterWhere(['like', 'question_custom', $this->question_custom]);

        return $dataProvider;
    }
}
