<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transactions".
 *
 * @property integer $id
 * @property integer $project_id
 * @property string $payer_email
 * @property string $txn_id
 * @property string $first_name
 * @property string $payer_id
 * @property double $payment_gross
 * @property string $last_name
 * @property string $residence_country
 */
class Transactions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transactions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'payer_email', 'txn_id', 'first_name', 'payer_id', 'payment_gross', 'last_name', 'residence_country'], 'required'],
            [['project_id'], 'integer'],
            [['payment_gross'], 'number'],
            [['payer_email', 'txn_id', 'first_name', 'payer_id', 'last_name'], 'string', 'max' => 255],
            [['residence_country'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project ID',
            'payer_email' => 'Payer Email',
            'txn_id' => 'Txn ID',
            'first_name' => 'First Name',
            'payer_id' => 'Payer ID',
            'payment_gross' => 'Payment Gross',
            'last_name' => 'Last Name',
            'residence_country' => 'Residence Country',
        ];
    }
}
