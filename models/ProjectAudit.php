<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "project_audit".
 *
 * @property integer $id
 * @property string $title
 * @property integer $size_team
 * @property integer $count_idea
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $token
 * @property integer $project_id
 * @property integer $status
 */
class ProjectAudit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project_audit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['size_team', 'count_idea', 'created_at', 'updated_at', 'project_id','status'], 'integer'],
            [['title', 'token'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'size_team' => 'Size Team',
            'count_idea' => 'Count Idea',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'token' => 'Token',
            'project_id' => 'Project ID',
            'status' => 'Status',
        ];
    }
}
