<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $subject;
    public $body;
    public $send_me;
    public $verifyCode;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'subject', 'body'], 'required'],
            // email has to be a valid email address
            ['send_me','string'],
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param  string  $email the target email address
     * @return boolean whether the model passes validation
     */
    public function contact($email)
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([$this->email => $this->name])
                ->setSubject($this->subject)
                ->setHtmlBody('My email - '.$this->email.'<br>'.$this->body)
                ->send();

            if ($this->send_me == true){
                Yii::$app->mailer->compose()
                    ->setTo($this->email)
                    ->setFrom([$this->email => $this->name])
                    ->setSubject($this->subject)
                    ->setHtmlBody('My email - '.$this->email.'<br>'.$this->body)
                    ->send();
            }

            $contact = new ContactUs();
            $contact->username=$this->name;
            $contact->email=$this->email;
            $contact->subject_mail=$this->subject;
            $contact->descriptions=$this->body;
            $contact->send_me=$this->send_me;
            if (Yii::$app->user->isGuest){
                $contact->parent_id=0;
            }else{
                $contact->parent_id=Yii::$app->user->id;
            }
            $contact->save();
            return true;
        }
        return false;
    }
}
