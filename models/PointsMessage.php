<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "points_message".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $message
 * @property integer $show
 * @property integer $url
 */
class PointsMessage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'points_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'show', 'url'], 'integer'],
            [['message'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'message' => 'Message',
            'show' => 'Show',
            'created_at' => 'Date',
        ];
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

  public function beforeSave($insert){

      $user = User::find()->where(['id'=>$this->user_id])->one();

      if($user) {
        if ($user->users_type == 'stormer') {
          if(($user->stormerinfo['notifications']==1) && ($user->stormerinfo['notification_project']==1) && ($user->stormerinfo['notifications_off']==2)){
            Yii::$app->mailer->compose()
              ->setFrom(Yii::$app->params['supportEmail'])
              ->setTo($user->email)
              ->setSubject('Braincloud notification')
              ->setTextBody($this->message)
              ->send();
          } elseif(strpos($this->message, 'New level') === true){
            Yii::$app->mailer->compose()
              ->setFrom(Yii::$app->params['supportEmail'])
              ->setTo($user->email)
              ->setSubject('Braincloud notification')
              ->setTextBody($this->message)
              ->send();
          }

        }
        if ($user->users_type == 'customer') {
          if(($user->customerinfo['notifications']==1) && ($user->customerinfo['notification_project']==1) && ($user->customerinfo['notifications_off']==2)) {
            Yii::$app->mailer->compose()
              ->setFrom(Yii::$app->params['supportEmail'])
              ->setTo($user->email)
              ->setSubject('Braincloud notification')
              ->setTextBody($this->message)
              ->send();
          } elseif(strpos($this->message, 'New level') === true){
            Yii::$app->mailer->compose()
              ->setFrom(Yii::$app->params['supportEmail'])
              ->setTo($user->email)
              ->setSubject('Braincloud notification')
              ->setTextBody($this->message)
              ->send();
          }
        }
      }


    return parent::beforeSave($insert);
  }

}
