<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "one_show_message".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $option
 * @property integer $status
 * @property string $type
 */
class OneShowMessage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'one_show_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'option', 'status'], 'integer'],
            [['type'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'option' => 'Option',
            'status' => 'Status',
            'type' => 'Type',
        ];
    }
}
