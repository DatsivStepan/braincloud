<?php

namespace app\models;

use \yii\web\IdentityInterface;
use Yii;

class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    public $password_repeat;
    public $accept_terms;

    public static function tableName()
    {
        return 'users';
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findByUsername($username)
    {
        return static::findOne(array('username' => $username));
    }

    public static function findById($id)
    {
        return static::findOne(array('id' => $id));
    }
    /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->id;
    }

    public function scenarios()
    {
        return [
            'login' => ['username', 'password'],
            'registration' => ['username', 'email', 'password', 'password_repeat', 'users_type', 'accept_terms','parent_id','name'],
            'registration_s' => ['username', 'email', 'password', 'password_repeat', 'users_type', 'signup_step', 'active','parent_id','name'],
            'change_status_registration' => ['signup_step','active'],
            'change_status_registration_email' => ['signup_step','active', 'email'],
            'generate_token' => ['activation_token'],
            'level' => ['level'],
            'update'=>['email'],
            'active_token'=>['active','activation_token'],
            'referal_key' => ['referal_key'],
            'social_reg' => ['username', 'password', 'password_repeat', 'users_type','parent_id','name','google_id','facebook','twitter_id'],
	];
    }

    public function validatePassword($password)
    {
        return \Yii::$app->security->validatePassword($password, $this->password_hash);
    }
    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @param string $authKey
     * @return boolean if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            ['username', 'unique'],
            ['email','required'],
            ['email','email'],
            //['name','unique'],
            ['username', 'match','pattern'=>'/^[A-Za-z0-9_.]+$/', 'message' => 'You entered invalid characters.'],
            ['email','unique'],
            ['accept_terms', 'required'],
            ['accept_terms', 'compare', 'compareValue' => 1, 'message' => 'You should accept terms and agreement'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['password_repeat', 'required'],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ],

        ];
    }
    public static function isActivationTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
            //print_r([$expire,$timestamp]);exit;
        return $timestamp + $expire >= time();
    }
    /**
     * Generates new password reset token
     */
    public function generateActivationToken()
    {
        $this->activation_token = Yii::$app->security->generateRandomString() . '_' . time();
    }
    
    public static function findByActivationToken($token)
    {
        if (!static::isActivationTokenValid($token)) {
            return null;
        }
        return static::findOne([
            'activation_token' => $token
        ]);
    }
    
    public static function getUsername($user_id){
        $userModel = static::findOne(array('id' => $user_id));
        
        $username = $userModel->username;
        return $username;
    }
    
    public function removeActivationToken()
    {
        $this->activation_token = null;
    }

    public function referal()
    {
        return Yii::$app->security->generateRandomString(2).$this->id;
    }
    public function getPoints()
    {
        return $this->hasMany(Points::className(), ['user_id' => 'id']);
    }
    public function getBan()
    {
        return $this->hasOne(UserBan::className(), ['stormer_id' => 'id']);
    }

  public function getStormerinfo()
  {
    return $this->hasOne(Stormerinfo::className(), ['stormer_id' => 'id']);
  }

  public function getCustomerinfo()
  {
    return $this->hasOne(Stormerinfo::className(), ['stormer_id' => 'id']);
  }

    public function beforeSave($insert){
        if ($this->isNewRecord){
            if (!$this->username){
                $string = preg_replace( '/[^\p{L}\p{Nd}]+/u', '_', $this->name );
                $this->username = strtolower($string);
            }
        }
        return parent::beforeSave($insert);
    }



    /*public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'app\commands\Slug',
                'in_attribute' => 'name',
                'out_attribute' => 'username',
                'translit' => false
            ]
        ];
    }*/
}
