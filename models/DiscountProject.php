<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "discount_project".
 *
 * @property integer $id
 * @property integer $count
 * @property integer $discount
 */
class DiscountProject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'discount_project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['count', 'discount'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'count' => 'Count',
            'discount' => 'Discount',
        ];
    }
}
