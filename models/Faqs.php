<?php

namespace app\models;

use \yii\web\IdentityInterface;

class Faqs extends \yii\db\ActiveRecord implements IdentityInterface
{
    
    public static function tableName()
    {
        return 'faqs';
    }

    
    public function scenarios()
    {
        return [
            'add_faqs' => ['question', 'answer','type'],
	];
    }
    
//    public function beforeSave($insert)
//    {
//        if ($this->isNewRecord)
//        {
//              $this->date_create = date("Y-m-d H:i:s");
//        }
//
//        return parent::beforeSave($insert);
//    }
    
    public function rules()
    {
        return [
            ['question', 'required'],
            ['answer', 'required'],
        ];
    }

    public function getAuthKey() {
        
    }

    public function getId() {
        
    }

    public function validateAuthKey($authKey) {
        
    }

    public static function findIdentity($id) {
        
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        
    }

}