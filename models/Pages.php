<?php

namespace app\models;

use \yii\web\IdentityInterface;

class Pages extends \yii\db\ActiveRecord implements IdentityInterface
{
    
    public static function tableName()
    {
        return 'pages';
    }

    public function scenarios()
    {
        return [
            'add_page' => ['page_url','content'],
            'update_faqs' => ['date_update','content']
	];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
              $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    public function rules()
    {
        return [
            ['page_url', 'required'],
            ['content', 'required'],
        ];
    }

    public function getAuthKey() {
        
    }

    public function getId() {
        
    }

    public function validateAuthKey($authKey) {
        
    }

    public static function findIdentity($id) {
        
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        
    }

}