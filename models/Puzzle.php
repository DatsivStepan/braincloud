<?php

namespace app\models;

use \yii\web\IdentityInterface;

class Puzzle extends \yii\db\ActiveRecord implements IdentityInterface
{
    
    public static function tableName()
    {
        return 'puzzle';
    }

    
//    public function scenarios()
//    {
//        return [
//            'customer_signup' => ['image_name', 'business_name','country','city',
//                'business_type','business_services','more_information', 'ask', 'customer_id']
//	];
//    }
    
    
    
    public function rules()
    {
        return [];
    }

    public function getAuthKey() {
        
    }

    public function getId() {
        
    }

    public function validateAuthKey($authKey) {
        
    }

    public static function findIdentity($id) {
        
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        
    }

}