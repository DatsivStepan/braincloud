<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "flagging_ideas".
 *
 * @property integer $id
 * @property integer $idea_id
 * @property integer $customer_id
 * @property string $why_flagging
 * @property integer $created_at
 * @property integer $updated_at
 */
class Flaggingideas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'flagging_ideas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idea_id', 'customer_id', 'why_flagging'], 'required'],
            [['idea_id', 'customer_id', 'created_at', 'updated_at'], 'integer'],
            [['why_flagging'], 'string', 'max' => 255],
        ];
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idea_id' => 'Idea ID',
            'customer_id' => 'Customer ID',
            'why_flagging' => 'Why Flagging',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'customer_id']);
    }

    public function getIdea()
    {
        return $this->hasOne(Ideas::className(), ['id' => 'idea_id']);
    }
}
