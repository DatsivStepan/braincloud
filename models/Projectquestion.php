<?php

namespace app\models;

use \yii\web\IdentityInterface;

class Projectquestion extends \yii\db\ActiveRecord implements IdentityInterface
{
    
    public static function tableName()
    {
        return 'project_question';
    }

    
    public function scenarios()
    {
        return [
            'add_question' => ['question_title','question_text','type'],
	];
    }
    
//    public function beforeSave($insert)
//    {
//        if ($this->isNewRecord)
//        {
//              $this->date_create = date("Y-m-d H:i:s");
//        }
//
//        return parent::beforeSave($insert);
//    }
    
    public function rules()
    {
        return [
            ['question_title', 'required'],
            ['question_text', 'required'],
            ['type', 'required'],
        ];
    }

    public function getAuthKey() {
        
    }

    public function getId() {
        
    }

    public function validateAuthKey($authKey) {
        
    }

    public static function findIdentity($id) {
        
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        
    }

}