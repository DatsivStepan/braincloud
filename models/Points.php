<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "points".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $point
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $type
 * @property integer $date_week
 * @property integer $othe
 */
class Points extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'points';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'point', 'created_at', 'updated_at', 'date_week','othe'], 'integer'],
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'point' => 'Point',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'type' => 'Type',
            'date_week' => 'Date Week',
            'othe' => 'Othe',
        ];
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    public function beforeSave($insert){

        if (Points::find()->where(['user_id'=>$this->user_id])->andWhere(['>','date_week',time()])->sum('point')<800){
            if ($this->type == 'project'){
                if(!Points::find()->where(['type'=>'project','user_id'=>$this->user_id,'othe'=>$this->othe])->one()){
                    $query = Points::find()->where(['type'=>'project','user_id'=>$this->user_id])->andWhere(['>','date_week',time()]);
                    if ($query->count()<5){
                        $this->date_week = $this->week();
                        return parent::beforeSave($insert);
                    }else{

                    }
                }

            }else{

                return parent::beforeSave($insert);
            }
        }

    }

    public function week(){
        $point = Points::find()->where(['user_id'=>$this->user_id])->orderBy(['created_at'=>SORT_DESC])->one();
        if($point){
            if (time() < $point->date_week){
                return $point->date_week;
            }else{
                if (Points::find()->where(['user_id'=>$this->user_id,'date_week'=>$point->date_week])->sum('point')>350){
                    $point_message = new PointsMessage();
                    $point_message->user_id=$this->user_id;
                    $point_message->show=0;
                    $point_message->message='Possible number of points after participating in minimum required engagement: 350';
                    $point_message->save();
                }
                return strtotime("next Monday");
            }
        }else{
            return strtotime("next Monday");
        }

    }

    public function afterSave($insert, $changedAttributes)
    {
        $point_user = Points::find()->where(['user_id'=>$this->user_id])->sum('point');
        $user = User::findById($this->user_id);
        $user->scenario = 'level';
//        var_dump($point_user);

        if ($user->users_type == 'stormer'){
            if ($point_user<=250){
                $level = round($point_user/25);
                if ($user->level <> $level){
                    $user->level = $level;
                    $user->save();
                    $point_message = new PointsMessage();
                    $point_message->user_id=$this->user_id;
                    $point_message->show=0;
                    $point_message->message = 'New level '.$level;
                    $point_message->save();
                }
            }elseif ($point_user<=500){
                $level = round($point_user/25);
                if ($user->level <> $level){
                    $user->level = $level;
                    $user->save();
                    $point_message = new PointsMessage();
                    $point_message->user_id=$this->user_id;
                    $point_message->show=0;
                    $point_message->message = 'New level '.$level;
                    $point_message->save();
                }
            }elseif ($point_user<=600){
                $level = round($point_user/20);
                if ($user->level <> $level){
                    $user->level = $level;
                    $user->save();
                    $point_message = new PointsMessage();
                    $point_message->user_id=$this->user_id;
                    $point_message->show=0;
                    $point_message->message = 'New level '.$level;
                    $point_message->save();
                }
            }elseif ($point_user<=700){
                $level = round($point_user/17.5);
                if ($user->level <> $level){
                    $user->level = $level;
                    $user->save();
                    $point_message = new PointsMessage();
                    $point_message->user_id=$this->user_id;
                    $point_message->show=0;
                    $point_message->message = 'New level '.$level;
                    $point_message->save();
                }
            }elseif ($point_user<=800){
                $level = round($point_user/16);
                if ($user->level <> $level){
                    $user->level = $level;
                    $user->save();
                    $point_message = new PointsMessage();
                    $point_message->user_id=$this->user_id;
                    $point_message->show=0;
                    $point_message->message = 'New level '.$level;
                    $point_message->save();
                }
            }elseif ($point_user<=1000){
                $level = round($point_user/16.6);
                if ($user->level <> $level){
                    $user->level = $level;
                    $user->save();
                    $point_message = new PointsMessage();
                    $point_message->user_id=$this->user_id;
                    $point_message->show=0;
                    $point_message->message = 'New level '.$level;
                    $point_message->save();
                }
            }elseif ($point_user<=1200){
                $level = round($point_user/17.14);
                if ($user->level <> $level){
                    $user->level = $level;
                    $user->save();
                    $point_message = new PointsMessage();
                    $point_message->user_id=$this->user_id;
                    $point_message->show=0;
                    $point_message->message = 'New level '.$level;
                    $point_message->save();
                }
            }elseif ($point_user<=1400){
                $level = round($point_user/17.5);
                if ($user->level <> $level){
                    $user->level = $level;
                    $user->save();
                    $point_message = new PointsMessage();
                    $point_message->user_id=$this->user_id;
                    $point_message->show=0;
                    $point_message->message = 'New level '.$level;
                    $point_message->save();
                }
            }elseif ($point_user<=1600){
                $level = round($point_user/17.78);
                if ($user->level <> $level){
                    $user->level = $level;
                    $user->save();
                    $point_message = new PointsMessage();
                    $point_message->user_id=$this->user_id;
                    $point_message->show=0;
                    $point_message->message = 'New level '.$level;
                    $point_message->save();
                }
            }elseif ($point_user<=2000){
                $level = round($point_user/20);
                if ($user->level <> $level){
                    $user->level = $level;
                    $user->save();
                    $point_message = new PointsMessage();
                    $point_message->user_id=$this->user_id;
                    $point_message->show=0;
                    $point_message->message = 'New level '.$level;
                    $point_message->save();
                }
            }

            if ($point_user > 2000){
                $user->level = 100;
                $user->save();
            }
        }

        if ($user->users_type == 'customer'){
            if ($point_user>=50 and $point_user<=100) {
                if ($user->level <> 1) {
                    $user->level = 1;
                    $user->save();
                    $point_message = new PointsMessage();
                    $point_message->user_id = $this->user_id;
                    $point_message->show = 0;
                    $point_message->message = 'New level ' . 1;
                    $point_message->save();
                }
            }

            if ($point_user>100 and $point_user<=150) {
                if ($user->level <> 2) {
                    $user->level = 2;
                    $user->save();
                    $point_message = new PointsMessage();
                    $point_message->user_id = $this->user_id;
                    $point_message->show = 0;
                    $point_message->message = 'New level ' . 2;
                    $point_message->save();
                }
            }
            if ($point_user>150 and $point_user<=200) {
                if ($user->level <> 3) {
                    $user->level = 3;
                    $user->save();
                    $point_message = new PointsMessage();
                    $point_message->user_id = $this->user_id;
                    $point_message->show = 0;
                    $point_message->message = 'New level ' . 3;
                    $point_message->save();
                }
            }

            if ($point_user>200 and $point_user<=250) {
                if ($user->level <> 4) {
                    $user->level = 4;
                    $user->save();
                    $point_message = new PointsMessage();
                    $point_message->user_id = $this->user_id;
                    $point_message->show = 0;
                    $point_message->message = 'New level ' . 4;
                    $point_message->save();
                }
            }
            if ($point_user>250) {
                if ($user->level <> 5) {
                    $user->level = 5;
                    $user->save();
                    $point_message = new PointsMessage();
                    $point_message->user_id = $this->user_id;
                    $point_message->show = 0;
                    $point_message->message = 'New level ' . 5;
                    $point_message->save();
                }
            }
        }

        $point_message = new PointsMessage();
        $point_message->user_id=$this->user_id;
        $point_message->show=0;
        switch($this->type){
            case 'idea_peer':
                $point_message->message = 'You’ve been awarded '.$this->point.' points for your storming efforts!';
                break;
            case 'idea_customer':
                $point_message->message = 'You been awarded '.$this->point.' points for all your hard work! Put your feet up!';
                break;
            case 'idea_open':
                $point_message->message = 'You’ve been awarded '.$this->point.' points for all your hard work! Keep it up!';
                break;
            case 'idea_ngo':
                $point_message->message = 'You’ve made the world shine a little brighter today! You’ve been awarded '.$this->point.' points';
                break;
            case 'stars':
                $point_message->message = 'Wow! You’ve been awarded a star for your ideas by the customer! We salute you! We’ve doubled your points been awarded in the last project!';
                break;
            case 'social_tell_word':
                $point_message->message = 'You’ve been awarded '.$this->point.' points for helping spread the word!';
                break;
            case 'NGO':
                $point_message->message = 'Congratulations! The NGO you recommended has decided to join us! You’ve been awarded a 50-point bonus!';
                break;
            case 'customer_50':
                $point_message->message = '+ '.$this->point.' points added for create project';
                break;
            case '30_point_ngo_10':
                $point_message->message = 'They should call you a saint! '.$this->point.'-point bonus for completing 10 NGO projects!';
                break;
            case '10_project_customer':
                $point_message->message = 'No stopping you now! Here’s a '.$this->point.' point bonus for completing 10 corporate projects!';
                break;
            case '5_project_customer':
                $point_message->message = 'On a roll! 25 points bonus for 5 projects completed!';
                break;
            case 'selected_stormer_50':
                $point_message->message = 'Hey! You’ve been selected as stormer of the week! Here’s a 50-point bonus!';
                break;
            case '10_points_glitch':
                $point_message->message = 'You’ve been awarded 10 points for pointing out a glitch! Many thanks!';
                break;
            case 'referal_stormer_create_project_150':
                $point_message->message = 'You’ve been awarded an extra 150-point bonus for successfully recommending a customer!';
                break;
            default;
                $point_message->message = 'You’ve been awarded '.$this->point.' points!';
                break;
        }

        $point_message->save();

        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }

}
