<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "projects".
 *
 * @property integer $id
 * @property integer $owner_id
 * @property integer $type
 * @property integer $active
 * @property string $title
 * @property string $description
 * @property string $goals
 * @property string $evaluation_benchmark
 * @property string $end_date
 * @property string $create_at
 * @property string $perspectives
 * @property string $category
 * @property string $question_convergent
 * @property string $question_divergent
 * @property integer $question_custom
 * @property integer $show_message
 * @property integer $discount
 * @property integer $ngo
 * @property integer $be_inspired
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $project_type
 */
class Projects extends \yii\db\ActiveRecord
{
    public $team;
    public $countIdeaUser;
    public $tmpCategory;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'projects';
    }
    public function either($attribute_name, $params)
    {
        $field1 = $this->getAttributeLabel($attribute_name);
        $field2 = $this->getAttributeLabel($params['other']);
        if (empty($this->$attribute_name) && empty($this->$params['other'])) {
            $this->addError($attribute_name, Yii::t('user', "either {$field1} or {$field2} is required."));
            return false;
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_id', 'title', 'description', 'goals', 'evaluation_benchmark', 'end_date', 'perspectives', 'category'], 'required'],
            [['owner_id', 'type', 'show_message','discount','ngo','be_inspired', 'created_at','updated_at'], 'integer'],
            [['description', 'goals', 'evaluation_benchmark', 'perspectives', 'question_convergent', 'question_divergent','project_type'], 'string'],
            [['end_date', 'create_at'], 'safe'],
            [['title', 'category'], 'string', 'max' => 255],
//            ['category','either','other'=>'tmpCategory']
        ];
    }
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->create_at = date("Y-m-d H:i:s");
            $this->status = 1;

            if ($this->owner_id){
                $user = User::findOne($this->owner_id);
                if($user->parent_id){
                    $parent = User::findOne($user->parent_id);
                    if ($parent and ($parent->role == 'stormer') ){
                        $point = new Points();
                        $point->point=150;
                        $point->type = 'referal_stormer_create_project_150';
                        $point->user_id = $parent->id;
                        $point->date_week = $point->week();
                        $point->save();
                    }
                }
            }
        }

        return parent::beforeSave($insert);
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'owner_id' => 'Owner ID',
            'type' => 'Type',
            'title' => 'Title',
            'description' => 'Description',
            'goals' => 'Goals',
            'evaluation_benchmark' => 'Evaluation Benchmark',
            'end_date' => 'End Date',
            'create_at' => 'Create At',
            'category' => 'Category',
            'question_convergent' => 'Question Convergent',
            'question_divergent' => 'Question Divergent',
            'question_custom' => 'Question Custom',
            'project_type' => 'Project Type',
            'NGO' => 'Project NGO',
            'be_inspired' => 'Be Inspired',
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function scenarios()
    {
        return [
            'project_stormer' => ['title','description','owner_id','project_type','be_inspired', 'created_at','updated_at'],
            'update_status' => ['active', 'created_at','updated_at'],
            'update_be_inspired' => ['be_inspired', 'created_at','updated_at'],
            'update_message' => ['show_message', 'created_at','updated_at'],
            'update_slider' => ['size_team','count_idea', 'created_at','updated_at','discount'],
            'create' => ['owner_id', 'type', 'title', 'description', 'goals', 'evaluation_benchmark', 'end_date', 'create_at','discount','project_type','ngo','be_inspired', 'created_at','updated_at'],
            'target' => ['owner_id', 'type', 'title', 'description', 'goals','tmpCategory', 'evaluation_benchmark', 'end_date', 'create_at','category','discount','size_team','count_idea','project_type','ngo','be_inspired', 'created_at','updated_at'],
            'target2' => ['question_convergent','question_divergent','question_custom','perspectives','project_type','ngo','be_inspired', 'created_at','updated_at']
        ];
    }
    public function getData(){
        return $this->hasMany(ProjectData::className(), ['project_id' => 'id']);
    }

    public function afterSave($insert, $changedAttributes) {
        if (($this->active == '1') && ($this->status == '1')){
            $stormer = Projectstormer::find()->where(['project_id'=>$this->id])->all();
            foreach($stormer as $item){
                $point_message = new PointsMessage();
                $point_message->user_id=$item->stormer_id;
                $point_message->show=0;
                $point_message->url=1;
                $point_message->message = 'You\'ve Invited to Join Project';
                $point_message->save();
            }
        }
        parent::afterSave($insert, $changedAttributes);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }
}
