<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Ideas;

/**
 * IdeasSearch represents the model behind the search form about `app\models\Ideas`.
 */
class IdeasSearch extends Ideas
{
    /**
     * @inheritdoc
     */
    public $category;
    public $cat_child;
    public function rules()
    {
        return [
            [['id', 'parent_id', 'project_id', 'stormer_id', 'organize', 'flagging'], 'integer'],
            [['ideas_description','category','cat_child'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ideas::find()->joinWith(['organization','user']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'parent_id' => $this->parent_id,
            'project_id' => $this->project_id,
            'stormer_id' => $this->stormer_id,
            'organize' => $this->organize,
            'flagging' => $this->flagging,
        ]);

        $query->andFilterWhere(['like', 'ideas_description', $this->ideas_description]);
        $query->andFilterWhere(['like', 'organize_idea.category', $this->category]);
        $query->andFilterWhere(['like', 'organize_idea.cat_child', $this->cat_child]);

        return $dataProvider;
    }
}
