<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "pay_stormer".
 *
 * @property integer $id
 * @property integer $user_id
 * @property double $price
 * @property double $referal_price
 * @property double $price_idea
 * @property string $date_to
 * @property string $else
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $date_for
 * @property string $paypal
 * @property string $projects
 * @property integer $status
 */
class PayStormer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pay_stormer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'created_at', 'updated_at', 'status'], 'integer'],
            [['price','price_idea','referal_price'], 'number'],
            [['date_for'], 'required'],
            [['date_to', 'date_for','paypal','projects'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'price' => 'Price',
            'referal_price' => 'Referal price',
            'price_idea' => 'Price idea',
            'date_to' => 'Date To',
            'else' => 'Else',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'date_for' => 'Date',
            'status' => 'Status',
            'paypal' => 'Paypal email',
        ];
    }
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    public function afterSave($insert, $changedAttributes) {

        $point_message = new PointsMessage();
        $point_message->user_id=$this->user_id;
        $point_message->show=0;
        $point_message->message = 'New earnings';
        $point_message->save();

        parent::afterSave($insert, $changedAttributes);
    }
}
