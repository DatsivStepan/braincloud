<?php
namespace app\models;

use app\models\User;
use yii\base\InvalidParamException;
use yii\base\Model;
use Yii;

/**
 * Password reset form
 */
class ActivateConfirm extends Model
{
    public $active;

    /**
     * @var \app\models\User
     */
    private $_user;


    /**
     * Creates a form model given a token.
     *
     * @param  string                          $token
     * @param  array                           $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Token cannot be blank.');
        }
        $this->_user = User::findByActivationToken($token);
        if (!$this->_user) {
            throw new InvalidParamException('Wrong token.');
        }
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['active', 'required'],
        ];
    }

    /**
     * Resets password.
     *
     * @return boolean if password was reset.
     */
    public function activate()
    {
        $user = $this->_user;
        // $user->setPassword($this->password);
        $user->active = 1;
        $user->removeActivationToken();

        return $user->save(false);
    }
}
