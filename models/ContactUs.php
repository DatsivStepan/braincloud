<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contact_us".
 *
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $subject_mail
 * @property string $descriptions
 * @property integer $send_me
 * @property string $date_mail
 * @property integer $show_mail
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $parent_id
 * @property integer $point
 * @property integer $timeline_inquire
 */
class ContactUs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contact_us';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['send_me', 'show_mail', 'created_at', 'updated_at', 'parent_id', 'point','timeline_inquire'], 'integer'],
            [['username', 'email', 'subject_mail', 'descriptions', 'date_mail'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'email' => 'Email',
            'subject_mail' => 'Subject Mail',
            'descriptions' => 'Descriptions',
            'send_me' => 'Send Me',
            'date_mail' => 'Date Mail',
            'show_mail' => 'Show Mail',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'parent_id' => 'Parent ID',
            'point' => 'Point',
        ];
    }
}
