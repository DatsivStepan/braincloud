<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "innovation_questions".
 *
 * @property integer $id
 * @property string $title
 * @property string $question
 * @property string $type
 */
class InnovationQuestions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'innovation_questions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question'], 'string'],
            [['title', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'question' => 'Question',
            'type' => 'Type',
        ];
    }
}
