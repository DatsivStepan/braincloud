<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property string $main_image
 * @property string $date_create
 * @property integer $notification
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'content'], 'required'],
            [['content','category'], 'string'],
            [['date_create'], 'safe'],
            [['notification'], 'integer'],
            [['title', 'main_image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'content' => 'Content',
            'category' => 'Category',
            'main_image' => 'Main Image',
            'date_create' => 'Date Create',
            'notification' => 'Notification',
        ];
    }
    public function scenarios()
    {
        return [
            'add_news' => ['title', 'content','notification','category'],
            'update_news' => ['title', 'content','notification','category']
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }
        if($this->notification==1){
            $users= User::find()->where(['users_type'=>'customer','active'=>1])->all();
            foreach($users as $user){
                $notification = Customerinfo::find()->where(['customer_id'=>$user->id])->one();
                if ($notification->notifications_off &&  $notification->notifications ){
                    Yii::$app->mailer->compose('notificationNew',['model'=>$this])
//                    Yii::$app->mailer->compose()
                        ->setTo($user->email)
                        ->setFrom([Yii::$app->params['adminEmail'] => 'braind'])
                        ->setSubject($this->title)
//                        ->setTextBody('test')
                        ->send();
                }
            }
        }
        return parent::beforeSave($insert);
    }
}
