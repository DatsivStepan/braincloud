<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "transaction".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $project_id
 * @property integer $complete
 * @property string $payment_id
 * @property string $hash
 * @property string $othe
 * @property double $price
 * @property integer $created_at
 * @property integer $updated_at
 */
class Transaction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id','project_id', 'complete', 'created_at', 'updated_at'], 'integer'],
            [['othe'], 'string'],
            [['price'], 'number'],
            [['payment_id', 'hash'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'complete' => 'Complete',
            'payment_id' => 'Payment ID',
            'hash' => 'Hash',
            'othe' => 'Description',
            'price' => 'Price',
            'created_at' => 'Created At',
            'updated_at' => 'Date',
        ];
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    public function getProject()
    {
        return $this->hasOne(Projects::className(), ['id' => 'project_id']);
    }
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
