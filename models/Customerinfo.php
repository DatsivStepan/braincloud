<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customer_info".
 *
 * @property integer $id
 * @property string $business_name
 * @property string $image_name
 * @property string $country
 * @property string $city
 * @property string $business_type
 * @property string $business_services
 * @property string $more_information
 * @property string $ask
 * @property integer $customer_id
 * @property integer $notifications
 * @property integer $notification_project
 * @property integer $notifications_off
 */
class Customerinfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['business_name', 'country', 'city', 'business_type', 'business_services', 'more_information', 'ask', 'customer_id'], 'required'],
            [['business_services', 'more_information'], 'string'],
            [['customer_id', 'notifications', 'notification_project', 'notifications_off'], 'integer'],
            [['business_name', 'image_name', 'business_type'], 'string', 'max' => 255],
            [['country', 'city'], 'string', 'max' => 50],
            [['ask'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'business_name' => 'Business Name',
            'image_name' => 'Image Name',
            'country' => 'Country',
            'city' => 'City',
            'business_type' => 'Business Type',
            'business_services' => 'Business Services',
            'more_information' => 'More Information',
            'ask' => 'Ask',
            'customer_id' => 'Customer ID',
            'notifications' => 'Notifications',
            'notification_project' => 'Notification Project',
            'notifications_off' => 'Notifications Off',
        ];
    }
    public function scenarios()
    {
        return [
            'customer_signup' => ['image_name', 'business_name','country','city',
                'business_type','business_services','more_information', 'ask', 'customer_id'],
            'customer_update' => ['image_name', 'business_name','country','city',
                'business_type','business_services','more_information', 'ask', 'customer_id','notifications','notification_project','notifications_off'],
            'customer_signup_basic'=>['customer_id'],
        ];
    }
    public function getLocationC(){
        return $this->hasOne(Country::className(),['id'=>'country']);
    }
    public function getLocationS(){
        return $this->hasOne(City::className(),['id'=>'city']);
    }
}
