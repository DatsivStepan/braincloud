<?php

namespace app\models;

use \yii\web\IdentityInterface;

class Stormerinfo extends \yii\db\ActiveRecord implements IdentityInterface
{
    
    public static function tableName()
    {
        return 'stormer_info';
    }

    
    public function scenarios()
    {
        return [
            'customer_save_step_2' => ['category', 'tags','stormer_id'],
            'customer_save_step_3' => ['country', 'city','gender','introduction', 'image_src','where_here_about_us','which_website','accept_term', 'facebook_link' ,'twitter_link','linkedin_link'],
            'update' => ['country', 'city','gender','introduction', 'image_src','where_here_about_us','which_website','accept_term', 'facebook_link' ,'twitter_link','linkedin_link','notifications','notification_project','notifications_off','show_email_customer','show_email_customer_option'],
            'save_points' => ['points'],
            'paypal'=>['paypal'],
            'sales_team'=>['sales_team'],
            'confidentialityagreement' => ['confidentialityagreement'],
        ];
    }
    
    
    
    public function rules()
    {
        return [
            ['category', 'required'],
            ['tags', 'required'],
            ['stormer_id', 'required'],
            ['country', 'required'],
            ['city', 'required'],
            ['gender', 'required'],
            ['where_here_about_us', 'required'],
            ['accept_term', 'required'],
            ['accept_term', 'compare', 'compareValue' => 1, 'message' => 'You should accept terms and agreement'],
            ['paypal','email']
        ];
    }

    public function getAuthKey() {
        
    }

    public function getId() {
        
    }

    public function validateAuthKey($authKey) {
        
    }

    public static function findIdentity($id) {
        
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        
    }

    public function getLocationC(){
        return $this->hasOne(Country::className(),['id'=>'country']);
    }
    public function getLocationS(){
        return $this->hasOne(City::className(),['id'=>'city']);
    }

}