<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "ideas".
 *
 * @property integer $id
 * @property integer $project_id
 * @property string $ideas_description
 * @property integer $stormer_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class Ideas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ideas';
    }

    public function scenarios()
    {
        return [
            'organize' => ['organize','created_at','updated_at'],
            'flagging' => ['flagging','created_at','updated_at'],
            'divergent' => ['project_id', 'ideas_description','stormer_id','created_at','updated_at'],
            'wholepicture' => ['project_id','parent_id', 'ideas_description','stormer_id','created_at','updated_at'],
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'ideas_description', 'stormer_id'], 'required'],
            [['project_id', 'stormer_id','created_at','updated_at'], 'integer'],
            [['ideas_description'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project ID',
            'ideas_description' => 'Ideas Description',
            'stormer_id' => 'Stormer ID',
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'stormer_id']);
    }
    public function getOrganize()
    {
        return $this->hasOne(Organizeidea::className(), ['idea_id' => 'id']);
    }

    public function getOrganization()
    {
        return $this->hasOne(Organizeidea::className(), ['idea_id' => 'id']);
    }
    public function getProject()
    {
        return $this->hasOne(Projects::className(), ['id' => 'project_id']);
    }

    public function Stormerv($project_id, $user_id){
        $stormers = Projectstormer::find()->where(['project_id'=>$project_id, 'confirmation'=>1])->orderBy(['updated_at'=>SORT_ASC])->all();
        $mas_stormer=null;
        if($stormers){
            $k=1;
            foreach($stormers as $stormer){
                $mas_stormer[$stormer->stormer_id]=$k;
                $k++;
            }
            return 'Stormer'.$mas_stormer[$user_id];
        }else{
            return 'Stormer';
        }

    }
    public function beforeSave($insert){
        if ($this->isNewRecord){


        }
        return parent::beforeSave($insert);
    }
}
