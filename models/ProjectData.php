<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "project_data".
 *
 * @property integer $id
 * @property integer $project_id
 * @property string $description
 * @property string $data
 * @property integer $type
 */
class ProjectData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'description', 'data', 'type'], 'required'],
            [['project_id', 'type'], 'integer'],
            [['data'], 'string'],
            [['description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project ID',
            'description' => 'Description',
            'data' => 'Data',
            'type' => 'Type',
        ];
    }
}
