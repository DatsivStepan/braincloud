<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "organize_idea".
 *
 * @property integer $id
 * @property integer $idea_id
 * @property integer $question_1
 * @property integer $question_2
 * @property integer $question_3
 * @property integer $question_4
 * @property integer $question_5
 * @property string $category
 * @property string $cat_child
 */
class Organizeidea extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'organize_idea';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['idea_id', 'question_1', 'question_2', 'question_3', 'question_4', 'question_5'], 'required'],
            [['idea_id', 'question_1', 'question_2', 'question_3', 'question_4', 'question_5'], 'integer'],
            [['category', 'cat_child'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idea_id' => 'Idea ID',
            'question_1' => 'Question 1',
            'question_2' => 'Question 2',
            'question_3' => 'Question 3',
            'question_4' => 'Question 4',
            'question_5' => 'Question 5',
            'category' => 'Category',
            'cat_child' => 'Cat Child',
        ];
    }
    public function scenarios()
    {
        return [
            'add_organize' => ['idea_id', 'question_1', 'question_2', 'question_3', 'question_4', 'question_5', 'category','cat_child']
        ];
    }
}
