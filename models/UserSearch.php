<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;

/**
 * UserSearch represents the model behind the search form of `app\models\User`.
 */
class UserSearch extends User
{
    /**
     * @inheritdoc
     */

    public function rules()
    {
        return [
            [['id', 'level', 'parent_id'], 'integer'],
            [['username', 'name', 'password', 'password_hash', 'password_reset_token', 'access_token', 'users_type', 'email', 'auth_key', 'activation_token', 'signup_step', 'active', 'rating', 'referal_key'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find()->select(['*','sum_point'=>'(SELECT SUM(point) FROM points WHERE users.id=user_id)']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'level' => $this->level,
            'parent_id' => $this->parent_id,
        ]);

        $query
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'access_token', $this->access_token])
            ->andFilterWhere(['like', 'users_type', $this->users_type])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'activation_token', $this->activation_token])
            ->andFilterWhere(['like', 'signup_step', $this->signup_step])
            ->andFilterWhere(['like', 'active', $this->active])
            ->andFilterWhere(['like', 'rating', $this->rating])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'referal_key', $this->referal_key])
            ->orderBy(['sum_point'=>SORT_DESC]);

        return $dataProvider;
    }
}
