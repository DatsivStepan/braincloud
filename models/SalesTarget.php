<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sales_target".
 *
 * @property integer $id
 * @property integer $namber_customer
 * @property double $sales_amount
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $date_for
 * @property string $date_to
 * @property integer $user_id
 */
class SalesTarget extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sales_target';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['namber_customer', 'created_at', 'updated_at', 'user_id'], 'integer'],
            [['sales_amount'], 'number'],
            [['date_for', 'date_to'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'namber_customer' => 'Namber Customer',
            'sales_amount' => 'Sales Amount',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'date_for' => 'Date For',
            'date_to' => 'Date To',
            'user_id' => 'User ID',
        ];
    }
}
