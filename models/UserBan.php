<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_ban".
 *
 * @property integer $id
 * @property integer $stormer_id
 * @property string $date
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $type
 */
class UserBan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_ban';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stormer_id', 'created_at', 'updated_at', 'type'], 'integer'],
            [['date'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'stormer_id' => 'Stormer ID',
            'date' => 'Date',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'type' => 'Type',
        ];
    }
}
