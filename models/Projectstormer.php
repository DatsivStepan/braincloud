<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;
use \yii\web\IdentityInterface;

class Projectstormer extends \yii\db\ActiveRecord implements IdentityInterface
{
    
    public static function tableName()
    {
        return 'project_stormer';
    }

    
    public function scenarios()
    {
        return [
            'add' => ['project_id','stormer_id','confirmation'],
            'confirmation' => ['confirmation'],
            'status' => ['status'],
        ];
    }
    
    
    
    public function rules()
    {
        return [
        ];
    }

    public function getAuthKey() {
        
    }

    public function getId() {
        
    }

    public function validateAuthKey($authKey) {
        
    }

    public static function findIdentity($id) {
        
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'stormer_id']);
    }

  public function getProject()
  {
    return $this->hasOne(Projects::className(), ['id' => 'project_id']);
  }

}