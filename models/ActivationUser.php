<?php
namespace app\models;

use app\models\User;
use yii\base\Model;

/**
 * Password reset request form
 */
class ActivationUser extends Model
{
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => 'app\models\User',
                'message' => 'There is no user with such email.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'email' => $this->email,
        ]);

        if ($user) {
          // var_dump($user->activation_token);
            if (!User::isActivationTokenValid($user->activation_token)) {
                $user->generateActivationToken();
            }
            $user->scenario = 'generate_token';

            if ($user->save()) {
                $some =  \Yii::$app->mailer->compose(['html' => 'activation-html', 'text' => 'activation-text'], ['user' => $user])
                    ->setFrom(\Yii::$app->params['supportEmail'])
                    ->setTo($this->email)
                    ->setSubject('Email confirmation ' . \Yii::$app->name)
                    ->send();


                    // var_dump($some);
                return $some;
                }
        }

        return false;
    }
}
