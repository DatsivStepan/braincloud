<?php
/**
 * Created by PhpStorm.
 * User: Listat
 * Date: 29.08.2016
 * Time: 18:54
 */

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadCsv extends Model {

    public $file;

    public function rules()
    {
        return [
            [['file'], 'file', 'checkExtensionByMimeType' => false, 'extensions' => 'csv'],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->file->saveAs('uploads/demo'.'csv');
            return true;
        } else {
            return false;
        }
    }
}