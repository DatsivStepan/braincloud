<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "feedback".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $ranting_1
 * @property integer $ranting_2
 * @property integer $ranting_3
 * @property string $about_experience
 * @property integer $question
 * @property string $date_create
 * @property integer $project_id
 * @property integer $show_status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Feedback extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'ranting_1', 'ranting_2', 'ranting_3', 'about_experience', 'question', 'date_create', 'project_id'], 'required'],
            [['user_id', 'ranting_1', 'ranting_2', 'ranting_3', 'question', 'project_id', 'show_status', 'created_at', 'updated_at'], 'integer'],
            [['date_create'], 'safe'],
            [['about_experience'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'ranting_1' => 'Ranting 1',
            'ranting_2' => 'Ranting 2',
            'ranting_3' => 'Ranting 3',
            'about_experience' => 'About Experience',
            'question' => 'Question',
            'date_create' => 'Date Create',
            'project_id' => 'Project ID',
            'show_status' => 'Show Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    public function scenarios()
    {
        return [
            'add_feedback' => ['ranting_1', 'ranting_2', 'ranting_3', 'about_experience', 'question', 'project_id','user_id'],
            'up_status'=>['show_status']
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function getProject()
    {
        return $this->hasOne(Projects::className(), ['id' => 'project_id']);
    }
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

}
